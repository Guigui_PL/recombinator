# READ ME !

# Instructions de compilation #

* Dépendances : boost (version minimale : 1.67), postgreSQl (version minimale : 9.1), driver C++ officiel de postgreSQL, compilateur supportant le C++17 et make
* Installation des dépendances sous Ubuntu :

```
#!bash
sudo apt-get install g++ build-essential libboost-all-dev postgresql libpq-dev

```
* Le driver C++ de postgresql nécessite d'être compilé. Il n'est nécessaire que pour compiler la partie insertion dans la base de données.
* Sous Ubuntu, installer le paquet postgresql et suivre les instructions de compilation du driver : https://www.tutorialspoint.com/postgresql/postgresql_c_cpp.htm
* Puis, dans un terminal, créer l'utilisateur genetixuser et la base genetixdb :

```
#!bash
sudo -u postgres psql -c "CREATE USER genetixuser WITH PASSWORD 'JN7m7a2d' CREATEDB;"
sudo -u postgres psql -c "CREATE DATABASE genetixdb WITH OWNER=genetixuser;"

```

* Pour compiler le générateur, il faut se placer dans le dossier src et lancer la commande make - l'exécutable se trouvera à la racine :

```
#!bash
cd src
make generator -j nombre_de_threads
```

* Pour compiler seulement l'API, il faut faire "make api".
* Pour la partie insertion dans la base de données, il faut faire "make db".
* Pour exécuter les tests unitaires, il faut faire "make tests".


* Pour savoir comment utiliser le programme de génération, exécuter :

```
#!bash
./generatorManager --help
```

* Pour lancer une génération des architectures sans croisement de sites avec n inputs, faire :

```
#!bash
./generatorManager -t nombre_de_threads -l IEDyck n
```
