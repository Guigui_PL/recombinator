#ifndef _ONEWAYSEMANTIC_H
#define _ONEWAYSEMANTIC_H

#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>
#include "EnumClassHash.hpp"

namespace recombinator
{
	namespace api 
	{
		class OneWaySemantic 
		{
		public: 
			
			enum class BioFunction : uint8_t
			{
				N, P, T, G, GP, X
			};
			
			static const std::unordered_map<BioFunction,std::vector<BioFunction>,EnumClassHash> subBioFunction;
			static const std::unordered_map<BioFunction, uint16_t, EnumClassHash> minLengthBF;
			static const std::unordered_map<BioFunction, uint16_t, EnumClassHash> minNbPartsBF;
			
			OneWaySemantic() = default;
			OneWaySemantic(BioFunction b);
			
			OneWaySemantic& operator+= (const OneWaySemantic& s)
			{
				return combine(s);
			}
			
			OneWaySemantic& setBioFunction(BioFunction b);
			BioFunction getBioFunction() const;
			
			bool isP() const;
			
			bool isT() const;
			
			bool isG() const;
			
			bool isX() const;
			
			bool isN() const;
			
			uint16_t getMinLength() const;
			
			uint16_t getMinNbParts() const;
			
			/**
			 * @param s
			 */
			bool isEqualTo(const OneWaySemantic& s) const;
			
			/**
			 * @param s
			 */
			OneWaySemantic& combine(const OneWaySemantic& s);
			
			/**
			 * @param s1
			 * @param s2
			 */
			static OneWaySemantic combine(const OneWaySemantic& s1, const OneWaySemantic& s2);
			
			bool isUsefulAfter(const OneWaySemantic& ows) const;
			bool isUsefulBefore(const OneWaySemantic& ows) const;
			bool isUsefulAfterPrefix(const OneWaySemantic& ows) const;
			bool isUsefulBeforeSuffix(const OneWaySemantic& ows) const;
			
			operator std::string () const;
			
		private: 
			BioFunction _bioFunction = BioFunction::N;
		};
		
		inline OneWaySemantic operator+ (const OneWaySemantic& s1, const OneWaySemantic& s2) 
		{
			return OneWaySemantic::combine(s1,s2);
		}
		
		inline bool operator== (const OneWaySemantic& s1, const OneWaySemantic& s2) 
		{
			return s1.isEqualTo(s2);
		}
		
		inline bool operator!= (const OneWaySemantic& s1, const OneWaySemantic& s2) 
		{
			return !s1.isEqualTo(s2);
		}
		
		inline bool operator< (const OneWaySemantic& s1, const OneWaySemantic& s2) 
		{
			return s1.getBioFunction() < s2.getBioFunction();
		}
		
		std::ostream& operator<< (std::ostream& os, OneWaySemantic::BioFunction bf);
		
	}
}
#endif //_ONEWAYSEMANTIC_H
