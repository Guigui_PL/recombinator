#ifndef _GENERICFUNCTIONNALSTRUCTURE_H
#define _GENERICFUNCTIONNALSTRUCTURE_H

#include <sstream>
#include <exception>
#include <boost/algorithm/string/trim.hpp>
#include <boost/functional/hash/extensions.hpp>
#include <boost/optional.hpp>
#include "FunctionnalStructure.hpp"

namespace recombinator 
{
	namespace api 
	{
		class GenericFunctionnalStructure : public FunctionnalStructure
		{
			
		public:
			class AMI
			{
			public:
				using Identifier = std::string;
				
				enum class SiteType : char
				{
					FORWARD_SITE = '>',
					REVERSE_SITE = '<',
					USED_FORWARD_SITE = '}',
					USED_REVERSE_SITE = '{'
				};
				
				enum class IntegraseType : char
				{
					B = 'B',
					P = 'P'
				};
				
				enum class ReactionType : uint8_t
				{
					EXCISION,
					INVERSION
				};
				
				AMI (SiteType site, Identifier identifier, IntegraseType type, uint8_t number) :
					site(site), identifier(identifier), type(type), number(number) {}
				AMI(std::string ami)
				{
					boost::algorithm::trim(ami);
					site = charToSiteType(ami[0]);
					identifier = std::string(1,ami[1]);
					type = charToIntegraseType(ami[2]);
					
					if (ami.size() == 4)
					{
						number = ami[3]-'0';
					}
				}
				
				Identifier getIdentifier() const { return identifier; }
				IntegraseType getIntegraseType() const { return type; }
				uint8_t getNumber() const { return number; }
				SiteType getSiteType() const { return site; }
				
				static SiteType charToSiteType (char site) 
				{
					if (site == '>')
						return SiteType::FORWARD_SITE;
					else if (site == '<')
						return SiteType::REVERSE_SITE;
					else if (site == '}')
						return SiteType::USED_FORWARD_SITE;
					else if (site == '{')
						return SiteType::USED_REVERSE_SITE;
					
					throw std::invalid_argument(site+" is not a valid site.");
				}
				
				static IntegraseType charToIntegraseType (char site) 
				{
					if (site == 'B')
						return IntegraseType::B;
					else if (site == 'P')
						return IntegraseType::P;
					
					throw std::invalid_argument(site+" is not a valid integrase type.");
				}
				
				void setNumber (uint8_t number)
				{
					this->number = number;
				}
				
				void reverse() 
				{
					switch (site)
					{
						case SiteType::FORWARD_SITE:
							site = SiteType::REVERSE_SITE;
							break;
						case SiteType::REVERSE_SITE:
							site = SiteType::FORWARD_SITE;
							break;
						case SiteType::USED_FORWARD_SITE:
							site = SiteType::USED_REVERSE_SITE;
							break;
						case SiteType::USED_REVERSE_SITE:
							site = SiteType::USED_FORWARD_SITE;
					}
				}
				
				void setUsed()
				{
					switch(site)
					{
						case SiteType::FORWARD_SITE:
							site = SiteType::USED_FORWARD_SITE;
							break;
						case SiteType::REVERSE_SITE:
							site = SiteType::USED_REVERSE_SITE;
							break;
						default:
							break;
							// NOTHING TO DO
					}
				}
				
				bool isUsed() const
				{
					return site == SiteType::USED_FORWARD_SITE
						|| site == SiteType::USED_REVERSE_SITE;
				}
				
				bool canReactWith (AMI other) const
				{
					return other.identifier == identifier
						&& other.type != type
						&& !other.isUsed()
						&& !isUsed();
				}
				
				std::optional<ReactionType> getReactionType (AMI other) const
				{
					if (!canReactWith(other))
						return {};
					
					return other.site == site ? ReactionType::EXCISION : ReactionType::INVERSION;
				}
				
				std::string to_string() const
				{
					return std::string(1,(char)site)+identifier+std::string(1,(char)type)+std::to_string(number);
				}
				
				operator std::string () const
				{
					return to_string();
				}
				
				bool equals (const AMI &a) const
				{
					return //site == a.site 
						isUsed() == a.isUsed()
						&& identifier == a.identifier 
						&& type == a.type 
						&& number == a.number;
				}
				
				uint16_t getLength() const
				{
					return 40;
				}
				
			private:
				SiteType site;
				Identifier identifier;
				IntegraseType type;
				uint8_t number = 0;
				
				
			}; /**< Activation's Mark of Integrase */
			
			using Structure = std::vector<AMI>;
			//using MultiActivation = std::vector<std::pair<uint8_t,uint8_t>>;
			
			template<class T>
			static std::unique_ptr<T> create(const std::string &structure)
			{
				struct make_shared_enabler : public T {};
				auto p = std::make_unique<make_shared_enabler>();
				p->setStructure(structure);
				p->buildActivations();
				p->buildDerivedBioDevicesStructures();
				p->buildSemsBeforeAndAfterSem();
				return p;
			}
			
			virtual ~GenericFunctionnalStructure() = default;
			
			virtual size_t size() const;
			
			virtual std::string getStructureWithIndexes() const;
			
			virtual std::string getStructure() const;
			
			virtual std::string getStructureOnDerived(size_t i) const;
			
			virtual std::vector<std::string> getStructuresOnDerived() const;
			
			virtual const SemBioDevStructContainer& getDerivedBioDevicesStructures() const;
			
			/**
			 * @param activation
			 */
			virtual const SemanticalBioDeviceStructure& getDerivedBioDevicesStructure(const Activation& activation) const;
			
			virtual size_t getIndexDerivedBioDevicesStructure(const Activation& activation) const;
			
			virtual size_t getNbDifferentDerivations() const;
			
			virtual const std::vector<Activation>& getActivations() const;
			
			virtual size_t getNbIntegrase() const;
			
			virtual bool isAValidAMI(char ami) const;
			
			virtual const std::unordered_set<int8_t>& getSemsAfterSem(int8_t numSem) const;
			
			virtual const std::unordered_set<int8_t>& getSemsBeforeSem(int8_t numSem) const;
			
			virtual uint16_t getLength() const;
			
			virtual uint16_t getLengthAMI(size_t i) const;
			
			virtual uint16_t getNbParts() const;
			
			virtual std::string getAMIWithIndex(size_t i) const;
			
			virtual bool operator== (const FunctionnalStructure& fs) const
			{ return getStructureWithIndexes() == fs.getStructureWithIndexes(); }
			
			boost::optional<ActivationNumber> getActivationNumber (const AMI &a, const AMI &b) const;
			
		protected:
			
			/**
			 * The constructor is protected because, to create our object, we use virtual functions 
			 * So, to create an object of this type, use the factory : shared_ptr< IEFunctionnalStructure> mystruct = create< IEFunctionnalStructure>(string)
			 * See C++ core guidelines : https://github.com/isocpp/CppCoreGuidelines/blob/master/CppCoreGuidelines.md#c50-use-a-factory-function-if-you-need-virtual-behavior-during-initialization
			 */
			GenericFunctionnalStructure() = default;
			
			virtual GenericFunctionnalStructure* clone_impl() const override { return new GenericFunctionnalStructure(*this); }; 
			
			
			Structure _structure;
			std::unordered_map<AMI::Identifier,std::pair<std::vector<AMI>,std::vector<AMI>>> _identifierToAMI;
			std::vector<std::pair<AMI,AMI>> _activationNumberToPair;
			uint16_t _length = 0;
			std::vector<std::string> _derivedStructures;
			SemBioDevStructContainer _derivedBioDevicesStructures;
			std::vector<Activation> _activationsList;
			std::unordered_map<Activation, size_t, boost::hash<Activation>> _activationToDerBioDevStructId;
			std::unordered_map<int8_t,std::unordered_set<int8_t>> _semsBeforeSem;
			std::unordered_map<int8_t,std::unordered_set<int8_t>> _semsAfterSem;
			
			
			/**
			 * @param string
			 */
			void setStructure (const std::string &structure);
			
			void buildActivations();
			
			void buildActivationsRec(const std::unordered_set<ActivationNumber> &remaining, const Structure &structure, const Activation &activation);
			
			void applyActivationNumberOnStructure (Structure &structure, const ActivationNumber &an) const;
			
			boost::optional<std::pair<size_t,size_t>> getPositionsAMI (const Structure &structure, const ActivationNumber &an) const;
			
			bool isAMIinStructure (const Structure &structure, const AMI &a) const;
			
			bool isAMIpairInStructure (const Structure &structure, const ActivationNumber &an) const;
			
			std::unordered_set<ActivationNumber> getRemainingActivations (const std::unordered_set<ActivationNumber> &remaining, const Structure &structure);
			
			void buildDerivedBioDevicesStructures();
			
			void applyActivationNumberOnSBDStructure (SemanticalBioDeviceStructure &SBDStructure, const Structure &structure, const ActivationNumber &an) const;
			
			static std::string structureToString (const Structure &structure);
			
			void buildSemsBeforeAndAfterSem();
		};
		
		bool operator== (const GenericFunctionnalStructure::AMI &a, const GenericFunctionnalStructure::AMI &b);
	}
}

#endif
 
