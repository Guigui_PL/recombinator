#include "IEFunctionnalStructure.hpp"
#include <cstdint>
#include <algorithm>
#include <exception>
#include <sstream>
#include <set>
#include <boost/algorithm/string/trim.hpp>

using namespace std;
using namespace recombinator::api;

const std::unordered_map<IEFunctionnalStructure::AMI, uint16_t, EnumClassHash> IEFunctionnalStructure::lengthAMI
{
	{AMI::INVERSION_OPENING, 40}, {AMI::INVERSION_CLOSING, 40}, {AMI::EXCISION_OPENING, 40}, {AMI::EXCISION_CLOSING, 40}
};

size_t IEFunctionnalStructure::size() const
{
	return _structure.size();
}

void IEFunctionnalStructure::setStructure(const string& structure)
{
	// Firstly, we prepare our string in order to use a string stream 
	// The elements must be separated by a space, so we add spaces around AMIs
	string structTmp;
	
	for (auto &e : structure)
	{
		if (isAValidAMI(e))
		{
			structTmp += string(" ")+e+" ";
		}
		else if (e >= '0' && e <= '9')
		{
			structTmp += e;
		}
		else if (e != ' ') // If there is a character forbidden in a IEFunctionnalStructure, we throw an exception
		{
			throw invalid_argument("IEFunctionnalStructure must only contain AMI and integers : "+structure);
		}
	}
	
	// We initialize our string stream
	istringstream structStream;
	structStream.str(boost::algorithm::trim_copy(structTmp));
	
	// With that stream, we can separate easily AMI and indexes
	Structure IEStructure;
	vector<uint8_t> indexes;
	int16_t index;
	char ami;
	
	while ((structStream >> ami) && (structStream >> index))
	{
		IEStructure.push_back(static_cast<AMI>(ami));
		indexes.push_back(index);
	}
	
	// Now, we must check the validity of our structure
	// Firstly, we check the AMIs
	int nbOpenInv(0), nbOpenExc(0);
	_length = 0;
	_nbExcisions = 0;
	_nbInversions = 0;
	
	for (auto &ami: IEStructure)
	{
		switch (ami)
		{
			case AMI::INVERSION_OPENING:
				++nbOpenInv;
				_length += lengthAMI.at(AMI::INVERSION_OPENING);
				++_nbInversions;
				break;
				
			case AMI::INVERSION_CLOSING:
				--nbOpenInv;
				_length += lengthAMI.at(AMI::INVERSION_CLOSING);
				break;
				
			case AMI::EXCISION_OPENING:
				++nbOpenExc;
				_length += lengthAMI.at(AMI::EXCISION_OPENING);
				++_nbExcisions;
				break;
				
			case AMI::EXCISION_CLOSING:
				--nbOpenExc;
				_length += lengthAMI.at(AMI::EXCISION_CLOSING);
				break;
		}
		
		// If we have a nbOpenX negative, the structure is invalid 
		// because a non-opened ami is closed !
		if (nbOpenExc < 0)
		{
			throw invalid_argument("The structure is invalid, an unopened excision is closed !");
		}
		if (nbOpenInv < 0)
		{
			throw invalid_argument("The structure is invalid, an unopened inversion is closed !");
		}
	}
	
	// After the loop, the nbOpenX must be equal to 0
	if (nbOpenExc != 0)
	{
		throw invalid_argument("The structure is invalid, an excision is unclosed !");
	}
	if (nbOpenInv != 0)
	{
		throw invalid_argument("The structure is invalid, an inversion is unclosed !");
	}
	
	// Finally, we check if the indexes are right
	unordered_map<uint8_t, pair<size_t,size_t>> indexPositions;
	
	for (size_t i(0); i < indexes.size(); ++i)
	{
		uint8_t index = indexes[i];
		if (!(indexPositions.find(index) != indexPositions.end()))
		{
			indexPositions[index] = make_pair(i, static_cast<size_t>(-1));
		}
		else if (indexPositions[index].second == static_cast<size_t>(-1))
		{
			indexPositions[index].second = i;
		}
		else 
		{
			throw invalid_argument("An index must appear twice in a structure !");
		}
	}
	
	// Check if there is no alone index
	for (auto &ip: indexPositions)
	{
		if (ip.second.second == static_cast<size_t>(-1))
		{
			throw invalid_argument("An index must appear twice in a structure !");
		}
	}
	
	// Finally, we check if two indexes are bound to a same type of ami
	// And we check if the first index corresponds to a opening ami
	for (auto &ip: indexPositions)
	{
		if ((IEStructure[ip.second.first] == AMI::EXCISION_OPENING && IEStructure[ip.second.second] != AMI::EXCISION_CLOSING)
			|| (IEStructure[ip.second.first] == AMI::INVERSION_OPENING && IEStructure[ip.second.second] != AMI::INVERSION_CLOSING)
			|| (IEStructure[ip.second.first] == AMI::EXCISION_CLOSING)
			|| (IEStructure[ip.second.first] == AMI::INVERSION_CLOSING))
		{
			throw invalid_argument("The indexes of the structure are not correctly bounded to AMIs !");
		}
	}
	
	// If we passed all the tests, we can set our attributes !
	_structure = IEStructure;
	
	// We add elements to _indexPositions attribute 
	// But, the indexes must begin to 0 and the value of the others is inscremented
	unordered_set<uint8_t> insertedIndexes;
	for (auto &index : indexes)
	{
		if (!(insertedIndexes.find(index) != insertedIndexes.end()))
		{
			_indexPositions.push_back(indexPositions[index]);
			insertedIndexes.insert(index);
		}
	}
	
	// We make the _structureIndexes with the new index values
	_structureIndexes.resize(indexes.size());
	for (size_t i(0); i < _indexPositions.size(); ++i)
	{
		_structureIndexes[_indexPositions[i].first] = i;
		_structureIndexes[_indexPositions[i].second] = i;
	}
}

string IEFunctionnalStructure::getStructureWithIndexes() const
{
	string result;
	for (size_t i(0); i < _structure.size(); ++i)
	{
		switch (_structure[i])
		{
			case AMI::INVERSION_OPENING:   
				result.push_back('(');
				break;
				
			case AMI::INVERSION_CLOSING:
				result.push_back(')');
				break;
				
			case AMI::EXCISION_OPENING:
				result.push_back('[');
				break;
				
			case AMI::EXCISION_CLOSING:
				result.push_back(']');
				break;
		}
		result += to_string(_structureIndexes[i]);
	}
	
	return result;
}

std::string  IEFunctionnalStructure::structureReverse(std::string structure)
{
	std::string rev;
	
	for (size_t i = 0; i < structure.size(); ++i)
	{
		string add;
		switch (structure[i])
		{
			case ']': add = "["; break;
			case '[': add = "]"; break;
			case '(': add = ")"; break;
			case ')': add = "("; break;
		}
		rev = add + structure[++i] + rev;
	}
	
	return rev;
}

string IEFunctionnalStructure::getStructure() const
{
	return getStructureWithIndexes();
}

std::string IEFunctionnalStructure::getStructureOnDerived(size_t i) const
{
	return _derivedStructures[i];
}

std::vector<std::string> IEFunctionnalStructure::getStructuresOnDerived() const
{
	return _derivedStructures;
}

const FunctionnalStructure::SemBioDevStructContainer& IEFunctionnalStructure::getDerivedBioDevicesStructures() const
{
	return _derivedBioDevicesStructures;
}

const FunctionnalStructure::SemanticalBioDeviceStructure& IEFunctionnalStructure::getDerivedBioDevicesStructure(const Activation& activation) const
{
	return _derivedBioDevicesStructures.at(_activationToDerBioDevStructId.at(activation));
}

size_t IEFunctionnalStructure::getIndexDerivedBioDevicesStructure(const Activation& activation) const
{
	return _activationToDerBioDevStructId.at(activation);
}

size_t IEFunctionnalStructure::getNbDifferentDerivations() const
{
	return _derivedBioDevicesStructures.size();
}

const vector<FunctionnalStructure::Activation>& IEFunctionnalStructure::getActivations() const
{
	return _activationsList;
}

bool IEFunctionnalStructure::isAValidAMI(char ami) const
{
	return ami == static_cast<char>(AMI::INVERSION_OPENING)
		|| ami == static_cast<char>(AMI::INVERSION_CLOSING)
		|| ami == static_cast<char>(AMI::EXCISION_OPENING)
		|| ami == static_cast<char>(AMI::EXCISION_CLOSING);
}

size_t IEFunctionnalStructure::getNbIntegrase() const
{
	return _structure.size()/2;
}

void IEFunctionnalStructure::buildActivations()
{
	// n : number of AMIs
	// Number of activations : 1 + sum i from 1 to n (arrangement(i,n))
	// First step : we create a powerset of {0,1,...,n-1}
	set<uint8_t> activationSet;
	for (uint8_t i(0); i < getNbIntegrase(); ++i)
	{
		activationSet.insert(i);
	}
	
	set<set<uint8_t>> powerSet;
    powerSet.emplace(); // Adding empty set
    for (auto&& activation: activationSet) 
	{
        set<set<uint8_t>> tmp;
        for (auto x: powerSet) 
		{
            x.insert(activation);
            tmp.insert(x);
        }
        powerSet.insert(begin(tmp), end(tmp));
    }
    
	// Second step : we transform our set of sets in vector of vectors
	vector<vector<uint8_t>> activationVector;
	for (auto &set : powerSet)
	{
		activationVector.push_back(vector<uint8_t>(set.begin(), set.end()));
	}
	_activationsList = activationVector;
	
	// Last step : we creates all the permutations of the vectors
	for (auto &activation : activationVector)
	{
		while (next_permutation(activation.begin(), activation.end()))
		{
			_activationsList.push_back(activation);
		}
	}
}

void IEFunctionnalStructure::buildDerivedBioDevicesStructures()
{
	// We use an integer to represent a deleted semantic in the bioDevStruct (after an excision)
	int deletedSemantic = INT_MAX; 
	// We use a char different of AMIs to represent a deleted AMI (after an excision)
	char deadAMI = '|';
	
	// FIrstly, we create the first derived bioDevice
	vector<int> derived;
	for (uint8_t i(0); i < _structure.size()+1; ++i)
	{
		derived.push_back(i);
	}
	
	// This derived is the derived BioDevice of no activation
	_derivedBioDevicesStructures.push_back(SemanticalBioDeviceStructure(derived.begin(), derived.end()));
	_activationToDerBioDevStructId[Activation()] = 0;
	vector<char> structureForNoActivation;
	transform(_structure.begin(), _structure.end(), back_inserter(structureForNoActivation), [](AMI ami) -> char { return static_cast<char>(ami); });
	string derivedStructure;
	for (size_t i = 0; i < _structureIndexes.size(); ++i)
	{
		derivedStructure += string(1,structureForNoActivation[i])+to_string(_structureIndexes[i])+" ";
	}
	_derivedStructures.push_back(derivedStructure);
	
	// Now, we will apply activations
	for (auto &act : _activationsList)
	{
		vector<char> structure;
		transform(_structure.begin(), _structure.end(), back_inserter(structure), [](AMI ami) -> char { return static_cast<char>(ami); });
		vector<int> newDerived(derived);
		auto indexPositions = _indexPositions;
		auto indexes = _structureIndexes;
		
		// For each index, we activate the associated AMI
		for (auto &index : act)
		{
			// We get the position of opening and closing
			size_t posOpening(indexPositions[index].first), posClosing(indexPositions[index].second);
			
			// First case : the AMI is an excision
			if (structure[posOpening] == static_cast<char>(AMI::EXCISION_OPENING))
			{
				// We set to dead the AMIs in the excision
				for (size_t i(posOpening+1); i < posClosing; ++i)
				{
					size_t indexDeadAMI = indexes[i];
					structure[indexPositions.at(indexDeadAMI).first] = deadAMI;
					structure[indexPositions.at(indexDeadAMI).second] = deadAMI;
				}
				
				// We set to "deleted" the semantics in the excision
				for (size_t i(posOpening+1); i <= posClosing; ++i)
				{
					newDerived[i] = deletedSemantic;
				}
			}
			// Second case : the AMI is an inversion
			else if (structure[posOpening] == static_cast<char>(AMI::INVERSION_OPENING))
			{
				// We "inverse" the AMIs with only a part in the inversion
				// i.e. excisions become inversions end conversely
				for (size_t i(posOpening+1); i < posClosing; ++i)
				{
					size_t indexAMI = indexes[i];
					
					// If the opening AMI is in the inversion but not the closing AMI 
					// Or if the closing AMI is in the inversion but not the opening AMI
					if (indexPositions.at(indexAMI).first > posOpening 
						&& indexPositions.at(indexAMI).first < posClosing 
						&& indexPositions.at(indexAMI).second > posClosing)
					{
						if (structure[indexPositions.at(indexAMI).first] == static_cast<char>(AMI::INVERSION_OPENING))
						{
							structure[indexPositions.at(indexAMI).first] = static_cast<char>(AMI::EXCISION_CLOSING);
							structure[indexPositions.at(indexAMI).second] = static_cast<char>(AMI::EXCISION_CLOSING);
						}
						else if (structure[indexPositions.at(indexAMI).first] == static_cast<char>(AMI::EXCISION_OPENING))
						{
							structure[indexPositions.at(indexAMI).first] = static_cast<char>(AMI::INVERSION_CLOSING);
							structure[indexPositions.at(indexAMI).second] = static_cast<char>(AMI::INVERSION_CLOSING);
						}
					}
					else if (indexPositions.at(indexAMI).first < posOpening
						&& indexPositions.at(indexAMI).second < posClosing
						&& indexPositions.at(indexAMI).second > posOpening)
					{
						if (structure[indexPositions.at(indexAMI).first] == static_cast<char>(AMI::INVERSION_OPENING))
						{
							structure[indexPositions.at(indexAMI).first] = static_cast<char>(AMI::EXCISION_OPENING);
							structure[indexPositions.at(indexAMI).second] = static_cast<char>(AMI::EXCISION_OPENING);
						}
						else if (structure[indexPositions.at(indexAMI).first] == static_cast<char>(AMI::EXCISION_OPENING))
						{
							structure[indexPositions.at(indexAMI).first] = static_cast<char>(AMI::INVERSION_OPENING);
							structure[indexPositions.at(indexAMI).second] = static_cast<char>(AMI::INVERSION_OPENING);
						}
					}
				}
				
				// We inverse the semantics in the inversion
				// So, we multiply the semantics by -1
				for (size_t i(posOpening+1); i <= posClosing; ++i)
				{
					if (newDerived[i] != deletedSemantic)
					{
						newDerived[i] *= -1;
					}
				}
				
				// We reverse
				reverse(newDerived.begin()+posOpening+1,newDerived.begin()+posClosing+1);
				reverse(indexes.begin()+posOpening+1,indexes.begin()+posClosing);
				reverse(structure.begin()+posOpening+1,structure.begin()+posClosing);
				for_each(structure.begin()+posOpening+1,structure.begin()+posClosing,[](char &c){
					switch (c)
					{
						case '[': c = ']'; break;
						case '(': c = ')'; break;
						case ']': c = '['; break;
						case ')': c = '('; break;
					}
				});
				
				// We re-make the indexPositions
				indexPositions.clear();
				indexPositions.resize(indexes.size()/2,make_pair(static_cast<size_t>(-1),static_cast<size_t>(-1)));
				for (size_t i(0); i < indexes.size(); ++i)
				{
					uint8_t index = indexes[i];
					if (indexPositions[index].first == static_cast<size_t>(-1))
					{
						indexPositions[index].first = i;
					}
					else if (indexPositions[index].second == static_cast<size_t>(-1))
					{
						indexPositions[index].second = i;
					}
					else 
					{
						throw invalid_argument("An index must appear twice in a structure !");
					}
				}
			}
		}
		
		// Now, we create the final derived BioDevStruct with the un-deletedSemantic
		// After that, we insert it in _derivedBioDevicesStructures
		// But, before, we check if the derived BioDevStruct already exists
		
		// We remove deleted Semantic
		newDerived.erase(std::remove(newDerived.begin(), newDerived.end(), deletedSemantic),newDerived.end());
		SemanticalBioDeviceStructure finalDerived(newDerived.begin(), newDerived.end());
		
		// If it exists, we get the position of the derived in the _derivedBioDevicesStructures
		ptrdiff_t posDerBioDevStruct(
			distance(_derivedBioDevicesStructures.begin(),
					 find(_derivedBioDevicesStructures.begin(), 
						  _derivedBioDevicesStructures.end(),
						  finalDerived
 						)));
		
		// If the derived already exists
		if (static_cast<size_t>(posDerBioDevStruct) < _derivedBioDevicesStructures.size() && posDerBioDevStruct >= 0)
		{
			_activationToDerBioDevStructId[act] = posDerBioDevStruct;
		}
		else 
		{
			_derivedBioDevicesStructures.push_back(finalDerived);
			_activationToDerBioDevStructId[act] = _derivedBioDevicesStructures.size()-1;
			
			// Now, the dervied structure
			string derivedStructure;
			for (size_t i = 0; i < indexes.size(); ++i)
			{
				derivedStructure += string(1,structure[i])+to_string(indexes[i])+" ";
			}
			_derivedStructures.push_back(derivedStructure);
		}
	}
}

const unordered_set<int8_t>& IEFunctionnalStructure::getSemsAfterSem(int8_t numSem) const
{
	if (!(_semsAfterSem.find(numSem) != _semsAfterSem.end()))
	{
		throw invalid_argument("numSem is not valid in IEFunctionnalStructure::getSemsAfterSem() !");
	}
	
	return _semsAfterSem.at(numSem);
}

const unordered_set<int8_t>& IEFunctionnalStructure::getSemsBeforeSem(int8_t numSem) const
{
	if (!(_semsBeforeSem.find(numSem) != _semsBeforeSem.end()))
	{
		throw invalid_argument("numSem is not valid in IEFunctionnalStructure::getSemsBeforeSem() !");
	}
	
	return _semsBeforeSem.at(numSem);
}

void IEFunctionnalStructure::buildSemsBeforeAndAfterSem()
{
	for (int8_t i(_structure.size()*(-1)); i <= static_cast<int8_t>(_structure.size()); ++i)
	{
		_semsAfterSem[i] = unordered_set<int8_t>();
		_semsBeforeSem[i] = unordered_set<int8_t>();
	}
	
	for (auto &der : _derivedBioDevicesStructures)
	{
		for (size_t i(0); static_cast<size_t>(i) < der.size(); ++i)
		{
			if (i > 0)
			{
				_semsBeforeSem[der[i]].insert(der[i-1]);
			}
			if (i < der.size()-1)
			{
				_semsAfterSem[der[i]].insert(der[i+1]);
			}
		}
	}
}

uint16_t IEFunctionnalStructure::getLength() const
{
	return _length;
}

uint16_t IEFunctionnalStructure::getNbParts() const
{
	return _structure.size();
}

uint16_t IEFunctionnalStructure::getNbExcisions() const
{
	return _nbExcisions;
}

uint16_t IEFunctionnalStructure::getNbInversions() const
{
	return _nbInversions;
}

uint16_t IEFunctionnalStructure::getLengthAMI(size_t i) const
{
	if (i >= _structure.size())
	{
		throw std::out_of_range("The i in argument of IEFunctionnalStructure::getLengthAMI() is too big");
	}
	
	return lengthAMI.at(_structure[i]);
}

string IEFunctionnalStructure::getAMIWithIndex(size_t i) const
{
	if (i >= _structure.size())
	{
		throw std::out_of_range("The i in argument of IEFunctionnalStructure::getAMIWithIndex() is too big");
	}
	
	return static_cast<char>(_structure[i]) + to_string(_structureIndexes[i]);
}

vector<uint16_t> IEFunctionnalStructure::getPositionsSemInAtomicAMI() const
{
	vector<uint16_t> positions;
	
	for (auto &p : _indexPositions)
	{
		if (p.first+1 == p.second)
		{
			positions.push_back(p.second);
		}
	}
	
	return positions;
}

vector<uint16_t> IEFunctionnalStructure::getPositionsSemInAtomicExcisions() const
{
	vector<uint16_t> positions;
	
	for (auto &p : _indexPositions)
	{
		if (_structure[p.first] == AMI::EXCISION_OPENING && p.first+1 == p.second)
		{
			positions.push_back(p.second);
		}
	}
	
	return positions;
}

vector<uint16_t> IEFunctionnalStructure::getPositionsSemInAtomicInversions() const
{
	vector<uint16_t> positions;
	
	for (auto &p : _indexPositions)
	{
		if (_structure[p.first] == AMI::INVERSION_OPENING && p.first+1 == p.second)
		{
			positions.push_back(p.second);
		}
	}
	
	return positions;
}

bool IEFunctionnalStructure::isInExcision(size_t i) const
{
	for (auto &pos : _indexPositions)
	{
		if (_structure[pos.first] == AMI::EXCISION_OPENING 
			&& i > pos.first && i <= pos.second)
		{
			return true;
		}
	}
	
	return false;
}

bool IEFunctionnalStructure::isInInversion(size_t i) const
{
	for (auto &pos : _indexPositions)
	{
		if (_structure[pos.first] == AMI::INVERSION_OPENING 
			&& i > pos.first && i <= pos.second)
		{
			return true;
		}
	}
	
	return false;
}
