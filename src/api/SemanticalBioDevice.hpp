#ifndef _SEMANTICALBIODEVICE_H
#define _SEMANTICALBIODEVICE_H

#include <cstddef>
#include <initializer_list>
#include <memory>
#include <vector>
#include "BioDevice.hpp"
#include "FunctionnalStructure.hpp"
#include "Semantic.hpp"


namespace recombinator 
{
	namespace api 
	{
		// Forward declarations
		class cDerivedSemanticalBioDeviceIterator;
		class cDerivedSemanticalBioDeviceReverseIterator;
		class DerivedSemanticalBioDevice;
		
		/**
		 * \class SemanticalBioDevice
		 * \brief   
		 */
		class SemanticalBioDevice : public BioDevice
		{
			// We declare friends the class which represents the semantics of Derived BioDevices
			friend DerivedSemanticalBioDevice; 
			
		public:
			typedef std::vector<DerivedSemanticalBioDevice>::const_iterator const_iterator;
			typedef FunctionnalStructure::Activation Activation;
			
			/**
			 * \brief
			 */
			
			SemanticalBioDevice();
			
			SemanticalBioDevice(const FunctionnalStructure &structure, std::vector<const Semantic*> semantics = {});
			
			SemanticalBioDevice(const SemanticalBioDevice &bioDevice);
			
			SemanticalBioDevice& operator= (const SemanticalBioDevice &bioDevice);
			
			/**
			 * Move constructor is implicitly deleted because the using of unique_ptr for _structure
			 * We demand to the compiler to use the default move constructor
			 */
			
			SemanticalBioDevice(SemanticalBioDevice && bioDevice) = default;
			SemanticalBioDevice& operator=(SemanticalBioDevice && bioDevice) = default;
			
			/**
			 * \brief
			 */
			SemanticalBioDevice(const std::string &bioDevice);
			virtual ~SemanticalBioDevice() = default;
			
			/**
			 * \brief 
			 *
			 * \return  
			 */
			virtual const FunctionnalStructure &getStructure() const;
			
			virtual size_t size() const;
			
			virtual SemanticalBioDevice getSymmetric() const;
			
			/**
			 * \brief 
			 *
			 * \param
			 * \return 
			 */
			virtual void setSemantic(size_t index, const Semantic* semantic);
			
			virtual void setSemantics(std::vector<const Semantic*> semantics);
			
			/**
			 * \brief
			 *
			 * \param
			 * \return 
			 */
			virtual const Semantic& getSemantic(size_t index) const;
			
			virtual const Semantic& getPrefix(size_t derived, size_t var) const;
			virtual const Semantic& getSuffix(size_t derived, size_t var) const;
			
			/**
			 * \brief
			 *
			 * \param
			 * \return 
			 */
			virtual const std::vector<const Semantic*> &getSemantics() const;
			
			/**
			 * \brief
			 *
			 * \param
			 * \return 
			 */
			virtual size_t getNbSemantics() const;
			
			/**
			 * \brief 
			 *
			 * \param
			 * \return 
			 */
			virtual const Semantic& getSemantic(size_t indexDerivedSemanticalBioDevice, size_t index) const;
			
			virtual size_t getNbDerivedSemanticalBioDevice() const;
			
			virtual const DerivedSemanticalBioDevice& getDerivedSemanticalBioDevice(size_t i) const;
			
			virtual const std::vector<Activation>& getActivations() const;
			
			virtual const DerivedSemanticalBioDevice& getDerivedSemanticalBioDevice(const Activation& activation) const;
			
			inline const DerivedSemanticalBioDevice& operator[](size_t i) const;
			
			/**
			 * \brief 
			 *
			 * \param
			 * \return  
			 */
			virtual bool isDefinedSemantic(size_t index) const;
			
			/**
			 * \brief 
			 * \return  
			 */
			virtual bool isAllSemanticsDefined() const;
			
			/**
			 * \brief 
			 * \return  
			 */
			virtual size_t getNbDefinedSemantics() const;
			
			/**
			 * \brief 
			 * \return  
			 */
			virtual bool hasGenesAtEnds() const;
			
			/**
			 * \brief 
			 * \return  
			 */
			virtual bool isRespectedWeakConstraint() const;
			
			/**
			 * \brief 
			 * \return  
			 */
			virtual bool isRespectedStrongConstraint() const;
			
			/**
			 * \brief 
			 * \return  
			 */
			virtual bool isReducible() const;
			
			const_iterator cbegin() const;
			const_iterator cend() const;
			const_iterator begin() const;
			const_iterator end() const;
			auto clone() const { return std::unique_ptr<SemanticalBioDevice>(clone_impl()); }
			
			virtual uint16_t getMinLength() const;
			
			virtual uint16_t getNbGenes() const;
			
			virtual uint16_t getNbPromoters() const;
			
			virtual uint16_t getNbTerminators() const;
			
			virtual uint16_t getMinNbParts() const;
			
			virtual uint16_t getMaxDistancePromoterToGene() const;
			
			virtual uint8_t getNbIntegrase() const;
			
			virtual bool hasSameExpressions(const SemanticalBioDevice& sbd) const;
			
			virtual uint16_t getNbTerminatorsReplaceableByDoubles() const;
			
			operator std::string () const;
			
			virtual bool operator== (const SemanticalBioDevice& sbd) const;
			virtual bool operator!= (const SemanticalBioDevice& sbd) const;
			
			virtual std::string getHexadecimalSemantics() const;
			virtual std::string getSemanticBytes() const;
			
		protected:
			std::unique_ptr<FunctionnalStructure> _structure;
			std::vector<const Semantic*> _semantics;
			size_t _nbDefinedSemantics = 0;
			mutable std::vector<DerivedSemanticalBioDevice> _derBioDevices;
			mutable std::vector<std::vector<const Semantic*>> _prefixes;
			mutable std::vector<std::vector<const Semantic*>> _suffixes;
			
			void buildDerivedSemanticalBioDevices() const;
			
			virtual SemanticalBioDevice* clone_impl() const { return new SemanticalBioDevice(*this); };     
		};
				
		class DerivedSemanticalBioDevice
		{
			friend cDerivedSemanticalBioDeviceIterator;
			friend cDerivedSemanticalBioDeviceReverseIterator;
			
			typedef cDerivedSemanticalBioDeviceIterator const_iterator;
			typedef cDerivedSemanticalBioDeviceReverseIterator const_reverse_iterator;
			typedef ptrdiff_t difference_type;
			typedef size_t size_type;
			typedef Semantic value_type;
			typedef Semantic * pointer;
			typedef Semantic & reference;
			
		public:
			DerivedSemanticalBioDevice(const SemanticalBioDevice& bioDev, size_t id);
			
			const_iterator cbegin() const;
			const_iterator cend() const;
			const_iterator begin() const;
			const_iterator end() const;
			
			const_reverse_iterator crbegin() const;
			const_reverse_iterator crend() const;
			const_reverse_iterator rbegin() const;
			const_reverse_iterator rend() const;
			
			bool isX() const;
			bool isIn(uint8_t var) const;
			bool isReversed(uint8_t var) const;
			size_t getVarPos(uint8_t var) const;
			
			inline size_t size() const;
			inline const Semantic& operator[] (size_t i) const;
			
			const FunctionnalStructure::SemanticalBioDeviceStructure& getSemStruct() const;
			
			operator std::string() const;
		private:
			const SemanticalBioDevice* _bioDev;
			size_t _id;
			std::unordered_set<uint8_t> _variablesIn;
			std::vector<size_t> _varPos;
			std::unordered_set<uint8_t> _variablesReversed;
		};
		
		class cDerivedSemanticalBioDeviceIterator
		{
		public:
			cDerivedSemanticalBioDeviceIterator(const DerivedSemanticalBioDevice& derSemBioDev, size_t pos);
			bool operator== (const cDerivedSemanticalBioDeviceIterator& cit) const;
			bool operator!= (const cDerivedSemanticalBioDeviceIterator& cit) const;
			const Semantic& operator* () const;
			cDerivedSemanticalBioDeviceIterator & operator++();
			cDerivedSemanticalBioDeviceIterator operator++ (int);
			cDerivedSemanticalBioDeviceIterator& operator+ (int i);
			cDerivedSemanticalBioDeviceIterator& operator- (int i);
			
		private:
			const DerivedSemanticalBioDevice* _derSemBioDev;
			size_t _pos;
		};
		
		class cDerivedSemanticalBioDeviceReverseIterator
		{
		public:
			cDerivedSemanticalBioDeviceReverseIterator(const DerivedSemanticalBioDevice& derSemBioDev, int pos);
			bool operator== (const cDerivedSemanticalBioDeviceReverseIterator& cit) const;
			bool operator!= (const cDerivedSemanticalBioDeviceReverseIterator& cit) const;
			const Semantic& operator* () const;
			cDerivedSemanticalBioDeviceReverseIterator & operator++();
			cDerivedSemanticalBioDeviceReverseIterator operator++ (int);
			cDerivedSemanticalBioDeviceReverseIterator &operator+ (int i);
			cDerivedSemanticalBioDeviceReverseIterator& operator- (int i);
			
		private:
			const DerivedSemanticalBioDevice* _derSemBioDev;
			int _pos;
		};
	}
}

#endif
