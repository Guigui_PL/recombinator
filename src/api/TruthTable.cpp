#include "TruthTable.hpp"
#include <cmath>
#include <unordered_set>
#include <boost/dynamic_bitset.hpp>

using namespace recombinator::api;
using namespace std;

TruthTable::TruthTable(uint8_t nbInputs) : _nbInputs(nbInputs)
{}

uint8_t TruthTable::getNbInputs() const
{
	return _nbInputs;
}

std::vector<std::string> TruthTable::getPEquivalentFunctions () const
{
	string function = getDisjunctiveNormalForm();
	unordered_set<string> allFunctions{function};
	vector<uint8_t> basePermutation(_nbInputs), nextPermutation(_nbInputs);
	vector<boost::dynamic_bitset<>> baseTerms;
	
	for (uint8_t i(0); i < _nbInputs; ++i)
	{
		basePermutation[i] = i;
		nextPermutation[i] = i;
	}
	
	for (uint64_t i(0); i < function.size(); ++i)
	{
		if (function[i] == '1')
		{
			baseTerms.emplace_back(_nbInputs,i);
		}
	}
	
	while (next_permutation(nextPermutation.begin(), nextPermutation.end()))
	{
		vector<boost::dynamic_bitset<>> newTerms (baseTerms.size(), boost::dynamic_bitset<>(_nbInputs,0));
		
		for (uint64_t i(0); i < baseTerms.size(); ++i)
		{
			for (uint8_t j(0); j < _nbInputs; ++j)
			{
				newTerms[i][nextPermutation[j]] = baseTerms[i][j];
			}
		}
		
		string newFunction(function.size(),'0');
		
		for (auto &t: newTerms)
		{
			newFunction[t.to_ulong()] = '1';
		}
		
		allFunctions.insert(newFunction);
	}
	
	return vector<string>(allFunctions.begin(), allFunctions.end());
}

std::vector<uint64_t> TruthTable::getPEquivalentFunctionsInt () const
{
	vector<string> functions = getPEquivalentFunctions();
	vector<uint64_t> result;
	result.reserve(functions.size());
	
	for (auto &it : functions)
	{
		result.push_back(stoull(it, 0, 2));
	}
	
	return result;
}

uint64_t TruthTable::getDisjunctiveNormalFormInt() const
{
	return stoull(getDisjunctiveNormalForm(), 0, 2);
}