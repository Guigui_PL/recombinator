#include "BioDeviceFile.hpp"
#include <iostream>
#include <stdexcept>
#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>
#include "IEFunctionnalStructure.hpp"
#include "IEDyckFunctionnalStructure.hpp"

using namespace std;
using namespace recombinator::api;
namespace fs = boost::filesystem;

/*
 * BioDeviceFile implementation
 */

BioDeviceFile::BioDeviceFile(string filePath) : _filePath(filePath) {}

const string& BioDeviceFile::getFilePath() const
{
	return _filePath;
}


/*
 * BioDeviceFileReader implementation
 */

BioDeviceFileReader::BioDeviceFileReader(string filePath) :
BioDeviceFile(filePath),
_file(_filePath, std::ios_base::binary)
{
	if (!isOpenFile())
	{
		throw invalid_argument("Invalid filePath : the file can't be opened - "+_filePath);
	}
}

shared_ptr<BioDevice> BioDeviceFileReader::getNextBioDevice()
{
	return shared_ptr<BioDevice>(getNextBioDeviceImpl());
}

bool BioDeviceFileReader::isOpenFile() const
{
	return _file.is_open();
}

BioDeviceFileReader::operator bool ()
{
	return (bool)_file;
}

/*
 * BioDeviceFileWriter implementation
 */

BioDeviceFileWriter::BioDeviceFileWriter(string filePath) : 
BioDeviceFile(filePath)
{}

BioDeviceFileWriter::~BioDeviceFileWriter()
{
	if (_file.is_open())
	{
		flush();
	}
}

bool BioDeviceFileWriter::isOpenFile() const
{
	return _file.is_open();
}

void BioDeviceFileWriter::flush()
{
	_file.flush();
}


/*
 * SemanticalBioDeviceFile implementation
 */

SemanticalBioDeviceFile::SemanticalBioDeviceFile() :
_fileType(FileType::CLASSICAL_BIODEVICE)
{}


/*
 * SemanticalBioDeviceFileReader implementation
 */

SemanticalBioDeviceFileReader::SemanticalBioDeviceFileReader(string filePath) :
BioDeviceFileReader(filePath)
{
	uint8_t typeNameSize(6);
	string readType (typeNameSize,'\0');
	
	_file.read(&readType[0],typeNameSize);
	
	if (readType == "UNIQUE")
	{
		_fileType = FileType::UNIQUE_STRUCTURE;
		
		char FSSize;
		_file.read(&FSSize,1);
		
		string FSString(FSSize,'\0');
		_file.read(&FSString[0], FSSize);
		
		_functionnalStructure = IEFunctionnalStructure::create<IEFunctionnalStructure>(FSString);
	}
	else if (readType == "MULTIP")
	{
		_fileType = FileType::MULTI_STRUCTURE;
	}
	else
	{
		_file.close();
		_file.open(_filePath);
		_fileType = FileType::CLASSICAL_BIODEVICE;
	}
}

shared_ptr<SemanticalBioDevice> SemanticalBioDeviceFileReader::getNextBioDevice()
{
	return shared_ptr<SemanticalBioDevice>(getNextBioDeviceImpl());
}

SemanticalBioDevice* SemanticalBioDeviceFileReader::getNextBioDeviceImpl()
{
	switch (_fileType)
	{
		case FileType::MULTI_STRUCTURE:
		{
			char FSSize;
			_file.read(&FSSize,1);
			
			if (_file.fail())
				return nullptr;
			
			string FSString(FSSize,'\0');
			_file.read(&FSString[0], FSSize);
			
			_functionnalStructure = IEFunctionnalStructure::create<IEFunctionnalStructure>(FSString);
		}
		[[fallthrough]];
			
		case FileType::UNIQUE_STRUCTURE:
		{
			auto sbd = new SemanticalBioDevice(*_functionnalStructure);
			
			for (size_t i{0}; i < _functionnalStructure->getNbIntegrase()*2+1; ++i)
			{
				char key;
				_file.read(&key,1);
				sbd->setSemantic(i, Semantic::getPointerWithKey(key));
			}
			
			return sbd;
		}
			
		case FileType::CLASSICAL_BIODEVICE:
		default:
		{
			string bioDeviceString;
			if (getline(_file, bioDeviceString))
			{
				boost::trim(bioDeviceString);
				return new SemanticalBioDevice(bioDeviceString);
			}
		}
	}
	
	return nullptr;
}


/*
 * SemanticalBioDeviceFileWriter implementation
 */

SemanticalBioDeviceFileWriter::SemanticalBioDeviceFileWriter(std::string filePath, SemanticalBioDeviceFile::FileType type, bool overWrite) :
BioDeviceFileWriter(filePath)
{
	_fileType = type;
	
	auto mode = ios_base::out;
	if (overWrite)
	{
		mode |= ios_base::trunc;
	}
	if (_fileType != FileType::CLASSICAL_BIODEVICE)
	{
		mode |= ios_base::binary;
	}
	_file.open(_filePath, mode);
	
	if (overWrite/* || fs::is_empty(fs::path(_filePath))*/)
	{
		switch (_fileType)
		{
			case FileType::MULTI_STRUCTURE:
				_file << "MULTIP";
				break;
				
			case FileType::UNIQUE_STRUCTURE:
				_file << "UNIQUE";
				break;
				
			case FileType::CLASSICAL_BIODEVICE:
			default:
				break;
		}
	}
	else if (_fileType == FileType::UNIQUE_STRUCTURE)
	{
		ifstream in (_filePath);
		uint8_t typeNameSize(6);
		string readType (typeNameSize,'\0');
		in.read(&readType[0],typeNameSize);
		
		if (in.eof()) 
		{
			_file << "UNIQUE";
		}
		else 
		{
			if (readType != "UNIQUE")
			{
				throw std::invalid_argument("The file to write is not UNIQUE format : "+_filePath);
			}
			else 
			{
				char FSSize;
				in.read(&FSSize,1);
				
				string FSString(FSSize,'\0');
				in.read(&FSString[0], FSSize);
			
				_functionnalStructure = IEFunctionnalStructure::create<IEFunctionnalStructure>(FSString);
			}
		}
		
		in.close();
	}
	
	if (!isOpenFile())
	{
		throw invalid_argument("Invalid filePath : the file can't be opened - "+filePath+" in "+__FILE__+" line "+to_string(__LINE__));
	}
}

void SemanticalBioDeviceFileWriter::writeBioDevice(const SemanticalBioDevice& bd)
{
	if (_fileType == FileType::UNIQUE_STRUCTURE && _functionnalStructure == nullptr)
	{
		_functionnalStructure = bd.getStructure().clone();
		_file << static_cast<uint8_t>(_functionnalStructure->getStructureWithIndexes().size()) 
			<< _functionnalStructure->getStructureWithIndexes();
	}
	
	switch (_fileType)
	{
		case FileType::MULTI_STRUCTURE:
			
			_file << static_cast<uint8_t>(bd.getStructure().getStructureWithIndexes().size())
				<< bd.getStructure().getStructureWithIndexes();
		[[fallthrough]];
			
		case FileType::UNIQUE_STRUCTURE:
			
			for (size_t i = 0; i < bd.getNbSemantics(); ++i)
			{
				_file << bd.getSemantic(i).getSemanticKey();
			}
			
			break;
			
		case FileType::CLASSICAL_BIODEVICE:
		default:
			_file << static_cast<string>(bd) << endl;
	}
}

/*
 * DyckSemanticalBioDeviceFileReader implementation
 */

DyckSemanticalBioDeviceFileReader::DyckSemanticalBioDeviceFileReader(string filePath) :
SemanticalBioDeviceFileReader(filePath)
{
	if (_fileType == FileType::UNIQUE_STRUCTURE)
	{
		_functionnalStructure = IEDyckFunctionnalStructure::create<IEDyckFunctionnalStructure>(_functionnalStructure->getStructureWithIndexes());
	}
}

shared_ptr<DyckSemanticalBioDevice> DyckSemanticalBioDeviceFileReader::getNextBioDevice()
{
	return shared_ptr<DyckSemanticalBioDevice>(getNextBioDeviceImpl());
}

DyckSemanticalBioDevice* DyckSemanticalBioDeviceFileReader::getNextBioDeviceImpl()
{
	switch (_fileType)
	{
		case FileType::MULTI_STRUCTURE:
		{
			char FSSize;
			_file.read(&FSSize,1);
			
			if (_file.fail())
				return nullptr;
			
			string FSString(FSSize,'\0');
			_file.read(&FSString[0], FSSize);
			
			_functionnalStructure = IEDyckFunctionnalStructure::create<IEDyckFunctionnalStructure>(FSString);
		}
		[[fallthrough]];
			
		case FileType::UNIQUE_STRUCTURE:
		{
			auto sbd = new DyckSemanticalBioDevice(*dynamic_pointer_cast<DyckFunctionnalStructure>(_functionnalStructure));
			
			for (size_t i{0}; i < _functionnalStructure->getNbIntegrase()*2+1; ++i)
			{
				char key;
				_file.read(&key,1);
				sbd->setSemantic(i, Semantic::getPointerWithKey(key));
			}
			
			return sbd;
		}
			
		case FileType::CLASSICAL_BIODEVICE:
		default:
		{
			string bioDeviceString;
			if (getline(_file, bioDeviceString))
			{
				return new DyckSemanticalBioDevice(bioDeviceString);
			}
		}
	}
	
	return nullptr;
}


/*
 * DyckSemanticalBioDeviceFileWriter implementation
 */

DyckSemanticalBioDeviceFileWriter::DyckSemanticalBioDeviceFileWriter(std::string filePath, SemanticalBioDeviceFile::FileType type, bool overWrite) :
SemanticalBioDeviceFileWriter(filePath, type, overWrite)
{}

void DyckSemanticalBioDeviceFileWriter::writeBioDevice(const DyckSemanticalBioDevice& bd)
{
	SemanticalBioDeviceFileWriter::writeBioDevice(bd);
}
