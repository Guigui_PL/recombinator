#ifndef _FUNCTIONNALSTRUCTURE_H
#define _FUNCTIONNALSTRUCTURE_H

#include <algorithm>
#include <memory>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

namespace recombinator 
{
	namespace api 
	{
		class FunctionnalStructure 
		{
			
		public:
			virtual ~FunctionnalStructure() = default;
			
			using SemanticalBioDeviceStructure = std::vector<int8_t> ;
			using SemBioDevStructContainer = std::vector<SemanticalBioDeviceStructure>;
			using ActivationNumber = uint8_t;
			using Activation = std::vector<ActivationNumber>;
			
			virtual size_t size() const = 0;
			
			virtual std::string getStructureWithIndexes() const = 0;
			
			virtual std::string getStructure() const = 0;
			
			virtual std::string getStructureOnDerived(size_t i) const = 0;
			
			virtual std::vector<std::string> getStructuresOnDerived() const = 0;
			
			virtual const SemBioDevStructContainer& getDerivedBioDevicesStructures() const = 0;
			
			/**
			 * @param activation
			 */
			virtual const SemanticalBioDeviceStructure& getDerivedBioDevicesStructure(const Activation& activation) const = 0;
			
			virtual size_t getIndexDerivedBioDevicesStructure(const Activation& activation) const = 0;
			
			virtual size_t getNbDifferentDerivations() const = 0;
			
			virtual const std::vector<Activation>& getActivations() const = 0;
			
			virtual size_t getNbIntegrase() const = 0;
			
			virtual bool isAValidAMI(char ami) const = 0;
			
			virtual const std::unordered_set<int8_t>& getSemsAfterSem(int8_t numSem) const = 0;
			
			virtual const std::unordered_set<int8_t>& getSemsBeforeSem(int8_t numSem) const = 0;
			
			auto clone() const { return std::unique_ptr<FunctionnalStructure>(clone_impl()); }
			
			virtual uint16_t getLength() const = 0;
			
			virtual uint16_t getLengthAMI(size_t i) const = 0;
			
			virtual uint16_t getNbParts() const = 0;
			
			virtual std::string getAMIWithIndex(size_t i) const = 0;
			
			/*virtual uint16_t getNbExcisions() const = 0;
			
			virtual uint16_t getNbInversions() const = 0;*/
			
			//virtual std::vector<uint16_t> getPositionsSemInAtomicAMI() const = 0;
			
			virtual bool operator== (const FunctionnalStructure& fs) const
			{ return getStructureWithIndexes() == fs.getStructureWithIndexes(); }
			
		protected:
			
			virtual FunctionnalStructure* clone_impl() const = 0;
		};
	}
}

#endif
