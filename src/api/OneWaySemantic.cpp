#include "OneWaySemantic.hpp"

using namespace std;

namespace recombinator 
{
	namespace api 
	{
		/**
		 * OneWaySemantic implementation
		 */
		const std::unordered_map<OneWaySemantic::BioFunction,std::vector<OneWaySemantic::BioFunction>,EnumClassHash> OneWaySemantic::subBioFunction ({
			{BioFunction::N,{}},
			{BioFunction::P,{BioFunction::N}},
			{BioFunction::T,{BioFunction::N}},
			{BioFunction::G,{BioFunction::N}},
			{BioFunction::GP,{BioFunction::N,BioFunction::G,BioFunction::P}},
			{BioFunction::X,{BioFunction::N,BioFunction::G,BioFunction::P}}
		});
		
		const std::unordered_map<OneWaySemantic::BioFunction, uint16_t, EnumClassHash> OneWaySemantic::minLengthBF {
			{BioFunction::G, 1000}, {BioFunction::T, 100}, {BioFunction::P, 40}, {BioFunction::X, 1040}, {BioFunction::GP, 1040}, {BioFunction::N, 0}
		};
		const std::unordered_map<OneWaySemantic::BioFunction, uint16_t, EnumClassHash> OneWaySemantic::minNbPartsBF {
			{BioFunction::G, 1}, {BioFunction::T, 1}, {BioFunction::P, 1}, {BioFunction::X, 2}, {BioFunction::GP, 2}, {BioFunction::N, 0}
		};
		
		OneWaySemantic::OneWaySemantic(BioFunction b) : _bioFunction(b) {}
		
		OneWaySemantic& OneWaySemantic::setBioFunction(OneWaySemantic::BioFunction b)
		{
			_bioFunction = b;
			
			return *this;
		}
		
		OneWaySemantic::BioFunction OneWaySemantic::getBioFunction() const
		{
			return _bioFunction;
		}
		
		/**
		 * @return bool
		 */
		bool OneWaySemantic::isP() const 
		{
			return _bioFunction == BioFunction::P || _bioFunction == BioFunction::GP;
		}
		
		/**
		 * @return bool 
		 */
		bool OneWaySemantic::isT() const 
		{
			return _bioFunction == BioFunction::T;
		}
		
		/**
		 * @return bool 
		 */
		bool OneWaySemantic::isG() const 
		{
			return _bioFunction == BioFunction::G || _bioFunction == BioFunction::GP;
		}
		
		/**
		 * @return bool 
		 */
		bool OneWaySemantic::isX() const 
		{
			return _bioFunction == BioFunction::X;
		}
		
		/**
		 * @return bool 
		 */
		bool OneWaySemantic::isN() const 
		{
			return _bioFunction == BioFunction::N;
		}
		
		/**
		 * @param s
		 * @return bool 
		 */
		bool OneWaySemantic::isEqualTo(const OneWaySemantic& s) const 
		{
			return _bioFunction == s._bioFunction;
		}
		
		/**
		 * @param s
		 * @return OneWaySemantic
		 */
		OneWaySemantic& OneWaySemantic::combine(const OneWaySemantic& s) 
		{
			if (_bioFunction == BioFunction::X)
			{
				return *this;
			}
			
			if (s._bioFunction == BioFunction::X)
			{
				_bioFunction = BioFunction::X;
				return *this;
			}
			
			switch (_bioFunction)
			{
				case BioFunction::N:
					_bioFunction = s._bioFunction;
					break;
					
				case BioFunction::P:
					if (s._bioFunction == BioFunction::T)
					{
						_bioFunction = BioFunction::T;
					}
					else if (s._bioFunction == BioFunction::G || s._bioFunction == BioFunction::GP)
					{
						_bioFunction = BioFunction::X;
					}
					break;
					
				case BioFunction::T:
					if (s._bioFunction == BioFunction::P || s._bioFunction == BioFunction::GP)
					{
						_bioFunction = BioFunction::P;
					}
					break;
					
				case BioFunction::G:
					if (s._bioFunction == BioFunction::P || s._bioFunction == BioFunction::GP)
					{
						_bioFunction = BioFunction::GP;
					}
					break;
					
				case BioFunction::GP:
					if (s._bioFunction == BioFunction::T)
					{
						_bioFunction = BioFunction::G;
					}
					else if (s._bioFunction == BioFunction::G || s._bioFunction == BioFunction::GP)
					{
						_bioFunction = BioFunction::X;
					}
					break;
				default:
					return *this;
			}
			
			return *this;
		}
		
		uint16_t OneWaySemantic::getMinLength() const
		{
			return minLengthBF.at(_bioFunction);
		}
		
		uint16_t OneWaySemantic::getMinNbParts() const
		{
			return minNbPartsBF.at(_bioFunction);
		}
		
		/**
		 * @param s1
		 * @param s2
		 * @return OneWaySemantic
		 */
		OneWaySemantic OneWaySemantic::combine(const OneWaySemantic& s1, const OneWaySemantic& s2) 
		{
			OneWaySemantic s3(s1);
			return s3.combine(s2);
		}
		
		bool OneWaySemantic::isUsefulAfter(const OneWaySemantic& ows) const
		{
			if (isN() || ows.isN())
			{
				return true;
			}
			
			if (ows.isX())
			{
				return false;
			}
			
			if (isG() && isP())
			{
				return false;
			}
			
			if (isP())
			{
				return !(ows.isP());
			}
			
			if (isT())
			{
				return ows.isP();
			}
			
			if (isG())
			{
				return ows.isP();
			}
			
			if (isX())
			{
				return (!(ows.isP()));
			}
			
			return false;
		}
		
		bool OneWaySemantic::isUsefulBefore(const OneWaySemantic& ows) const
		{
			if (isN() || ows.isN())
			{
				return true;
			}
			
			if (ows.isX())
			{
				return false;
			}
			
			if (isG() && isP())
			{
				return false;
			}
			
			if (isP())
			{
				return ows.isG();
			}
			
			if (isT())
			{
				return ows.isG();
			}
			
			if (isG())
			{
				return !(ows.isG());
			}
			
			if (isX())
			{
				return !(ows.isG());
			}
			
			return false;
		}
		
		bool OneWaySemantic::isUsefulAfterPrefix(const OneWaySemantic& ows) const
		{
			if (ows.isN())
			{
				return !(isT() || isG());
			}
			
			return isUsefulAfter(ows);
		}
		
		bool OneWaySemantic::isUsefulBeforeSuffix(const OneWaySemantic& ows) const
		{
			if (ows.isN())
			{
				return !(isP() || isT());
			}
			
			return isUsefulBefore(ows);
		}
		
		OneWaySemantic::operator string () const
		{
			switch (_bioFunction)
			{
				case OneWaySemantic::BioFunction::N:
					return string("fN");
					break;
					
				case OneWaySemantic::BioFunction::P:
					return string("fP");
					break;
					
				case OneWaySemantic::BioFunction::GP:
					return string("fGP");
					break;
					
				case OneWaySemantic::BioFunction::T:
					return string("fT");
					break;
					
				case OneWaySemantic::BioFunction::X:
					return string("fX");
					break;
					
				case OneWaySemantic::BioFunction::G:
					return string("fG");
					break;
			}
			return string();
		}
		
		std::ostream& operator<< (std::ostream& os, OneWaySemantic::BioFunction bf)
		{
			switch (bf)
			{
				case OneWaySemantic::BioFunction::N:
					os << "fN";
					break;
					
				case OneWaySemantic::BioFunction::P:
					os << "fP";
					break;
					
				case OneWaySemantic::BioFunction::GP:
					os << "fGP";
					break;
					
				case OneWaySemantic::BioFunction::T:
					os << "fT";
					break;
					
				case OneWaySemantic::BioFunction::X:
					os << "fX";
					break;
					
				case OneWaySemantic::BioFunction::G:
					os << "fG";
					break;
			}
			return os;
		}
	}
}
