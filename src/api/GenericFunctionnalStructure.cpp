#include "GenericFunctionnalStructure.hpp"
#include <algorithm> 
#include <numeric>

using namespace std;
using namespace recombinator::api;

size_t GenericFunctionnalStructure::size() const
{
	return _structure.size();
}

void GenericFunctionnalStructure::setStructure(const string& structure)
{
	// Firstly, we prepare our string in order to use a string stream 
	// The elements must be separated by a space, so we add spaces around AMIs
	string structTmp;
	
	for (auto &it : structure)
	{
		if (isAValidAMI(it))
		{
			structTmp += " "+string(1,it);
		}
		else if (it != ' ')
		{
			structTmp += it;
		}
	}
	
	// We initialize our string stream
	istringstream structStream(boost::algorithm::trim_copy(structTmp));
	
	string buffer;
	while (structStream >> buffer)
	{
		AMI a(buffer);
		
		if (a.getIntegraseType() == AMI::IntegraseType::B)
		{
			a.setNumber((uint8_t)_identifierToAMI[a.getIdentifier()].first.size());
			_identifierToAMI[a.getIdentifier()].first.push_back(a);
		}
		else
		{
			a.setNumber((uint8_t)_identifierToAMI[a.getIdentifier()].second.size());
			_identifierToAMI[a.getIdentifier()].second.push_back(a);
		}
		
		_structure.push_back(a);
		_length += a.getLength();
	}
}

bool GenericFunctionnalStructure::isAValidAMI(char ami) const
{
	return ami == (char)AMI::SiteType::FORWARD_SITE
		|| ami == (char)AMI::SiteType::REVERSE_SITE
		|| ami == (char)AMI::SiteType::USED_FORWARD_SITE
		|| ami == (char)AMI::SiteType::USED_REVERSE_SITE;
}

void GenericFunctionnalStructure::buildActivations()
{
	for (const auto &it : _identifierToAMI)
	{
		for (const auto &B : it.second.first)
		{
			for (const auto &P : it.second.second)
			{
				_activationNumberToPair.emplace_back(B, P);
			}
		}
	}
	
	std::unordered_set<ActivationNumber> remaining;
	const Structure structure = _structure;
	const Activation activation;
	generate_n(inserter(remaining, remaining.end()), _activationNumberToPair.size(), [count = 0]() mutable { return count++; });
	
	buildActivationsRec(remaining, _structure, activation);
}

void GenericFunctionnalStructure::buildActivationsRec(
	const std::unordered_set<ActivationNumber> &remaining, 
	const Structure &structure, 
	const Activation &activation)
{
	_activationsList.push_back(activation);
	
	for (auto &activationNumber : remaining)
	{
		Structure s = structure;
		Activation a = activation;
		
		a.push_back(activationNumber);
		applyActivationNumberOnStructure(s, activationNumber);
		buildActivationsRec(getRemainingActivations(remaining, s), s, a);
	}
}

void GenericFunctionnalStructure::applyActivationNumberOnStructure (
	GenericFunctionnalStructure::Structure &structure, 
	const GenericFunctionnalStructure::ActivationNumber &an) const
{
	if (const auto pos = getPositionsAMI(structure, an))
	{
		auto AMIpair = _activationNumberToPair[an];
		if (const auto reaction = AMIpair.first.getReactionType(AMIpair.second))
		{
			if (reaction == AMI::ReactionType::EXCISION)
			{
				structure.erase(structure.begin()+pos->first+1, structure.begin()+pos->second+1);
				structure[pos->first].setUsed();
			}
			else 
			{
				if (pos->first+1 < pos->second)
				{
					reverse(structure.begin()+pos->first+1, structure.begin()+pos->second);
					for_each(structure.begin()+pos->first+1, structure.begin()+pos->second, [](auto &ami){ ami.reverse(); });
				}
				structure[pos->first].setUsed();
				structure[pos->second].setUsed();
			}
		}
	}
}

boost::optional<std::pair<size_t,size_t>> GenericFunctionnalStructure::getPositionsAMI (
	const GenericFunctionnalStructure::Structure &structure, 
	const GenericFunctionnalStructure::ActivationNumber &an) const
{
	auto it = find(structure.cbegin(), structure.cend(), _activationNumberToPair[an].first);
	if (it != structure.cend())
	{
		auto it2 = find(structure.cbegin(), structure.cend(), _activationNumberToPair[an].second);
		if (it2 != structure.cend())
		{
			const auto i1 = distance(structure.cbegin(), it), i2 = distance(structure.cbegin(), it2);
			if (i1 < i2)
			{
				return boost::optional<std::pair<size_t,size_t>>(make_pair(i1, i2));
			}
			else 
			{
				return boost::optional<std::pair<size_t,size_t>>(make_pair(i2, i1));
			}
		}
	}
	
	return {};
}

bool GenericFunctionnalStructure::isAMIinStructure (
	const GenericFunctionnalStructure::Structure &structure, 
	const GenericFunctionnalStructure::AMI &a) const
{
	return find(structure.cbegin(), structure.cend(), a) != structure.cend();
}

bool GenericFunctionnalStructure::isAMIpairInStructure (
	const GenericFunctionnalStructure::Structure &structure, 
	const GenericFunctionnalStructure::ActivationNumber &an) const
{
	return isAMIinStructure(structure, _activationNumberToPair[an].first)
		&& isAMIinStructure(structure, _activationNumberToPair[an].second);
}

std::unordered_set<GenericFunctionnalStructure::ActivationNumber> GenericFunctionnalStructure::getRemainingActivations (
	const std::unordered_set<GenericFunctionnalStructure::ActivationNumber> &remaining, 
	const GenericFunctionnalStructure::Structure &structure)
{
	std::unordered_set<ActivationNumber> r;
	for (const auto &an : remaining)
	{
		if (isAMIpairInStructure(structure, an))
		{
			r.insert(an);
		}
	}
	
	return r;
}

void GenericFunctionnalStructure::buildDerivedBioDevicesStructures()
{
	SemanticalBioDeviceStructure SBDS(_structure.size()+1);
	iota(SBDS.begin(), SBDS.end(), 0);
	
	for (const auto &activation : _activationsList)
	{
		Structure s = _structure;
		SemanticalBioDeviceStructure SBDStructure = SBDS;
		
		for (const auto &an : activation)
		{
			applyActivationNumberOnSBDStructure(SBDStructure, s, an);
			applyActivationNumberOnStructure(s, an);
		}
		
		auto it = find(_derivedBioDevicesStructures.cbegin(), _derivedBioDevicesStructures.cend(), SBDStructure);
		if (it == _derivedBioDevicesStructures.cend())
		{
			_derivedBioDevicesStructures.push_back(SBDStructure);
			_derivedStructures.push_back(structureToString(s));
			it = --(_derivedBioDevicesStructures.cend());
		}
		
		const auto &pos = distance(_derivedBioDevicesStructures.cbegin(), it);
		_activationToDerBioDevStructId[activation] = pos;
	}
}

void GenericFunctionnalStructure::applyActivationNumberOnSBDStructure (
	GenericFunctionnalStructure::SemanticalBioDeviceStructure &SBDStructure, 
	const GenericFunctionnalStructure::Structure &structure, 
	const GenericFunctionnalStructure::ActivationNumber &an) const
{
	if (const auto pos = getPositionsAMI(structure, an))
	{
		auto AMIpair = _activationNumberToPair[an];
		if (const auto reaction = AMIpair.first.getReactionType(AMIpair.second))
		{
			if (reaction == AMI::ReactionType::EXCISION)
			{
				SBDStructure.erase(SBDStructure.begin()+pos->first+1, SBDStructure.begin()+pos->second+1);
			}
			else 
			{
				reverse(SBDStructure.begin()+pos->first+1, SBDStructure.begin()+pos->second+1);
				for_each(SBDStructure.begin()+pos->first+1, SBDStructure.begin()+pos->second+1, [](auto &n){ n *= -1; });
			}
		}
	}
}

void GenericFunctionnalStructure::buildSemsBeforeAndAfterSem()
{
	for (int8_t i(_structure.size()*(-1)); i <= static_cast<int8_t>(_structure.size()); ++i)
	{
		_semsAfterSem[i] = unordered_set<int8_t>();
		_semsBeforeSem[i] = unordered_set<int8_t>();
	}
	
	for (auto &der : _derivedBioDevicesStructures)
	{
		for (size_t i(0); static_cast<size_t>(i) < der.size(); ++i)
		{
			if (i > 0)
			{
				_semsBeforeSem[der[i]].insert(der[i-1]);
			}
			if (i < der.size()-1)
			{
				_semsAfterSem[der[i]].insert(der[i+1]);
			}
		}
	}
}

std::string GenericFunctionnalStructure::getStructureWithIndexes() const
{
	return structureToString(_structure);
}

std::string GenericFunctionnalStructure::getStructure() const
{
	return getStructureWithIndexes();
}

std::string GenericFunctionnalStructure::getStructureOnDerived(size_t i) const
{
	return _derivedStructures[i];
}

std::vector<std::string> GenericFunctionnalStructure::getStructuresOnDerived() const
{
	return _derivedStructures;
}

std::string GenericFunctionnalStructure::structureToString (const Structure &structure)
{
	string result;
	for (const auto &ami : structure)
	{
		result += ((string)ami)+" ";
	}
	return result;
}

const GenericFunctionnalStructure::SemBioDevStructContainer& GenericFunctionnalStructure::getDerivedBioDevicesStructures() const
{
	return _derivedBioDevicesStructures;
}

const GenericFunctionnalStructure::SemanticalBioDeviceStructure& GenericFunctionnalStructure::getDerivedBioDevicesStructure(const Activation& activation) const
{
	return _derivedBioDevicesStructures.at(_activationToDerBioDevStructId.at(activation));
}

size_t GenericFunctionnalStructure::getIndexDerivedBioDevicesStructure(const Activation& activation) const
{
	return _activationToDerBioDevStructId.at(activation);
}

size_t GenericFunctionnalStructure::getNbDifferentDerivations() const
{
	return _derivedBioDevicesStructures.size();
}

const vector<FunctionnalStructure::Activation>& GenericFunctionnalStructure::getActivations() const
{
	return _activationsList;
}

size_t GenericFunctionnalStructure::getNbIntegrase() const
{
	return _identifierToAMI.size();
}

const unordered_set<int8_t>& GenericFunctionnalStructure::getSemsAfterSem(int8_t numSem) const
{
	if (!(_semsAfterSem.find(numSem) != _semsAfterSem.end()))
	{
		throw invalid_argument("numSem is not valid in GenericFunctionnalStructure::getSemsAfterSem() !");
	}
	
	return _semsAfterSem.at(numSem);
}

const unordered_set<int8_t>& GenericFunctionnalStructure::getSemsBeforeSem(int8_t numSem) const
{
	if (!(_semsBeforeSem.find(numSem) != _semsBeforeSem.end()))
	{
		throw invalid_argument("numSem is not valid in GenericFunctionnalStructure::getSemsBeforeSem() !");
	}
	
	return _semsBeforeSem.at(numSem);
}

uint16_t GenericFunctionnalStructure::getLength() const
{
	return _length;
}

uint16_t GenericFunctionnalStructure::getNbParts() const
{
	return _structure.size();
}

uint16_t GenericFunctionnalStructure::getLengthAMI(size_t i) const
{
	if (i >= _structure.size())
	{
		throw std::out_of_range("The i in argument of GenericFunctionnalStructure::getLengthAMI() is too big");
	}
	
	return _structure[i].getLength();
}

string GenericFunctionnalStructure::getAMIWithIndex(size_t i) const
{
	return _structure[i];
}

bool ::recombinator::api::operator== (const GenericFunctionnalStructure::AMI &a, const GenericFunctionnalStructure::AMI &b)
{
	return a.equals(b);
}

boost::optional<GenericFunctionnalStructure::ActivationNumber> GenericFunctionnalStructure::getActivationNumber (const AMI &a, const AMI &b) const
{
	
	for (size_t i = 0; i < _activationNumberToPair.size(); ++i)
	{
		const auto &it = _activationNumberToPair[i];
		if ((it.first == a && it.second == b) || (it.first == b && it.second == a))
		{
			return boost::optional<GenericFunctionnalStructure::ActivationNumber>{i};
		}
	}
	
	return {};
}