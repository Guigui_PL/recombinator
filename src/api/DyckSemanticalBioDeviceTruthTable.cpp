#include "DyckSemanticalBioDeviceTruthTable.hpp"
#include <algorithm>
#include <functional>
#include <cmath>
#include <set>
#include <boost/dynamic_bitset.hpp>

using namespace std;
using namespace recombinator::api;

DyckSemanticalBioDeviceTruthTable::DyckSemanticalBioDeviceTruthTable(const DyckSemanticalBioDevice* semBioDevice) :
TruthTable(semBioDevice->getNbIntegrase()),
_semBioDevice(semBioDevice)
{
	for (uint8_t i (0); i < _semBioDevice->getNbIntegrase(); ++i)
	{
		_association[static_cast<char>(i+97)] = vector<uint8_t>{i};
		_variables.push_back(static_cast<char>(i+97));
	}
	sort(_variables.begin(),_variables.end(),greater<char>());
}

DyckSemanticalBioDeviceTruthTable::DyckSemanticalBioDeviceTruthTable(const DyckSemanticalBioDevice* semBioDevice, const std::unordered_map<char, std::vector<uint8_t>>& association) :
TruthTable(association.size()),
_semBioDevice(semBioDevice),
_association(association)
{
	if (association.size() > semBioDevice->getNbIntegrase())
	{
		throw invalid_argument("There are too many variables for this BioDevice !");
	}
	
	for (auto &v: _association)
	{
		_variables.push_back(v.first);
	}
	sort(_variables.begin(),_variables.end(),greater<char>());
}

string DyckSemanticalBioDeviceTruthTable::getDisjunctiveNormalForm() const
{
	if (_disjunctiveNormalForm.empty())
		buildMinimalDisjunctiveForm();
	
	return _disjunctiveNormalForm;
}

void DyckSemanticalBioDeviceTruthTable::buildMinimalDisjunctiveForm() const
{
	size_t nbActivations(pow(2,getNbInputs()));
	
	for (size_t i(0); i < nbActivations; ++i)
	{
		boost::dynamic_bitset<> activatedVariables(getNbInputs(), i);
		set<uint8_t> activatedIntegrases;
		
		for (uint8_t j(0); j < activatedVariables.size(); ++j)
		{
			if (activatedVariables[j])
			{
				activatedIntegrases.insert(_association.at(_variables[j]).begin(), _association.at(_variables[j]).end());
			}
		}
		
		DyckSemanticalBioDevice::Activation activation(activatedIntegrases.begin(), activatedIntegrases.end());
		
		if (_semBioDevice->getDerivedSemanticalBioDevice(activation).isX())
		{
			_disjunctiveNormalForm += "1";
		}
		else 
		{
			_disjunctiveNormalForm += "0";
		}
	}
}
