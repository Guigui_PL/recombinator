#ifndef _IEDYCKFUNCTIONNALSTRUCTURE_H
#define _IEDYCKFUNCTIONNALSTRUCTURE_H

#include "IEFunctionnalStructure.hpp"
#include "DyckFunctionnalStructure.hpp"


namespace recombinator 
{
	namespace api 
	{
		class IEDyckFunctionnalStructure : public IEFunctionnalStructure, public DyckFunctionnalStructure
		{
			
		public:
			virtual ~IEDyckFunctionnalStructure() = default;
			
			template<class T>
			friend std::unique_ptr<T> IEFunctionnalStructure::create(const std::string &structure);
			
			static std::string reverseDyckWord(std::string dyckWord);
			
			virtual std::set<std::pair<size_t, size_t>> getNPEquivalentScopes() const;
			
			virtual std::set<std::pair<size_t, size_t>> getNPEquivalentForConstraintExtension() const;
			
			virtual std::string getStructure() const;
			
		protected:
			/**
			 * The constructor is protected because, to create our object, we use virtual functions 
			 * So, to create an object of this type, use the factory : shared_ptr< IEDyckFunctionnalStructure> mystruct = create< IEDyckFunctionnalStructure>(string)
			 * See C++ core guidelines : https://github.com/isocpp/CppCoreGuidelines/blob/master/CppCoreGuidelines.md#c50-use-a-factory-function-if-you-need-virtual-behavior-during-initialization
			 */
			IEDyckFunctionnalStructure() = default;
			
			virtual void setStructure (const std::string &structure);
			
			virtual void buildActivations();
			
			virtual IEDyckFunctionnalStructure* clone_impl() const override { return new IEDyckFunctionnalStructure(*this); };     
			
			virtual bool operator== (const FunctionnalStructure& fs) const
			{ return getStructure() == fs.getStructure(); }
			
			
		};
	}
}

#endif //_IEDYCKFUNCTIONNALSTRUCTURE_H
