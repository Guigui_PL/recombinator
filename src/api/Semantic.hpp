#ifndef _SEMANTIC_H
#define _SEMANTIC_H
#include <array>
#include <exception>
#include <string>
#include <boost/functional/hash.hpp>
#include "OneWaySemantic.hpp"

namespace recombinator 
{
	namespace api 
	{
		struct SemanticHash;
		
		class Semantic 
		{
		public: 
			typedef OneWaySemantic::BioFunction BF;
			
			
			Semantic() = default;
			Semantic(const OneWaySemantic &forward, const OneWaySemantic &reverse);
			Semantic(OneWaySemantic::BioFunction forward, OneWaySemantic::BioFunction reverse);
			
			Semantic(const std::string &biologicalParts);
    
			Semantic& operator+= (const Semantic& s);
			
			Semantic& excise();
			
			Semantic& reverse();
			
			bool isPF() const;
			
			bool isTF() const;
			
			bool isGF() const;
			
			bool isX() const;
			
			bool isXF() const;
			
			bool isXR() const;
			
			bool isGR() const;
			
			bool isTR() const;
			
			bool isPR() const;
			
			bool isN() const;
			
			bool isMirror() const;
			
			uint16_t getMinLength() const;
			
			uint16_t getMinNbParts() const;
			
			/**
			 * @param s
			 */
			bool isEqualTo(const Semantic& s) const;
			
			/**
			 * @param Semantic s
			 */
			Semantic& combine(const Semantic& s);
			
			/**
			 * @param Semantic s1
			 * @param Semantic s2
			 */
			static Semantic combine(const Semantic& s1, const Semantic& s2);
			
			const OneWaySemantic& getSemanticForward() const;
			
			const OneWaySemantic& getSemanticReverse() const;
			
			uint8_t getSemanticKey() const;
			
			static const Semantic* getPointerWithKey(uint8_t key);
			
			bool isAnInversionOf(const Semantic& s) const;
			
			class InvalidSemanticArgument : public std::invalid_argument 
			{
			public:
				InvalidSemanticArgument(const std::string &s) : std::invalid_argument(s) {}
			};
			
			enum class Utility : uint8_t
			{
				USELESS = 0, // 00
				UTILITY_FORWARD = 1, // 01
				UTILITY_REVERSE = 2, // 10
				UTILITY_REVERSE_AND_FORWARD = 3, // 11
			};
			
			Utility isUsefulAfter(const Semantic& s) const;
			Utility isUsefulBefore(const Semantic& s) const;
			Utility isUsefulAfterPrefix(const Semantic& s) const;
			Utility isUsefulBeforeSuffix(const Semantic& s) const;
			
			operator std::string () const;
			
			static const std::array<Semantic,26> elementarySemantics;
			static const std::array<Semantic,28> allSemantics;
			static const std::unordered_map<Semantic,std::string,SemanticHash> elementaryToString;
			static const std::array<std::array<const Semantic*,6>,6> bioFunctionToPointer;
			static const std::unordered_map<Semantic,std::vector<Semantic>,SemanticHash> subSemantic;
		
		private: 
			OneWaySemantic _forward = OneWaySemantic();
			OneWaySemantic _reverse = OneWaySemantic();
			
			static std::array<std::array<const Semantic*,6>,6> buildBioFunctionToPointer();
			static std::unordered_map<Semantic,std::vector<Semantic>,SemanticHash> buildSubSemantic();
			static std::unordered_map<Semantic,std::string,SemanticHash> buildElementaryToString();
		};
		
		inline Semantic operator+ (const Semantic& s1, const Semantic& s2) 
		{
			return Semantic::combine(s1,s2);
		}
		
		inline bool operator== (const Semantic& s1, const Semantic& s2) 
		{
			return s1.isEqualTo(s2);
		}
		
		inline bool operator!= (const Semantic& s1, const Semantic& s2) 
		{
			return !s1.isEqualTo(s2);
		}
		
		inline bool operator< (const Semantic& s1, const Semantic& s2) 
		{
			return s1.getSemanticKey() < s2.getSemanticKey();
		}
		
		struct SemanticHash
		{
			template <typename T>
			std::size_t operator()(T t) const
			{
				return t.getSemanticKey();
			}
		};
		
		std::ostream& operator<< (std::ostream& os, Semantic s);
		size_t hash_value(const ::recombinator::api::Semantic &s);
	}
}

#endif //_SEMANTIC_H
