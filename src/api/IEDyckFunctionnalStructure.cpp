#include "IEDyckFunctionnalStructure.hpp"
#include <functional>
#include <set>

using namespace std;
using namespace recombinator::api;

/**
 * DyckFunctionnalStructure implementation
 */

void IEDyckFunctionnalStructure::setStructure(const string &structure)
{
	// For a structure in a dyck word form, the indexes are not mandatory
	// So, if there is no indexes, we must add them
	bool hasIndexes = false;
	
	for (auto &c : structure)
	{
		if (!isAValidAMI(c))
		{
			hasIndexes = true;
			break;
		}
	}
	
	if (hasIndexes)
	{
		IEFunctionnalStructure::setStructure(structure);
	}
	else // We must add indexes before calling setStructure 
	{
		string structWithIndexes;
		set<size_t,greater<size_t>> idOpened;
		uint8_t index = 0;
		for (size_t i(0); i < structure.size(); ++i)
		{
			if (structure[i] == static_cast<char>(AMI::INVERSION_OPENING) || structure[i] == static_cast<char>(AMI::EXCISION_OPENING))
			{
				idOpened.insert(index);
				structWithIndexes += structure[i] + to_string(index++);
			}
			else 
			{
				structWithIndexes += structure[i] + to_string(*(idOpened.begin()));
				idOpened.erase(idOpened.begin());
			}
		}
		IEFunctionnalStructure::setStructure(structWithIndexes);
	}
	
	// Now, we must check if the structure is a dyck word
	// Otherwise, we throw an exception
	set<size_t,greater<size_t>> idOpened;
	for (size_t i(0); i < _structure.size(); ++i)
	{
		if (_structure[i] == AMI::INVERSION_OPENING || _structure[i] == AMI::EXCISION_OPENING)
		{
			idOpened.insert(_structureIndexes[i]);
		}
		else 
		{
			if (_structureIndexes[i] != *(idOpened.begin()))
			{
				throw invalid_argument("The structure is a not a dyck word in IEDyckFunctionnalStructure!");
			}
			idOpened.erase(idOpened.begin());
		}
	}
}

string IEDyckFunctionnalStructure::getStructure() const
{
	string result;
	for (size_t i(0); i < _structure.size(); ++i)
	{
		switch (_structure[i])
		{
			case AMI::INVERSION_OPENING:   
				result.push_back('(');
				break;
				
			case AMI::INVERSION_CLOSING:
				result.push_back(')');
				break;
				
			case AMI::EXCISION_OPENING:
				result.push_back('[');
				break;
				
			case AMI::EXCISION_CLOSING:
				result.push_back(']');
				break;
		}
	}
	
	return result;
}

void IEDyckFunctionnalStructure::buildActivations()
{
	// n : number of AMIs
	// Number of activations : 2^n
	// First step : we create a powerset of {0,1,...,n-1}
	set<uint8_t> activationSet;
	for (uint8_t i(0); i < getNbIntegrase(); ++i)
	{
		activationSet.insert(i);
	}
	
	set<set<uint8_t>> powerSet;
	powerSet.emplace(); // Adding empty set
	for (auto&& activation: activationSet) 
	{
		set<set<uint8_t>> tmp;
		for (auto x: powerSet) 
		{
			x.insert(activation);
			tmp.insert(x);
		}
		powerSet.insert(begin(tmp), end(tmp));
	}
	
	// Second step : we transform our set of sets in vector of vectors
	for (auto &set : powerSet)
	{
		_activationsList.push_back(vector<uint8_t>(set.begin(), set.end()));
	}
}

std::string IEDyckFunctionnalStructure::reverseDyckWord(std::string dyckWord)
{
	string dyckWordReverse;
	for (auto it = dyckWord.rbegin(); it != dyckWord.rend(); ++it)
	{
		switch (*it)
		{
			case '(': dyckWordReverse.push_back(')'); break;
			case ')': dyckWordReverse.push_back('('); break;
			case '[': dyckWordReverse.push_back(']'); break;
			case ']': dyckWordReverse.push_back('['); break;
		}
	}
	
	return dyckWordReverse; 
}

std::set<std::pair<size_t, size_t>> IEDyckFunctionnalStructure::getNPEquivalentScopes() const
{
	string structure = getStructure();
	std::set<std::pair<size_t, size_t>> pairs;
	
	for (auto &it : _indexPositions)
	{
		if (_structure[it.first] == AMI::INVERSION_OPENING)
		{
			string substring = structure.substr(it.first,it.second-it.first+1);
			if (substring == reverseDyckWord(substring))
			{
				pairs.insert(make_pair(it.first+1,it.second));
			}
		}
	}
	
	return pairs;
}

std::set<std::pair<size_t, size_t>> IEDyckFunctionnalStructure::getNPEquivalentForConstraintExtension() const
{
	std::set<std::pair<size_t, size_t>> pairs;
	
	for (size_t i = 0; i < size(); ++i)
	{
		std::vector<std::vector<bool>> varsInTheSameColumn (2, std::vector<bool>(size()+1, false));
		
		for (auto &it : getDerivedBioDevicesStructures())
		{
			if (it.size() == size()+1)
			{
				if (it[i] > 0)
				{
					varsInTheSameColumn[0][it[i]] = true;
				}
				else
				{
					varsInTheSameColumn[1][abs(it[i])] = true;
				}
			}
		}
		
		if (varsInTheSameColumn[0][i] && varsInTheSameColumn[1][i])
		{
			for (size_t j = 1; j < size(); ++j)
			{
				if (j != i && varsInTheSameColumn[0][j] && varsInTheSameColumn[1][j]
					&& pairs.find(make_pair(j,i)) == pairs.end()
				)
				{
					pairs.insert(make_pair(i,j));
				}
			}
		}
	}
	
	return pairs;
}