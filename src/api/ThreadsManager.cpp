/**
 * Copyright (C) Inserm Montpellier / CBS
 * 
 * U 1054 - mixte avec Université de Montpellier et CNRS
 * Centre de Biochimie Structurale
 * Directeur : Pierre-Emmanuel Milhiet
 * 29, rue de Navacelles
 * 34090 MONTPELLIER
 * 
 * Recombinator project : http://recombinator.lirmm.fr/
 * 
 * Team : Synthetic biology at CBS : http://www.cbs.cnrs.fr/index.php/en/research-equipea4
 * 
 * 
 * @auhtor : Guillaume Pérution-Kihli 
 */

#include "ThreadsManager.hpp"
#include <chrono>
#include <unordered_set>

using namespace recombinator::api;
using namespace std;

/**
 * ThreadsManager implementation
 */

size_t ThreadsManager::_limitNbLaunchedThreads = static_cast<size_t>(-1);
ThreadsManager ThreadsManager::_instance;

bool ThreadsManager::canLaunchNewThread() const
{
	std::lock_guard<std::mutex> lock(_lockThreadsManager);
	return _limitNbLaunchedThreads >= _launchedThreads.size();
}

void ThreadsManager::updateLaunchedThreads()
{
	std::lock_guard<std::mutex> lock(_lockThreadsManager);
	unordered_set<ThreadId> toRemove;
	for (auto &t : _launchedThreads)
	{
		if (!t.second.joinable())
		{
			toRemove.insert(t.first);
		}
	}
	
	for (auto &id : toRemove)
	{
		_launchedThreads.erase(id);
	}
}

void ThreadsManager::wait(ThreadsManager::ThreadId id)
{
	std::lock_guard<std::mutex> lock(_lockWaiting[id]);
	if (_launchedThreads.find(id) != _launchedThreads.end() && _launchedThreads[id].joinable())
	{
		_launchedThreads[id].join();
	}
}

ThreadsManager& ThreadsManager::getThreadsManager()
{
	return _instance;
}

ThreadsManager::ThreadId ThreadsManager::getNextId()
{
	std::lock_guard<std::mutex> lock(_lockThreadsManager);
	return _nextId++;
}

