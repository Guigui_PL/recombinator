#ifndef _IEFUNCTIONNALSTRUCTURE_H
#define _IEFUNCTIONNALSTRUCTURE_H

#include <memory>
#include <unordered_map>
#include <unordered_set>
#include <boost/functional/hash/extensions.hpp>
#include "EnumClassHash.hpp"
#include "FunctionnalStructure.hpp"

namespace recombinator 
{
	namespace api 
	{
		class IEFunctionnalStructure : virtual public FunctionnalStructure
		{
		public:
			enum class AMI : char
			{
				INVERSION_OPENING = '(',
				INVERSION_CLOSING = ')',
				EXCISION_OPENING = '[',
				EXCISION_CLOSING = ']'
			}; /**< Activation's Mark of Integrase */
			
			static const std::unordered_map<AMI, uint16_t, EnumClassHash> lengthAMI;
			
			typedef std::vector<AMI> Structure;
			
			/**
			 * @param string
			 */
			
			template<class T>
			static std::unique_ptr<T> create(const std::string &structure)
			{
				struct make_shared_enabler : public T {};
				auto p = std::make_unique<make_shared_enabler>();
				p->setStructure(structure);
				p->buildActivations();
				p->buildDerivedBioDevicesStructures();
				p->buildSemsBeforeAndAfterSem();
				return p;
			}
			
			virtual ~IEFunctionnalStructure() = default;
			
			static std::string structureReverse(std::string structure);
			
			virtual size_t size() const;
			
			virtual std::string getStructureWithIndexes() const;
			
			virtual std::string getStructure() const;
			
			virtual std::string getStructureOnDerived(size_t i) const;
			
			virtual std::vector<std::string> getStructuresOnDerived() const;
			
			virtual const SemBioDevStructContainer& getDerivedBioDevicesStructures() const;
			
			/**
			 * @param Activation
			 */
			virtual const SemanticalBioDeviceStructure& getDerivedBioDevicesStructure(const Activation& activation) const;
			
			virtual size_t getIndexDerivedBioDevicesStructure(const Activation& activation) const;
			
			virtual size_t getNbDifferentDerivations() const;
			
			virtual const std::vector<Activation>& getActivations() const;
			
			virtual bool isAValidAMI(char ami) const;
			
			virtual size_t getNbIntegrase() const;
			
			virtual const std::unordered_set<int8_t>& getSemsAfterSem(int8_t numSem) const;
			
			virtual const std::unordered_set<int8_t>& getSemsBeforeSem(int8_t numSem) const;
			
			virtual uint16_t getLength() const;
			
			virtual uint16_t getLengthAMI(size_t i) const;
			
			virtual uint16_t getNbParts() const;
			
			virtual uint16_t getNbExcisions() const;
			
			virtual uint16_t getNbInversions() const;
			
			virtual std::string getAMIWithIndex(size_t i) const;
			
			virtual std::vector<uint16_t> getPositionsSemInAtomicAMI() const;
			
			virtual std::vector<uint16_t> getPositionsSemInAtomicExcisions() const;
			
			virtual std::vector<uint16_t> getPositionsSemInAtomicInversions() const;
			
			virtual bool isInExcision(size_t i) const;
			
			virtual bool isInInversion(size_t i) const;
			
		protected:
			
			Structure _structure;
			std::vector<uint8_t> _structureIndexes;
			uint16_t _length = 0;
			uint16_t _nbExcisions = 0;
			uint16_t _nbInversions = 0;
			std::vector<std::pair<size_t,size_t>> _indexPositions;
			SemBioDevStructContainer _derivedBioDevicesStructures;
			std::vector<std::string> _derivedStructures;
			std::unordered_map<Activation, size_t, boost::hash<Activation>> _activationToDerBioDevStructId;
			std::vector<Activation> _activationsList;
			std::unordered_map<int8_t,std::unordered_set<int8_t>> _semsBeforeSem;
			std::unordered_map<int8_t,std::unordered_set<int8_t>> _semsAfterSem;
			
			/**
			 * The constructor is protected because, to create our object, we use virtual functions 
			 * So, to create an object of this type, use the factory : shared_ptr< IEFunctionnalStructure> mystruct = create< IEFunctionnalStructure>(string)
			 * See C++ core guidelines : https://github.com/isocpp/CppCoreGuidelines/blob/master/CppCoreGuidelines.md#c50-use-a-factory-function-if-you-need-virtual-behavior-during-initialization
			 */
			IEFunctionnalStructure() = default;
			
			/**
			 * @param string
			 */
			virtual void setStructure (const std::string &structure);
			
			virtual void buildActivations();
			
			virtual void buildDerivedBioDevicesStructures();
			
			virtual void buildSemsBeforeAndAfterSem();
			
			virtual IEFunctionnalStructure* clone_impl() const override { return new IEFunctionnalStructure(*this); };     
		};
	}
}

#endif //_IEFUNCTIONNALSTRUCTURE_H
