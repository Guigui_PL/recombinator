#ifndef _DYCKSEMANTICALBIODEVICETRUTHTABLE_H
#define _DYCKSEMANTICALBIODEVICETRUTHTABLE_H
#include <unordered_map>
#include <vector>
#include "TruthTable.hpp"
#include "DyckSemanticalBioDevice.hpp"

namespace recombinator 
{
	namespace api 
	{
		class DyckSemanticalBioDeviceTruthTable : virtual public TruthTable 
		{
		public:
			DyckSemanticalBioDeviceTruthTable(const DyckSemanticalBioDevice* semBioDevice);
			DyckSemanticalBioDeviceTruthTable(const DyckSemanticalBioDevice* semBioDevice, const std::unordered_map<char, std::vector<uint8_t>>& association);
			
			virtual std::string getDisjunctiveNormalForm() const;
			
		protected:
			virtual void buildMinimalDisjunctiveForm() const;
			
			const DyckSemanticalBioDevice* _semBioDevice;
			std::unordered_map<char, std::vector<uint8_t>> _association;
			std::vector<char> _variables;
			mutable std::string _disjunctiveNormalForm;
		};
	}
}

#endif 
