#ifndef _DYCKFUNCTIONNALSTRUCTURE_H
#define _DYCKFUNCTIONNALSTRUCTURE_H

#include "FunctionnalStructure.hpp"

namespace recombinator 
{
	namespace api 
	{
		class DyckFunctionnalStructure : virtual public FunctionnalStructure
		{
		public:
			virtual ~DyckFunctionnalStructure() = default;
			
		};
	}
}
#endif //_DYCKFUNCTIONNALSTRUCTURE_H
