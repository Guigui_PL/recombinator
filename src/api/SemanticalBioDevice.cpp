#include "SemanticalBioDevice.hpp"
#include "IEFunctionnalStructure.hpp"
#include <algorithm>
#include <iostream>
#include <iomanip>
#include <regex>
#include <sstream>
#include <stdexcept>
#include <boost/algorithm/string/trim.hpp>
#include <boost/functional/hash/extensions.hpp>

using namespace std;

namespace recombinator
{
	namespace api
	{
		
		/**
		 * SemanticalBioDevice
		 */
		SemanticalBioDevice::SemanticalBioDevice() : 
		SemanticalBioDevice(*IEFunctionnalStructure::create<IEFunctionnalStructure>(""), {&Semantic::allSemantics[0]})
		{}
		
		SemanticalBioDevice::SemanticalBioDevice(const FunctionnalStructure &structure, std::vector<const Semantic*> semantics) :
		_structure(structure.clone()),
		_semantics(semantics)
		{
			for (auto &sem : _semantics)
			{
				if (sem != nullptr)
				{
					++_nbDefinedSemantics;
				}
			}
			
			if (_semantics.size() && _structure->size()+1 != _semantics.size())
			{
				throw std::invalid_argument("There must be "+to_string(_structure->size()+1)+" semantics, not "+to_string(_semantics.size()));
			}
			else if (_semantics.empty())
			{
				_semantics = vector<const Semantic*> (_structure->size()+1, nullptr);
			}
			
			buildDerivedSemanticalBioDevices();
			
			_prefixes.resize(getNbDerivedSemanticalBioDevice(), std::vector<const Semantic*>(_semantics.size(),nullptr));
			_suffixes.resize(getNbDerivedSemanticalBioDevice(), std::vector<const Semantic*>(_semantics.size(),nullptr));
		}
		
		SemanticalBioDevice::SemanticalBioDevice(const SemanticalBioDevice &bioDevice) : 
		_structure(bioDevice._structure->clone()),
		_semantics(bioDevice._semantics), 
		_nbDefinedSemantics(bioDevice._nbDefinedSemantics),
		_prefixes(bioDevice._prefixes),
		_suffixes(bioDevice._suffixes) 
		{}
		
		SemanticalBioDevice& SemanticalBioDevice::operator= (const SemanticalBioDevice &bioDevice)
		{
			if (&bioDevice != this)
			{
				_semantics = bioDevice._semantics; 
				_prefixes = bioDevice._prefixes; 
				_suffixes = bioDevice._suffixes; 
				_structure = bioDevice._structure->clone();
				_nbDefinedSemantics = bioDevice._nbDefinedSemantics;
				buildDerivedSemanticalBioDevices();
			}
			return *this;
		}
		
		SemanticalBioDevice::SemanticalBioDevice(const std::string &bioDevice)
		{
			// Pattern to detect a site 
			regex sitePattern ("^(?:S[RF]|\\[|\\]|\\(|\\))(\\w)$");
			
			// We use an istringstream to browse the string 
			istringstream bioParts;
			bioParts.str(boost::algorithm::trim_copy(bioDevice));
			string bioPart;
			Semantic sem;
			vector<pair<string,uint8_t>> sites;
			unordered_map<uint8_t,pair<string,string>> sitePairs;
			
			while (bioParts >> bioPart)
			{
				boost::algorithm::trim(bioPart);
				
				// If the bioPart is a site
				if (regex_match(bioPart, sitePattern))
				{
					// We add the semantic before the site to our vector of semantics
					_semantics.push_back(
						Semantic::bioFunctionToPointer
							[static_cast<size_t>(sem.getSemanticForward().getBioFunction())]
							[static_cast<size_t>(sem.getSemanticReverse().getBioFunction())]);
					sem = Semantic();
					
					char name;
					
					if (bioPart.size() == 2)
					{
						name = bioPart[1];
					}
					else if (bioPart.size() == 3)
					{
						name = bioPart[2];
					}
					else
					{
						throw invalid_argument("There is an invalid site!");
					}
					
					// We add the site to our table of sites (without its letter)
					if (isupper(name))
					{
						name -= 65;
					}
					else if (islower(name))
					{
						name -= 97;
					}
					uint8_t numSite(name);
					sites.push_back(make_pair(bioPart,numSite));
					bioPart.pop_back();
					if (sitePairs.find(numSite) != sitePairs.end())
					{
						sitePairs[numSite].second = bioPart;
					}
					else 
					{
						sitePairs[numSite] = make_pair(bioPart, "");
					}
				}
				else
				{
					// We add its semantic to intersite semantic
					Semantic semBioPart(bioPart);
					sem += semBioPart;
				}
			}
			
			// Adding the last semantic
			_semantics.push_back(
				Semantic::bioFunctionToPointer
					[static_cast<size_t>(sem.getSemanticForward().getBioFunction())]
					[static_cast<size_t>(sem.getSemanticReverse().getBioFunction())]);
			
			// Update _nbDefinedSemantics
			_nbDefinedSemantics = _semantics.size();
			
			// Now, we build our structure
			string structure;
			unordered_set<uint8_t> openedSites;
			
			for (auto &s : sites)
			{
				uint8_t numSite(s.second);
				
				// If the other site of the pair doesn't exist
				if (sitePairs[numSite].first == "" || sitePairs[numSite].second == "")
				{
					throw invalid_argument("There is an invalid site!");
				}
				
				if (sitePairs[numSite].first == "(" && sitePairs[numSite].second == ")")
				{
					if (openedSites.find(numSite) != openedSites.end())
					{
						structure += string(")")+to_string(numSite);
					}
					else 
					{
						openedSites.insert(numSite);
						structure += string("(")+to_string(numSite);
					}
				}
				else if (sitePairs[numSite].first == "[" && sitePairs[numSite].second == "]")
				{
					if (openedSites.find(numSite) != openedSites.end())
					{
						structure += string("]")+to_string(numSite);
					}
					else 
					{
						openedSites.insert(numSite);
						structure += string("[")+to_string(numSite);
					}
				}
				// If the two sites are the same, it's an inversion
				else if (sitePairs[numSite].first != sitePairs[numSite].second)
				{
					if (openedSites.find(numSite) != openedSites.end())
					{
						structure += string(")")+to_string(numSite);
					}
					else 
					{
						openedSites.insert(numSite);
						structure += string("(")+to_string(numSite);
					}
				}
				else
				{
					if (openedSites.find(numSite) != openedSites.end())
					{
						structure += string("]")+to_string(numSite);
					}
					else 
					{
						openedSites.insert(numSite);
						structure += string("[")+to_string(numSite);
					}
				}
			}
			
			_structure = IEFunctionnalStructure::create<IEFunctionnalStructure>(structure);
			
			if (_structure->size()+1 != _semantics.size())
			{
				throw std::invalid_argument("There must be "+to_string(_structure->size()+1)+" semantics, not "+to_string(_semantics.size()));
			}
			
			_prefixes.resize(getNbDerivedSemanticalBioDevice(), std::vector<const Semantic*>(_semantics.size(),nullptr));
			_suffixes.resize(getNbDerivedSemanticalBioDevice(), std::vector<const Semantic*>(_semantics.size(),nullptr));
			
			buildDerivedSemanticalBioDevices();
		}
		
		SemanticalBioDevice SemanticalBioDevice::getSymmetric() const
		{
			auto structure = IEFunctionnalStructure::create<IEFunctionnalStructure>(IEFunctionnalStructure::structureReverse(_structure->getStructureWithIndexes()));
			SemanticalBioDevice sym(*structure);
			
			for (size_t i = 0; i < getNbSemantics(); ++i)
			{
				sym.setSemantic(i, Semantic::bioFunctionToPointer
							[static_cast<size_t>(getSemantic(i).getSemanticReverse().getBioFunction())]
							[static_cast<size_t>(getSemantic(i).getSemanticForward().getBioFunction())]);
			}
			
			return sym;
		}
		
		const FunctionnalStructure& SemanticalBioDevice::getStructure() const
		{
			return *_structure;
		}
		
		size_t SemanticalBioDevice::size() const
		{
			return getStructure().size();
		}
		
		void SemanticalBioDevice::setSemantic(size_t index, const Semantic* semantic)
		{
			if (index >= _semantics.size())
			{
				throw std::out_of_range("The index in argument of SemanticalBioDevice::setSemantic() is too big");
			}
			
			if (_semantics[index] == nullptr && semantic != nullptr)
			{
				++_nbDefinedSemantics;
			}
			else if (_semantics[index] != nullptr && semantic == nullptr)
			{
				--_nbDefinedSemantics;
			}
			
			for (size_t i{0}; i < getNbDerivedSemanticalBioDevice(); ++i)
			{
				if (getDerivedSemanticalBioDevice(i).isIn(index))
				{
					size_t pos = getDerivedSemanticalBioDevice(i).getVarPos(index);
					const auto& structure = getStructure().getDerivedBioDevicesStructures()[i];
					
					for (size_t j{pos}; j < structure.size() && _prefixes[i][abs(structure[j])] != nullptr; ++j)
					{
						_prefixes[i][abs(structure[j])] = nullptr;
					}
					
					for (int j{(int)pos}; j >= 0 && _suffixes[i][abs(structure[j])] != nullptr; --j)
					{
						_suffixes[i][abs(structure[j])] = nullptr;
					}
				}
			}
			
			_semantics[index] = semantic;
		}
		
		void SemanticalBioDevice::setSemantics(std::vector<const Semantic*> semantics)
		{
			_semantics = semantics;
			_nbDefinedSemantics = 0;
			_prefixes = std::vector<std::vector<const Semantic*>>(getNbDerivedSemanticalBioDevice(), std::vector<const Semantic*>(_semantics.size(),nullptr));
			_suffixes = std::vector<std::vector<const Semantic*>>(getNbDerivedSemanticalBioDevice(), std::vector<const Semantic*>(_semantics.size(),nullptr));
			
			for (auto &sem : _semantics)
			{
				if (sem != nullptr)
				{
					++_nbDefinedSemantics;
				}
			}
			
			if (_semantics.size() && _structure->size()+1 != _semantics.size())
			{
				throw std::invalid_argument("There must be "+to_string(_structure->size()+1)+" semantics, not "+to_string(_semantics.size()));
			}
			else if (_semantics.empty())
			{
				_semantics = vector<const Semantic*> (_structure->size()+1, nullptr);
			}
		}
		
		const Semantic& SemanticalBioDevice::getSemantic (size_t index) const
		{
			if (index >= _semantics.size())
			{
				throw std::out_of_range("The index in argument of SemanticalBioDevice::getSemantic() is too big");
			}
			
			return *_semantics[index];
		}
		
		const Semantic& SemanticalBioDevice::getPrefix(size_t derived, size_t var) const
		{
			if (var >= _semantics.size())
			{
				throw std::out_of_range("The index in argument of SemanticalBioDevice::getPrefix() is too big");
			}
			
			if (_prefixes[derived][var] == nullptr)
			{
				Semantic s;
				if (var > 0)
				{
					size_t precVar = abs(getStructure().getDerivedBioDevicesStructures()[derived]
						[getDerivedSemanticalBioDevice(derived).getVarPos(var)-1]);
					s = getPrefix(derived, precVar);
					Semantic toAdd = *(_semantics[precVar]);
					if (getDerivedSemanticalBioDevice(derived).isReversed(precVar))
					{
						s += toAdd.reverse();
					}
					else
					{
						s += toAdd;
					}
				}
				
				_prefixes[derived][var] = Semantic::bioFunctionToPointer
							[static_cast<size_t>(s.getSemanticForward().getBioFunction())]
							[static_cast<size_t>(s.getSemanticReverse().getBioFunction())];
			}
			
			return *_prefixes[derived][var];
		}
		
		const Semantic& SemanticalBioDevice::getSuffix(size_t derived, size_t var) const
		{
			if (var >= _semantics.size())
			{
				throw std::out_of_range("The index in argument of SemanticalBioDevice::getSemantic() is too big");
			}
			
			if (_suffixes[derived][var] == nullptr)
			{
				Semantic s;
				if (var < _semantics.size()-1)
				{
					size_t nextVar = abs(getStructure().getDerivedBioDevicesStructures()[derived]
						[getDerivedSemanticalBioDevice(derived).getVarPos(var)+1]);
					s = getSuffix(derived, nextVar);
					Semantic toAdd = *(_semantics[nextVar]);
					if (getDerivedSemanticalBioDevice(derived).isReversed(nextVar))
					{
						s = toAdd.reverse() + s;
					}
					else
					{
						s = toAdd + s;
					}
				}
				
				
				_suffixes[derived][var] = Semantic::bioFunctionToPointer
							[static_cast<size_t>(s.getSemanticForward().getBioFunction())]
							[static_cast<size_t>(s.getSemanticReverse().getBioFunction())];
			}
			
			return *_suffixes[derived][var];
		}
		
		const std::vector<const Semantic*> &SemanticalBioDevice::getSemantics() const
		{
			return _semantics;
		}
		
		size_t SemanticalBioDevice::getNbSemantics() const
		{
			return _semantics.size();
		}
		
		const Semantic& SemanticalBioDevice::getSemantic(size_t indexDerivedSemanticalBioDevice, size_t index) const
		{
			return (*this)[indexDerivedSemanticalBioDevice][index];
		}
		
		size_t SemanticalBioDevice::getNbDerivedSemanticalBioDevice() const
		{
			if (_derBioDevices.empty())
				buildDerivedSemanticalBioDevices();
			return _derBioDevices.size();
		}
			
		const DerivedSemanticalBioDevice& SemanticalBioDevice::getDerivedSemanticalBioDevice(size_t i) const
		{
			if (_derBioDevices.empty())
				buildDerivedSemanticalBioDevices();
			
			if (i >= _derBioDevices.size())
			{
				throw std::out_of_range("The i in argument of SemanticalBioDevice::getDerivedSemanticalBioDevice() is too big");
			}
			
			return _derBioDevices[i];
		}
			
		const std::vector<SemanticalBioDevice::Activation>& SemanticalBioDevice::getActivations() const
		{
			return _structure->getActivations();
		}
		
		const DerivedSemanticalBioDevice& SemanticalBioDevice::getDerivedSemanticalBioDevice(const Activation& activation) const
		{
			if (_derBioDevices.empty())
				buildDerivedSemanticalBioDevices();
			
			return _derBioDevices.at(_structure->getIndexDerivedBioDevicesStructure(activation));
		}
		
		const DerivedSemanticalBioDevice& SemanticalBioDevice::operator[](size_t i) const
		{
			return getDerivedSemanticalBioDevice(i);
		}
		
		bool SemanticalBioDevice::isDefinedSemantic(size_t index) const
		{
			if (index >= _semantics.size())
			{
				throw std::out_of_range("The index in argument of SemanticalBioDevice::isDefinedSemantic() is too big");
			}
			
			return _semantics[index] != nullptr;
		}
		
		bool SemanticalBioDevice::isAllSemanticsDefined() const
		{
			return _nbDefinedSemantics == _semantics.size();
		}
		
		size_t SemanticalBioDevice::getNbDefinedSemantics() const
		{
			return _nbDefinedSemantics;
		}
		
		bool SemanticalBioDevice::hasGenesAtEnds() const
		{
			for (size_t i(1); i < _semantics.size()-1; ++i)
			{
				if (_semantics[i]->isGF() || _semantics[i]->isGR())
				{
					return false;
				}
			}
			
			return (_semantics.front()->isGR() && !_semantics.front()->isPF() && !(_semantics.back()->isGF() && _semantics.back()->isPR()))
			|| (_semantics.back()->isGF() && !_semantics.back()->isPR() && !(_semantics.front()->isGR() && _semantics.front()->isPF()));
		}
		
		bool SemanticalBioDevice::isRespectedWeakConstraint() const
		{
			for (auto &derBioDevice : _derBioDevices)
			{
				if (derBioDevice.isX())
				{
					bool hasPF (false), hasValidX (false);
					for (size_t i(0); i < derBioDevice.size(); ++i)
					{
						if (derBioDevice[i].isPF() && !hasPF)
						{
							hasPF = true;
						}
						else if (hasPF && (derBioDevice[i].isPR() || derBioDevice[i].isTF()))
						{
							hasPF = false;
						}
						else if ((hasPF && derBioDevice[i].isGF()) || derBioDevice[i].isXF())
						{
							hasPF = false;
							hasValidX = true;
							for (size_t j(i+1); j < derBioDevice.size(); ++j)
							{
								if (derBioDevice[j].isPR())
								{
									hasValidX = false;
									break;
								}
								if (derBioDevice[j].isGR() || derBioDevice[j].isTR())
								{
									break;
								}
							}
							
							if (hasValidX)
							{
								break;
							}
						}
					}
					
					if (hasValidX)
					{
						continue;
					}
					
					bool hasPR (false);
					for (int i(derBioDevice.size()-1); i >= 0; --i)
					{
						if (derBioDevice[i].isPR() && !hasPR)
						{
							hasPR = true;
						}
						else if (hasPR && (derBioDevice[i].isPF() || derBioDevice[i].isTR()))
						{
							hasPR = false;
						}
						else if ((hasPR && derBioDevice[i].isGR()) || derBioDevice[i].isXR())
						{
							hasPR = false;
							hasValidX = true;
							for (int j(i-1); j >= 0; --j)
							{
								if (derBioDevice[j].isPF())
								{
									hasValidX = false;
									break;
								}
								if (derBioDevice[j].isGF() || derBioDevice[j].isTF())
								{
									break;
								}
							}
							
							if (hasValidX)
							{
								break;
							}
						}
					}
					
					if (!hasValidX)
					{
						return false;
					}
				}
			}
			
			return true;
		}
		
		bool SemanticalBioDevice::isRespectedStrongConstraint() const
		{
			for (auto &derBioDevices : _derBioDevices)
			{
				bool isPF(false);
				for (const auto &sem : derBioDevices)
				{
					if (isPF && (sem.isPR() || sem.isXR()))
					{
						return false;
					}
					
					isPF = isPF || sem.isPF() || sem.isXF();
				}
			}
			
			return true;
		}
		
		bool SemanticalBioDevice::isReducible() const
		{
			// First step : we check which DerivedSemanticalBioDevice is expressed or not
			// We use a vector<uint8_t> instead of vector<bool> because the performances of 
			// the first is better
			unordered_map<Activation,uint8_t, boost::hash<Activation>> expDerBioDevice(_derBioDevices.size());
			for (auto &a : getActivations())
			{
				expDerBioDevice[a] = this->getDerivedSemanticalBioDevice(a).isX();
			}
			
			// Second step : we try simplifications on the semantics
			for (size_t i(0); i < _semantics.size(); ++i)
			{
				// We clone this object in order to modify the copy
				auto BDClone = this->clone();
				
				for (auto &subSem : Semantic::subSemantic.at(*_semantics[i]))
				{
					BDClone->setSemantic(i, &subSem);
					
					bool equal = true;
					for (auto &a : getActivations())
					{
						if (expDerBioDevice[a] != BDClone->getDerivedSemanticalBioDevice(a).isX())
						{
							equal = false;
							break;
						}
					}
					
					if (equal)
					{
						return true;
					}
				}
			}
			
			return false;
		}
		
		void SemanticalBioDevice::buildDerivedSemanticalBioDevices() const
		{
			_derBioDevices.clear();
			
			size_t s(getStructure().getDerivedBioDevicesStructures().size());
			
			_derBioDevices = vector<DerivedSemanticalBioDevice>();
			
			for (size_t i(0); i < s; ++i)
			{
				_derBioDevices.push_back(DerivedSemanticalBioDevice(*this, i));
			}
		}
		
		SemanticalBioDevice::const_iterator SemanticalBioDevice::cbegin() const
		{
			return _derBioDevices.cbegin();
		}
		
		SemanticalBioDevice::const_iterator SemanticalBioDevice::cend() const
		{
			return _derBioDevices.cbegin();
		}
		
		SemanticalBioDevice::const_iterator SemanticalBioDevice::begin() const
		{
			return _derBioDevices.cbegin();
		}
		
		SemanticalBioDevice::const_iterator SemanticalBioDevice::end() const
		{
			return _derBioDevices.cbegin();
		}
		
		uint16_t SemanticalBioDevice::getMinLength() const
		{
			uint16_t minLength (0);
			for_each(_semantics.begin(), _semantics.end(), [&minLength] (auto sem) { minLength += sem->getMinLength(); });
			minLength += _structure->getLength();
			
			return minLength;
		}
		
		uint16_t SemanticalBioDevice::getNbGenes() const
		{
			uint16_t nbGenes (0);
			for_each(_semantics.begin(), _semantics.end(), [&nbGenes] (auto sem) { nbGenes += sem->isGF() ? (sem->isGR() ? 2 : 1) : (sem->isX() || sem->isGR() ? 1 : 0); });
			
			return nbGenes;
		}
		
		uint16_t SemanticalBioDevice::getNbPromoters() const
		{
			uint16_t nbPromoters (0);
			for_each(_semantics.begin(), _semantics.end(), [&nbPromoters] (auto sem) { nbPromoters += sem->isPF() ? (sem->isPR() ? 2 : 1) : (sem->isX() || sem->isPR() ? 1 : 0); });
			
			return nbPromoters;
		}
		
		uint16_t SemanticalBioDevice::getNbTerminators() const
		{
			uint16_t nbTerminators (0);
			for_each(_semantics.begin(), _semantics.end(), [&nbTerminators] (auto sem) { nbTerminators += sem->isTF() ? (sem->isTR() ? 2 : 1) : (sem->isTR() ? 1 : 0); });
			
			return nbTerminators;
		}
		
		uint16_t SemanticalBioDevice::getMinNbParts() const
		{
			uint16_t minNbParts (0);
			for_each(_semantics.begin(), _semantics.end(), [&minNbParts] (auto sem) { minNbParts += sem->getMinNbParts(); });
			minNbParts += _structure->getNbParts();
			
			return minNbParts;
		}
		
		uint16_t SemanticalBioDevice::getMaxDistancePromoterToGene() const
		{
			if (_derBioDevices.empty())
				buildDerivedSemanticalBioDevices();
			
			uint16_t maxDistance (0);
			for (size_t i(0); i < _derBioDevices.size(); ++i)
			{
				auto derBioDevice = _derBioDevices[i];
				if (derBioDevice.isX())
				{
					bool hasPF (false);
					uint16_t posPF(0), minDistanceInDerived(numeric_limits<uint16_t>::max());
					
					for (size_t j(0); j < derBioDevice.size(); ++j)
					{
						if (derBioDevice[j].isX())
						{
							minDistanceInDerived = 0;
							break;
						}
						
						if (hasPF)
						{
							if (derBioDevice[j].isGF())
							{
								uint16_t distance(0);
								for_each(derBioDevice.begin()+posPF+1, derBioDevice.end()+j-1, [&distance] (auto sem) { distance += sem.getMinLength(); });
								
								int8_t numPF(_structure->getDerivedBioDevicesStructures()[i][posPF]);
								int8_t numGF(_structure->getDerivedBioDevicesStructures()[i][j]);
								numPF *= numPF < 0 ? -1 : 1;
								numGF *= numGF < 0 ? -1 : 1;
								for (int k(numPF); k < numGF; ++k)
								{
									distance += _structure->getLengthAMI(k);
								}
								
								minDistanceInDerived = min (minDistanceInDerived,distance);
								
								hasPF = false;
							}
							else if (derBioDevice[j].isTF())
							{
								hasPF = false;
							}
						}
						
						if (derBioDevice[j].isPF())
						{
							hasPF = true;
							posPF = j;
						}
					}
					
					if (minDistanceInDerived != numeric_limits<uint16_t>::max())
					{
						maxDistance = max(maxDistance, minDistanceInDerived);
					}
					
					bool hasPR (false);
					size_t posPR(0);
					
					for (int j(derBioDevice.size()-1); j >= 0 ; --j)
					{
						if (derBioDevice[j].isX())
						{
							minDistanceInDerived = 0;
							break;
						}
						
						if (hasPR)
						{
							if (derBioDevice[j].isGR())
							{
								uint16_t distance(0);
								for_each(derBioDevice.begin()+j+1, derBioDevice.end()+posPR-1, [&distance] (auto sem) { distance += sem.getMinLength(); });
								
								int8_t numPR(_structure->getDerivedBioDevicesStructures()[i][posPR]);
								int8_t numGR(_structure->getDerivedBioDevicesStructures()[i][j]);
								numPR *= numPR < 0 ? -1 : 1;
								numGR *= numGR < 0 ? -1 : 1;
								
								for (int k(numGR); k < numPR; ++k)
								{
									distance += _structure->getLengthAMI(k);
								}
								
								minDistanceInDerived = min (minDistanceInDerived,distance);
								
								hasPR = false;
							}
							else if (derBioDevice[j].isTR())
							{
								hasPR = false;
							}
						}
						
						if (derBioDevice[j].isPR())
						{
							hasPR = true;
							posPR = j;
						}
					}
					
					maxDistance = max(maxDistance, minDistanceInDerived);
				} 
			}
			
			return maxDistance;
		}
		
		bool SemanticalBioDevice::hasSameExpressions(const SemanticalBioDevice& sbd) const
		{
			if (sbd.getNbIntegrase() != getNbIntegrase())
				return false;
			
			for (auto it1 = sbd.begin(), it2 = begin(); 
				 it1 != sbd.end() && it2 != end();
				++it1, ++it2)
			{
				if (it1->isX() != it2->isX())
				{
					return false;
				}
			}
			
			return true;
		}
		
		uint16_t SemanticalBioDevice::getNbTerminatorsReplaceableByDoubles() const
		{
			uint16_t nbReplaceable{0};
			for (size_t i{0}; i < _semantics.size(); ++i)
			{
				auto &sem = *_semantics[i];
				if (sem.isTF() && sem.isTR())
				{
					nbReplaceable += 2;
				}
				else if (sem.isTF() || sem.isTR())
				{
					if (sem.getSemanticForward().isN() || sem.getSemanticReverse().isN())
					{
						SemanticalBioDevice sbd = *this;
						
						sbd.setSemantic(i, Semantic::bioFunctionToPointer[(int)Semantic::BF::T][(int)Semantic::BF::T]);
						if (sbd.hasSameExpressions(*this))
						{
							++nbReplaceable;
						}
					}
					else
					{
						++nbReplaceable;
					}
				}
			}
			
			return nbReplaceable;
		}
		
		uint8_t SemanticalBioDevice::getNbIntegrase() const
		{
			return _structure->getNbIntegrase();
		}
		
		SemanticalBioDevice::operator std::string () const
		{
			std::string semanticalBioDevice;
			
			for (size_t i(0); i < _structure->size(); ++i)
			{
				string sem = _semantics[i] == nullptr ? "null" : Semantic::elementaryToString.at(*(_semantics[i]));
				semanticalBioDevice += sem + " " + _structure->getAMIWithIndex(i) + " ";
			}
			
			string sem = _semantics.back() == nullptr ? "null" : Semantic::elementaryToString.at(*(_semantics.back()));
			semanticalBioDevice += sem;
			
			return semanticalBioDevice;
		}
		
		bool SemanticalBioDevice::operator== (const SemanticalBioDevice& sbd) const
		{
			return *_structure == *(sbd._structure)
				&& std::equal(_semantics.begin(), _semantics.end(), sbd._semantics.begin(), [](auto &s1, auto &s2){ return *s1 == *s2; });
		}
		
		bool SemanticalBioDevice::operator!= (const SemanticalBioDevice& sbd) const
		{
			return !(*_structure == *(sbd._structure))
				|| !std::equal(_semantics.begin(), _semantics.end(), sbd._semantics.begin(), [](auto &s1, auto &s2){ return *s1 == *s2; });
		}
		
		string SemanticalBioDevice::getHexadecimalSemantics() const
		{
			std::ostringstream stream;
			for (auto &s : _semantics)
			{
				stream<< setfill('0') << std::setw(2) << std::hex  << static_cast<int>(s->getSemanticKey());
			}
			return stream.str();
		}
		
		string SemanticalBioDevice::getSemanticBytes() const
		{
			string result;
			for (auto &s : _semantics)
			{
				result += s->getSemanticKey();
			}
			return result;
		}
		
		
		/**
		 * DerivedSemanticalBioDevice
		 */
		
		DerivedSemanticalBioDevice::DerivedSemanticalBioDevice(const SemanticalBioDevice& bioDev, size_t id) 
		: _bioDev(&bioDev), 
		_id(id),
		_varPos(_bioDev->getStructure().size()+1)
		{
			for (size_t i{0}; i < _bioDev->getStructure().getDerivedBioDevicesStructures()[_id].size(); ++i)
			{
				auto &var = _bioDev->getStructure().getDerivedBioDevicesStructures()[_id][i];
				_variablesIn.insert(abs(var));
				_varPos[abs(var)] = i;
				if (var < 0)
				{
					_variablesReversed.insert(abs(var));
				}
			}
		}
		
		size_t DerivedSemanticalBioDevice::size() const
		{
			return _bioDev->getStructure().getDerivedBioDevicesStructures()[_id].size();
		}
		
		const FunctionnalStructure::SemanticalBioDeviceStructure& DerivedSemanticalBioDevice::getSemStruct() const
		{
			return _bioDev->getStructure().getDerivedBioDevicesStructures()[_id];
		}
		
		const Semantic& DerivedSemanticalBioDevice::operator[] (size_t i) const
		{
			if (i >= size())
			{
				throw std::out_of_range("The i in argument of DerivedSemanticalBioDevice::operator[] is too big");
			}
			
			int idSemantic = getSemStruct()[i];
			
			if (idSemantic >= 0)
			{
				return *_bioDev->_semantics[idSemantic];
			}
			
			Semantic sem = *(_bioDev->_semantics[idSemantic*-1]);
			
			return *(Semantic::bioFunctionToPointer
				[static_cast<size_t>(sem.getSemanticReverse().getBioFunction())]
				[static_cast<size_t>(sem.getSemanticForward().getBioFunction())]);
		}
		
		DerivedSemanticalBioDevice::const_iterator DerivedSemanticalBioDevice::cbegin() const
		{
			return const_iterator(*this, 0);
		}
		
		DerivedSemanticalBioDevice::const_iterator DerivedSemanticalBioDevice::cend() const
		{
			return const_iterator(*this, size());
		}
		
		DerivedSemanticalBioDevice::const_iterator DerivedSemanticalBioDevice::begin() const
		{
			return cbegin();
		}
		
		DerivedSemanticalBioDevice::const_iterator DerivedSemanticalBioDevice::end() const
		{
			return cend();
		}
		
		DerivedSemanticalBioDevice::const_reverse_iterator DerivedSemanticalBioDevice::crbegin() const
		{
			return const_reverse_iterator(*this, size()-1);
		}
		
		DerivedSemanticalBioDevice::const_reverse_iterator DerivedSemanticalBioDevice::crend() const
		{
			return const_reverse_iterator(*this, -1);
		}
		
		DerivedSemanticalBioDevice::const_reverse_iterator DerivedSemanticalBioDevice::rbegin() const
		{
			return crbegin();
		}
		
		DerivedSemanticalBioDevice::const_reverse_iterator DerivedSemanticalBioDevice::rend() const
		{
			return crend();
		}
		
		bool DerivedSemanticalBioDevice::isX() const
		{
			Semantic combination;
			
			for (auto &sem :  *this)
			{
				combination += sem;
				if (combination.isX())
				{
					return true;
				}
			}
			
			return false;
		}
		
		bool  DerivedSemanticalBioDevice::isIn(uint8_t var) const
		{
			return _variablesIn.find(var) != _variablesIn.end();
		}
		
		bool  DerivedSemanticalBioDevice::isReversed(uint8_t var) const
		{
			return _variablesReversed.find(var) != _variablesReversed.end();
		}
		
		size_t DerivedSemanticalBioDevice::getVarPos(uint8_t var) const
		{
			return _varPos[var];
		}
		
		DerivedSemanticalBioDevice::operator std::string() const
		{
			string derived;
			for (const auto &s : *this)
			{
				derived += static_cast<string>(s)+" ";
			}
			return derived;
		}
		
		/**
		 * cDerivedSemanticalBioDeviceIterator
		 */
		
		cDerivedSemanticalBioDeviceIterator::cDerivedSemanticalBioDeviceIterator(const DerivedSemanticalBioDevice& derSemBioDev, size_t pos) :
		_derSemBioDev(&derSemBioDev), 
		_pos(pos) 
		{}
		
		bool cDerivedSemanticalBioDeviceIterator::operator== (const cDerivedSemanticalBioDeviceIterator& cit) const 
		{ 
			return cit._derSemBioDev == _derSemBioDev && cit._pos == _pos; 
		}
		
		bool cDerivedSemanticalBioDeviceIterator::operator!= (const cDerivedSemanticalBioDeviceIterator& cit) const 
		{ 
			return !(cit==*this); 
		}
		
		const Semantic& cDerivedSemanticalBioDeviceIterator::operator* () const
		{ 
			return (*_derSemBioDev)[_pos];
		}
		
		cDerivedSemanticalBioDeviceIterator & cDerivedSemanticalBioDeviceIterator::operator++() 
		{ 
			if (_pos < _derSemBioDev->size()) 
			{
				++_pos; 
			}
			return *this;
		}
		
		cDerivedSemanticalBioDeviceIterator cDerivedSemanticalBioDeviceIterator::operator++ (int)
		{ 
			cDerivedSemanticalBioDeviceIterator clone(*this); 
			if (_pos < _derSemBioDev->size()) 
			{
				++_pos; 
			}
			return clone; 
		}
		
		cDerivedSemanticalBioDeviceIterator& cDerivedSemanticalBioDeviceIterator::operator+ (int i)
		{
			if (_pos+i < _derSemBioDev->size()) 
			{
				_pos += i; 
			}
			return *this;
		}
		
		cDerivedSemanticalBioDeviceIterator& cDerivedSemanticalBioDeviceIterator::operator- (int i)
		{
			if (static_cast<int64_t>(_pos)-i > -1) 
			{
				_pos -= i; 
			}
			return *this;
		}
		
		/**
		 * cDerivedSemanticalBioDeviceReverseIterator
		 */
		
		cDerivedSemanticalBioDeviceReverseIterator::cDerivedSemanticalBioDeviceReverseIterator(const DerivedSemanticalBioDevice& derSemBioDev, int pos) :
		_derSemBioDev(&derSemBioDev), 
		_pos(pos) 
		{}
		
		bool cDerivedSemanticalBioDeviceReverseIterator::operator== (const cDerivedSemanticalBioDeviceReverseIterator& cit) const 
		{ 
			return cit._derSemBioDev == _derSemBioDev && cit._pos == _pos; 
		}
		
		bool cDerivedSemanticalBioDeviceReverseIterator::operator!= (const cDerivedSemanticalBioDeviceReverseIterator& cit) const 
		{ 
			return !(cit==*this); 
		}
		
		const Semantic& cDerivedSemanticalBioDeviceReverseIterator::operator* () const
		{ 
			return (*_derSemBioDev)[_pos];
		}
		
		cDerivedSemanticalBioDeviceReverseIterator & cDerivedSemanticalBioDeviceReverseIterator::operator++() 
		{ 
			if (_pos > -1) 
			{
				--_pos; 
			}
			return *this;
		}
		
		cDerivedSemanticalBioDeviceReverseIterator cDerivedSemanticalBioDeviceReverseIterator::operator++ (int)
		{ 
			cDerivedSemanticalBioDeviceReverseIterator clone(*this); 
			if (_pos > -1) 
			{
				--_pos; 
			}
			return clone; 
		}
		
		cDerivedSemanticalBioDeviceReverseIterator& cDerivedSemanticalBioDeviceReverseIterator::operator+ (int i)
		{
			if (static_cast<int64_t>(_pos)-i > -1) 
			{
				_pos -= i; 
			}
			return *this;
		}
		
		cDerivedSemanticalBioDeviceReverseIterator& cDerivedSemanticalBioDeviceReverseIterator::operator- (int i)
		{
			if (_pos+i < static_cast<int64_t>(_derSemBioDev->size())) 
			{
				_pos += i; 
			}
			return *this;
		}
	}
}
