#include "DyckSemanticalBioDevice.hpp"
#include "IEDyckFunctionnalStructure.hpp"
#include <unordered_map>
#include <vector>
#include <boost/functional/hash/extensions.hpp>

using namespace std;

namespace recombinator
{
	namespace api
	{
		
		DyckSemanticalBioDevice::DyckSemanticalBioDevice() : 
		DyckSemanticalBioDevice(*IEDyckFunctionnalStructure::create<IEDyckFunctionnalStructure>(""), {&Semantic::allSemantics[0]})
		{}
		
		DyckSemanticalBioDevice::DyckSemanticalBioDevice(
			const DyckFunctionnalStructure &structure, 
			vector<const Semantic*> semantics)
		: SemanticalBioDevice(structure, semantics) 
		{}
					
		DyckSemanticalBioDevice::DyckSemanticalBioDevice(const string &bioDevice)
		: SemanticalBioDevice(bioDevice)
		{
			// We convert the structure created by the parent constructor to a DyckFunctionnalStructure
			_structure = IEDyckFunctionnalStructure::create<IEDyckFunctionnalStructure>(_structure->getStructureWithIndexes());
			_derBioDevices.clear();
			buildDerivedSemanticalBioDevices();
		}
		
		DyckSemanticalBioDevice DyckSemanticalBioDevice::getDyckSymmetric() const
		{
			auto structure = IEDyckFunctionnalStructure::create<IEDyckFunctionnalStructure>(IEFunctionnalStructure::structureReverse(_structure->getStructureWithIndexes()));
			DyckSemanticalBioDevice sym(*structure);
			
			for (size_t i = 0; i < getNbSemantics(); ++i)
			{
				sym.setSemantic(getNbSemantics()-i-1, Semantic::bioFunctionToPointer
							[static_cast<size_t>(getSemantic(i).getSemanticReverse().getBioFunction())]
							[static_cast<size_t>(getSemantic(i).getSemanticForward().getBioFunction())]);
			}
			
			return sym;
		}
		
		bool DyckSemanticalBioDevice::isIrredundant() const
		{
			// We get the activations
			const vector<Activation> &activations = getActivations();
			
			// A derived bioDevice got by applying an activation does contain an expressed gene ?
			unordered_map<Activation,bool,boost::hash<Activation>> expressedGene;
			for (const auto &a: activations)
			{
				expressedGene[a] = getDerivedSemanticalBioDevice(a).isX();
			}
			
			// For each integrase, we create a list of activations which activates it
			unordered_map<uint8_t,vector<Activation>> integraseInActivation;
			for (auto &a : activations)
			{
				for (auto &integrase : a)
				{
					integraseInActivation[integrase].push_back(a);
				}
			}
			
			// For each activation a which contains an integrase i
			// We compare the derived d1 got with a with the derived d2 got with a whithout i
			// If we can't have a exressed(d1) != expressed(d2), the bioDevice implements a simplifiable boolean function 
			for (size_t i(0); i < _structure->getNbIntegrase(); ++i)
			{
				bool allEquals = true;
				
				for (auto &a : integraseInActivation.at(i))
				{
					Activation aWhithoutI(a);
					aWhithoutI.erase(find(aWhithoutI.begin(),aWhithoutI.end(),i));
					
					if (expressedGene.at(a) != expressedGene.at(aWhithoutI))
					{
						allEquals = false;
						break;
					}
				}
				
				if (allEquals)
				{
					return false;
				}
			}
			
			return true;
		}
		
		const DyckFunctionnalStructure &DyckSemanticalBioDevice::getStructure() const
		{
			return dynamic_cast<const DyckFunctionnalStructure&> (*_structure);
		}
		
		std::vector<DyckSemanticalBioDevice> DyckSemanticalBioDevice::getNPEquivalents() const
		{
			std::vector<DyckSemanticalBioDevice> result;
			
			for (size_t k = 0; k < getStructure().getNbDifferentDerivations(); ++k)
			{
				auto &structure = getStructure().getDerivedBioDevicesStructures()[k];
				if (structure.size() == getSemantics().size())
				{
					DyckSemanticalBioDevice sbd(getStructure().getStructureOnDerived(k));
					size_t j = 0;
					for (auto &i : structure)
					{
						Semantic s = getSemantic(abs(i));
						if (i < 0)
						{
							s.reverse();
						}
						sbd.setSemantic(j++, Semantic::bioFunctionToPointer
							[(int)s.getSemanticForward().getBioFunction()]
							[(int)s.getSemanticReverse().getBioFunction()]);
					}
					
					result.push_back(sbd);
				}
			}
			
			return result;
		}
		
		std::vector<DyckSemanticalBioDevice> DyckSemanticalBioDevice::getNPEquivalents(const std::unordered_set<std::string>& allowedDyckWords) const
		{
			std::vector<DyckSemanticalBioDevice> result;
			
			for (size_t k = 0; k < getStructure().getNbDifferentDerivations(); ++k)
			{
				auto &structure = getStructure().getDerivedBioDevicesStructures()[k];
				if (structure.size() == getSemantics().size())
				{
					DyckSemanticalBioDevice sbd(getStructure().getStructureOnDerived(k));
					
					if (allowedDyckWords.find(sbd.getStructure().getStructure()) == allowedDyckWords.end())
						continue;
					
					size_t j = 0;
					for (auto &i : structure)
					{
						Semantic s = getSemantic(abs(i));
						if (i < 0)
						{
							s.reverse();
						}
						sbd.setSemantic(j++, Semantic::bioFunctionToPointer
							[(int)s.getSemanticForward().getBioFunction()]
							[(int)s.getSemanticReverse().getBioFunction()]);
					}
					
					result.push_back(sbd);
				}
			}
			
			return result;
		}
	}
}
