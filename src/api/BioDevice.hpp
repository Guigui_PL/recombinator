#ifndef _BIODEVICE_H
#define _BIODEVICE_H

namespace recombinator 
{
	namespace api 
	{
		/**
		 * \class BioDevice
		 * \brief  abstract class
		 */
		class BioDevice
		{
		public:
			virtual ~BioDevice() = default;
			
			/**
			 * \brief 
			 *\return  
			 */
			virtual bool hasGenesAtEnds() const =0;
			
			
			/**
			 * \brief 
			 *\return  
			 */
			virtual bool isRespectedWeakConstraint() const =0;
			
			
			/**
			 * \brief 
			 *\return  
			 */
			virtual bool isRespectedStrongConstraint() const =0;
			
			virtual uint8_t getNbIntegrase() const = 0;
		};
	}
}
#endif