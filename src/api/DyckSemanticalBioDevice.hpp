#ifndef _DYCKSEMANTICALBIODEVICE_H
#define _DYCKSEMANTICALBIODEVICE_H

#include "SemanticalBioDevice.hpp"
#include "DyckFunctionnalStructure.hpp"

namespace recombinator 
{
	namespace api 
	{
		/**
		 * \class DyckSemanticalBioDevice
		 * \brief  
		 */
		class DyckSemanticalBioDevice : public SemanticalBioDevice
		{
		public:
			
			DyckSemanticalBioDevice();
			/**
			 * \brief Constructor which receives a DyckFunctionnalStructure and a list of semantics
			 * \param structure : The DyckFunctionnalStructure of the semantical BioDevice 
			 * \param semantics : the semantics of the semantical BioDevice (if the structure has n sites, there must be n+1 semantics)
			 */
			DyckSemanticalBioDevice(const DyckFunctionnalStructure &structure, std::vector<const Semantic*> semantics = {});
			
			/**
			 * \brief Construct which receives a classical bioDevice
			 * \param bioDevice : string which contains a classical bioDevice
			 */
			DyckSemanticalBioDevice(const std::string &bioDevice);
			
			virtual ~DyckSemanticalBioDevice() = default;
			virtual DyckSemanticalBioDevice getDyckSymmetric() const;
			
			/**
			 * \brief 
			 *
			 * \return  
			 */
			bool isIrredundant() const;
			
			/**
			 * \brief 
			 *
			 * \return  
			 */
			const DyckFunctionnalStructure &getStructure() const;
			
			std::vector<DyckSemanticalBioDevice> getNPEquivalents() const;
			std::vector<DyckSemanticalBioDevice> getNPEquivalents(const std::unordered_set<std::string>& allowedDyckWords) const;
			
		protected:
			virtual DyckSemanticalBioDevice* clone_impl() const override { return new DyckSemanticalBioDevice(*this); };     
			
			
		};
	}
}

#endif
