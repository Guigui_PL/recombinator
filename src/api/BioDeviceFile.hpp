#ifndef _BIODEVICEFILE_HPP
#define _BIODEVICEFILE_HPP
#include <fstream>
#include <memory>
#include <string>
#include "BioDevice.hpp"
#include "SemanticalBioDevice.hpp"
#include "DyckSemanticalBioDevice.hpp"

namespace recombinator 
{
	namespace api 
	{
		class BioDeviceFile 
		{
		public:
			virtual bool isOpenFile() const = 0;
			
		protected:
			BioDeviceFile(std::string filePath);
			
			virtual const std::string& getFilePath() const;
			
			std::string _filePath;
		};
		
		class BioDeviceFileReader : public BioDeviceFile
		{
		public:
			virtual bool isOpenFile() const;
			
			std::shared_ptr<BioDevice> getNextBioDevice();
			template <typename T>
			BioDeviceFileReader& operator>> (T &bd)
			{
				auto nextbd = getNextBioDevice();
				if (nextbd != nullptr)
				{
					bd = std::move(*dynamic_cast<T*>(nextbd.get()));
				}
				return *this;
			}
			operator bool ();
			
		protected:
			BioDeviceFileReader(std::string filePath);
			
			virtual BioDevice* getNextBioDeviceImpl() = 0;
			
			std::ifstream _file;
		};
		
		class BioDeviceFileWriter : public BioDeviceFile
		{
		public:
			virtual bool isOpenFile() const;
			
			virtual void flush();
			virtual ~BioDeviceFileWriter();
			BioDeviceFileWriter(BioDeviceFileWriter &&) = default;
			virtual BioDeviceFileWriter& operator=(BioDeviceFileWriter &&) = default;
			
		protected:
			BioDeviceFileWriter(std::string filePath);
			
			std::ofstream _file;
		};
		
		class SemanticalBioDeviceFile
		{
		public:
			enum class FileType
			{
				CLASSICAL_BIODEVICE,
				UNIQUE_STRUCTURE,
				MULTI_STRUCTURE
			};
			
		protected:
			SemanticalBioDeviceFile();
			FileType _fileType;
			
		};
		
		class SemanticalBioDeviceFileReader : public SemanticalBioDeviceFile, public BioDeviceFileReader
		{
		public:
			SemanticalBioDeviceFileReader(std::string filePath);
			
			std::shared_ptr<SemanticalBioDevice> getNextBioDevice();
		protected:
			virtual SemanticalBioDevice* getNextBioDeviceImpl() override;
			std::shared_ptr<FunctionnalStructure> _functionnalStructure;
		};
		
		class SemanticalBioDeviceFileWriter : public SemanticalBioDeviceFile, public BioDeviceFileWriter
		{
		public:
			SemanticalBioDeviceFileWriter(std::string filePath, SemanticalBioDeviceFile::FileType type, bool overWrite = true);
			virtual void writeBioDevice(const SemanticalBioDevice& bd);
			template <typename T>
			SemanticalBioDeviceFileWriter& operator<< (const T& bd)
			{
				writeBioDevice(bd);
				return *this;
			}
		protected:
			std::shared_ptr<FunctionnalStructure> _functionnalStructure;
		};
		
		
		
		class DyckSemanticalBioDeviceFileReader : public SemanticalBioDeviceFileReader
		{
		public:
			DyckSemanticalBioDeviceFileReader(std::string filePath);
			
			std::shared_ptr<DyckSemanticalBioDevice> getNextBioDevice();
		protected:
			virtual DyckSemanticalBioDevice* getNextBioDeviceImpl() override;
			
			
		};
		
		class DyckSemanticalBioDeviceFileWriter : public SemanticalBioDeviceFileWriter
		{
		public:
			DyckSemanticalBioDeviceFileWriter(std::string filePath, SemanticalBioDeviceFile::FileType type, bool overWrite = true);
			virtual void writeBioDevice(const DyckSemanticalBioDevice& bd);
		};
	}
}

#endif
