#include "Semantic.hpp"
#include <sstream>
#include <boost/algorithm/string/trim.hpp>

using namespace std;

namespace recombinator 
{
	namespace api 
	{
		/**
		 * Semantic implementation
		 */
		
		const array<Semantic,26> Semantic::elementarySemantics {
			{
				Semantic(BF::N, BF::N), Semantic(BF::N, BF::P), Semantic(BF::N, BF::T), Semantic(BF::N, BF::G), Semantic(BF::N, BF::GP),
				Semantic(BF::P, BF::N), Semantic(BF::P, BF::P), Semantic(BF::P, BF::T), Semantic(BF::P, BF::G), Semantic(BF::P, BF::GP),
				Semantic(BF::T, BF::N), Semantic(BF::T, BF::P), Semantic(BF::T, BF::T), Semantic(BF::T, BF::G), Semantic(BF::T, BF::GP),
				Semantic(BF::G, BF::N), Semantic(BF::G, BF::P), Semantic(BF::G, BF::T), Semantic(BF::G, BF::G), Semantic(BF::G, BF::GP),
				Semantic(BF::GP, BF::N), Semantic(BF::GP, BF::P), Semantic(BF::GP, BF::T), Semantic(BF::GP, BF::G), Semantic(BF::GP, BF::GP),
				Semantic(BF::X, BF::X)
			}
		};
		
		const array<Semantic,28> Semantic::allSemantics {
			{
				Semantic(BF::N, BF::N), Semantic(BF::N, BF::P), Semantic(BF::N, BF::T), Semantic(BF::N, BF::G), Semantic(BF::N, BF::GP),
				Semantic(BF::P, BF::N), Semantic(BF::P, BF::P), Semantic(BF::P, BF::T), Semantic(BF::P, BF::G), Semantic(BF::P, BF::GP),
				Semantic(BF::T, BF::N), Semantic(BF::T, BF::P), Semantic(BF::T, BF::T), Semantic(BF::T, BF::G), Semantic(BF::T, BF::GP),
				Semantic(BF::G, BF::N), Semantic(BF::G, BF::P), Semantic(BF::G, BF::T), Semantic(BF::G, BF::G), Semantic(BF::G, BF::GP),
				Semantic(BF::GP, BF::N), Semantic(BF::GP, BF::P), Semantic(BF::GP, BF::T), Semantic(BF::GP, BF::G), Semantic(BF::GP, BF::GP),
				Semantic(BF::X, BF::X), Semantic(BF::N, BF::X), Semantic(BF::X, BF::N)
			}
		};
		
		const std::unordered_map<Semantic,std::string,SemanticHash> Semantic::elementaryToString = Semantic::buildElementaryToString();
		
		const array<array<const Semantic*,6>,6> Semantic::bioFunctionToPointer = Semantic::buildBioFunctionToPointer();
		
		const unordered_map<Semantic,vector<Semantic>,SemanticHash> Semantic::subSemantic = Semantic::buildSubSemantic();
		
		unordered_map<Semantic,vector<Semantic>,SemanticHash> Semantic::buildSubSemantic()
		{
			unordered_map<Semantic,vector<Semantic>,SemanticHash> subSem;
			for (auto &sem : allSemantics)
			{
				subSem[sem];
				
				if (!(sem.isXF() && sem.isXR()))
				{
					for (auto &bf1 : OneWaySemantic::subBioFunction.at(sem._forward.getBioFunction()))
					{
						if (!sem.isXR())
						{
							subSem[sem].push_back(Semantic(bf1,sem._reverse.getBioFunction()));
						}
						for (auto &bf2 : OneWaySemantic::subBioFunction.at(sem._reverse.getBioFunction()))
						{
							subSem[sem].push_back(Semantic(bf1,bf2));
						}
					}
					
					if (!sem.isXF())
					{
						for (auto &bf2 : OneWaySemantic::subBioFunction.at(sem._reverse.getBioFunction()))
						{
							subSem[sem].push_back(Semantic(sem._forward.getBioFunction(),bf2));
						}
					}
				}
				else 
				{
					for (auto &bf2 : OneWaySemantic::subBioFunction.at(sem._reverse.getBioFunction()))
					{
						subSem[sem].push_back(Semantic(bf2,BF::N));
						if (bf2 != BF::N)
						{
							subSem[sem].push_back(Semantic(BF::N,bf2));
						}
					}
				}
			}
			
			return subSem;
		}
		
		array<array<const Semantic*,6>,6> Semantic::buildBioFunctionToPointer()
		{
			array<array<const Semantic*,6>,6> bftp = {array<const Semantic*,6>{nullptr}};
			for (auto &sem : allSemantics)
			{
				size_t bfForward(static_cast<size_t>(sem.getSemanticForward().getBioFunction()));
				size_t bfReverse(static_cast<size_t>(sem.getSemanticReverse().getBioFunction()));
				bftp[bfForward][bfReverse] = &sem;
			}
			return bftp;
		}
		
		std::unordered_map<Semantic,std::string,SemanticHash> Semantic::buildElementaryToString()
		{
			std::unordered_map<Semantic,std::string,SemanticHash> result;
			vector<string> elementaryStrings {{"",
			"PF", "PR", "TF", "TR", "GF", "GR",
			"TR PF", "PF GF", "GR PR", "PF GR", "PR PF", "PR TF", "GF TR",
			"GF PR", "PR GR", "TF TR", "TF GR", "GF PF", "GF GR",
			"PR TF GR", "GF TR PF", "GF PF GR", "GF PR PF", "GF PR GR", "PR PF GR",
			"GF PR PF GR"}};
			
			for (auto &it : elementaryStrings)
			{
				Semantic s(it);
				result[s] = it;
			}
			result[Semantic(BF::X, BF::X)] = "PF GF";
			
			return result;
		}
		
		Semantic::Semantic(const OneWaySemantic &forward, const OneWaySemantic &reverse) :
			_forward(forward),
			_reverse(reverse)
		{
			if (_forward.isX() && !_reverse.isN() && !_reverse.isX())
			{
				throw InvalidSemanticArgument("Invalid Semantic : fXF is incompatible with "+static_cast<string>(_reverse)+"R !");
				_reverse = OneWaySemantic(OneWaySemantic::BioFunction::N);
			}
			if (_reverse.isX() && !_forward.isN() && !_forward.isX())
			{
				throw InvalidSemanticArgument("Invalid Semantic : fXR is incompatible with "+static_cast<string>(_forward)+"F !");
				_forward = OneWaySemantic(OneWaySemantic::BioFunction::N);
			}
		}
		
		// We use here a delegate constructor
		Semantic::Semantic(OneWaySemantic::BioFunction forward, OneWaySemantic::BioFunction reverse) :
			Semantic(OneWaySemantic(forward), OneWaySemantic(reverse))
		{}
		
		Semantic::Semantic(const std::string &biologicalParts)
		{
			istringstream parts;
			parts.str(boost::algorithm::trim_copy(biologicalParts));
			string bioPart;
			
			while (parts >> bioPart)
			{
				boost::algorithm::trim(bioPart);
				Semantic toCombine;
				
				if (bioPart == "PF")
				{
					toCombine = Semantic(BF::P,BF::N);
				}
				else if (bioPart == "PR")
				{
					toCombine = Semantic(BF::N,BF::P);
				}
				else if (bioPart == "TF")
				{
					toCombine = Semantic(BF::T,BF::N);
				}
				else if (bioPart == "TR")
				{
					toCombine = Semantic(BF::N,BF::T);
				}
				else if (bioPart == "GF")
				{
					toCombine = Semantic(BF::G,BF::N);
				}
				else if (bioPart == "GR")
				{
					toCombine = Semantic(BF::N,BF::G);
				}
				else 
				{
					throw std::invalid_argument("The biological parts can't be converted to a Semantic : invalid biological part : "+bioPart);
				}
				*this += toCombine;
			}
		}
		
		Semantic& Semantic::operator+= (const Semantic& s)
		{
			return combine(s);
		}
		
		/*
		 * Excision = we set the Semantic to null
		 * return a reference on the object
		 */
		Semantic& Semantic::excise() 
		{
			_forward.setBioFunction(OneWaySemantic::BioFunction::N);
			_reverse.setBioFunction(OneWaySemantic::BioFunction::N);
			return *this;
		}
		
		/*
		 * Reverse = we exchange the attributes _reverse and _forward
		 * return a reference on the object
		 */
		Semantic& Semantic::reverse() 
		{
			OneWaySemantic ows(_forward);
			_forward = _reverse;
			_reverse = ows;
			return *this;
		}
		
		/**
		 * @return bool
		 */
		bool Semantic::isPF() const
		{
			return _forward.isP();
		}
		
		/**
		 * @return bool
		 */
		bool Semantic::isTF() const
		{
			return _forward.isT();
		}
		
		/**
		 * @return bool
		 */
		bool Semantic::isGF() const
		{
			return _forward.isG();
		}
		
		/**
		 * @return bool
		 */
		bool Semantic::isX() const
		{
			return _forward.isX() || _reverse.isX();
		}
		
		bool Semantic::isXF() const
		{
			return _forward.isX();
		}
		
		bool Semantic::isXR() const
		{
			return _reverse.isX();
		}
		
		/**
		 * @return bool
		 */
		bool Semantic::isGR() const
		{
			return _reverse.isG();
		}
		
		/**
		 * @return bool
		 */
		bool Semantic::isTR() const
		{
			return _reverse.isT();
		}
		
		/**
		 * @return bool
		 */
		bool Semantic::isPR() const
		{
			return _reverse.isP();
		}
		
		/**
		 * @return bool
		 */
		bool Semantic::isN() const
		{
			return _reverse.isN() && _forward.isN();
		}
		
		/**
		 * @return bool
		 */
		bool Semantic::isMirror() const
		{
			return _reverse == _forward;
		}
		
		bool Semantic::isEqualTo(const Semantic& s) const
		{
			return (s.isX() && isX()) || (_forward == s._forward && _reverse == s._reverse);
		}
		
		/**
		 * @param Semantic s
		 * @return Semantic (a reference to the object, not the result of the combination)
		 */
		Semantic& Semantic::combine(const Semantic &s) 
		{
			_forward.combine(s._forward);
			_reverse = OneWaySemantic::combine(s._reverse, _reverse);
			
			if (_forward.isX() && !_reverse.isX())
			{
				_reverse.setBioFunction(BF::N);
			}
			else if (_reverse.isX() && !_forward.isX())
			{
				_forward.setBioFunction(BF::N);
			}
			
			return *this;
		}
		
		/**
		 * @param Semantic s1
		 * @param Semantic s2
		 * @return Semantic
		 */
		Semantic Semantic::combine(const Semantic &s1, const Semantic &s2) 
		{
			return Semantic(s1).combine(s2);
		}
		
		/**
		 * @return OneWaySemantic
		 */
		const OneWaySemantic& Semantic::getSemanticForward() const
		{
			return _forward;
		}
		
		/**
		 * @return OneWaySemantic
		 */
		const OneWaySemantic& Semantic::getSemanticReverse() const
		{
			return _reverse;
		}
		
		/**
		 * @return int
		 */
		uint8_t Semantic::getSemanticKey() const
		{
			return static_cast<uint8_t>(_forward.getBioFunction())*6+static_cast<uint8_t>(_reverse.getBioFunction());
		}
		
		const Semantic* Semantic::getPointerWithKey(uint8_t key)
		{
			return bioFunctionToPointer[key/6][key%6];
		}
		
		Semantic::operator string () const
		{
			return "("+static_cast<string>(_forward)+", "
				+static_cast<string>(_reverse)+")";
		}
		
		uint16_t Semantic::getMinLength() const
		{
			return _forward.getMinLength() + _reverse.getMinLength();
		}
		
		uint16_t Semantic::getMinNbParts() const
		{
			return _forward.getMinNbParts() + _reverse.getMinNbParts();
		}
		
		bool Semantic::isAnInversionOf(const Semantic& s) const
		{
			return _forward == s._reverse && _reverse == s._forward;
		}
		
		Semantic::Utility Semantic::isUsefulAfter(const Semantic& s) const
		{
			/*if (s.isX())
			{
				return isN() ? Utility::UTILITY_REVERSE_AND_FORWARD : Utility::USELESS;
			}*/
			
			return static_cast<Utility>(_forward.isUsefulAfter(s._forward) + 2*_reverse.isUsefulBefore(s._reverse));
		}
		
		Semantic::Utility Semantic::isUsefulBefore(const Semantic& s) const
		{
			/*if (s.isX())
			{
				return isN() ? Utility::UTILITY_REVERSE_AND_FORWARD : Utility::USELESS;
			}*/
			
			return static_cast<Utility>(_forward.isUsefulBefore(s._forward) + 2*_reverse.isUsefulAfter(s._reverse));
		}
		
		Semantic::Utility Semantic::isUsefulAfterPrefix(const Semantic& s) const
		{
			/*if (s.isX())
			{
				return isN() ? Utility::UTILITY_REVERSE_AND_FORWARD : Utility::USELESS;
			}*/
			
			return static_cast<Utility>(_forward.isUsefulAfterPrefix(s._forward) + 2*_reverse.isUsefulBeforeSuffix(s._reverse));
		}
		
		Semantic::Utility Semantic::isUsefulBeforeSuffix(const Semantic& s) const
		{
			/*if (s.isX())
			{
				return isN() ? Utility::UTILITY_REVERSE_AND_FORWARD : Utility::USELESS;
			}*/
			
			return static_cast<Utility>(_forward.isUsefulBeforeSuffix(s._forward) + 2*_reverse.isUsefulAfterPrefix(s._reverse));
		}
		
		std::ostream& operator<< (std::ostream& os, Semantic s)
		{
			os << static_cast<string>(s);
			return os;
		}
		
		size_t hash_value(const ::recombinator::api::Semantic &s) 
		{
			return s.getSemanticKey();
		}
	}
}
