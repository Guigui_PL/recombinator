#ifndef _TRUTHTABLE_H
#define _TRUTHTABLE_H 
#include <string>
#include <vector>

namespace recombinator 
{
	namespace api 
	{
		class TruthTable 
		{
		public:
			
			virtual std::string getDisjunctiveNormalForm() const = 0;
			virtual uint64_t getDisjunctiveNormalFormInt() const;
			virtual uint8_t getNbInputs() const;
			virtual std::vector<std::string> getPEquivalentFunctions () const;
			virtual std::vector<uint64_t> getPEquivalentFunctionsInt () const;
			
		protected:
			TruthTable() = default;
			TruthTable(uint8_t nbInputs);
			
			uint8_t _nbInputs = 0;
		};
	}
}

#endif