/**
 * Copyright (C) Inserm Montpellier / CBS
 * 
 * U 1054 - mixte avec Université de Montpellier et CNRS
 * Centre de Biochimie Structurale
 * Directeur : Pierre-Emmanuel Milhiet
 * 29, rue de Navacelles
 * 34090 MONTPELLIER
 * 
 * Recombinator project : http://recombinator.lirmm.fr/
 * 
 * Team : Synthetic biology at CBS : http://www.cbs.cnrs.fr/index.php/en/research-equipea4
 * 
 * 
 * @auhtor : Guillaume Pérution-Kihli 
 */

#ifndef _THREADSMANAGER_H
#define _THREADSMANAGER_H
#include <chrono>
#include <thread>
#include <vector>
#include <mutex>
#include <unordered_map>

namespace recombinator 
{
	namespace api 
	{
		class ThreadsManager {
		public: 
			using Thread = std::thread;
			using ThreadId = size_t;
			
			/**
			* @param t
			*/
			template<class Function, class... Args>
			ThreadId addThread(Function&& f, Args&&... args);
			
			void wait(ThreadId id);
			
			static ThreadsManager& getThreadsManager();
			
			static size_t _limitNbLaunchedThreads;
		private: 
			static ThreadsManager _instance;
			std::unordered_map<ThreadId,Thread> _launchedThreads;
			std::unordered_map<ThreadId,std::mutex> _lockWaiting;
			ThreadId _nextId = 0;
			mutable std::mutex _lockThreadsManager;
			mutable std::mutex _lockAddingThread;
			
			bool canLaunchNewThread() const;
			
			ThreadId getNextId();
			
			void updateLaunchedThreads();
			
			ThreadsManager() = default;
		};

		template<class Function, class... Args>
		ThreadsManager::ThreadId ThreadsManager::addThread(Function&& f, Args&&... args)
		{
			std::lock_guard<std::mutex> lock(_lockAddingThread);
			while (!canLaunchNewThread())
			{
				updateLaunchedThreads();
				std::this_thread::sleep_for(std::chrono::milliseconds(100));
			}
			
			ThreadId id = getNextId();
			std::lock_guard<std::mutex> lock2(_lockThreadsManager);
			_launchedThreads.insert(std::make_pair(id, std::thread(f, args...)));
			_lockWaiting[id];
			
			return id;
		}
	}
}

#endif //_THREADSMANAGER_H
