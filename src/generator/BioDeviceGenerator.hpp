/**
 * Copyright (C) Inserm Montpellier / CBS
 * 
 * U 1054 - mixte avec Université de Montpellier et CNRS
 * Centre de Biochimie Structurale
 * Directeur : Pierre-Emmanuel Milhiet
 * 29, rue de Navacelles
 * 34090 MONTPELLIER
 * 
 * Recombinator project : http://recombinator.lirmm.fr/
 * 
 * Team : Synthetic biology at CBS : http://www.cbs.cnrs.fr/index.php/en/research-equipea4
 * 
 * 
 * @auhtor : Guillaume Pérution-Kihli 
 */

#ifndef _BIODEVICEGENERATOR_H
#define _BIODEVICEGENERATOR_H

#include "Generator.hpp"


class BioDeviceGenerator: public Generator 
{
protected: 
    BioDeviceGenerator(uint8_t nbInputs) : _nbInputs(nbInputs) {}
	uint8_t _nbInputs;
};

#endif //_BIODEVICEGENERATOR_H