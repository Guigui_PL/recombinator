/**
 * Copyright (C) Inserm Montpellier / CBS
 * 
 * U 1054 - mixte avec Université de Montpellier et CNRS
 * Centre de Biochimie Structurale
 * Directeur : Pierre-Emmanuel Milhiet
 * 29, rue de Navacelles
 * 34090 MONTPELLIER
 * 
 * Recombinator project : http://recombinator.lirmm.fr/
 * 
 * Team : Synthetic biology at CBS : http://www.cbs.cnrs.fr/index.php/en/research-equipea4
 * 
 * 
 * @auhtor : Guillaume Pérution-Kihli 
 */

#ifndef _SBDCONSTRAINTEXTENSION_H
#define _SBDCONSTRAINTEXTENSION_H
#include <vector>
#include <unordered_set>
#include "../api/Semantic.hpp"
#include "SemanticalBioDeviceConstraint.hpp"


namespace recombinator 
{
	namespace generator 
	{
		class SBDConstraintExtension: public SemanticalBioDeviceConstraint {
		public: 
			using Variable = SemanticalBioDeviceConstraint::Variable;
			
			SBDConstraintExtension(
				::recombinator::api::SemanticalBioDevice* bioDevice, 
				IEDyckSemanticalBioDeviceBuilder* builder, 
				std::vector<Variable> scope,
				std::unordered_set<std::vector<recombinator::api::Semantic>,boost::hash<std::vector<::recombinator::api::Semantic>>> auhtorizedTuples = {},
				std::string name = "SBDConstraintExtension"
			);
			
			/**
			* @param sems
			*/
			void setAuthorizedTuples(std::unordered_set<std::vector<::recombinator::api::Semantic>,boost::hash<std::vector<::recombinator::api::Semantic>>> sems);
			
			void addAuthorizedTuple(std::vector<::recombinator::api::Semantic> sems);
			
			const std::unordered_set<std::vector<::recombinator::api::Semantic>,boost::hash<std::vector<::recombinator::api::Semantic>>>& getAuthorizedTuples() const;
			
			bool forwardCheck();
			
			bool isViolated() const;
			
			std::string getName() const;
		protected: 
			std::unordered_set<std::vector<::recombinator::api::Semantic>,boost::hash<std::vector<::recombinator::api::Semantic>>> _authorizedTuples;
			std::string _name;
		};
	}
}

#endif //_SBDCONSTRAINTEXTENSION_H
