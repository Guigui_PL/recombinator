/**
 * Copyright (C) Inserm Montpellier / CBS
 * 
 * U 1054 - mixte avec Université de Montpellier et CNRS
 * Centre de Biochimie Structurale
 * Directeur : Pierre-Emmanuel Milhiet
 * 29, rue de Navacelles
 * 34090 MONTPELLIER
 * 
 * Recombinator project : http://recombinator.lirmm.fr/
 * 
 * Team : Synthetic biology at CBS : http://www.cbs.cnrs.fr/index.php/en/research-equipea4
 * 
 * 
 * @auhtor : Guillaume Pérution-Kihli 
 */

#include "CheckUtilityPrefixAndSuffix.hpp"

using namespace recombinator::api;
using namespace recombinator::generator;
using namespace std;

/**
 * CheckUtilityPrefixAndSuffix implementation
 */

CheckUtilityPrefixAndSuffix::CheckUtilityPrefixAndSuffix(::recombinator::api::SemanticalBioDevice* bioDevice, IEDyckSemanticalBioDeviceBuilder* builder, CheckUtility::Variable var) : 
CheckUtility(bioDevice, builder, var), 
CheckUtilityAfterPrefix(bioDevice, builder, var),
CheckUtilityBeforeSuffix(bioDevice, builder, var)
{
	_scope.erase(unique(_scope.begin(), _scope.end()), _scope.end());
	_derivedToCheck.erase(unique(_derivedToCheck.begin(), _derivedToCheck.end()), _derivedToCheck.end());
}

Semantic::Utility CheckUtilityPrefixAndSuffix::getUtilityOnDerived (uint8_t derived, const Semantic* s) const
{
	return static_cast<Semantic::Utility>(((uint8_t)CheckUtilityBeforeSuffix::getUtilityOnDerived(derived, s)) & ((uint8_t)CheckUtilityAfterPrefix::getUtilityOnDerived(derived, s)));
}

Semantic::Utility CheckUtilityPrefixAndSuffix::getUtility (const Semantic* s) const
{
	return CheckUtilityAfterPrefix::getUtility(s);
}

std::string CheckUtilityPrefixAndSuffix::getName() const
{
	return string("CheckUtilityPrefixAndSuffix - ")+to_string(_checkedVar);
}
