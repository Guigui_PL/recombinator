/**
 * Copyright (C) Inserm Montpellier / CBS
 * 
 * U 1054 - mixte avec Université de Montpellier et CNRS
 * Centre de Biochimie Structurale
 * Directeur : Pierre-Emmanuel Milhiet
 * 29, rue de Navacelles
 * 34090 MONTPELLIER
 * 
 * Recombinator project : http://recombinator.lirmm.fr/
 * 
 * Team : Synthetic biology at CBS : http://www.cbs.cnrs.fr/index.php/en/research-equipea4
 * 
 * 
 * @auhtor : Guillaume Pérution-Kihli 
 */

#ifndef _IEDYCKSEMANTICALBIODEVICEBUILDER_H
#define _IEDYCKSEMANTICALBIODEVICEBUILDER_H
#include <atomic>
#include <mutex>
#include <vector>
#include <string>
#include <unordered_set>
#include <boost/lockfree/spsc_queue.hpp>
#include <boost/property_tree/ptree.hpp>
#include "../api/BioDeviceFile.hpp"
#include "../api/DyckSemanticalBioDevice.hpp"
#include "../api/IEFunctionnalStructure.hpp"
#include "../api/Semantic.hpp"
#include "DyckSemanticalBioDeviceBuilder.hpp"
#include "SemanticalBioDeviceConstraintBuilder.hpp"
#include "Solver.hpp"

namespace recombinator 
{
	namespace generator 
	{
		class IEDyckSemanticalBioDeviceBuilder: public DyckSemanticalBioDeviceBuilder, public Solver<const ::recombinator::api::Semantic*> {
		public: 
			
			enum class Setting 
			{
				ALL_CONSTRAINTS, 
				UTILITY_CONSTRAINTS, 
				ATOMIC_SITES_CONSTRAINTS,
				IRREDUNDANCY_CONSTRAINT,
				NO_IPALINDROME_CONSTRAINT,
				NP_EQUIVALENT_CONSTRAINT
			};
			
			/**
			* @param bd
			*/
			IEDyckSemanticalBioDeviceBuilder(::recombinator::api::DyckSemanticalBioDevice* bd, const std::unordered_set<std::string> *dyckWords, std::unordered_set<Setting> s = {Setting::ALL_CONSTRAINTS});
			
			void launch();
			
			void treatBuiltSemanticalBioDevices();
			
			void generateConstraints();
			
			void generateUtilityConstraints();
			
			void generateAtomicSitesConstraints();
			
			void generateExpOnlyInExcisionConstraints();
			
			void generateNoIPalindromeConstraints();
			
			void generateNPEquivalentsConstraints();
			
			/**
			* @param i
			* @param val
			*/
			void assignVar(size_t i, const ::recombinator::api::Semantic* val);
			
			/**
			* @param i
			*/
			bool isAssignedVar(size_t i) const;
			
			void initDomains();
			
			void flush();
			
			const ::recombinator::api::DyckSemanticalBioDevice& getSBD() const;
			
			void updateState();
			
			::boost::property_tree::ptree getState() const;
			
			void restoreState(const ::boost::property_tree::ptree &state);
			
			size_t getNbGoodSBD() const;
			
			size_t getNbGoodIPalindrome() const;
			
			size_t getNbBadSBD() const;
			
			void treatCompleteAssignation();
			
			bool isSet (Setting setting) const;
			
			void removeLastAssignation ();
			
			size_t getNbVariables() const;
			
		private: 
			
			::boost::lockfree::spsc_queue<::std::vector<const ::recombinator::api::Semantic*>,::boost::lockfree::capacity<10000>> _generatedBioDevices;
			
			::boost::property_tree::ptree _state;
			mutable std::mutex _stateLock;
			
			::std::atomic<size_t> _nbGoodSBD{0};
			::std::atomic<size_t> _nbGoodIPalindrome{0};
			::std::atomic<size_t> _nbBadSBD{0};
			
			::std::atomic<size_t> _nbSBDToTreat{0};
			
			::std::unordered_set<Setting> _settings;
			
			::std::string _outputDirectory;
			::recombinator::api::DyckSemanticalBioDeviceFileWriter _goodSBDwriter;
			::recombinator::api::DyckSemanticalBioDeviceFileWriter _badSBDwriter;
			
			SemanticalBioDeviceConstraintBuilder _SBDConstraintBuilder;
			
			const std::unordered_set<std::string> *_dyckWords;
		};
	}
}

#endif //_IEDYCKSEMANTICALBIODEVICEBUILDER_H
