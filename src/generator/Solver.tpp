/**
 * Copyright (C) Inserm Montpellier / CBS
 * 
 * U 1054 - mixte avec Université de Montpellier et CNRS
 * Centre de Biochimie Structurale
 * Directeur : Pierre-Emmanuel Milhiet
 * 29, rue de Navacelles
 * 34090 MONTPELLIER
 * 
 * Recombinator project : http://recombinator.lirmm.fr/
 * 
 * Team : Synthetic biology at CBS : http://www.cbs.cnrs.fr/index.php/en/research-equipea4
 * 
 * 
 * @auhtor : Guillaume Pérution-Kihli 
 */

#include "Constraint.hpp"

/**
 * @return bool
 */
template<typename T>
recombinator::generator::Solver<T>::Solver(size_t nbVariables) : 
_domains(nbVariables),
_varToConstraints(nbVariables),
_nbVariables(nbVariables)
{
	
}

template<typename T>
void recombinator::generator::Solver<T>::checkConstraints() 
{
    for (auto &constraint : _varToConstraints[_lastAssignations.back()])
    //for (auto &constraint : _constraints)
	{
		if (constraint->isCheckable())
		{
			if (!constraint->forwardCheck())
				return;
		}
	}
}

template<typename T>
void recombinator::generator::Solver<T>::solve(size_t depth) 
{
	if (_stop)
	{
		return;
	}
	
	if (_hasToUpdateState)
	{
		updateState();
		_hasToUpdateState = false;
		_notifyUpdate->notify_all();
	}
	
	/*if (depth < _lastAssignations.size())
	{
		solve(depth+1);
	}*/
	Variable varToAssign{chooseNextAssignation()};
	//std::list<T> domainVar{_domains[varToAssign]};
	
	size_t i = 0;
	bool restore = false;
	if (_restoredLastAssignationIndexes.size() > depth && _restoredLastAssignationIndexes[depth] != (size_t)-1)
	{
		i = _restoredLastAssignationIndexes[depth];
		_restoredLastAssignationIndexes[depth] = (size_t)-1;
		varToAssign = _lastAssignations[depth];
		restore = true;
	}
	
	auto it = _domains[varToAssign].begin();
	if (i > 0)
	{
		for (size_t j = 0; j < i; ++j)
		{
			++it;
		}
	}
	
	//for (auto &it : _domains[varToAssign])
	for (; i < _domains[varToAssign].size(); ++i)
	{
		if (_stop)
		{
			return;
		}
		
		assignVar(varToAssign, *it);
		
		if (restore)
		{
			restore = false;
			_domainsRemovedElements.pop_back();
			_lastAssignations.pop_back();
			_domainsRemovedElements.push_back(_restoredDomainsRemovedElements[depth]);
		}
		
		_lastAssignationIndexes.push_back(i);
		
		if (_nbVariables != _lastAssignations.size())
		{
			checkConstraints();
			if (!existsEmptyDomain())
			{
				solve(depth+1);
			}
		}
		else 
		{
			++_exploredTreeSize;
			treatCompleteAssignation();
		}
		removeLastAssignation();
		_lastAssignationIndexes.pop_back();
		
		++it;
	}
	
	if (depth == 0)
	{
		_finished = true;
	}
}

template<typename T>
void recombinator::generator::Solver<T>::assignVar(size_t i, T val __attribute__((unused))) 
{
	_domainsRemovedElements.push_back(std::vector<::std::vector<T>>(getNbVariables()));
	_lastAssignations.push_back(i);
}

/**
 * @param constraint
 */

template<typename T>
void recombinator::generator::Solver<T>::addConstraint(Constraint<T>& constraint) 
{
	// We add the constraint
	_constraints.emplace_back(&constraint);
	
	// If it is a unary constraint, we add it to the list of unary constraints 
	if (_constraints.back()->isCheckable())
	{
		_unaryConstraints.push_back(_constraints.back().get());
	}
	
	// Finally, we link variables in the scope of the constraint to this constraint
	for (auto &var : _constraints.back()->getScope())
	{
		_varToConstraints[var].push_back(_constraints.back().get());
	}
}

template<typename T>
void recombinator::generator::Solver<T>::addConstraint(std::shared_ptr<Constraint<T>>& constraint) 
{
	// We add the constraint
	_constraints.emplace_back(std::move(constraint));
	
	// If it is a unary constraint, we add it to the list of unary constraints 
	if (_constraints.back()->isCheckable())
	{
		_unaryConstraints.push_back(_constraints.back().get());
	}
	
	// Finally, we link variables in the scope of the constraint to this constraint
	for (auto &var : _constraints.back()->getScope())
	{
		_varToConstraints[var].push_back(_constraints.back().get());
	}
}

/**
 * @param recombinator::generator::Solver<T>::Variable i
 * @return bool
 */

template<typename T>
bool recombinator::generator::Solver<T>::hasEmptyDomain(recombinator::generator::Solver<T>::Variable i) 
{
    return _domains[i].empty();
}

template<typename T>
bool recombinator::generator::Solver<T>::existsEmptyDomain()
{
	for (Variable i{0}; i < _nbVariables; ++i)
	{
		if (hasEmptyDomain(i))
		{
			return true;
		}
	}
	
	return false;
}

/**
 * @return recombinator::generator::Solver<T>::Variable
 */

template<typename T>
typename recombinator::generator::Solver<T>::Variable recombinator::generator::Solver<T>::chooseNextAssignation() 
{
    size_t tiniestDomain{static_cast<size_t>(-1)};
	Variable linkedVar{0};
	
	for (Variable i{0}; i < _nbVariables; ++i)
	{
		if (isAssignedVar(i))
			continue;
		
		if (_domains[i].size() < tiniestDomain)
		{
			tiniestDomain = _domains[i].size();
			linkedVar = i;
		}
	}
	
	return linkedVar;
}

template<typename T>
void recombinator::generator::Solver<T>::removeLastAssignation ()
{
	auto &domainsToRestore = _domainsRemovedElements.back();
	for (size_t i{0}; i < domainsToRestore.size(); ++i)
	{
		_domains[i].insert(_domains[i].end(), domainsToRestore[i].begin(), domainsToRestore[i].end());
		//domainsToRestore[i].clear();
	}
	
	_lastAssignations.pop_back();
	_domainsRemovedElements.pop_back();
}

template<typename T>
void recombinator::generator::Solver<T>::stop()
{
	_stop = true;
}

template<typename T>
bool recombinator::generator::Solver<T>::hasStopped() const
{
	return _hasStopped;
}

template<typename T>
bool recombinator::generator::Solver<T>::isFinished() const
{
	return _finished;
}

template<typename T>
void recombinator::generator::Solver<T>::removeUnaryConstraints()
{
	_domainsRemovedElements.push_back(std::vector<::std::vector<T>>(getNbVariables()));
	for (auto &constraint : _unaryConstraints)
	{
		constraint->forwardCheck();
		std::cout << (*constraint) << std::endl;
		
		for (auto &var : constraint->getCheckableScope())
		{
			for (size_t i{0}; i < _varToConstraints[var].size(); ++i)
			{
				if (_varToConstraints[var][i] == constraint)
				{
					_varToConstraints[var].erase(_varToConstraints[var].begin()+i);
					break;
				}
			}
		}
		
		for (size_t i{0}; i < _constraints.size(); ++i)
		{
			if (_constraints[i].get() == constraint)
			{
				_constraints.erase(_constraints.begin()+i);
				break;
			}
		}
	}
	
	_unaryConstraints.clear();
	_domainsRemovedElements.pop_back();
}

template<typename T>
void recombinator::generator::Solver<T>::addConstraints(::std::vector<::std::shared_ptr<Constraint<T>>> constraints)
{
	for (auto &constraint : constraints)
	{
		addConstraint(constraint);
	}
}

template<typename T>
size_t recombinator::generator::Solver<T>::getNbVariables() const
{
	return _nbVariables;
}

template<typename T>
size_t recombinator::generator::Solver<T>::getNbAssignations() const
{
	return _lastAssignations.size();
}

template<typename T>
void recombinator::generator::Solver<T>::removeDomainElement(recombinator::generator::Solver<T>::Variable var, typename std::list<T>::iterator& iterator)
{
	_domainsRemovedElements.back()[var].push_back(*iterator);
	_domains[var].erase(iterator);
	_exploredTreeSize += computePruningSize(var);
}

template<typename T>
void recombinator::generator::Solver<T>::removeDomainElement(recombinator::generator::Solver<T>::Variable var, typename std::list<T>::const_iterator& iterator)
{
	_domainsRemovedElements.back()[var].push_back(*iterator);
	_domains[var].erase(iterator);
	_exploredTreeSize += computePruningSize(var);
}

template<typename T>
const std::list<T>& recombinator::generator::Solver<T>::getDomain(recombinator::generator::Solver<T>::Variable var) const
{
	return _domains[var];
}

template<typename T>
const std::vector<typename recombinator::generator::Solver<T>::Variable> &recombinator::generator::Solver<T>::getLastAssignations() const
{
	return _lastAssignations;
}

template<typename T>
typename recombinator::generator::Solver<T>::Variable recombinator::generator::Solver<T>::getLastAssignedVar() const
{
	return _lastAssignations.back();
}

template<typename T>
void recombinator::generator::Solver<T>::printStatistics(std::ostream &os) const
{
	for (auto &c : _constraints)
	{
		c->printStatistics(os);
		os << std::endl;
	}
}

template<typename T>
void recombinator::generator::Solver<T>::computeExplorationTreeSize()
{
	_explorationTreeSize = 1;
	for (auto& tuples : _domains)
	{
		_explorationTreeSize *= tuples.size();
	}
	_exploredTreeSize = 0;
}

template<typename T>
size_t recombinator::generator::Solver<T>::getExplorationTreeSize() const
{
	return _explorationTreeSize;
}

template<typename T>
size_t recombinator::generator::Solver<T>::getProgress() const
{
	return _exploredTreeSize;
}

template<typename T>
size_t recombinator::generator::Solver<T>::computePruningSize(Variable variableOfDeletedElement)
{
	size_t pruningSize = 1;
	for (size_t i = 0; i < getNbVariables(); ++i)
	{
		if (isAssignedVar(i) || i == variableOfDeletedElement)
		{
			continue;
		}
		pruningSize *= getDomain(i).size();
	}
	return pruningSize;
}

template<typename T>
void recombinator::generator::Solver<T>::requestUpdateState(std::condition_variable* toNotify)
{
	if (isFinished())
	{
		toNotify->notify_all();
		return;
	}
	
	_notifyUpdate = toNotify;
	_hasToUpdateState = true;
}
