/**
 * Copyright (C) Inserm Montpellier / CBS
 * 
 * U 1054 - mixte avec Université de Montpellier et CNRS
 * Centre de Biochimie Structurale
 * Directeur : Pierre-Emmanuel Milhiet
 * 29, rue de Navacelles
 * 34090 MONTPELLIER
 * 
 * Recombinator project : http://recombinator.lirmm.fr/
 * 
 * Team : Synthetic biology at CBS : http://www.cbs.cnrs.fr/index.php/en/research-equipea4
 * 
 * 
 * @auhtor : Guillaume Pérution-Kihli 
 */

#include "CheckUtility.hpp"

using namespace recombinator::api;
using namespace recombinator::generator;
using namespace std;

/**
 * checkUtility implementation
 */

CheckUtility::CheckUtility(::recombinator::api::SemanticalBioDevice* bioDevice, IEDyckSemanticalBioDeviceBuilder* builder, Variable var) :
	SemanticalBioDeviceConstraint(bioDevice, builder),
	_checkedVar(var), 
	_checkableScope({var})
{
	
}
/*
bool CheckUtility::isCheckable() const
{
	if (_solver->isAssignedVar(_checkedVar))
	{
		return false;
	}
	
	for (const auto &var : getScope())
	{
		if (var != _checkedVar && !_solver->isAssignedVar(var))
		{
			return false;
		}
	}
	
	return true;
}*/

/**
 * @return bool
 */
bool CheckUtility::forwardCheck() 
{
	if (!_solver->isAssignedVar(_checkedVar))
	{
		for (auto it = _solver->getDomain(_checkedVar).begin(); it != _solver->getDomain(_checkedVar).end(); ++it)
		{
			if (getUtility(*it) != Semantic::Utility::UTILITY_REVERSE_AND_FORWARD)
			{
				auto copy = it;
				--it;
				_solver->removeDomainElement(_checkedVar, copy);
				++_nbRemovedElements[_checkedVar];
			}
		}
			
		return !_solver->hasEmptyDomain(_checkedVar);
	}
	else 
	{
		Variable unassigned = getUnassignedVar();
		
		for (auto it = _solver->getDomain(unassigned).begin(); it != _solver->getDomain(unassigned).end(); ++it)
		{
			_bioDevice->setSemantic(unassigned, *it);
			if (getUtility(&_bioDevice->getSemantic(_checkedVar)) != Semantic::Utility::UTILITY_REVERSE_AND_FORWARD)
			{
				auto copy = it;
				--it;
				_solver->removeDomainElement(unassigned, copy);
				++_nbRemovedElements[unassigned];
			}
			_bioDevice->setSemantic(unassigned, nullptr);
		}
			
		return !_solver->hasEmptyDomain(_checkedVar);
	}
}

/**
 * @return bool
 */
bool CheckUtility::isViolated() const
{
    return _solver->isAssignedVar(_checkedVar) && getUtility(&_bioDevice->getSemantic(_checkedVar)) != Semantic::Utility::UTILITY_REVERSE_AND_FORWARD;
}

const std::vector<SemanticalBioDeviceConstraint::Variable>& CheckUtility::getCheckableScope() const
{
	return _checkableScope;
}

size_t CheckUtility::getCheckableArity() const
{
	return _checkableScope.size();
}
