/**
 * Copyright (C) Inserm Montpellier / CBS
 * 
 * U 1054 - mixte avec Université de Montpellier et CNRS
 * Centre de Biochimie Structurale
 * Directeur : Pierre-Emmanuel Milhiet
 * 29, rue de Navacelles
 * 34090 MONTPELLIER
 * 
 * Recombinator project : http://recombinator.lirmm.fr/
 * 
 * Team : Synthetic biology at CBS : http://www.cbs.cnrs.fr/index.php/en/research-equipea4
 * 
 * 
 * @auhtor : Guillaume Pérution-Kihli 
 */

#include "CheckUtilityBeforeSuffix.hpp"

using namespace recombinator::api;
using namespace recombinator::generator;
using namespace std;

/**
 * CheckUtilityBeforeSuffix implementation
 */

const std::array<std::array<recombinator::api::Semantic::Utility,36>,36> CheckUtilityBeforeSuffix::_usefulBefore = CheckUtilityBeforeSuffix::buildUsefulBefore();

CheckUtilityBeforeSuffix::CheckUtilityBeforeSuffix(::recombinator::api::SemanticalBioDevice* bioDevice, IEDyckSemanticalBioDeviceBuilder* builder, CheckUtility::Variable var) : 
CheckUtility(bioDevice, builder, var)
{
	unordered_set<CheckUtility::Variable> scope{{var}};
	for (size_t i{0}; i < _bioDevice->getNbDerivedSemanticalBioDevice(); ++i)
	{
		unordered_set<CheckUtility::Variable> suffixVars;
		FunctionnalStructure::SemanticalBioDeviceStructure derived = _bioDevice->getStructure().getDerivedBioDevicesStructures()[i];
		bool found = false;
		
		if (!_bioDevice->getDerivedSemanticalBioDevice(i).isReversed(var))
		{
			for (auto it = derived.rbegin(); it != derived.rend(); ++it)
			{
				suffixVars.insert(abs(*it));
				if ((size_t)abs(*it) == var)
				{
					found = true;
					break;
				}
			}
		}
		else 
		{
			for (auto& it : derived)
			{
				suffixVars.insert(abs(it));
				if ((size_t)abs(it) == var)
				{
					found = true;
					break;
				}
			}
		}
		
		if (found)
		{
			scope.insert(suffixVars.begin(), suffixVars.end());
			_derivedToCheck.push_back(i);
		}
	}
	
	_scope.insert(_scope.end(), scope.begin(), scope.end());
}

Semantic::Utility CheckUtilityBeforeSuffix::getUtilityOnDerived (uint8_t derived, const Semantic* s) const
{
	Semantic suffix;
	if (_bioDevice->getDerivedSemanticalBioDevice(derived).isReversed(_checkedVar))
	{
		suffix = Semantic(_bioDevice->getPrefix(derived, _checkedVar)).reverse();
	}
	else
	{
		suffix = _bioDevice->getSuffix(derived, _checkedVar);
	}
	
	return _usefulBefore[s->getSemanticKey()][suffix.getSemanticKey()];
}

Semantic::Utility CheckUtilityBeforeSuffix::getUtility (const Semantic* s) const
{
	if (s->isGF() && s->isPF())
		return static_cast<Semantic::Utility>((uint8_t)getUtility(Semantic::bioFunctionToPointer[(size_t)Semantic::BF::P][(size_t)s->getSemanticReverse().getBioFunction()]) & 
			(uint8_t)getUtility(Semantic::bioFunctionToPointer[(size_t)Semantic::BF::G][(size_t)s->getSemanticReverse().getBioFunction()]));
	if (s->isGR() && s->isPR())
		return static_cast<Semantic::Utility>((uint8_t)getUtility(Semantic::bioFunctionToPointer[(size_t)s->getSemanticForward().getBioFunction()][(size_t)Semantic::BF::P]) & 
			(uint8_t)getUtility(Semantic::bioFunctionToPointer[(size_t)s->getSemanticForward().getBioFunction()][(size_t)Semantic::BF::G]));
	if (s->isX())
		return Semantic::Utility::UTILITY_REVERSE_AND_FORWARD;
		
	uint8_t utility = (uint8_t) Semantic::Utility::USELESS;
	for (auto& idDer : _derivedToCheck)
	{
		utility |= (uint8_t) getUtilityOnDerived(idDer, s);
		if (s->isX())
		{
			cout << (string)*s << "." << (string)_bioDevice->getSuffix(idDer, _checkedVar) << " " << to_string((uint8_t) getUtilityOnDerived(idDer, s)) << endl;
		}
		if (utility == (uint8_t) Semantic::Utility::UTILITY_REVERSE_AND_FORWARD)
		{
			break;
		}
	}
	return static_cast<Semantic::Utility>(utility);
}

std::array<std::array<recombinator::api::Semantic::Utility,36>,36> CheckUtilityBeforeSuffix::buildUsefulBefore() 
{
    std::array<std::array<recombinator::api::Semantic::Utility,36>,36> usefulBefore;
    
    for (auto &sem1 : Semantic::allSemantics)
    {
        for (auto &sem2 : Semantic::allSemantics)
        {
            usefulBefore[sem1.getSemanticKey()][sem2.getSemanticKey()] = sem1.isUsefulBeforeSuffix(sem2);
        }
    }
    
    return usefulBefore;
}

std::string CheckUtilityBeforeSuffix::getName() const
{
	return string("CheckUtilityBeforeSuffix - ")+to_string(_checkedVar);
}
/*
bool CheckUtilityBeforeSuffix::forwardCheck() 
{
	if (!_solver->isAssignedVar(_checkedVar))
	{
		return CheckUtility::forwardCheck();
	}
	else 
	{
		Variable unassigned = getUnassignedVar();
		
		for (auto it = _solver->getDomain(unassigned).begin(); it != _solver->getDomain(unassigned).end(); ++it)
		{
			_bioDevice->setSemantic(unassigned, *it);
			if (getUtility(&_bioDevice->getSemantic(_checkedVar)) != Semantic::Utility::UTILITY_REVERSE_AND_FORWARD)
			{
				auto copy = it;
				--it;
				_solver->removeDomainElement(unassigned, copy);
				++_nbRemovedElements[unassigned];
			}
			_bioDevice->setSemantic(unassigned, nullptr);
		}
			
		return !_solver->hasEmptyDomain(_checkedVar);
	}
}*/
