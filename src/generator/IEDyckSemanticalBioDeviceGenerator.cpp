/**
 * Copyright (C) Inserm Montpellier / CBS
 * 
 * U 1054 - mixte avec Université de Montpellier et CNRS
 * Centre de Biochimie Structurale
 * Directeur : Pierre-Emmanuel Milhiet
 * 29, rue de Navacelles
 * 34090 MONTPELLIER
 * 
 * Recombinator project : http://recombinator.lirmm.fr/
 * 
 * Team : Synthetic biology at CBS : http://www.cbs.cnrs.fr/index.php/en/research-equipea4
 * 
 * 
 * @auhtor : Guillaume Pérution-Kihli 
 */

#include "IEDyckSemanticalBioDeviceGenerator.hpp"
#include <exception>
#include <fstream>
#include <boost/filesystem.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include "../api/IEDyckFunctionnalStructure.hpp"

using namespace recombinator::api;
using namespace recombinator::generator;
using namespace std;
namespace fs = boost::filesystem;
namespace pt = boost::property_tree;

/**
 * IEDyckSemanticalBioDeviceGenerator implementation
 */


/**
 * @param nbInputs
 */
IEDyckSemanticalBioDeviceGenerator::IEDyckSemanticalBioDeviceGenerator(uint8_t nbInputs, 
									   std::vector<Setting> settings) :
	DyckSemanticalBioDeviceGenerator(nbInputs),
	_settings(settings.begin(), settings.end()),
	_saveFile(string("work/saves/ieDyck_")+to_string(nbInputs)+".json"),
	_chronoStart(chrono::system_clock::now())
{
	prepareDirectories();
}

void IEDyckSemanticalBioDeviceGenerator::launchGeneration() 
{
	if (existsSavedState() && askToContinue())
	{
		restoreState();
	}
	else 
	{
		makeDyckWords();
		makeDyckSemanticalBioDevices();
		makeBuilders();
	}
	
	// Get the thread manager 
	auto &threadsManager = ThreadsManager::getThreadsManager();
	
	// Launch the thread which print the statistics during the execution
	ThreadsManager::ThreadId idDisplayer = threadsManager.addThread(&IEDyckSemanticalBioDeviceGenerator::displayStats, this);
	
	for (auto &builder : _builders)
	{
		if (_stop)
			break;
		
		_launchedThreads.push_back(threadsManager.addThread(&IEDyckSemanticalBioDeviceBuilder::launch, builder.get()));
		_launchedThreads.push_back(threadsManager.addThread(&IEDyckSemanticalBioDeviceBuilder::treatBuiltSemanticalBioDevices, builder.get()));
	}
	
	for (auto &id : _launchedThreads)
	{
		threadsManager.wait(id);
	}
	
	_stop = true;
	threadsManager.wait(idDisplayer);
	updateStats();
	
	for (auto &builder : _builders)
	{
		builder->flush();
	}
	
	cout << "The generation is finished!" << endl;
	cout << "Total time : " << getElapsedTime() << " seconds." << endl;
	cout << _nbGoodSBD << " good SDBs \t" <<
		_nbGoodIPalindrome << " IPalindromes \t" <<
		_nbBadSBD << " bad SBDs \t" << endl;
	_notifyFinished.notify_all();
}

void IEDyckSemanticalBioDeviceGenerator::updateStats()
{
	_nbGoodSBD = _nbBadSBD = _nbGoodIPalindrome = 0;
	
	for (auto &builder : _builders)
	{
		_nbBadSBD += builder->getNbBadSBD();
		_nbGoodSBD += builder->getNbGoodSBD();
		_nbGoodIPalindrome += builder->getNbGoodIPalindrome();
	}
}

void IEDyckSemanticalBioDeviceGenerator::displayStats()
{
	while (!_stop)
	{
		updateStats();
		size_t elapsedTime{getElapsedTime()}, domainSize{0};
		float percentProgress{0};
		
		for (auto &builder : _builders)
		{
			percentProgress += builder->getProgress();
			domainSize += builder->getExplorationTreeSize();
		}
		
		percentProgress = percentProgress / domainSize * 100;
		
		float estimatedTime = elapsedTime * (100/percentProgress) - elapsedTime;
		
		cout << _nbGoodSBD << " good SDBs \t" <<
		_nbGoodIPalindrome << " IPalindromes \t" <<
		_nbBadSBD << " bad SBDs \t" <<
		"Progress : " << percentProgress << "% \t" <<
		"Estimated remaining time : " << static_cast<size_t>(estimatedTime) << "s \t" <<
		"Elapsed time : " << static_cast<size_t>(elapsedTime) << "s" << endl;
		
		for (size_t i(0); i < 5; ++i)
		{
			std::this_thread::sleep_for(std::chrono::seconds(1));
			
			elapsedTime = getElapsedTime();
			if (static_cast<int>(elapsedTime) % 3600 == 0)
			{
				for (auto &builder : _builders)
				{
					builder->flush();
				}
				saveState();
			}
			if (_stop)
				break;
		}
	}
}

void IEDyckSemanticalBioDeviceGenerator::stopGeneration() 
{
	cout << "The generation is stopping..." << endl;
	_stop = true;
	for (auto &builder : _builders)
	{
		builder->stop();
	}
	
	cout << "Waiting the end of the threads..." << endl;
	
	/*auto &threadsManager = ThreadsManager::getThreadsManager();
	for (auto &id : _launchedThreads)
	{
		cout << id << endl;
		threadsManager.wait(id);
		cout << id << endl;
	}*/
	std::mutex m;
	std::unique_lock<std::mutex> lk(m);
	_notifyFinished.wait(lk);
	
	cout << "The generation is being saved..." << endl;
	saveState();
	
	cout << "Generation stopped !" << endl;
}

size_t IEDyckSemanticalBioDeviceGenerator::getElapsedTime() const
{
	return static_cast<size_t>(
		chrono::duration_cast<chrono::seconds>(chrono::system_clock::now() - _chronoStart).count())
		+ _restoredElapsedTime;
}

bool IEDyckSemanticalBioDeviceGenerator::askToContinue() const
{
	string answer;
	
	do
	{
		cout << "Do you want to continue the last generation ? (Y / n)" << endl;
		cin >> answer;
		
		if (answer == "Y" || answer == "y" || answer.empty())
		{
			return true;
		}
	}
	while (answer != "Y" && answer != "N" && answer != "n" && answer != "y" && !answer.empty());
	
	return false;
}

bool IEDyckSemanticalBioDeviceGenerator::existsSavedState() const
{
	return fs::exists(fs::path(_saveFile));
}

void IEDyckSemanticalBioDeviceGenerator::saveState() 
{
	fs::path saveFileParentPath{fs::path{_saveFile}.parent_path()};
	
	if (!fs::exists(saveFileParentPath) && !fs::create_directories(saveFileParentPath))
	{
		throw runtime_error("The program can't save its progression : it can't create the directory for the saves.");
	}
	
	string pathTmpSave = saveFileParentPath.string()+"/~dyck"+to_string(_nbInputs)+".json";
	
	pt::ptree save, builders;
    save.put("nbGoodSBD", _nbGoodSBD);
    save.put("nbBadSBD", _nbBadSBD);
    save.put("elapsedTime", getElapsedTime());
	
	for (auto &builder : _builders)
	{
		std::mutex m;
		std::condition_variable cv;
		builder->requestUpdateState(&cv);
		std::unique_lock<std::mutex> lk(m);
		cv.wait_for(lk, std::chrono::milliseconds(1000));
		builders.push_back(std::make_pair(builder->getSBD().getStructure().getStructure(), builder->getState()) );
	}
	
	save.add_child("builders", builders);
    pt::write_json(pathTmpSave, save);
	
	/*if (!fs::remove(fs::path{_saveFile}))
	{
		throw runtime_error("The program can't save its progression.");
	}
	else
	{*/
		fs::rename(fs::path{pathTmpSave}, fs::path{_saveFile});
	//}
	
	fs::remove(fs::path{pathTmpSave});
}

void IEDyckSemanticalBioDeviceGenerator::restoreState() 
{
	if (!fs::exists(_saveFile))
	{
		throw runtime_error("The program can't restore its previous state : there is no save file.");
	}
	
    pt::ptree save;
    pt::read_json(_saveFile, save);
	
    _nbGoodSBD = save.get<size_t>("nbGoodSBD");
    _nbBadSBD = save.get<size_t>("nbBadSBD");
    _restoredElapsedTime = save.get<size_t>("elapsedTime");
	
    for (auto &it : save.get_child("builders"))
	{
		string dw = it.first.data();
		_dyckWords.insert(dw);
		
		auto fs = IEDyckFunctionnalStructure::create<IEDyckFunctionnalStructure>(dw);
		_semanticalBioDevices.emplace_back(*fs);
		_builders.emplace_back(make_shared<IEDyckSemanticalBioDeviceBuilder>(&(_semanticalBioDevices.back()), &_dyckWords));
		_builders.back()->restoreState(it.second);
	}
}

void IEDyckSemanticalBioDeviceGenerator::makeDyckWords() 
{
	unordered_set<string> dyckWords;
	dyckWords.insert(string());
	
	bool noStop = true;
	
	while (noStop)
	{
		noStop = false;
		
		for (auto &it : dyckWords)
		{
			if (it.size() < _nbInputs*2)
			{
				string newDyck = "(" + it + ")";
				string newDyck2 = "[" + it + "]";
				
				if (!(dyckWords.find(newDyck) != dyckWords.end()))
				{
					dyckWords.insert(newDyck);
					dyckWords.insert(newDyck2);
					
					noStop = true;
				}
			}
		}
		
		for (auto &it : dyckWords)
		{
			for (auto &it2 : dyckWords)
			{
				if (it.size() + it2.size() <= _nbInputs*2)
				{
					string newDyck = it + it2;
					
					if (!(dyckWords.find(newDyck) != dyckWords.end()) && !(dyckWords.find(newDyck) != dyckWords.end()))
					{
						dyckWords.insert(newDyck);
						
						noStop = true;
					}
				}
			}
		}
	}
	
	for (auto &it : dyckWords)
	{
		if (it.size() == _nbInputs*2
			&& _dyckWords.find(it) == _dyckWords.end()
			&& (!(isSet(Setting::NO_IPALINDROME_CONSTRAINT) || isSet(Setting::ALL_CONSTRAINTS) )
				|| _dyckWords.find(reverseDyckWord(it)) == _dyckWords.end()))
			_dyckWords.insert(it);
	}
}

void IEDyckSemanticalBioDeviceGenerator::makeDyckSemanticalBioDevices() 
{
	unordered_set<string> generatedList;
	
	for (auto &dw : _dyckWords)
	{
		if (!(isSet(Setting::NP_EQUIVALENT_CONSTRAINT) || isSet(Setting::ALL_CONSTRAINTS))
			|| (generatedList.find(dw) == generatedList.end())
		)
		{
			auto fs = IEDyckFunctionnalStructure::create<IEDyckFunctionnalStructure>(dw);
			_semanticalBioDevices.emplace_back(*fs);
			
			if (isSet(Setting::NP_EQUIVALENT_CONSTRAINT) || isSet(Setting::ALL_CONSTRAINTS))
			{
				for (auto &it : fs->getStructuresOnDerived())
				{
					try 
					{
						auto derivedFs = IEDyckFunctionnalStructure::create<IEDyckFunctionnalStructure>(it);
						generatedList.insert(derivedFs->getStructure());
					}
					catch (...) {}
				}
			}
		}
	}
}

void IEDyckSemanticalBioDeviceGenerator::makeBuilders() 
{
	for (auto &sbd : _semanticalBioDevices)
	{
		_builders.emplace_back(make_shared<IEDyckSemanticalBioDeviceBuilder>(&sbd, &_dyckWords));
	}
}

std::string IEDyckSemanticalBioDeviceGenerator::reverseDyckWord(std::string dyckWord)
{
	string dyckWordReverse;
	for (auto it = dyckWord.rbegin(); it != dyckWord.rend(); ++it)
	{
		switch (*it)
		{
			case '(': dyckWordReverse.push_back(')'); break;
			case ')': dyckWordReverse.push_back('('); break;
			case '[': dyckWordReverse.push_back(']'); break;
			case ']': dyckWordReverse.push_back('['); break;
		}
	}
	
	return dyckWordReverse; 
}

void IEDyckSemanticalBioDeviceGenerator::prepareDirectories()
{
	fs::path p("work/"+to_string(_nbInputs));
	if (!fs::exists(p))
		fs::create_directory(p);
	
	fs::path good(p), bad(p);
	good += fs::path("/good");
	bad += fs::path("/bad");
	
	if (!fs::exists(good))
		fs::create_directory(good);
	if (!fs::exists(bad))
		fs::create_directory(bad);
}

bool IEDyckSemanticalBioDeviceGenerator::isSet (Setting setting) const
{
	return _settings.find(setting) != _settings.end();
}
