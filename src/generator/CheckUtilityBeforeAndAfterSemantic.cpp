/**
 * Copyright (C) Inserm Montpellier / CBS
 * 
 * U 1054 - mixte avec Université de Montpellier et CNRS
 * Centre de Biochimie Structurale
 * Directeur : Pierre-Emmanuel Milhiet
 * 29, rue de Navacelles
 * 34090 MONTPELLIER
 * 
 * Recombinator project : http://recombinator.lirmm.fr/
 * 
 * Team : Synthetic biology at CBS : http://www.cbs.cnrs.fr/index.php/en/research-equipea4
 * 
 * 
 * @auhtor : Guillaume Pérution-Kihli 
 */

#include "CheckUtilityBeforeAndAfterSemantic.hpp"
#include "CheckUtilityAfterSemantic.hpp"
#include "CheckUtilityBeforeSemantic.hpp"

using namespace recombinator::api;
using namespace recombinator::generator;
using namespace std;

/**
 * CheckUtilityBeforeAndAfterSemantic implementation
 */

CheckUtilityBeforeAndAfterSemantic::CheckUtilityBeforeAndAfterSemantic(::recombinator::api::SemanticalBioDevice* bioDevice, IEDyckSemanticalBioDeviceBuilder* builder, CheckUtility::Variable var) : 
CheckUtility(bioDevice, builder, var)
{
	unordered_set<CheckUtility::Variable> scope{{var}};
	set<tuple<int,int,int>> checkList;
	for (size_t i{0}; i < _bioDevice->getNbDerivedSemanticalBioDevice(); ++i)
	{
		FunctionnalStructure::SemanticalBioDeviceStructure derived = _bioDevice->getStructure().getDerivedBioDevicesStructures()[i];
		
		for (size_t j{1}; j < derived.size()-1; ++j)
		{
			if ((size_t)abs(derived[j]) == var)
			{
				scope.insert(abs(derived[j+1]));
				scope.insert(abs(derived[j-1]));
				checkList.insert(make_tuple(derived[j-1], derived[j], derived[j+1]));
			}
		}
	}
	
	_scope.insert(_scope.end(),scope.begin(), scope.end());
	_checkList.insert(_checkList.end(), checkList.begin(), checkList.end());
}

Semantic::Utility CheckUtilityBeforeAndAfterSemantic::getUtility (const Semantic* s) const
{
	if (s->isGF() && s->isPF())
		return static_cast<Semantic::Utility>((uint8_t)getUtility(Semantic::bioFunctionToPointer[(size_t)Semantic::BF::P][(size_t)s->getSemanticReverse().getBioFunction()]) & 
			(uint8_t)getUtility(Semantic::bioFunctionToPointer[(size_t)Semantic::BF::G][(size_t)s->getSemanticReverse().getBioFunction()]));
	if (s->isGR() && s->isPR())
		return static_cast<Semantic::Utility>((uint8_t)getUtility(Semantic::bioFunctionToPointer[(size_t)s->getSemanticForward().getBioFunction()][(size_t)Semantic::BF::P]) & 
			(uint8_t)getUtility(Semantic::bioFunctionToPointer[(size_t)s->getSemanticForward().getBioFunction()][(size_t)Semantic::BF::G]));
	if (s->isX())
		return Semantic::Utility::UTILITY_REVERSE_AND_FORWARD;
		
	uint8_t utility = (uint8_t) Semantic::Utility::USELESS;
	
	for (auto& it : _checkList)
	{
		if (get<1>(it) >= 0)
		{
			Semantic after = _bioDevice->getSemantic(abs(get<2>(it)));
			if (get<2>(it) < 0)
			{
				after.reverse();
			}
			Semantic before = _bioDevice->getSemantic(abs(get<0>(it)));
			if (get<0>(it) < 0)
			{
				before.reverse();
			}
			
			utility |= (uint8_t) CheckUtilityBeforeSemantic::_usefulBefore[s->getSemanticKey()][after.getSemanticKey()] 
				& (uint8_t) CheckUtilityAfterSemantic::_usefulAfter[s->getSemanticKey()][before.getSemanticKey()];
		}
		else 
		{
			Semantic after = _bioDevice->getSemantic(abs(get<0>(it)));
			if (get<0>(it) >= 0)
			{
				after.reverse();
			}
			Semantic before = _bioDevice->getSemantic(abs(get<2>(it)));
			if (get<2>(it) >= 0)
			{
				before.reverse();
			}
			
			utility |= (uint8_t) CheckUtilityBeforeSemantic::_usefulBefore[s->getSemanticKey()][after.getSemanticKey()] 
				& (uint8_t) CheckUtilityAfterSemantic::_usefulAfter[s->getSemanticKey()][before.getSemanticKey()];
		}
	}
	
	return static_cast<Semantic::Utility>(utility);
}

std::string CheckUtilityBeforeAndAfterSemantic::getName() const
{
	return string("CheckUtilityBeforeAndAfterSemantic - ")+to_string(_checkedVar);
}
 
