/**
 * Copyright (C) Inserm Montpellier / CBS
 * 
 * U 1054 - mixte avec Université de Montpellier et CNRS
 * Centre de Biochimie Structurale
 * Directeur : Pierre-Emmanuel Milhiet
 * 29, rue de Navacelles
 * 34090 MONTPELLIER
 * 
 * Recombinator project : http://recombinator.lirmm.fr/
 * 
 * Team : Synthetic biology at CBS : http://www.cbs.cnrs.fr/index.php/en/research-equipea4
 * 
 * 
 * @auhtor : Guillaume Pérution-Kihli 
 */

#ifndef _SOLVER_H
#define _SOLVER_H
#include <atomic>
#include <condition_variable>
#include <list>
#include <memory>
#include <vector>
#include "Constraint.hpp"

namespace recombinator 
{
	namespace generator 
	{
		template<typename T>
		class Solver;
	}
}

template<typename T>
class recombinator::generator::Solver 
{
public: 
	using Variable = size_t;
	
	Solver(size_t nbVariables);
	
	/**
	* @param i
	* @param val
	*/
	virtual void assignVar(Variable i, T val);
	
	virtual void checkConstraints();
	
	virtual void solve(size_t depth = 0);
	
	/**
	* @param Variable i
	*/
	virtual bool isAssignedVar(Variable i) const = 0;
	
	/**
	* @param constraint
	*/
	virtual void addConstraint(Constraint<T>& constraint);
	
	virtual void addConstraint(std::shared_ptr<Constraint<T>>& constraint);
	
	virtual void addConstraints(::std::vector<::std::shared_ptr<Constraint<T>>> constraints);
	
	/**
	* @param Variable i
	*/
	virtual bool hasEmptyDomain(Variable i);
	
	virtual bool existsEmptyDomain();
	
	virtual Variable chooseNextAssignation();
	
	virtual void stop();
	
	virtual bool hasStopped() const;
	
	virtual void treatCompleteAssignation() = 0;
	
	virtual void removeLastAssignation ();
	
	virtual bool isFinished() const;
	
	virtual void removeUnaryConstraints();
	
	virtual size_t getNbVariables() const;
	
	virtual size_t getNbAssignations() const;
	
	virtual void removeDomainElement(Variable var, typename std::list<T>::iterator& iterator);
	
	virtual void removeDomainElement(Variable var, typename std::list<T>::const_iterator& iterator);
	
	virtual const std::list<T>& getDomain(Variable var) const;
	
	virtual const std::vector<Variable> &getLastAssignations() const;
	virtual Variable getLastAssignedVar() const;
	
	virtual size_t getExplorationTreeSize() const;
	
	virtual size_t getProgress() const;
	
	virtual void printStatistics(std::ostream &os) const;
	
	virtual size_t computePruningSize(Variable variableOfDeletedElement);
	
	virtual void requestUpdateState(std::condition_variable* toNotify);
	
	virtual void updateState() = 0;
protected: 
	void computeExplorationTreeSize();
			
	::std::vector<::std::list<T>> _domains;
	::std::vector<::std::vector<::std::vector<T>>> _domainsRemovedElements;
	::std::vector<Variable> _lastAssignations;
	::std::vector<size_t> _lastAssignationIndexes;
	::std::vector<Constraint<T>*> _unaryConstraints;
	::std::vector<::std::vector<Constraint<T>*>> _varToConstraints;
	::std::vector<::std::shared_ptr<Constraint<T>>> _constraints;
	
	::std::atomic<bool> _stop{false};
	::std::atomic<bool> _hasStopped{false};
	::std::atomic<bool> _finished{false};
	size_t _nbVariables;
	
	size_t _explorationTreeSize{1};
	::std::atomic<size_t> _exploredTreeSize{0};
	
	std::atomic<bool> _hasToUpdateState{false};
	std::condition_variable* _notifyUpdate{nullptr};
	
	::std::vector<::std::vector<::std::vector<T>>> _restoredDomainsRemovedElements;
	::std::vector<size_t> _restoredLastAssignationIndexes;
};
		
#include "Solver.tpp"

#endif //_SOLVER_H
