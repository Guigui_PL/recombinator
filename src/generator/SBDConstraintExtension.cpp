/**
 * Copyright (C) Inserm Montpellier / CBS
 * 
 * U 1054 - mixte avec Université de Montpellier et CNRS
 * Centre de Biochimie Structurale
 * Directeur : Pierre-Emmanuel Milhiet
 * 29, rue de Navacelles
 * 34090 MONTPELLIER
 * 
 * Recombinator project : http://recombinator.lirmm.fr/
 * 
 * Team : Synthetic biology at CBS : http://www.cbs.cnrs.fr/index.php/en/research-equipea4
 * 
 * 
 * @auhtor : Guillaume Pérution-Kihli 
 */

#include "SBDConstraintExtension.hpp"

using namespace recombinator::api;
using namespace recombinator::generator;
using namespace std;

/**
 * SBDConstraintExtension implementation
 */

SBDConstraintExtension::SBDConstraintExtension(
	::recombinator::api::SemanticalBioDevice* bioDevice, 
	IEDyckSemanticalBioDeviceBuilder* builder, 
	std::vector<Variable> scope,
	std::unordered_set<std::vector<recombinator::api::Semantic>,boost::hash<std::vector<::recombinator::api::Semantic>>> auhtorizedTuples,
	std::string name
) :
	SemanticalBioDeviceConstraint(bioDevice, builder),
	_authorizedTuples(auhtorizedTuples),
	_name(name)
{
	_scope = scope;
	
	for (auto &it : _authorizedTuples)
	{
		if (it.size() != _scope.size())
		{
			throw std::invalid_argument("There is a tuple of a bad size");
		}
	}
}

/**
 * @param sems
 */
void SBDConstraintExtension::setAuthorizedTuples(unordered_set<vector<Semantic>,boost::hash<std::vector<::recombinator::api::Semantic>>> sems) 
{
	for (auto &it : sems)
	{
		addAuthorizedTuple(it);
	}
}

void SBDConstraintExtension::addAuthorizedTuple(vector<Semantic> sems)
{
	if (sems.size() != _scope.size())
	{
		throw std::invalid_argument("There is a tuple of a bad size");
	}
	
	_authorizedTuples.insert(sems);
}

/**
 * @return vector<Semantic>
 */
const unordered_set<vector<Semantic>,boost::hash<std::vector<::recombinator::api::Semantic>>>& SBDConstraintExtension::getAuthorizedTuples() const 
{
    return _authorizedTuples;
}

/**
 * @return bool
 */
bool SBDConstraintExtension::forwardCheck() 
{
	size_t indexUnassignedVar = getIndexUnassignedVar();
	Variable unassignedVar = _scope[indexUnassignedVar];
	vector<Semantic> researchedTuple(_scope.size());
	
	for (size_t i{0}; i < _scope.size(); ++i)
	{
		if (i != indexUnassignedVar)
		{
			researchedTuple[i] = _bioDevice->getSemantic(_scope[i]);
		}
	}
	
	for (auto it = _solver->getDomain(unassignedVar).begin(); it != _solver->getDomain(unassignedVar).end(); ++it)
	{
		researchedTuple[indexUnassignedVar] = **it;
		if (_authorizedTuples.find(researchedTuple) == _authorizedTuples.end())
		{
			auto copy = it;
			--it;
			_solver->removeDomainElement(unassignedVar, copy);
			++_nbRemovedElements[unassignedVar];
		}
	}
	
    return !_solver->hasEmptyDomain(unassignedVar);
}

/**
 * @return bool
 */
bool SBDConstraintExtension::isViolated() const 
{
	vector<Semantic> researchedTuple(_scope.size());
	
	for (size_t i{0}; i < _scope.size(); ++i)
	{
		researchedTuple[i] = _bioDevice->getSemantic(i);
	}
	
    return _authorizedTuples.find(researchedTuple) == _authorizedTuples.end();
}

std::string SBDConstraintExtension::getName() const
{
	return _name;
}

