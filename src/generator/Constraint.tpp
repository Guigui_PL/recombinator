/**
 * Copyright (C) Inserm Montpellier / CBS
 * 
 * U 1054 - mixte avec Université de Montpellier et CNRS
 * Centre de Biochimie Structurale
 * Directeur : Pierre-Emmanuel Milhiet
 * 29, rue de Navacelles
 * 34090 MONTPELLIER
 * 
 * Recombinator project : http://recombinator.lirmm.fr/
 * 
 * Team : Synthetic biology at CBS : http://www.cbs.cnrs.fr/index.php/en/research-equipea4
 * 
 * 
 * @auhtor : Guillaume Pérution-Kihli 
 */

#include "Solver.hpp"
/**
 * @param solver
 */
template <typename T>
recombinator::generator::Constraint<T>::Constraint(recombinator::generator::Solver<T>* solver) : _solver(solver)
{}

template <typename T>
const std::vector<typename recombinator::generator::Constraint<T>::Variable>& recombinator::generator::Constraint<T>::getCheckableScope() const
{
	return getScope();
}

template<typename T>
std::ostream& operator<< (std::ostream& os, recombinator::generator::Constraint<T> &c)
{
	c.printStatistics(os);
	return os;
}