/**
 * Copyright (C) Inserm Montpellier / CBS
 * 
 * U 1054 - mixte avec Université de Montpellier et CNRS
 * Centre de Biochimie Structurale
 * Directeur : Pierre-Emmanuel Milhiet
 * 29, rue de Navacelles
 * 34090 MONTPELLIER
 * 
 * Recombinator project : http://recombinator.lirmm.fr/
 * 
 * Team : Synthetic biology at CBS : http://www.cbs.cnrs.fr/index.php/en/research-equipea4
 * 
 * 
 * @auhtor : Guillaume Pérution-Kihli 
 */

#ifndef _CHECKUTILITYBEFORESUFFIX_H
#define _CHECKUTILITYBEFORESUFFIX_H
#include "CheckUtility.hpp"

namespace recombinator 
{
	namespace generator 
	{
		class CheckUtilityBeforeSuffix: public virtual CheckUtility {
		public: 
			CheckUtilityBeforeSuffix(::recombinator::api::SemanticalBioDevice* bioDevice, IEDyckSemanticalBioDeviceBuilder* builder, CheckUtility::Variable var);
			
			std::string getName() const;
			
			//bool forwardCheck();
		protected: 
			static const std::array<std::array<recombinator::api::Semantic::Utility,36>,36> _usefulBefore;
			std::vector<size_t> _derivedToCheck;
			
			virtual recombinator::api::Semantic::Utility getUtilityOnDerived (uint8_t derived, const recombinator::api::Semantic* s) const;
			
			recombinator::api::Semantic::Utility getUtility (const recombinator::api::Semantic* s) const;
			
			static std::array<std::array<recombinator::api::Semantic::Utility,36>,36> buildUsefulBefore();
		};
	}
}

#endif //_CHECKUTILITYBEFORESUFFIX_H
