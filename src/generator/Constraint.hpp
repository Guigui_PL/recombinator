/**
 * Copyright (C) Inserm Montpellier / CBS
 * 
 * U 1054 - mixte avec Université de Montpellier et CNRS
 * Centre de Biochimie Structurale
 * Directeur : Pierre-Emmanuel Milhiet
 * 29, rue de Navacelles
 * 34090 MONTPELLIER
 * 
 * Recombinator project : http://recombinator.lirmm.fr/
 * 
 * Team : Synthetic biology at CBS : http://www.cbs.cnrs.fr/index.php/en/research-equipea4
 * 
 * 
 * @auhtor : Guillaume Pérution-Kihli 
 */

#ifndef _CONSTRAINT_H
#define _CONSTRAINT_H
#include <vector>
#include <string>

namespace recombinator 
{
	namespace generator 
	{
		template<typename T>
		class Solver;
		
		template<typename T>
		class Constraint;
	}
}

template<typename T>
class recombinator::generator::Constraint 
{
public: 
	using Variable = typename Solver<T>::Variable;
	
	/**
	* @param solver
	*/
	Constraint(Solver<T>* solver);
	
	virtual bool forwardCheck() = 0;
	
	virtual size_t getArity() const = 0;
	
	virtual size_t getCheckableArity() const = 0;
	
	virtual bool isViolated() const = 0;
	
	virtual bool isCheckable() const = 0;
	
	virtual const ::std::vector<Variable>& getScope() const = 0;
	
	virtual const ::std::vector<Variable>& getCheckableScope() const;
	
	virtual std::string getName() const = 0;
	
	virtual void printStatistics(std::ostream &os) const = 0;
protected: 
	Solver<T>* _solver;
};
		
template<typename T>
std::ostream& operator<< (std::ostream& os, recombinator::generator::Constraint<T> &c);

#include "Constraint.tpp"
		

#endif //_CONSTRAINT_H
