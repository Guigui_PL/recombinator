#ifndef NPEQUIVALENTCONSTRAINT_HPP
#define NPEQUIVALENTCONSTRAINT_HPP
#include <optional>
#include <unordered_set>
#include <vector>
#include <boost/functional/hash.hpp>
#include "../api/Semantic.hpp"
#include "NoIPalindromeConstraint.hpp"


namespace recombinator 
{
	namespace generator 
	{
		class NPEquivalentConstraint: public NoIPalindromeConstraint
		{
		public: 
			using Variable = SemanticalBioDeviceConstraint::Variable;
			
			NPEquivalentConstraint(
				::recombinator::api::SemanticalBioDevice* bioDevice, 
				IEDyckSemanticalBioDeviceBuilder* builder, 
				Variable begin, 
				Variable end,
				std::unordered_set<std::pair<Variable,Variable>,boost::hash< std::pair<Variable, Variable>>> strongerConstraintPairs = {}
  			);
			
			virtual bool isCheckable() const;
			std::string getName() const;
			bool forwardCheck();
			
			virtual std::optional<std::pair<Variable,Variable>> getVariablesToCheck() const;
			//virtual bool isSymmetricForbidden(Variable checkedVar, Variable opposite) const;
			
			inline bool checkStrongerConstraint(std::pair<Variable,Variable> &p);
			
		protected:
			static std::unordered_set<std::vector<::recombinator::api::Semantic>,boost::hash<std::vector<::recombinator::api::Semantic>>> buildAuthorizedStrongerPatterns();
			
			Variable _begin;
			Variable _end;
			std::unordered_set<std::pair<Variable,Variable>,boost::hash< std::pair<Variable, Variable>>> _strongerConstraintPairs;
			
			static const std::unordered_set<std::vector<::recombinator::api::Semantic>,boost::hash<std::vector<::recombinator::api::Semantic>>> _authorizedStrongerPatterns;
			
		};
	}
}

#endif
