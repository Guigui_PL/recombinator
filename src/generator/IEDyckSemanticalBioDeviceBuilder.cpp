/**
 * Copyright (C) Inserm Montpellier / CBS
 * 
 * U 1054 - mixte avec Université de Montpellier et CNRS
 * Centre de Biochimie Structurale
 * Directeur : Pierre-Emmanuel Milhiet
 * 29, rue de Navacelles
 * 34090 MONTPELLIER
 * 
 * Recombinator project : http://recombinator.lirmm.fr/
 * 
 * Team : Synthetic biology at CBS : http://www.cbs.cnrs.fr/index.php/en/research-equipea4
 * 
 * 
 * @auhtor : Guillaume Pérution-Kihli 
 */

#include "IEDyckSemanticalBioDeviceBuilder.hpp"
#include <chrono>
#include <thread>
#include "../api/IEDyckFunctionnalStructure.hpp"
#include "IEDyckSemanticalBioDeviceGenerator.hpp"
#include "NoIPalindromeConstraint.hpp"

using namespace recombinator::api;
using namespace recombinator::generator;
using namespace std;
namespace pt = boost::property_tree;

/**
 * IEDyckSemanticalBioDeviceBuilder implementation
 */

IEDyckSemanticalBioDeviceBuilder::IEDyckSemanticalBioDeviceBuilder(DyckSemanticalBioDevice* bd, const std::unordered_set<std::string> *dyckWords, unordered_set<Setting> s) :
	DyckSemanticalBioDeviceBuilder(bd),
	Solver(bd->getNbSemantics()),
	_settings(s),
	_outputDirectory(string("work/")+to_string(_bioDevice->getNbIntegrase())+"/"),
	_goodSBDwriter(_outputDirectory+"good/"+bd->getStructure().getStructure(), SemanticalBioDeviceFile::FileType::CLASSICAL_BIODEVICE, false),
	_badSBDwriter(_outputDirectory+"bad/"+bd->getStructure().getStructure(), SemanticalBioDeviceFile::FileType::CLASSICAL_BIODEVICE, false),
	_SBDConstraintBuilder(_bioDevice, this),
	_dyckWords(dyckWords)
{
	initDomains();
	generateConstraints();
	removeUnaryConstraints();
	computeExplorationTreeSize();
}

void IEDyckSemanticalBioDeviceBuilder::launch() 
{
	cout << "Builder of " << _bioDevice->getStructure().getStructure() << " has begun!" << endl;
	solve();
	cout << "Builder of " << _bioDevice->getStructure().getStructure() << " has finished!" << endl;
	printStatistics(cout);
}
/*
void IEDyckSemanticalBioDeviceBuilder::treatBuiltSemanticalBioDevices() 
{
	DyckSemanticalBioDevice SBD = *_bioDevice;
	do
	{
		vector<const Semantic*> semantics;
		
		while (_generatedBioDevices.pop(semantics))
		{
			--_nbSBDToTreat;
			SBD.setSemantics(semantics);
			
			if (!SBD.isReducible() && 
				((!isSet(Setting::IRREDUNDANCY_CONSTRAINT) && !isSet(Setting::ALL_CONSTRAINTS)) 
					|| SBD.isIrredundant()))
			{
				++_nbGoodSBD;
				_goodSBDwriter << SBD;
				
				if (SBD.getDyckSymmetric() == SBD)
				{
					++_nbGoodIPalindrome;
				}
			}
			else
			{
				++_nbBadSBD;
				//_badSBDwriter << SBD;
			}
		}
		
		std::this_thread::sleep_for(std::chrono::milliseconds(1000));
	} while (_nbSBDToTreat > 0 || !isFinished());
	
	_hasStopped = true;
}*/

void IEDyckSemanticalBioDeviceBuilder::treatBuiltSemanticalBioDevices() 
{
	DyckSemanticalBioDevice SBD = *_bioDevice;
	do
	{
		vector<const Semantic*> semantics;
		
		while (_generatedBioDevices.pop(semantics))
		{
			--_nbSBDToTreat;
			SBD.setSemantics(semantics);
			
			if (!SBD.isReducible() && 
				((!isSet(Setting::IRREDUNDANCY_CONSTRAINT) && !isSet(Setting::ALL_CONSTRAINTS)) 
					|| SBD.isIrredundant()))
			{
	
				if ((isSet(Setting::ALL_CONSTRAINTS) || isSet(Setting::NP_EQUIVALENT_CONSTRAINT))
					&& (dynamic_cast<const IEDyckFunctionnalStructure*>(&(SBD.getStructure())))->getNbInversions() > 0)
				{
					vector<DyckSemanticalBioDevice> toWrite{{SBD}};
					for (auto &it :  SBD.getNPEquivalents(*_dyckWords))
					{
						bool writeIt = true;
						
						if ( (isSet(Setting::ALL_CONSTRAINTS) || isSet(Setting::NO_IPALINDROME_CONSTRAINT))
							&& IEDyckFunctionnalStructure::reverseDyckWord(SBD.getStructure().getStructure()) != SBD.getStructure().getStructure()
							&& IEDyckFunctionnalStructure::reverseDyckWord(SBD.getStructure().getStructure()) == it.getStructure().getStructure())
						{
							writeIt = false;
						}
						else 
						{
							for (auto &it2 : toWrite)
							{
								if (it == it2 /*|| it == it2.getDyckSymmetric()*/)
								{
									writeIt = false;
									break;
								}
							}
						}
						
						if (writeIt)
						{
							toWrite.push_back(it);
						}
					}
					
					for (auto &it : toWrite)
					{
						if (IEDyckSemanticalBioDeviceGenerator::reverseDyckWord(_bioDevice->getStructure().getStructure())
								!= _bioDevice->getStructure().getStructure() ||
							(!(isSet(Setting::ALL_CONSTRAINTS) || isSet(Setting::NO_IPALINDROME_CONSTRAINT))
							|| NoIPalindromeConstraint::isAuthorizedSBD(it)))
						{
							++_nbGoodSBD;
							_goodSBDwriter << it;
				
							if (it.getDyckSymmetric() == it)
							{
								++_nbGoodIPalindrome;
							}
						}
					}
				}
				else 
				{
					++_nbGoodSBD;
					_goodSBDwriter << SBD;
				
					if (SBD.getDyckSymmetric() == SBD)
					{
						++_nbGoodIPalindrome;
					}
				}
			}
			else
			{
				++_nbBadSBD;
				//_badSBDwriter << SBD;
			}
		}
		
		std::this_thread::sleep_for(std::chrono::milliseconds(1000));
	} while (_nbSBDToTreat > 0 || !isFinished());
	
	updateState();
	
	if (_notifyUpdate != nullptr)
	{
		_notifyUpdate->notify_all();
	}
	
	_hasStopped = true;
}

void IEDyckSemanticalBioDeviceBuilder::generateConstraints() 
{
	if (isSet(Setting::ALL_CONSTRAINTS) || isSet(Setting::UTILITY_CONSTRAINTS))
	{
		generateUtilityConstraints();
	}
	
	if (isSet(Setting::ALL_CONSTRAINTS) || isSet(Setting::ATOMIC_SITES_CONSTRAINTS))
	{
		generateAtomicSitesConstraints();
	}
	
	if (isSet(Setting::ALL_CONSTRAINTS))
	{
		generateExpOnlyInExcisionConstraints();
	}
	
	if (isSet(Setting::ALL_CONSTRAINTS) || isSet(Setting::NO_IPALINDROME_CONSTRAINT))
	{
		generateNoIPalindromeConstraints();
	}
	
	if (isSet(Setting::ALL_CONSTRAINTS) || isSet(Setting::NP_EQUIVALENT_CONSTRAINT))
	{
		generateNPEquivalentsConstraints();
	}
}

void IEDyckSemanticalBioDeviceBuilder::generateUtilityConstraints() 
{
	addConstraints(_SBDConstraintBuilder.buildCheckUtilityPrefixAndSuffixConstraints());
	addConstraints(_SBDConstraintBuilder.buildCheckUtilityAfterPrefixConstraints());
	addConstraints(_SBDConstraintBuilder.buildCheckUtilityBeforeSuffixConstraints());
	addConstraints(_SBDConstraintBuilder.buildCheckUtilityBeforeAndAfterSemanticConstraints());
	addConstraints(_SBDConstraintBuilder.buildCheckUtilityAfterSemanticConstraints());
	addConstraints(_SBDConstraintBuilder.buildCheckUtilityBeforeSemanticConstraints());
}

void IEDyckSemanticalBioDeviceBuilder::assignVar(size_t i, const Semantic* val) 
{
	Solver<const Semantic*>::assignVar(i, val);
	_bioDevice->setSemantic(i, val);
}

bool IEDyckSemanticalBioDeviceBuilder::isAssignedVar(size_t i) const
{
	return _bioDevice->isDefinedSemantic(i);
}

void IEDyckSemanticalBioDeviceBuilder::initDomains() 
{
	size_t nbVar = getNbVariables();
	_domains.resize(nbVar);
	for (size_t i{0}; i < nbVar; ++i)
	{
		for (const auto& sem : Semantic::elementarySemantics)
		{
			_domains[i].push_back(&sem);
		}
	}
}

void IEDyckSemanticalBioDeviceBuilder::flush()
{
	_goodSBDwriter.flush();
	_badSBDwriter.flush();
}

const DyckSemanticalBioDevice& IEDyckSemanticalBioDeviceBuilder::getSBD() const
{
	return *_bioDevice;
}

void IEDyckSemanticalBioDeviceBuilder::updateState()
{
    std::lock_guard<std::mutex> lock (_stateLock);
	_state = pt::ptree();
	
	pt::ptree domains, domainsRemovedElements, lastAssignations, lastAssignationIndexes, settings;
	
	for (size_t i{0}; i < _domains.size(); ++i)
	{
		pt::ptree domainTree;
		for (auto &element : _domains[i])
		{
			pt::ptree e;
			e.put("", element->getSemanticKey());
			domainTree.push_back(std::make_pair("", e));
		}
		domains.push_back(std::make_pair(to_string(i), domainTree));
	}
	_state.push_back(std::make_pair("domains", domains));
	
	_state.put("nbAssignations", _lastAssignations.size());
	
	for (size_t i{0}; i < _domainsRemovedElements.size(); ++i)
	{
		pt::ptree domainsRemovedElementsLevelTree;
		for (size_t j{0}; j < _domainsRemovedElements[i].size(); ++j)
		{
			pt::ptree domainRemovedElementsTree;
			for (auto &element :_domainsRemovedElements[i][j])
			{
				pt::ptree e;
				e.put("", element->getSemanticKey());
				domainRemovedElementsTree.push_back(std::make_pair("", e));
			}
			domainsRemovedElementsLevelTree.push_back(std::make_pair(to_string(j), domainRemovedElementsTree));
		}
		domainsRemovedElements.push_back(std::make_pair(to_string(i), domainsRemovedElementsLevelTree));
	}
	_state.push_back(std::make_pair("domainsRemovedElements", domainsRemovedElements));
	
	for (size_t i{0}; i < _lastAssignations.size(); ++i)
	{
		pt::ptree e;
		e.put("", _lastAssignations[i]);
		lastAssignations.push_back(std::make_pair(to_string(i), e));
	}
	_state.push_back(std::make_pair("lastAssignations", lastAssignations));
	
	for (size_t i{0}; i < _lastAssignationIndexes.size(); ++i)
	{
		pt::ptree e;
		e.put("", _lastAssignationIndexes[i]);
		lastAssignationIndexes.push_back(std::make_pair(to_string(i), e));
	}
	_state.push_back(std::make_pair("lastAssignationIndexes", lastAssignationIndexes));
	
	for (auto &s : _settings)
	{
		pt::ptree e;
		e.put("", static_cast<int>(s));
		settings.push_back(std::make_pair("", e));
	}
	_state.push_back(std::make_pair("settings", settings));
	
	_state.put("explorationTreeSize", _explorationTreeSize);
	_state.put("exploredTreeSize", _exploredTreeSize);
}

void IEDyckSemanticalBioDeviceBuilder::generateAtomicSitesConstraints()
{
	addConstraints(_SBDConstraintBuilder.buildAtomicSitesConstraints());
}

pt::ptree IEDyckSemanticalBioDeviceBuilder::getState() const
{
    std::lock_guard<std::mutex> lock (_stateLock);
	return _state;
}

void IEDyckSemanticalBioDeviceBuilder::restoreState(const boost::property_tree::ptree &state)
{
    std::lock_guard<std::mutex> lock (_stateLock);
	
	size_t nbVar = getNbVariables();
	size_t nbAssignations = state.get_child("lastAssignations").size();
	_domains.resize(nbVar);
	
	for (auto &it : state.get_child("domains"))
	{
		size_t i = stoi(it.first.data());
		for (auto &it2 : state.get_child(string("domains.")+it.first.data()))
		{
			_domains[i].push_back(Semantic::getPointerWithKey(stoi(it2.second.data())));
		}
	}
	
	_restoredDomainsRemovedElements.resize(nbAssignations);
	for (auto &it : state.get_child("domainsRemovedElements"))
	{
		size_t i = stoi(it.first.data());
		_restoredDomainsRemovedElements[i].resize(nbVar);
		for (auto &it2 : state.get_child(string("domainsRemovedElements.")+it.first.data()))
		{
			size_t j = stoi(it2.first.data());
			for (auto &it3 : state.get_child(
				string("domainsRemovedElements.")+it.first.data()+"."+it2.first.data()))
			{
				_restoredDomainsRemovedElements[i][j].push_back(Semantic::getPointerWithKey(stoi(it3.second.data())));
			}
		}
	}
	
	_lastAssignations.resize(nbAssignations);
	for (auto &it : state.get_child("lastAssignations"))
	{
		size_t i = stoi(it.first.data());
		_lastAssignations[i] = stoi(it.second.data());
	}
	
	_restoredLastAssignationIndexes.resize(nbAssignations);
	for (auto &it : state.get_child("lastAssignationIndexes"))
	{
		size_t i = stoi(it.first.data());
		_restoredLastAssignationIndexes[i] = stoi(it.second.data());
	}
	
	for (auto &it : state.get_child("settings"))
	{
		_settings.insert(static_cast<Setting>(stoi(it.second.data())));
	}
	
	_explorationTreeSize = state.get<size_t>("explorationTreeSize");
	_exploredTreeSize = state.get<size_t>("exploredTreeSize");
	
	generateConstraints();
	removeUnaryConstraints();
}

size_t IEDyckSemanticalBioDeviceBuilder::getNbGoodSBD() const
{
	return _nbGoodSBD;
}

size_t IEDyckSemanticalBioDeviceBuilder::getNbGoodIPalindrome() const
{
	return _nbGoodIPalindrome;
}

size_t IEDyckSemanticalBioDeviceBuilder::getNbBadSBD() const
{
	return _nbBadSBD;
}

void IEDyckSemanticalBioDeviceBuilder::treatCompleteAssignation()
{
	++_nbSBDToTreat;
    while (!_generatedBioDevices.push(_bioDevice->getSemantics()))
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
}

bool IEDyckSemanticalBioDeviceBuilder::isSet (Setting setting) const
{
	return _settings.find(setting) != _settings.end();
}

void IEDyckSemanticalBioDeviceBuilder::removeLastAssignation ()
{
	size_t lastAssignedVar = _lastAssignations.back();
	_bioDevice->setSemantic(lastAssignedVar, nullptr);
	
	Solver<const Semantic*>::removeLastAssignation();
}

size_t IEDyckSemanticalBioDeviceBuilder::getNbVariables() const
{
	return _bioDevice->getNbSemantics();
}

void IEDyckSemanticalBioDeviceBuilder::generateExpOnlyInExcisionConstraints()
{
	addConstraints(_SBDConstraintBuilder.buildExpOnlyInExcisionConstraints());
}

void IEDyckSemanticalBioDeviceBuilder::generateNoIPalindromeConstraints()
{
	if ((isSet(Setting::ALL_CONSTRAINTS) || isSet(Setting::NO_IPALINDROME_CONSTRAINT)) &&
		IEDyckSemanticalBioDeviceGenerator::reverseDyckWord(_bioDevice->getStructure().getStructure())
		== _bioDevice->getStructure().getStructure())
	{
		addConstraints(_SBDConstraintBuilder.buildNoIPalindromeConstraints());
	}
}

void IEDyckSemanticalBioDeviceBuilder::generateNPEquivalentsConstraints()
{
	if (isSet(Setting::ALL_CONSTRAINTS) || isSet(Setting::NP_EQUIVALENT_CONSTRAINT))
	{
		addConstraints(_SBDConstraintBuilder.buildNPEquivalentsConstraint());
	}
}


bool NoIPalindromeConstraint::isAuthorizedSBD(const SemanticalBioDevice& sbd)
{
	auto nbVar = sbd.getNbSemantics();
	
	for (size_t i = 0; i <= nbVar/2; ++i)
	{
		auto opposite = nbVar - (i+1);
		
		if (i != opposite)
		{
			vector<Semantic> researchedTuple(2);
			researchedTuple[0] = sbd.getSemantic(i);
			researchedTuple[1] = sbd.getSemantic(opposite);
			if (_authorizedPatternInMirrors.find(researchedTuple) == _authorizedPatternInMirrors.end())
			{
				return false;
			}
			
			if (!sbd.getSemantic(i).isAnInversionOf(sbd.getSemantic(opposite)))
			{
				return true;
			}
		}
		else 
		{
			vector<Semantic> researchedTuple(1);
			researchedTuple[0] = sbd.getSemantic(i);
			if (_auhtorizedSemanticInMiddleMirrors.find(researchedTuple) == _auhtorizedSemanticInMiddleMirrors.end())
			{
				return false;
			}
		}
	}
	
	return true;
}
