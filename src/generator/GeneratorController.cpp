/**
 * Copyright (C) Inserm Montpellier / CBS
 * 
 * U 1054 - mixte avec Université de Montpellier et CNRS
 * Centre de Biochimie Structurale
 * Directeur : Pierre-Emmanuel Milhiet
 * 29, rue de Navacelles
 * 34090 MONTPELLIER
 * 
 * Recombinator project : http://recombinator.lirmm.fr/
 * 
 * Team : Synthetic biology at CBS : http://www.cbs.cnrs.fr/index.php/en/research-equipea4
 * 
 * 
 * @auhtor : Guillaume Pérution-Kihli 
 */

#include "GeneratorController.hpp"
#include <cstddef>
#include <boost/filesystem.hpp>
#include "IEDyckSemanticalBioDeviceGenerator.hpp"

using namespace recombinator::generator;
using namespace std;
namespace fs = boost::filesystem;

/**
 * GeneratorController implementation
 */

GeneratorController GeneratorController::_instance;

GeneratorController::GeneratorController()
{
	prepareDirectories();
}

/**
 * @return GeneratorController
 */
GeneratorController& GeneratorController::getInstance() 
{
    return _instance;
}

/**
 * @param nbInputs
 */
void GeneratorController::launchIEDyckSemanticalBioDeviceGenerator(size_t nbInputs) 
{
	_generators.emplace_back(new IEDyckSemanticalBioDeviceGenerator(nbInputs));
	_generators.back()->launchGeneration();
}

void GeneratorController::stopGenerators() 
{
	for (auto &g : _generators)
	{
		g->stopGeneration();
	}
}

/**
 * @param settings
 */
void GeneratorController::setSettings(std::vector<Setting> settings) 
{
	_settings.insert(settings.begin(), settings.end());
}

void GeneratorController::prepareDirectories()
{
	fs::path p("work");
	if (!fs::exists(p))
		fs::create_directory(p);
	
	p += "/save";
	if (!fs::exists(p))
		fs::create_directory(p);
}
