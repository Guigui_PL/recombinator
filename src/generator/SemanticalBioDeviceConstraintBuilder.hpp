/**
 * Copyright (C) Inserm Montpellier / CBS
 * 
 * U 1054 - mixte avec Université de Montpellier et CNRS
 * Centre de Biochimie Structurale
 * Directeur : Pierre-Emmanuel Milhiet
 * 29, rue de Navacelles
 * 34090 MONTPELLIER
 * 
 * Recombinator project : http://recombinator.lirmm.fr/
 * 
 * Team : Synthetic biology at CBS : http://www.cbs.cnrs.fr/index.php/en/research-equipea4
 * 
 * 
 * @auhtor : Guillaume Pérution-Kihli 
 */

#ifndef _SMANTICALBIODEVICECONSTRAINTBUILDER_H
#define _SMANTICALBIODEVICECONSTRAINTBUILDER_H
#include <memory>
#include <set>
#include <vector>
#include "../api/SemanticalBioDevice.hpp"
#include "Solver.hpp"

namespace recombinator 
{
	namespace generator 
	{
		class IEDyckSemanticalBioDeviceBuilder;
		class SemanticalBioDeviceConstraintBuilder {
		public: 
			
			/**
			* @param bioDevice
			* @param solver
			*/
			SemanticalBioDeviceConstraintBuilder(::recombinator::api::SemanticalBioDevice* bioDevice, 
												IEDyckSemanticalBioDeviceBuilder* solver);
			
			std::vector<std::shared_ptr<Constraint<const ::recombinator::api::Semantic*>>> buildCheckUtilityAfterPrefixConstraints();
			
			std::vector<std::shared_ptr<Constraint<const ::recombinator::api::Semantic*>>> buildCheckUtilityPrefixAndSuffixConstraints();
			
			std::vector<std::shared_ptr<Constraint<const ::recombinator::api::Semantic*>>> buildCheckUtilityAfterSemanticConstraints();
			
			std::vector<std::shared_ptr<Constraint<const ::recombinator::api::Semantic*>>> buildCheckUtilityBeforeSuffixConstraints();
			
			std::vector<std::shared_ptr<Constraint<const ::recombinator::api::Semantic*>>> buildCheckUtilityBeforeSemanticConstraints();
			
			std::vector<std::shared_ptr<Constraint<const ::recombinator::api::Semantic*>>> buildCheckUtilityBeforeAndAfterSemanticConstraints();
			
			std::vector<std::shared_ptr<Constraint<const ::recombinator::api::Semantic*>>> buildAtomicSitesConstraints();
			
			std::vector<std::shared_ptr<Constraint<const ::recombinator::api::Semantic*>>> buildExpOnlyInExcisionConstraints();
			
			std::vector<std::shared_ptr<Constraint<const ::recombinator::api::Semantic*>>> buildNoIPalindromeConstraints();
			
			std::vector<std::shared_ptr<Constraint<const ::recombinator::api::Semantic*>>> buildNPEquivalentsConstraint();
			
			
		private: 
			::recombinator::api::SemanticalBioDevice* _bioDevice;
			IEDyckSemanticalBioDeviceBuilder* _solver;
		};
	}
}

#endif //_SMANTICALBIODEVICECONSTRAINTBUILDER_H
