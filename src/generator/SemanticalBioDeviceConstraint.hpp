/**
 * Copyright (C) Inserm Montpellier / CBS
 * 
 * U 1054 - mixte avec Université de Montpellier et CNRS
 * Centre de Biochimie Structurale
 * Directeur : Pierre-Emmanuel Milhiet
 * 29, rue de Navacelles
 * 34090 MONTPELLIER
 * 
 * Recombinator project : http://recombinator.lirmm.fr/
 * 
 * Team : Synthetic biology at CBS : http://www.cbs.cnrs.fr/index.php/en/research-equipea4
 * 
 * 
 * @auhtor : Guillaume Pérution-Kihli 
 */

#ifndef _SEMANTICALBIODEVICECONSTRAINT_H
#define _SEMANTICALBIODEVICECONSTRAINT_H
#include <atomic>
#include <deque>
#include <vector>
#include "../api/Semantic.hpp"
#include "../api/SemanticalBioDevice.hpp"
#include "Constraint.hpp"
#include "IEDyckSemanticalBioDeviceBuilder.hpp"


namespace recombinator 
{
	namespace generator 
	{
		class SemanticalBioDeviceConstraint: public Constraint<const ::recombinator::api::Semantic*>
		{
		public: 
			using Variable = Constraint<const ::recombinator::api::Semantic*>::Variable;
			
			const std::vector<Variable>& getScope() const;
			
			size_t getArity() const;
			
			size_t getCheckableArity() const;
			
			virtual bool isCheckable() const;
			
			virtual Variable getUnassignedVar() const;
			
			virtual size_t getIndexUnassignedVar() const;
			
			virtual void printStatistics(std::ostream &os) const;
			
		protected: 
			/**
			* @param bioDevice
			*/
			SemanticalBioDeviceConstraint(::recombinator::api::SemanticalBioDevice* bioDevice, IEDyckSemanticalBioDeviceBuilder* builder);
			
			/**
			* @param vars
			*/
			void setScope(std::vector<Variable> vars);
			
			::recombinator::api::SemanticalBioDevice* _bioDevice;
			std::vector<Variable> _scope;
			
			std::deque<std::atomic<size_t>> _nbRemovedElements;
		};
	}
}

#endif //_SEMANTICALBIODEVICECONSTRAINT_H
