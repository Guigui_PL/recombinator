/**
 * Copyright (C) Inserm Montpellier / CBS
 * 
 * U 1054 - mixte avec Université de Montpellier et CNRS
 * Centre de Biochimie Structurale
 * Directeur : Pierre-Emmanuel Milhiet
 * 29, rue de Navacelles
 * 34090 MONTPELLIER
 * 
 * Recombinator project : http://recombinator.lirmm.fr/
 * 
 * Team : Synthetic biology at CBS : http://www.cbs.cnrs.fr/index.php/en/research-equipea4
 * 
 * 
 * @auhtor : Guillaume Pérution-Kihli 
 */

#ifndef _CHECKUTILITYAFTERSEMANTIC_H
#define _CHECKUTILITYAFTERSEMANTIC_H
#include <tuple>
#include "CheckUtility.hpp"

namespace recombinator 
{
	namespace generator 
	{
		class CheckUtilityBeforeAndAfterSemantic: public CheckUtility
		{
		public: 
			CheckUtilityBeforeAndAfterSemantic(::recombinator::api::SemanticalBioDevice* bioDevice, IEDyckSemanticalBioDeviceBuilder* builder, CheckUtility::Variable var);
			
			std::string getName() const;
			
		protected: 
			std::vector<std::tuple<int,int,int>> _checkList;
			
			recombinator::api::Semantic::Utility getUtility (const recombinator::api::Semantic* s) const;
		};
	}
}

#endif //_CHECKUTILITYAFTERSEMANTIC_H
 
