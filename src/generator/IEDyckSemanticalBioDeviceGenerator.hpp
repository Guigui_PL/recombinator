/**
 * Copyright (C) Inserm Montpellier / CBS
 * 
 * U 1054 - mixte avec Université de Montpellier et CNRS
 * Centre de Biochimie Structurale
 * Directeur : Pierre-Emmanuel Milhiet
 * 29, rue de Navacelles
 * 34090 MONTPELLIER
 * 
 * Recombinator project : http://recombinator.lirmm.fr/
 * 
 * Team : Synthetic biology at CBS : http://www.cbs.cnrs.fr/index.php/en/research-equipea4
 * 
 * 
 * @auhtor : Guillaume Pérution-Kihli 
 */

#ifndef _IEDYSCKSEMANTICALBIODEVICEGENERATOR_H
#define _IEDYSCKSEMANTICALBIODEVICEGENERATOR_H
#include <atomic>
#include <chrono>
#include <condition_variable>
#include <memory>
#include <vector>
#include <unordered_set>
#include "../api/DyckSemanticalBioDevice.hpp"
#include "../api/ThreadsManager.hpp"
#include "DyckSemanticalBioDeviceGenerator.hpp"
#include "IEDyckSemanticalBioDeviceBuilder.hpp"

namespace recombinator 
{
	namespace generator 
	{
		class IEDyckSemanticalBioDeviceGenerator: public DyckSemanticalBioDeviceGenerator {
		public: 
			
			enum Setting 
			{ 
				UTILITY_PREFIX_CONSTRAINT, 
				UTILITY_SUFFIX_CONSTRAINT, 
				IRREDUNDANCY_CONSTRAINT, 
				ALL_CONSTRAINTS ,
				NO_IPALINDROME_CONSTRAINT,
				NP_EQUIVALENT_CONSTRAINT
			};
			
			using tp = std::chrono::system_clock::time_point;
			/**
			* @param nbInputs
			*/
			IEDyckSemanticalBioDeviceGenerator(uint8_t nbInputs, 
											std::vector<Setting> settings = {Setting::ALL_CONSTRAINTS});
			
			virtual void launchGeneration();
			
			virtual void stopGeneration();
			
			virtual size_t getElapsedTime() const;
			
			static std::string reverseDyckWord(std::string dyckWord);
			
		private: 
			
			virtual void saveState();
			
			virtual void restoreState();
			
			virtual bool askToContinue() const;
			
			virtual bool existsSavedState() const;
			
			virtual void makeDyckWords();
			
			virtual void makeDyckSemanticalBioDevices();
			
			virtual void makeBuilders();
			
			virtual void updateStats();
			
			virtual void displayStats();
			
			void prepareDirectories();
			
			bool isSet (Setting setting) const;
			
			std::unordered_set<Setting> _settings;
			std::unordered_set<std::string> _dyckWords;
			std::vector<recombinator::api::DyckSemanticalBioDevice> _semanticalBioDevices;
			std::vector<std::shared_ptr<IEDyckSemanticalBioDeviceBuilder>> _builders;
			std::vector<recombinator::api::ThreadsManager::ThreadId> _launchedThreads;
			
			std::string _saveFile;
			
			std::atomic<size_t> _nbGoodSBD{0};
			std::atomic<size_t> _nbBadSBD{0};
			std::atomic<size_t> _nbGoodIPalindrome{0};
			
			std::atomic<size_t> _restoredElapsedTime{0};
			tp _chronoStart;
			std::atomic<bool> _stop{false};
			std::condition_variable _notifyFinished;
				
			std::atomic<bool> generationLaunched{false};
		};
	}
}

#endif //_IEDYSCKSEMANTICALBIODEVICEGENERATOR_H
