/**
 * Copyright (C) Inserm Montpellier / CBS
 * 
 * U 1054 - mixte avec Université de Montpellier et CNRS
 * Centre de Biochimie Structurale
 * Directeur : Pierre-Emmanuel Milhiet
 * 29, rue de Navacelles
 * 34090 MONTPELLIER
 * 
 * Recombinator project : http://recombinator.lirmm.fr/
 * 
 * Team : Synthetic biology at CBS : http://www.cbs.cnrs.fr/index.php/en/research-equipea4
 * 
 * 
 * @auhtor : Guillaume Pérution-Kihli 
 */

#ifndef _CHECKUTILITY_H
#define _CHECKUTILITY_H
#include <vector>
#include "../api/Semantic.hpp"
#include "SemanticalBioDeviceConstraint.hpp"


namespace recombinator 
{
	namespace generator 
	{
		class CheckUtility : public SemanticalBioDeviceConstraint
		{
		public: 
			using Variable = SemanticalBioDeviceConstraint::Variable;
			
			//bool isCheckable() const;
			
			const std::vector<Variable>& getCheckableScope() const;
			
			bool forwardCheck();
			
			bool isViolated() const;
			
			size_t getCheckableArity() const;
			
		protected: 
			CheckUtility(::recombinator::api::SemanticalBioDevice* bioDevice, IEDyckSemanticalBioDeviceBuilder* builder, Variable var);
			virtual recombinator::api::Semantic::Utility getUtility (const recombinator::api::Semantic* s) const = 0;
			Variable _checkedVar;
			std::vector<Variable> _checkableScope;
		};
	}
}

#endif //_CHECKUTILITY_H
