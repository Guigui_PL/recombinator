/**
 * Copyright (C) Inserm Montpellier / CBS
 * 
 * U 1054 - mixte avec Université de Montpellier et CNRS
 * Centre de Biochimie Structurale
 * Directeur : Pierre-Emmanuel Milhiet
 * 29, rue de Navacelles
 * 34090 MONTPELLIER
 * 
 * Recombinator project : http://recombinator.lirmm.fr/
 * 
 * Team : Synthetic biology at CBS : http://www.cbs.cnrs.fr/index.php/en/research-equipea4
 * 
 * 
 * @auhtor : Guillaume Pérution-Kihli 
 */

#include "SemanticalBioDeviceConstraint.hpp"

using namespace recombinator::api;
using namespace recombinator::generator;
using namespace std;

/**
 * SemanticalBioDeviceConstraint implementation
 */


/**
 * @param bioDevice
 */
SemanticalBioDeviceConstraint::SemanticalBioDeviceConstraint(SemanticalBioDevice* bioDevice, IEDyckSemanticalBioDeviceBuilder* builder) :
	Constraint<const ::recombinator::api::Semantic*>(builder), 
	_bioDevice(bioDevice),
	_nbRemovedElements(builder->getNbVariables())
{
	for (auto &it : _nbRemovedElements)
	{
		it = 0;
	}
}

/**
 * @return Vector<int>
 */
const vector<SemanticalBioDeviceConstraint::Variable>& SemanticalBioDeviceConstraint::getScope() const
{
    return _scope;
}

/**
 * @param vars
 */
void SemanticalBioDeviceConstraint::setScope(vector<SemanticalBioDeviceConstraint::Variable> vars) 
{
	_scope = vars;
}

/**
 * @return size_t
 */
size_t SemanticalBioDeviceConstraint::getArity() const
{
    return _scope.size();
}

size_t SemanticalBioDeviceConstraint::getCheckableArity() const
{
	return getArity();
}

bool SemanticalBioDeviceConstraint::isCheckable() const
{
	size_t nbUnassigned{0};
	
	for (auto &it : _scope)
	{
		if (!_solver->isAssignedVar(it))
		{
			++nbUnassigned;
		}
		
		if (nbUnassigned > 1)
		{
			return false;
		}
	}
	
	return nbUnassigned == 1;
}

size_t SemanticalBioDeviceConstraint::getIndexUnassignedVar() const
{
	for (size_t i{0}; i < _scope.size(); ++i)
	{
		if (!_solver->isAssignedVar(_scope[i]))
		{
			return i;
		}
	}
	
	return 0;
}

SemanticalBioDeviceConstraint::Variable SemanticalBioDeviceConstraint::getUnassignedVar() const
{
	for (auto &it : _scope)
	{
		if (!_solver->isAssignedVar(it))
		{
			return it;
		}
	}
	
	return 0;
}
			
void SemanticalBioDeviceConstraint::printStatistics(std::ostream &os) const
{
	os << getName() << " - nb removed : ";
	for (auto &it : _nbRemovedElements)
	{
		os << it << " ";
	}
}