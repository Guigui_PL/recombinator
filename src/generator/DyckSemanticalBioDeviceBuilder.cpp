/**
 * Copyright (C) Inserm Montpellier / CBS
 * 
 * U 1054 - mixte avec Université de Montpellier et CNRS
 * Centre de Biochimie Structurale
 * Directeur : Pierre-Emmanuel Milhiet
 * 29, rue de Navacelles
 * 34090 MONTPELLIER
 * 
 * Recombinator project : http://recombinator.lirmm.fr/
 * 
 * Team : Synthetic biology at CBS : http://www.cbs.cnrs.fr/index.php/en/research-equipea4
 * 
 * 
 * @auhtor : Guillaume Pérution-Kihli 
 */

#include "DyckSemanticalBioDeviceBuilder.hpp"

using namespace recombinator::api;
using namespace recombinator::generator;
using namespace std;

/**
 * DyckSemanticalBioDeviceBuilder implementation
 */


/**
 * @param bd
 */
DyckSemanticalBioDeviceBuilder::DyckSemanticalBioDeviceBuilder(DyckSemanticalBioDevice* bd) :
	_bioDevice(bd)
{}