/**
 * Copyright (C) Inserm Montpellier / CBS
 * 
 * U 1054 - mixte avec Université de Montpellier et CNRS
 * Centre de Biochimie Structurale
 * Directeur : Pierre-Emmanuel Milhiet
 * 29, rue de Navacelles
 * 34090 MONTPELLIER
 * 
 * Recombinator project : http://recombinator.lirmm.fr/
 * 
 * Team : Synthetic biology at CBS : http://www.cbs.cnrs.fr/index.php/en/research-equipea4
 * 
 * 
 * @auhtor : Guillaume Pérution-Kihli 
 */

#ifndef _DYCKSEMANTICALBIODEVICEGENERATOR_H
#define _DYCKSEMANTICALBIODEVICEGENERATOR_H

#include "SemanticalBioDeviceGenerator.hpp"


class DyckSemanticalBioDeviceGenerator: public SemanticalBioDeviceGenerator {
public: 
    DyckSemanticalBioDeviceGenerator(uint8_t nbInputs) : SemanticalBioDeviceGenerator(nbInputs) {}
    virtual void makeDyckWords() = 0;
};

#endif //_DYCKSEMANTICALBIODEVICEGENERATOR_H