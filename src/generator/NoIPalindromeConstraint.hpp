#ifndef NOIPALINDROMECONSTRAINT_HPP
#define NOIPALINDROMECONSTRAINT_HPP
#include <optional>
#include <vector>
#include "../api/Semantic.hpp"
#include "SBDConstraintExtension.hpp"


namespace recombinator 
{
	namespace generator 
	{
		class NoIPalindromeConstraint: public SemanticalBioDeviceConstraint
		{
		public: 
			using Variable = SemanticalBioDeviceConstraint::Variable;
			
			NoIPalindromeConstraint(::recombinator::api::SemanticalBioDevice* bioDevice, IEDyckSemanticalBioDeviceBuilder* builder);
			
			bool forwardCheck();
			bool forwardCheck(std::pair<Variable,Variable> &p);
			bool isViolated() const;
			virtual bool isCheckable() const;
			
			std::string getName() const;
			
			virtual std::optional<std::pair<Variable,Variable>> getVariablesToCheck() const;
			virtual bool isSymmetricForbidden(Variable checkedVar, Variable opposite) const;
			static bool isAuthorizedSBD(const ::recombinator::api::SemanticalBioDevice& sbd);
			
		protected: 
			static std::unordered_set<std::vector<::recombinator::api::Semantic>,boost::hash<std::vector<::recombinator::api::Semantic>>> buildAuthorizedPatternInMirrors();
			static std::unordered_set<std::vector<::recombinator::api::Semantic>,boost::hash<std::vector<::recombinator::api::Semantic>>> buildAuthorizedSemanticInMiddleMirrors();
		
			static const std::unordered_set<std::vector<::recombinator::api::Semantic>,boost::hash<std::vector<::recombinator::api::Semantic>>> _authorizedPatternInMirrors;
			static const std::unordered_set<std::vector<::recombinator::api::Semantic>,boost::hash<std::vector<::recombinator::api::Semantic>>> _auhtorizedSemanticInMiddleMirrors;
		};
	}
}

#endif
