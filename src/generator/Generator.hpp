/**
 * Copyright (C) Inserm Montpellier / CBS
 * 
 * U 1054 - mixte avec Université de Montpellier et CNRS
 * Centre de Biochimie Structurale
 * Directeur : Pierre-Emmanuel Milhiet
 * 29, rue de Navacelles
 * 34090 MONTPELLIER
 * 
 * Recombinator project : http://recombinator.lirmm.fr/
 * 
 * Team : Synthetic biology at CBS : http://www.cbs.cnrs.fr/index.php/en/research-equipea4
 * 
 * 
 * @auhtor : Guillaume Pérution-Kihli 
 */

#ifndef _GENERATOR_H
#define _GENERATOR_H

class Generator {
public: 
    
    virtual void launchGeneration() = 0;
    
    virtual void stopGeneration() = 0;
    
    virtual void saveState() = 0;
    
    virtual void restoreState() = 0;
	
	virtual bool askToContinue() const = 0;
	
	virtual bool existsSavedState() const = 0;
};

#endif //_GENERATOR_H
