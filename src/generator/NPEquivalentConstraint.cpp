#include "NPEquivalentConstraint.hpp"

using namespace recombinator::api;
using namespace recombinator::generator;
using namespace std;

const std::unordered_set<std::vector<::recombinator::api::Semantic>,boost::hash<std::vector<::recombinator::api::Semantic>>> NPEquivalentConstraint::_authorizedStrongerPatterns =
	NPEquivalentConstraint::buildAuthorizedStrongerPatterns();

NPEquivalentConstraint::NPEquivalentConstraint(
	::recombinator::api::SemanticalBioDevice* bioDevice, 
	IEDyckSemanticalBioDeviceBuilder* builder, 
	Variable begin, 
	Variable end,
	std::unordered_set<std::pair<size_t,size_t>,boost::hash< std::pair<Variable, Variable>>> strongerConstraintPairs
) :
	NoIPalindromeConstraint(bioDevice, builder),
	_begin(begin),
	_end(end),
	_strongerConstraintPairs(strongerConstraintPairs)
{
	_scope.clear();
	for (size_t i{begin}; i <= end; ++i)
	{
		_scope.push_back(i);
	}
}

bool NPEquivalentConstraint::isCheckable() const
{
	if (getScope().size() == 1)
	{
		return true;
	}
	
	for (auto &it : getScope())
	{
		if (_solver->isAssignedVar(it))
		{
			return true;
		}
	}
	
	return false;
}

bool NPEquivalentConstraint::forwardCheck()
{
	if (auto result = getVariablesToCheck())
	{
		if (_strongerConstraintPairs.find(result.value()) != _strongerConstraintPairs.end())
		{
			return NoIPalindromeConstraint::forwardCheck(result.value())/* && checkStrongerConstraint(result.value())*/;
		}
		else 
		{
			return NoIPalindromeConstraint::forwardCheck(result.value());
		}
	}
	else 
	{
		return true;
	}
}

std::optional<pair<NPEquivalentConstraint::Variable,NPEquivalentConstraint::Variable>> NPEquivalentConstraint::getVariablesToCheck() const
{
	Variable checkedVar = _begin;
	Variable opposite = _end;
	
	for (size_t i = checkedVar; i <= (_begin+_end)/2; ++i)
	{
		auto opposedI = _end-(i-_begin);
		
		if (_solver->isAssignedVar(i) 
			&& _solver->isAssignedVar(opposedI) 
			&& !_bioDevice->getSemantic(opposedI).isAnInversionOf(_bioDevice->getSemantic(i))
			/*&& (_strongerConstraintPairs.find(make_pair(i, opposedI)) == _strongerConstraintPairs.end()
				|| _bioDevice->getSemantic(opposedI) != _bioDevice->getSemantic(i))*/
		   )
		{
			return {};
		}
		else if (!_solver->isAssignedVar(i) && !_solver->isAssignedVar(opposedI) && i != opposedI)
		{
			return {};
		}
		else if (_solver->isAssignedVar(i) && !_solver->isAssignedVar(opposedI))
		{
			checkedVar = i;
			opposite = _end-(i-_begin);
			break;
		}
		else if (!_solver->isAssignedVar(i) && _solver->isAssignedVar(opposedI))
		{
			checkedVar = opposedI;
			opposite = _end-(checkedVar-_begin);
			break;
		}
		else if (i == opposedI && ! _solver->isAssignedVar(i))
		{
			checkedVar = opposite = i;
			break;
		}
	}
	
	return make_optional(make_pair(checkedVar, opposite));
}

std::string NPEquivalentConstraint::getName() const
{
	return string("NPEquivalents constraint ")+to_string(_begin)+" "+to_string(_end)+" - ";
}

bool NPEquivalentConstraint::checkStrongerConstraint(std::pair<Variable,Variable> &p)
{
	Variable checkedVar = p.first;
	Variable opposite = p.second;
	
	if (!_solver->isAssignedVar(opposite))
	{
		auto minVar = min(checkedVar, opposite);
		vector<Semantic> researchedTuple(2);
		auto i = checkedVar == minVar ? 0 : 1;
		researchedTuple[i] = _bioDevice->getSemantic(checkedVar);
		
		for (auto it = _solver->getDomain(opposite).begin(); it != _solver->getDomain(opposite).end(); ++it)
		{
			researchedTuple[1-i] = **it;
			if (_authorizedStrongerPatterns.find(researchedTuple) == _authorizedStrongerPatterns.end())
			{
				auto copy = it;
				--it;
				_solver->removeDomainElement(opposite, copy);
				++_nbRemovedElements[opposite];
			}
		}
		
		return !_solver->hasEmptyDomain(opposite);
	}
	
	return true;
}

std::unordered_set<std::vector<::recombinator::api::Semantic>,boost::hash<std::vector<::recombinator::api::Semantic>>> NPEquivalentConstraint::buildAuthorizedStrongerPatterns()
{
	std::unordered_set<std::vector<recombinator::api::Semantic>,boost::hash<std::vector<::recombinator::api::Semantic>>> tuples;
	auto authorizedPatternInMirrors = NoIPalindromeConstraint::buildAuthorizedPatternInMirrors();
	
	for (auto &s1 : Semantic::elementarySemantics)
	{
		for (auto &s2 : Semantic::elementarySemantics)
		{
			if (authorizedPatternInMirrors.find(std::vector({s1,s2})) != authorizedPatternInMirrors.end() 
				&& tuples.find(std::vector({s2,s1})) == tuples.end()
				&& (s1 != s2 || tuples.find(std::vector({Semantic(s1).reverse(),Semantic(s2).reverse()})) == tuples.end())
			)
			{
				tuples.insert(std::vector({s1,s2}));
			}
		}
	}
	
	return tuples;
}
/*
bool NPEquivalentConstraint::isSymmetricForbidden(NoIPalindromeConstraint::Variable checkedVar, NoIPalindromeConstraint::Variable opposite) const
{
	auto AfterMinVar = min(checkedVar, opposite)+1;
	auto beforeMaxVar = max(checkedVar, opposite)-1;
	
	while (AfterMinVar < beforeMaxVar && _solver->isAssignedVar(AfterMinVar) && _solver->isAssignedVar(beforeMaxVar) 
		&& (_bioDevice->getSemantic(beforeMaxVar).isAnInversionOf(_bioDevice->getSemantic(AfterMinVar))
			|| (_strongerConstraintPairs.find(make_pair(AfterMinVar, beforeMaxVar)) != _strongerConstraintPairs.end()
					&& _bioDevice->getSemantic(beforeMaxVar) == _bioDevice->getSemantic(AfterMinVar))
		))
	{
		--beforeMaxVar;
		++AfterMinVar;
	}
	
	if (_solver->isAssignedVar(AfterMinVar) && _solver->isAssignedVar(beforeMaxVar))
	{
		if (AfterMinVar != beforeMaxVar)
		{
			vector<Semantic> researchedTuple({
				_bioDevice->getSemantic(AfterMinVar),
				_bioDevice->getSemantic(beforeMaxVar)});
			if (_authorizedPatternInMirrors.find(researchedTuple) == _authorizedPatternInMirrors.end()
				|| (_strongerConstraintPairs.find(make_pair(AfterMinVar, beforeMaxVar)) != _strongerConstraintPairs.end()
					&& _authorizedStrongerPatterns.find(researchedTuple) == _authorizedStrongerPatterns.end()
				)
			)
			{
				return true;
			}
		}
		else 
		{
			vector<Semantic> researchedTuple(1);
			researchedTuple[0] = _bioDevice->getSemantic(AfterMinVar);
			if (_auhtorizedSemanticInMiddleMirrors.find(researchedTuple) == _auhtorizedSemanticInMiddleMirrors.end())
			{
				return true;
			}
		}
	}
	
	return false;
}*/