/**
 * Copyright (C) Inserm Montpellier / CBS
 * 
 * U 1054 - mixte avec Université de Montpellier et CNRS
 * Centre de Biochimie Structurale
 * Directeur : Pierre-Emmanuel Milhiet
 * 29, rue de Navacelles
 * 34090 MONTPELLIER
 * 
 * Recombinator project : http://recombinator.lirmm.fr/
 * 
 * Team : Synthetic biology at CBS : http://www.cbs.cnrs.fr/index.php/en/research-equipea4
 * 
 * 
 * @auhtor : Guillaume Pérution-Kihli 
 */

#ifndef _CHECKUTILITYBEFORESEMANTIC_H
#define _CHECKUTILITYBEFORESEMANTIC_H
#include "CheckUtility.hpp"


namespace recombinator 
{
	namespace generator 
	{
		class CheckUtilityBeforeSemantic : public CheckUtility {
		public:
			CheckUtilityBeforeSemantic(::recombinator::api::SemanticalBioDevice* bioDevice, IEDyckSemanticalBioDeviceBuilder* builder, CheckUtility::Variable var); 
			
			std::string getName() const;
			
			static const std::array<std::array<recombinator::api::Semantic::Utility,36>,36> _usefulBefore;
		protected: 
			
			std::vector<std::pair<int,int>> _checkList;
			
			recombinator::api::Semantic::Utility getUtility (const recombinator::api::Semantic* s) const;
			
			static std::array<std::array<recombinator::api::Semantic::Utility,36>,36> buildUsefulBefore();
		};
	}
}

#endif //_CHECKUTILITYBEFORESEMANTIC_H
