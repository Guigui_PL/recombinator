#include "NoIPalindromeConstraint.hpp"

using namespace recombinator::api;
using namespace recombinator::generator;
using namespace std;

const std::unordered_set<std::vector<::recombinator::api::Semantic>,boost::hash<std::vector<::recombinator::api::Semantic>>> NoIPalindromeConstraint::_authorizedPatternInMirrors = NoIPalindromeConstraint::buildAuthorizedPatternInMirrors();
const std::unordered_set<std::vector<::recombinator::api::Semantic>,boost::hash<std::vector<::recombinator::api::Semantic>>> NoIPalindromeConstraint::_auhtorizedSemanticInMiddleMirrors = NoIPalindromeConstraint::buildAuthorizedSemanticInMiddleMirrors();

NoIPalindromeConstraint::NoIPalindromeConstraint(::recombinator::api::SemanticalBioDevice* bioDevice, IEDyckSemanticalBioDeviceBuilder* builder) :
	SemanticalBioDeviceConstraint(bioDevice, builder)
{
	auto nbVar = _solver->getNbVariables();
	for (size_t i{0}; i < nbVar; ++i)
	{
		_scope.push_back(i);
	}
}

bool NoIPalindromeConstraint::isCheckable() const
{
	return _solver->getNbAssignations() > 0;
}

bool NoIPalindromeConstraint::forwardCheck() 
{
	if (auto result = getVariablesToCheck())
	{
		return forwardCheck(result.value());
	}
	else 
	{
		return true;
	}
}

bool NoIPalindromeConstraint::forwardCheck(std::pair<Variable,Variable> &p) 
{
	//auto nbVar = _solver->getNbVariables();
	/*auto checkedVar = _solver->getLastAssignedVar();
	auto opposite = nbVar - (checkedVar+1);
	auto minVar = min(checkedVar, opposite);*/
	Variable checkedVar = p.first;
	Variable opposite = p.second;
	
	if (checkedVar != opposite)
	{
		if (!_solver->isAssignedVar(opposite))
		{
			auto minVar = min(checkedVar, opposite);
			vector<Semantic> researchedTuple(2);
			auto i = checkedVar == minVar ? 0 : 1;
			researchedTuple[i] = _bioDevice->getSemantic(checkedVar);
			
			for (auto it = _solver->getDomain(opposite).begin(), end = _solver->getDomain(opposite).end(); it != end; ++it)
			{
				researchedTuple[1-i] = **it;
				if (_authorizedPatternInMirrors.find(researchedTuple) == _authorizedPatternInMirrors.end())
				{
					auto copy = it;
					--it;
					_solver->removeDomainElement(opposite, copy);
					++_nbRemovedElements[opposite];
				}
			}
		}
		
		if (isSymmetricForbidden(checkedVar, opposite))
		{
			for (auto it = _solver->getDomain(opposite).begin(), end = _solver->getDomain(opposite).end(); it != end; ++it)
			{
				if ((*it)->isAnInversionOf(_bioDevice->getSemantic(checkedVar)))
				{
					auto copy = it;
					--it;
					_solver->removeDomainElement(opposite, copy);
					++_nbRemovedElements[opposite];
					break;
				}
			}
		}
	}
	else 
	{
		vector<Semantic> researchedTuple(1);
		for (auto it = _solver->getDomain(opposite).begin(), end = _solver->getDomain(opposite).end(); it != end; ++it)
		{
			researchedTuple[0] = **it;
			if (_auhtorizedSemanticInMiddleMirrors.find(researchedTuple) == _auhtorizedSemanticInMiddleMirrors.end())
			{
				auto copy = it;
				--it;
				_solver->removeDomainElement(opposite, copy);
				++_nbRemovedElements[opposite];
			}
		}
	}
	
	return !_solver->hasEmptyDomain(opposite);
}

std::optional<pair<NoIPalindromeConstraint::Variable,NoIPalindromeConstraint::Variable>> NoIPalindromeConstraint::getVariablesToCheck() const
{
	auto nbVar = _solver->getNbVariables();
	Variable checkedVar = 0;
	Variable opposite = nbVar-1;
	
	for (size_t i = 0; i <= nbVar/2; ++i)
	{
		auto opposedI = nbVar - (i+1);
		
		if (_solver->isAssignedVar(i) 
			&& _solver->isAssignedVar(opposedI) 
			&& !_bioDevice->getSemantic(opposedI).isAnInversionOf(_bioDevice->getSemantic(i)))
		{
			return {};
		}
		else if (!_solver->isAssignedVar(i) && !_solver->isAssignedVar(opposedI) && i != opposedI)
		{
			return {};
		}
		else if (_solver->isAssignedVar(i) && !_solver->isAssignedVar(opposedI))
		{
			checkedVar = i;
			opposite = nbVar - (checkedVar+1);
			break;
		}
		else if (!_solver->isAssignedVar(i) && _solver->isAssignedVar(opposedI))
		{
			checkedVar = opposedI;
			opposite = nbVar - (checkedVar+1);
			break;
		}
		else if (i == opposedI && ! _solver->isAssignedVar(i))
		{
			checkedVar = opposite = i;
			break;
		}
	}
	
	return make_optional(make_pair(checkedVar, opposite));
}

bool NoIPalindromeConstraint::isSymmetricForbidden(NoIPalindromeConstraint::Variable checkedVar, NoIPalindromeConstraint::Variable opposite) const
{
	auto AfterMinVar = min(checkedVar, opposite)+1;
	auto beforeMaxVar = max(checkedVar, opposite)-1;
	
	while (AfterMinVar < beforeMaxVar && _solver->isAssignedVar(AfterMinVar) && _solver->isAssignedVar(beforeMaxVar) 
		&& _bioDevice->getSemantic(beforeMaxVar).isAnInversionOf(_bioDevice->getSemantic(AfterMinVar)))
	{
		--beforeMaxVar;
		++AfterMinVar;
	}
	
	if (_solver->isAssignedVar(AfterMinVar) && _solver->isAssignedVar(beforeMaxVar))
	{
		if (AfterMinVar != beforeMaxVar)
		{
			vector<Semantic> researchedTuple({
				_bioDevice->getSemantic(AfterMinVar),
				_bioDevice->getSemantic(beforeMaxVar)});
			if (_authorizedPatternInMirrors.find(researchedTuple) == _authorizedPatternInMirrors.end())
			{
				return true;
			}
		}
		else 
		{
			vector<Semantic> researchedTuple(1);
			researchedTuple[0] = _bioDevice->getSemantic(AfterMinVar);
			if (_auhtorizedSemanticInMiddleMirrors.find(researchedTuple) == _auhtorizedSemanticInMiddleMirrors.end())
			{
				return true;
			}
		}
	}
	
	return false;
}

bool NoIPalindromeConstraint::isViolated() const
{
	return true;
}

std::string NoIPalindromeConstraint::getName() const
{
	return string("NoIPalindromeConstraint - ");
}

std::unordered_set<std::vector<::recombinator::api::Semantic>,boost::hash<std::vector<::recombinator::api::Semantic>>> NoIPalindromeConstraint::buildAuthorizedPatternInMirrors()
{
	std::unordered_set<std::vector<::recombinator::api::Semantic>,boost::hash<std::vector<::recombinator::api::Semantic>>> authorizedPatternInMirrors;
	for (auto &sem1 : Semantic::elementarySemantics)
	{
		for (auto &sem2 : Semantic::elementarySemantics)
		{
			auto sem1R = sem1, sem2R = sem2;
			sem1R.reverse(); sem2R.reverse();
			if (authorizedPatternInMirrors.find({{sem2R,sem1R}}) == authorizedPatternInMirrors.end())
			{
				authorizedPatternInMirrors.insert({{sem1,sem2}});
			}
		}
	}
	
	return authorizedPatternInMirrors;
}

std::unordered_set<std::vector<::recombinator::api::Semantic>,boost::hash<std::vector<::recombinator::api::Semantic>>> NoIPalindromeConstraint::buildAuthorizedSemanticInMiddleMirrors()
{
	std::unordered_set<std::vector<::recombinator::api::Semantic>,boost::hash<std::vector<::recombinator::api::Semantic>>> auhtorizedSemanticInMiddleMirrors;
	
	for (auto &sem : Semantic::elementarySemantics)
	{
		auto semR = sem;
		if (auhtorizedSemanticInMiddleMirrors.find({{semR.reverse()}}) == auhtorizedSemanticInMiddleMirrors.end())
		{
			auhtorizedSemanticInMiddleMirrors.insert({{sem}});
		}
	}
	
	return auhtorizedSemanticInMiddleMirrors;
}
