/**
 * Copyright (C) Inserm Montpellier / CBS
 * 
 * U 1054 - mixte avec Université de Montpellier et CNRS
 * Centre de Biochimie Structurale
 * Directeur : Pierre-Emmanuel Milhiet
 * 29, rue de Navacelles
 * 34090 MONTPELLIER
 * 
 * Recombinator project : http://recombinator.lirmm.fr/
 * 
 * Team : Synthetic biology at CBS : http://www.cbs.cnrs.fr/index.php/en/research-equipea4
 * 
 * 
 * @auhtor : Guillaume Pérution-Kihli 
 */

#ifndef _CHECKUTILITYPREFIXANDSUFFIX_H
#define _CHECKUTILITYPREFIXANDSUFFIX_H
#include "CheckUtilityAfterPrefix.hpp"
#include "CheckUtilityBeforeSuffix.hpp"

namespace recombinator 
{
	namespace generator 
	{
		class CheckUtilityPrefixAndSuffix: virtual public CheckUtilityAfterPrefix, virtual public CheckUtilityBeforeSuffix {
		public: 
			CheckUtilityPrefixAndSuffix(::recombinator::api::SemanticalBioDevice* bioDevice, IEDyckSemanticalBioDeviceBuilder* builder, CheckUtility::Variable var);
			
			std::string getName() const;
			
			using CheckUtilityAfterPrefix::_derivedToCheck;
		protected: 
			recombinator::api::Semantic::Utility getUtilityOnDerived (uint8_t derived, const recombinator::api::Semantic* s) const;
			
			recombinator::api::Semantic::Utility getUtility (const recombinator::api::Semantic* s) const;
		};
	}
}

#endif //_CHECKUTILITYBEFORESUFFIX_H
 
