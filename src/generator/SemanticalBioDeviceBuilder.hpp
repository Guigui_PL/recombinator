/**
 * Copyright (C) Inserm Montpellier / CBS
 * 
 * U 1054 - mixte avec Université de Montpellier et CNRS
 * Centre de Biochimie Structurale
 * Directeur : Pierre-Emmanuel Milhiet
 * 29, rue de Navacelles
 * 34090 MONTPELLIER
 * 
 * Recombinator project : http://recombinator.lirmm.fr/
 * 
 * Team : Synthetic biology at CBS : http://www.cbs.cnrs.fr/index.php/en/research-equipea4
 * 
 * 
 * @auhtor : Guillaume Pérution-Kihli 
 */

#ifndef _SEMANTICALBIODEVICEBUILDER_H
#define _SEMANTICALBIODEVICEBUILDER_H

#include "BioDeviceBuilder.hpp"


namespace recombinator 
{
	namespace generator 
	{
		class SemanticalBioDeviceBuilder: public BioDeviceBuilder 
		{
		};
	}
}

#endif //_SEMANTICALBIODEVICEBUILDER_H