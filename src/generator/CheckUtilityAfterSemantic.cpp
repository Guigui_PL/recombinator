/**
 * Copyright (C) Inserm Montpellier / CBS
 * 
 * U 1054 - mixte avec Université de Montpellier et CNRS
 * Centre de Biochimie Structurale
 * Directeur : Pierre-Emmanuel Milhiet
 * 29, rue de Navacelles
 * 34090 MONTPELLIER
 * 
 * Recombinator project : http://recombinator.lirmm.fr/
 * 
 * Team : Synthetic biology at CBS : http://www.cbs.cnrs.fr/index.php/en/research-equipea4
 * 
 * 
 * @auhtor : Guillaume Pérution-Kihli 
 */

#include "CheckUtilityAfterSemantic.hpp"

using namespace recombinator::api;
using namespace recombinator::generator;
using namespace std;

/**
 * CheckUtilityAfterSemantic implementation
 */

const std::array<std::array<recombinator::api::Semantic::Utility,36>,36> CheckUtilityAfterSemantic::_usefulAfter = CheckUtilityAfterSemantic::buildUsefulAfter();

CheckUtilityAfterSemantic::CheckUtilityAfterSemantic(::recombinator::api::SemanticalBioDevice* bioDevice, IEDyckSemanticalBioDeviceBuilder* builder, CheckUtility::Variable var) : 
CheckUtility(bioDevice, builder, var)
{
	unordered_set<CheckUtility::Variable> scope{{var}};
	set<pair<int,int>> checkList;
	for (size_t i{0}; i < _bioDevice->getNbDerivedSemanticalBioDevice(); ++i)
	{
		FunctionnalStructure::SemanticalBioDeviceStructure derived = _bioDevice->getStructure().getDerivedBioDevicesStructures()[i];
		
		for (size_t j{1}; j < derived.size(); ++j)
		{
			if ((size_t)abs(derived[j]) == var)
			{
				if (derived[j] >= 0)
				{
					scope.insert(abs(derived[j-1]));
					checkList.insert(make_pair(derived[j-1], derived[j]));
				}
				else
				{
					scope.insert(abs(derived[j+1]));
					checkList.insert(make_pair(derived[j+1], derived[j]));
				}
			}
		}
	}
	
	_scope.insert(_scope.end(),scope.begin(), scope.end());
	_checkList.insert(_checkList.end(), checkList.begin(), checkList.end());
}

Semantic::Utility CheckUtilityAfterSemantic::getUtility (const Semantic* s) const
{
	if (s->isGF() && s->isPF())
		return static_cast<Semantic::Utility>((uint8_t)getUtility(Semantic::bioFunctionToPointer[(size_t)Semantic::BF::P][(size_t)s->getSemanticReverse().getBioFunction()]) & 
			(uint8_t)getUtility(Semantic::bioFunctionToPointer[(size_t)Semantic::BF::G][(size_t)s->getSemanticReverse().getBioFunction()]));
	if (s->isGR() && s->isPR())
		return static_cast<Semantic::Utility>((uint8_t)getUtility(Semantic::bioFunctionToPointer[(size_t)s->getSemanticForward().getBioFunction()][(size_t)Semantic::BF::P]) & 
			(uint8_t)getUtility(Semantic::bioFunctionToPointer[(size_t)s->getSemanticForward().getBioFunction()][(size_t)Semantic::BF::G]));
	if (s->isX())
		return Semantic::Utility::UTILITY_REVERSE_AND_FORWARD;
		
	uint8_t utility = (uint8_t) Semantic::Utility::USELESS;
	
	for (auto& it : _checkList)
	{
		if (it.second >= 0)
		{
			Semantic before = _bioDevice->getSemantic(abs(it.first));
			if (it.first < 0)
			{
				before.reverse();
			}
			
			utility |= (uint8_t) _usefulAfter[s->getSemanticKey()][before.getSemanticKey()];
		}
		else 
		{
			Semantic before = _bioDevice->getSemantic(abs(it.first));
			if (it.first >= 0)
			{
				before.reverse();
			}
			
			utility |= (uint8_t) _usefulAfter[s->getSemanticKey()][before.getSemanticKey()];
		}
	}
	
	return static_cast<Semantic::Utility>(utility);
}

std::array<std::array<recombinator::api::Semantic::Utility,36>,36> CheckUtilityAfterSemantic::buildUsefulAfter() 
{
    std::array<std::array<recombinator::api::Semantic::Utility,36>,36> usefulAfter;
    
    for (auto &sem1 : Semantic::allSemantics)
    {
        for (auto &sem2 : Semantic::allSemantics)
        {
            usefulAfter[sem1.getSemanticKey()][sem2.getSemanticKey()] = sem1.isUsefulAfter(sem2);
        }
    }
    
    return usefulAfter;
}

std::string CheckUtilityAfterSemantic::getName() const
{
	return string("CheckUtilityAfterSemantic - ")+to_string(_checkedVar);
}
