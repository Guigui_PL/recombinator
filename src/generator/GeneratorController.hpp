/**
 * Copyright (C) Inserm Montpellier / CBS
 * 
 * U 1054 - mixte avec Université de Montpellier et CNRS
 * Centre de Biochimie Structurale
 * Directeur : Pierre-Emmanuel Milhiet
 * 29, rue de Navacelles
 * 34090 MONTPELLIER
 * 
 * Recombinator project : http://recombinator.lirmm.fr/
 * 
 * Team : Synthetic biology at CBS : http://www.cbs.cnrs.fr/index.php/en/research-equipea4
 * 
 * 
 * @auhtor : Guillaume Pérution-Kihli 
 */

#ifndef _GENERATORCONTROLLER_H
#define _GENERATORCONTROLLER_H
#include <cstddef>
#include <memory>
#include <unordered_set>
#include <vector>
#include "Generator.hpp"

namespace recombinator 
{
	namespace generator 
	{
		class GeneratorController 
		{
		public: 
			
			enum class Setting { PRINT_STATISTICS, PRINT_ERRORS };
			static GeneratorController& getInstance();
			
			/**
			* @param nbInputs
			*/
			void launchIEDyckSemanticalBioDeviceGenerator(size_t nbInputs);
			
			void stopGenerators();
			
			/**
			* @param settings
			*/
			void setSettings(std::vector<Setting> settings = {});
		private: 
			static GeneratorController _instance;
			std::vector<std::unique_ptr<Generator>> _generators;
			std::unordered_set<Setting> _settings;
			
			GeneratorController();
			
			void prepareDirectories();
		};
	}
}

#endif //_GENERATORCONTROLLER_H
