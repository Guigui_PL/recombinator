/**
 * Copyright (C) Inserm Montpellier / CBS
 * 
 * U 1054 - mixte avec Université de Montpellier et CNRS
 * Centre de Biochimie Structurale
 * Directeur : Pierre-Emmanuel Milhiet
 * 29, rue de Navacelles
 * 34090 MONTPELLIER
 * 
 * Recombinator project : http://recombinator.lirmm.fr/
 * 
 * Team : Synthetic biology at CBS : http://www.cbs.cnrs.fr/index.php/en/research-equipea4
 * 
 * 
 * @auhtor : Guillaume Pérution-Kihli 
 */

#include "SemanticalBioDeviceConstraintBuilder.hpp"
#include <algorithm>
#include "CheckUtilityAfterPrefix.hpp"
#include "CheckUtilityAfterSemantic.hpp"
#include "CheckUtilityBeforeSuffix.hpp"
#include "CheckUtilityBeforeSemantic.hpp"
#include "CheckUtilityPrefixAndSuffix.hpp"
#include "CheckUtilityBeforeAndAfterSemantic.hpp"
#include "SBDConstraintExtension.hpp"
#include "IEDyckSemanticalBioDeviceBuilder.hpp"
#include "../api/IEDyckFunctionnalStructure.hpp"
#include "NoIPalindromeConstraint.hpp"
#include "NPEquivalentConstraint.hpp"

using namespace recombinator::api;
using namespace recombinator::generator;
using namespace std;

SemanticalBioDeviceConstraintBuilder::SemanticalBioDeviceConstraintBuilder(
	SemanticalBioDevice* bioDevice, 
	IEDyckSemanticalBioDeviceBuilder* solver) :
	_bioDevice(bioDevice),
    _solver(solver)
{
	
}

std::vector<std::shared_ptr<Constraint<const ::recombinator::api::Semantic*>>> SemanticalBioDeviceConstraintBuilder::buildCheckUtilityAfterPrefixConstraints()
{
	vector<std::shared_ptr<Constraint<const ::recombinator::api::Semantic*>>> constraints;
	for (size_t i{0}; i < _solver->getNbVariables(); ++i)
	{
		constraints.emplace_back(new CheckUtilityAfterPrefix(_bioDevice, _solver, i));
	}
	return constraints;
}

std::vector<std::shared_ptr<Constraint<const ::recombinator::api::Semantic*>>> SemanticalBioDeviceConstraintBuilder::buildCheckUtilityPrefixAndSuffixConstraints()
{
	vector<std::shared_ptr<Constraint<const ::recombinator::api::Semantic*>>> constraints;
	for (size_t i{0}; i < _solver->getNbVariables(); ++i)
	{
		constraints.emplace_back(new CheckUtilityPrefixAndSuffix(_bioDevice, _solver, i));
	}
	return constraints;
}

std::vector<std::shared_ptr<Constraint<const ::recombinator::api::Semantic*>>> SemanticalBioDeviceConstraintBuilder::buildCheckUtilityAfterSemanticConstraints()
{
	vector<std::shared_ptr<Constraint<const ::recombinator::api::Semantic*>>> constraints;
	for (size_t i{1}; i < _solver->getNbVariables(); ++i)
	{
		constraints.emplace_back(new CheckUtilityAfterSemantic(_bioDevice, _solver, i));
	}
	return constraints;
}

std::vector<std::shared_ptr<Constraint<const ::recombinator::api::Semantic*>>> SemanticalBioDeviceConstraintBuilder::buildCheckUtilityBeforeSuffixConstraints()
{
	vector<std::shared_ptr<Constraint<const ::recombinator::api::Semantic*>>> constraints;
	for (size_t i{0}; i < _solver->getNbVariables(); ++i)
	{
		constraints.emplace_back(new CheckUtilityBeforeSuffix(_bioDevice, _solver, i));
	}
	return constraints;
}

std::vector<std::shared_ptr<Constraint<const ::recombinator::api::Semantic*>>> SemanticalBioDeviceConstraintBuilder::buildCheckUtilityBeforeSemanticConstraints()
{
	vector<std::shared_ptr<Constraint<const ::recombinator::api::Semantic*>>> constraints;
	for (size_t i{0}; i < _solver->getNbVariables()-1; ++i)
	{
		constraints.emplace_back(new CheckUtilityBeforeSemantic(_bioDevice, _solver, i));
	}
	return constraints;
}

std::vector<std::shared_ptr<Constraint<const ::recombinator::api::Semantic*>>> SemanticalBioDeviceConstraintBuilder::buildCheckUtilityBeforeAndAfterSemanticConstraints()
{
	vector<std::shared_ptr<Constraint<const ::recombinator::api::Semantic*>>> constraints;
	for (size_t i{1}; i < _solver->getNbVariables()-1; ++i)
	{
		constraints.emplace_back(new CheckUtilityBeforeAndAfterSemantic(_bioDevice, _solver, i));
	}
	return constraints;
}

std::vector<std::shared_ptr<Constraint<const ::recombinator::api::Semantic*>>> SemanticalBioDeviceConstraintBuilder::buildAtomicSitesConstraints()
{
	vector<std::shared_ptr<Constraint<const ::recombinator::api::Semantic*>>> constraints;
	
	// No null semantic in atomic excisions
	vector<uint16_t> positions = dynamic_cast<const IEFunctionnalStructure*>(&(_bioDevice->getStructure()))->getPositionsSemInAtomicExcisions();
	std::unordered_set<std::vector<recombinator::api::Semantic>,boost::hash<std::vector<::recombinator::api::Semantic>>> tuples;
	
	for (auto &sem : Semantic::elementarySemantics)
	{
		if (!sem.isN())
		{
			tuples.emplace(1,sem);
		}
	}
	
	for (auto &pos : positions)
	{
		constraints.emplace_back(new SBDConstraintExtension(_bioDevice, _solver, {{pos}}, tuples, "atomic excision constraint "+to_string(pos)));
	}
	
	tuples.clear();
	positions.clear();
	
	// No symetric semantics in atomic inversions
	positions = dynamic_cast<const IEFunctionnalStructure*>(&(_bioDevice->getStructure()))->getPositionsSemInAtomicInversions();
	for (auto &sem : Semantic::elementarySemantics)
	{
		if (!sem.isMirror())
		{
			tuples.emplace(1,sem);
		}
	}
	
	for (auto &pos : positions)
	{
		constraints.emplace_back(new SBDConstraintExtension(_bioDevice, _solver, {{pos}}, tuples, "atomic inversion constraint "+to_string(pos)));
	}
	
	return constraints;
}

std::vector<std::shared_ptr<Constraint<const ::recombinator::api::Semantic*>>> SemanticalBioDeviceConstraintBuilder::buildExpOnlyInExcisionConstraints()
{
	vector<std::shared_ptr<Constraint<const ::recombinator::api::Semantic*>>> constraints;
	std::unordered_set<std::vector<recombinator::api::Semantic>,boost::hash<std::vector<::recombinator::api::Semantic>>> tuples;
	
	for (auto &sem : Semantic::elementarySemantics)
	{
		if (!sem.isX())
		{
			tuples.emplace(1,sem);
		}
	}
	
	for (size_t i{0}; i < _solver->getNbVariables(); ++i)
	{
		if (!dynamic_cast<const IEFunctionnalStructure*>(&(_bioDevice->getStructure()))->isInExcision(i))
		{
			constraints.emplace_back(new SBDConstraintExtension(_bioDevice, _solver, {{i}}, tuples, "exp constraint "+to_string(i)));
		}
	}
	
	return constraints;
}

std::vector<std::shared_ptr<Constraint<const ::recombinator::api::Semantic*>>> SemanticalBioDeviceConstraintBuilder::buildNPEquivalentsConstraint()
{
	vector<std::shared_ptr<Constraint<const ::recombinator::api::Semantic*>>> constraints;
	
	auto pairs = dynamic_cast<const IEDyckFunctionnalStructure*>(&(_bioDevice->getStructure()))->getNPEquivalentScopes();
	auto strongerPairs = dynamic_cast<const IEDyckFunctionnalStructure*>(&(_bioDevice->getStructure()))->getNPEquivalentForConstraintExtension();
	
	for (auto &p : pairs)
	{
		std::unordered_set<std::pair<size_t,size_t>,boost::hash< std::pair<size_t, size_t>>> strongerPairsToAdd;
		
		for (auto sp : strongerPairs)
		{
			if (sp.first > sp.second)
			{
				swap(sp.first, sp.second);
			}
			
			if (sp.first >= p.first && sp.second <= p.second)
			{
				strongerPairsToAdd.insert(sp);
			}
		}
		
		constraints.emplace_back(new NPEquivalentConstraint(_bioDevice, _solver, p.first, p.second, strongerPairsToAdd));
	}
	
	/*
	for (auto &p : pairs)
	{
		constraints.emplace_back(new SBDConstraintExtension(
			_bioDevice, 
			_solver, 
			{{p.first, p.second}}, 
			tuples, 
			"NP equivalent constraint in extension "+to_string(p.first)+" - "+to_string(p.second)));
	}*/
	
	return constraints;
}

std::vector<std::shared_ptr<Constraint<const ::recombinator::api::Semantic*>>> SemanticalBioDeviceConstraintBuilder::buildNoIPalindromeConstraints()
{
	vector<std::shared_ptr<Constraint<const ::recombinator::api::Semantic*>>> constraints;
	constraints.emplace_back(new NoIPalindromeConstraint(_bioDevice, _solver));
	
	return constraints;
}
