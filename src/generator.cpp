#include <csignal>
#include <string>
#include <iostream>
#include "boost/program_options.hpp"
#include "boost/algorithm/string/trim.hpp"
#include "generator/GeneratorController.hpp"
#include "api/ThreadsManager.hpp"
#include "api/DyckSemanticalBioDevice.hpp"

using namespace std;
using namespace recombinator::generator;
using namespace recombinator::api;

// Return value for the main function
namespace 
{
	const size_t SUCCESS = 0;
	const size_t ERROR_IN_COMMAND_LINE = 1;
	const size_t ERROR_IN_MAIN = 2;
	const size_t UNKNOWN_EXCEPTION = 3;
}

void handlerSIGINT (int);

int main (int argc, char** argv)
{
	// For manage the program options, we use the library boost::program_options
	namespace po = boost::program_options;
	
	// We create container for paramaters of options
	vector<string> optContainer;
	po::variables_map vm;
	po::options_description desc("Allowed options");
	
	try
	{
		// We create the list of all options of our program
		desc.add_options()
		("launch,l", po::value<vector<string>>(&optContainer)->multitoken(),
		 "arg : \t<folder>\n")
		("test,t", 
		 "arg : \t<folder>\n")
		("threadsLimit,t", po::value<size_t>(), "Modify the limit of launched threads' number.\n")
		;
		
		// We parse the options used by the user
		po::store(po::parse_command_line(argc, argv, desc), vm);
		po::notify(vm);
		
		/* ----------
		 * launch-l
		 * Launch a generation
		 * ---------- */
		
		if (vm.count("launch"))
		{
			auto& generatorController = GeneratorController::getInstance();
			auto& threadsManager = ThreadsManager::getThreadsManager();
			signal(SIGINT, handlerSIGINT);
			
			if (vm.count("threadsLimit") && vm["threadsLimit"].as<size_t>())
				threadsManager._limitNbLaunchedThreads = vm["threadsLimit"].as<size_t>();
			
			if (optContainer[0] == "IEDyck")
			{
				generatorController.launchIEDyckSemanticalBioDeviceGenerator(stoi(optContainer[1]));
			}
			
			return SUCCESS;
		}
		
		if (vm.count("test"))
		{
			DyckSemanticalBioDevice sbd("(a PF )a GF [b PF GF ]b GR [c TR PF [d GF ]d ]c PR ");
			
			size_t prefixOf = 4;
			cout << "prefixes of " << (string)sbd.getSemantic(prefixOf) << " in " << (string)sbd << endl;
			for (size_t i = 0; i < sbd.getNbDerivedSemanticalBioDevice(); ++i)
			{
				if (sbd.getDerivedSemanticalBioDevice(i).isIn(prefixOf))
				{
					cout << (string)sbd.getPrefix(i,prefixOf) << " - semantics of derived : ";
					for (auto &it : sbd.getDerivedSemanticalBioDevice(i))
					{
						cout << (string)it << " - ";
					}
					cout << endl;
				}
			}
			
			size_t suffixOf = 2;
			cout << "suffixes of " << (string)sbd.getSemantic(suffixOf) << " in " << (string)sbd << endl;
			for (size_t i = 0; i < sbd.getNbDerivedSemanticalBioDevice(); ++i)
			{
				if (sbd.getDerivedSemanticalBioDevice(i).isIn(suffixOf))
				{
					cout << (string)sbd.getSuffix(i,suffixOf) << " - semantics of derived : ";
					for (auto &it : sbd.getDerivedSemanticalBioDevice(i))
					{
						cout << (string)it << " - ";
					}
					cout << endl;
				}
			}
			
			return SUCCESS;
		}
		
		/* ----------
		 *        Help-h
		 * ---------- */
		
		if(vm.count("help"))
		{
			cout << desc << endl;
			return SUCCESS;
		}
	}
	catch(boost::program_options::error& e)
	{
		cerr << "Command line error: " << e.what() << endl << "For further help, read the --help." << endl;
		return ERROR_IN_COMMAND_LINE;
	}
	catch(exception& e)
	{
		cerr << e.what() << endl << "End of process." << endl;
		return UNKNOWN_EXCEPTION;
	}
	
	return 0;
}

void handlerSIGINT (int)
{
    GeneratorController::getInstance().stopGenerators();
    cout << endl << "Stopping, please wait during the saving of the state..." << endl << endl;
}