#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MAIN
#include <exception>
#include <set>
#include <vector>
#include <boost/test/unit_test.hpp>
#include "../api/IEDyckFunctionnalStructure.hpp"

using namespace recombinator::api;
using namespace std; 


BOOST_AUTO_TEST_CASE(checkStructureValidity)
{
	// We start by create a vector with invalid structures
	vector<string> invalid ({
		"(1[2&]2)1", // Invalid character in structure 
		"(1(2)2)1)0", "(1(2)2)1]0", // Too closing AMIs
		"(1(2)2", "[1(2)2", // Some AMIs unclosed
		"(1(2)3)1", "(1(1)1)0", // Invalid indexes
		"(1[2]1)2", "[1(2)1]2", // Wrong associations between indexes and AMIs of different types
		"(1[2)1]2", "(1(2)1)2", "[1[2]1]2" // Structures not of dyck word shape
	});
	
	for (auto &i : invalid)
	{
		BOOST_CHECK_THROW(IEDyckFunctionnalStructure::create<IEDyckFunctionnalStructure>(i), invalid_argument);
	}
} 

BOOST_AUTO_TEST_CASE(checkNbOfDistinctActivations)
{
	// We create a vector of (valid) structures 
	// number of activations -> structure
	unordered_multimap<int,string> structures ({
		{1,""},
		{2,"[0]0"}, {2,"(0)0"},
		{4, "(1[2]2)1"},
		{8, "(1(2[3]3)2)1"},
		{16, "(1(2[3]3(4)4)2)1"}
	});
	
	for (auto &e : structures)
	{
		auto structure = IEDyckFunctionnalStructure::create<IEDyckFunctionnalStructure>(e.second);
		BOOST_REQUIRE_EQUAL(e.first,structure->getActivations().size());
		set<vector<uint8_t>> distinctActivations (structure->getActivations().begin(), structure->getActivations().end());
		BOOST_REQUIRE_EQUAL(e.first,distinctActivations.size());
	}
}

BOOST_AUTO_TEST_CASE(checkDerivedBioDevs)
{
	auto structure = IEDyckFunctionnalStructure::create<IEDyckFunctionnalStructure>("(1[2]2)1");
	map<IEDyckFunctionnalStructure::Activation, IEDyckFunctionnalStructure::SemanticalBioDeviceStructure> structAct ({
		{{},{0,1,2,3,4}},
		{{0},{0,-3,-2,-1,4}},
		{{1},{0,1,3,4}},
		{{0,1},{0,-3,-1,4}}
	});
	
	for (auto &a: structAct)
	{
		BOOST_CHECK(structure->getDerivedBioDevicesStructure(a.first) == a.second);
	}
}