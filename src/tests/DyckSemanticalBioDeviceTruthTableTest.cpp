#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MAIN
#include <algorithm>
#include <fstream>
#include <vector>
#include <boost/test/unit_test.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>
#include "../api/DyckSemanticalBioDeviceTruthTable.hpp"

using namespace recombinator::api;
using namespace std;
 
BOOST_AUTO_TEST_CASE(checkImplementedBooleanFunction)
{
	namespace fs = boost::filesystem;
	vector <string> folders;
	folders.push_back("tests/files/classicalBioDevices");
	
	while (!folders.empty())
	{
		string inputPath = folders.back();
		folders.pop_back();
		
		for(fs::directory_iterator it(inputPath), end; it != end; ++it) 
		{
			const fs::path path(it->path());
			if(fs::is_regular_file(it->status()))
			{
				string line;
				ifstream file (path.string());
				
				if (file.is_open())
				{
					while (getline(file, line))
					{
						boost::trim(line);
						DyckSemanticalBioDevice sbd(line);
						
						DyckSemanticalBioDeviceTruthTable truthTable(&sbd);
						string implementedBooleanFunction = path.filename().string();
						string reversed = implementedBooleanFunction;
						reverse(reversed.begin(), reversed.end());
						
						BOOST_REQUIRE_MESSAGE(implementedBooleanFunction == truthTable.getDisjunctiveNormalForm() || reversed == truthTable.getDisjunctiveNormalForm(),static_cast<string>(sbd)+
							" does not implement "+ truthTable.getDisjunctiveNormalForm() + 
							" but " + implementedBooleanFunction + "!");
					}
				}
			}
			else if (is_directory(path))
			{
				folders.push_back(path.string());
			}
		}
	}
} 
