#!/bin/bash  
set -e;
path=../build/tests/
tests=( $(ls $path) )
for i in ${tests[*]}
do
	echo
	echo "--------------------"
	echo $i
	chmod +x $path$i
	$path$i
	echo "--------------------"
	#rm $path$i
	done