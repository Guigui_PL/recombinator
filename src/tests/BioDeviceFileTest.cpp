#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MAIN
#include <algorithm>
#include <fstream>
#include <unordered_map>
#include <vector>
#include <boost/test/unit_test.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>
#include "../api/BioDeviceFile.hpp"
#include "../api/DyckSemanticalBioDeviceTruthTable.hpp"

using namespace recombinator::api;
using namespace std;
namespace fs = boost::filesystem;
 
BOOST_AUTO_TEST_CASE(checkDyckSemanticalBioDeviceFileReader)
{
	vector <string> folders;
	folders.push_back("tests/files/classicalBioDevices");
	
	fs::path tmp{fs::temp_directory_path()}, recombinatorTests{tmp};
	recombinatorTests += "/recombinatorTests";
	fs::path uniqueFolder{recombinatorTests}, multipFolder{recombinatorTests};
	uniqueFolder += "/unique";
	multipFolder += "/multip";
	BOOST_REQUIRE_MESSAGE((fs::exists(recombinatorTests) || create_directory(recombinatorTests)) 
        && (fs::exists(uniqueFolder) || create_directory(uniqueFolder))
        && (fs::exists(multipFolder) || create_directory(multipFolder)),
		"The programm can't create a temporary folder to execute its tests on reading / writing files containing biodevices");
	
	unordered_map<string,DyckSemanticalBioDeviceFileWriter> writers;
	
	while (!folders.empty())
	{
		string inputPath = folders.back();
		folders.pop_back();
		
		for(fs::directory_iterator it(inputPath), end; it != end; ++it) 
		{
			const fs::path path(it->path());
			if(fs::is_regular_file(it->status()))
			{
				DyckSemanticalBioDeviceFileReader reader(path.string());
				
				DyckSemanticalBioDevice sbd;
				
				while (reader >> sbd)
				{
					DyckSemanticalBioDeviceTruthTable truthTable(&sbd);
					string implementedBooleanFunction = path.filename().string();
					string reversed = implementedBooleanFunction;
					reverse(reversed.begin(), reversed.end());
					
					BOOST_REQUIRE_MESSAGE(implementedBooleanFunction == truthTable.getDisjunctiveNormalForm() || reversed == truthTable.getDisjunctiveNormalForm(),static_cast<string>(sbd)+
						" does not implement "+ truthTable.getDisjunctiveNormalForm() + 
						" but " + implementedBooleanFunction + "!");
					
					if (writers.find(truthTable.getDisjunctiveNormalForm()) == writers.end())
					{
						writers.insert({truthTable.getDisjunctiveNormalForm(),
							DyckSemanticalBioDeviceFileWriter(multipFolder.string()+"/"+truthTable.getDisjunctiveNormalForm(), 
																   SemanticalBioDeviceFile::FileType::MULTI_STRUCTURE)});
					}
					
					writers.at(truthTable.getDisjunctiveNormalForm()) << sbd;
				}
			}
			else if (is_directory(path))
			{
				folders.push_back(path.string());
			}
		}
	}
	
	writers.clear();
	folders.push_back(multipFolder.string());
	
	while (!folders.empty())
	{
		string inputPath = folders.back();
		folders.pop_back();
		
		for(fs::directory_iterator it(inputPath), end; it != end; ++it) 
		{
			const fs::path path(it->path());
			if(fs::is_regular_file(it->status()))
			{
				DyckSemanticalBioDeviceFileReader reader(path.string());
				
				DyckSemanticalBioDevice sbd;
				
				while (reader >> sbd)
				{
					DyckSemanticalBioDeviceTruthTable truthTable(&sbd);
					string implementedBooleanFunction = path.filename().string();
					
					BOOST_REQUIRE_MESSAGE(implementedBooleanFunction == truthTable.getDisjunctiveNormalForm(),static_cast<string>(sbd)+
						" does not implement "+ truthTable.getDisjunctiveNormalForm() + 
						" but " + implementedBooleanFunction + "!");
				}
			}
			else if (is_directory(path))
			{
				folders.push_back(path.string());
			}
		}
	}
	
	
}  
