#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MAIN
#include <array>
#include <cmath>
#include <set>
#include <vector>
#include <fstream>
#include <boost/test/unit_test.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>
#include "../api/SemanticalBioDevice.hpp"
#include "../api/IEFunctionnalStructure.hpp"

using namespace recombinator::api;
using namespace std;

// const array<Semantic,28> Semantic::allSemantics {
// 			{
// 				Semantic(BF::N, BF::N), Semantic(BF::N, BF::P), Semantic(BF::N, BF::T), Semantic(BF::N, BF::G), Semantic(BF::N, BF::GP),
// 				Semantic(BF::P, BF::N), Semantic(BF::P, BF::P), Semantic(BF::P, BF::T), Semantic(BF::P, BF::G), Semantic(BF::P, BF::GP),
// 				Semantic(BF::T, BF::N), Semantic(BF::T, BF::P), Semantic(BF::T, BF::T), Semantic(BF::T, BF::G), Semantic(BF::T, BF::GP),
// 				Semantic(BF::G, BF::N), Semantic(BF::G, BF::P), Semantic(BF::G, BF::T), Semantic(BF::G, BF::G), Semantic(BF::G, BF::GP),
// 				Semantic(BF::GP, BF::N), Semantic(BF::GP, BF::P), Semantic(BF::GP, BF::T), Semantic(BF::GP, BF::G), Semantic(BF::GP, BF::GP),
// 				Semantic(BF::X, BF::X), Semantic(BF::N, BF::X), Semantic(BF::X, BF::N)
// 			}
// 		};

auto &sems = Semantic::allSemantics;

BOOST_AUTO_TEST_CASE(checkProperties4toTest)
{
	namespace fs = boost::filesystem;
	vector <string> folders;
	folders.push_back("tests/files/4toTest");
	
	while (!folders.empty())
	{
		string inputPath = folders.back();
		folders.pop_back();
		
		for(fs::directory_iterator it(inputPath), end; it != end; ++it) 
		{
			const fs::path path(it->path());
			if(fs::is_regular_file(it->status()))
			{
				string line;
				ifstream file (path.string());
				
				if (file.is_open())
				{
					while (getline(file, line))
					{
						boost::trim(line);
						SemanticalBioDevice sbd(line);
						BOOST_REQUIRE_MESSAGE(!sbd.isReducible(),static_cast<string>(sbd)+" is not reducible !");
						BOOST_REQUIRE_MESSAGE(
							sbd.isRespectedStrongConstraint()<=sbd.isRespectedWeakConstraint(),
							line + " : "+static_cast<string>(sbd)+" respects weak constraint !"+to_string(sbd.isRespectedStrongConstraint())+"<="+to_string(sbd.isRespectedWeakConstraint()));
						BOOST_REQUIRE_MESSAGE(
							sbd.isAllSemanticsDefined(),
							line + " : "+static_cast<string>(sbd)+" has all of its semantics defined !");
						for (size_t j{0}; j < sbd.getNbSemantics(); ++j)
						{
							if (sbd.getSemantic(j).isX())
								sbd.setSemantic(j, &Semantic::elementarySemantics[25]);
						}
						BOOST_REQUIRE_MESSAGE(!sbd.isReducible(),static_cast<string>(sbd)+" is not reducible !");
					}
				}
			}
			else if (is_directory(path))
			{
				folders.push_back(path.string());
			}
		}
	}
} 

BOOST_AUTO_TEST_CASE(checkConstructorStructureAndSemantics)
{
	const vector<pair<shared_ptr<FunctionnalStructure>,vector<const Semantic*>>> basesToBuild ({
		{shared_ptr<FunctionnalStructure>(IEFunctionnalStructure::create<IEFunctionnalStructure>("[0]0").release()),{&sems[5], &sems[10], &sems[15]}},
		{shared_ptr<FunctionnalStructure>(IEFunctionnalStructure::create<IEFunctionnalStructure>("(0)0").release()),{&sems[5], &sems[10], &sems[15]}},
		{shared_ptr<FunctionnalStructure>(IEFunctionnalStructure::create<IEFunctionnalStructure>("[0(1)1]0").release()),{&sems[5], &sems[10], &sems[1], &sems[10], &sems[15]}},
		{shared_ptr<FunctionnalStructure>(IEFunctionnalStructure::create<IEFunctionnalStructure>("[0[1]1]0").release()),{&sems[5], &sems[10], &sems[1], &sems[10], &sems[15]}},
		{shared_ptr<FunctionnalStructure>(IEFunctionnalStructure::create<IEFunctionnalStructure>("[0[1]0]1").release()),{&sems[5], &sems[10], &sems[1], &sems[10], &sems[15]}}
	});
	
	for (const auto &b: basesToBuild)
	{
		SemanticalBioDevice sbd(*(b.first),b.second);
		
		BOOST_REQUIRE_MESSAGE(
			sbd.getNbDefinedSemantics() == b.second.size(), 
			"nbDefinedSemantics not valid in "+static_cast<string>(sbd)+"\n"
			+to_string(sbd.getNbDefinedSemantics())+" != "+to_string(b.second.size())
		);
		BOOST_REQUIRE_MESSAGE(
			sbd.getNbSemantics() == b.second.size(), 
			"nbSemantics not valid in "+static_cast<string>(sbd)+"\n"
			+to_string(sbd.getNbDefinedSemantics())+" != "+to_string(b.second.size())
		);
		BOOST_REQUIRE_MESSAGE(
			sbd.getStructure().size() == b.first->size(), 
			"structure size not valid in "+static_cast<string>(sbd)+"\n"
			+to_string(sbd.getStructure().size())+" != "+to_string(b.first->size())
		);
		BOOST_REQUIRE_MESSAGE(
			sbd.isAllSemanticsDefined(), 
			"isAllSemanticsDefined not valid in "+static_cast<string>(sbd)+"\n"
			+"It should return true"
		);
	}
	
	const vector<pair<shared_ptr<FunctionnalStructure>,vector<const Semantic*>>> invalids ({
		{shared_ptr<FunctionnalStructure>(IEFunctionnalStructure::create<IEFunctionnalStructure>("[0(1)1]0").release()),{&sems[5], &sems[1], &sems[10], &sems[15]}},
		{shared_ptr<FunctionnalStructure>(IEFunctionnalStructure::create<IEFunctionnalStructure>("[0]0").release()),{&sems[5], &sems[10], &sems[1], &sems[15]}},
	});
	
	for (const auto &b: invalids)
	{
		BOOST_CHECK_THROW(SemanticalBioDevice(*(b.first),b.second), invalid_argument);
	}
} 

BOOST_AUTO_TEST_CASE(checkConstructorString)
{
	const vector<string> basesToBuild ({
		"PF SFa TF SFA GF",
		"PR SRA TF SRA GF",
		"PF SFA TF SFA SFB GF SFB GF",
		"PF SFA TF SFA SFB TF SFB GF",
		"PF SFB TF SFA SFB TF SFA GF"
	});
	
	for (const auto &b: basesToBuild)
	{
		SemanticalBioDevice sbd(b);
		
		BOOST_REQUIRE_MESSAGE(
			sbd.getNbSemantics() == sbd.getNbDefinedSemantics(), 
			"nbSemantics != nbDefinedSemantics in "+static_cast<string>(sbd)+"\n"
			+to_string(sbd.getNbDefinedSemantics())+" != "+to_string(sbd.getNbDefinedSemantics())
		);
		BOOST_REQUIRE_MESSAGE(
			sbd.isAllSemanticsDefined(), 
			"isAllSemanticsDefined not valid in "+static_cast<string>(sbd)+"\n"
			+"It should return true"
		);
	}
	
	const vector<string> invalids ({
		"PF SFa TF SFA sfa GF",
		"PR SRA TF SRA GF sdsq",
		"PF SFA TF SFA SFB GF SFa GF",
		"PF SFA TF SFA SFB TF SF GF",
		"PF SFB TF SFA TF SFA GF"
	});
	
	for (const string &i: invalids)
	{
		BOOST_CHECK_THROW(SemanticalBioDevice sbd(i), invalid_argument);
	}
} 

BOOST_AUTO_TEST_CASE(checkProperties)
{
	namespace fs = boost::filesystem;
	vector <string> folders;
	folders.push_back("tests/files/classicalBioDevices");
	
	while (!folders.empty())
	{
		string inputPath = folders.back();
		folders.pop_back();
		
		for(fs::directory_iterator it(inputPath), end; it != end; ++it) 
		{
			const fs::path path(it->path());
			if(fs::is_regular_file(it->status()))
			{
				string line;
				ifstream file (path.string());
				
				if (file.is_open())
				{
					while (getline(file, line))
					{
						boost::trim(line);
						SemanticalBioDevice sbd(line);
						//BOOST_REQUIRE_MESSAGE(!sbd.isReducible(),static_cast<string>(sbd)+" is not reducible !");
						BOOST_REQUIRE_MESSAGE(
							sbd.isRespectedStrongConstraint()<=sbd.isRespectedWeakConstraint(),
							line + " : "+static_cast<string>(sbd)+" respects weak constraint !"+to_string(sbd.isRespectedStrongConstraint())+"<="+to_string(sbd.isRespectedWeakConstraint()));
						BOOST_REQUIRE_MESSAGE(
							sbd.isAllSemanticsDefined(),
							line + " : "+static_cast<string>(sbd)+" has all of its semantics defined !");
					}
				}
			}
			else if (is_directory(path))
			{
				folders.push_back(path.string());
			}
		}
	}
} 

BOOST_AUTO_TEST_CASE(checkDistancePG)
{
	const vector<pair<int,SemanticalBioDevice>> toTest ({
		{40,SemanticalBioDevice("(a GF )a PR")},
	});
	
	for (auto &it : toTest)
	{
		BOOST_REQUIRE_MESSAGE(
							it.second.getMaxDistancePromoterToGene() == it.first,
							static_cast<string>(it.second)+" has distance PG = "+to_string(it.first)+", not "+to_string(it.second.getMaxDistancePromoterToGene())+"!");
	}
}
