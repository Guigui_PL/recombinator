#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MAIN
#include <array>
#include <set>
#include <vector>
#include <boost/test/unit_test.hpp>
#include "../api/Semantic.hpp"

using namespace recombinator::api;
using namespace std;

typedef OneWaySemantic::BioFunction BF;

vector<OneWaySemantic> OWSList {{OneWaySemantic(BF::N), OneWaySemantic(BF::X), OneWaySemantic(BF::T), OneWaySemantic(BF::P), OneWaySemantic(BF::G), OneWaySemantic(BF::GP)}};
vector<BF> BFList {{BF::N, BF::P, BF::T, BF::G, BF::GP, BF::X}};

BOOST_AUTO_TEST_CASE(checkConstructors)
{
	// Test of the constructor which receives two OneWaySemantic in arguments
	for (auto &ows1 : OWSList)
	{
		for (auto &ows2 : OWSList)
		{
			// If we try to create an invalid Semantic, we must throw InvalidSemanticArgument exception
			if (
				(ows1.getBioFunction() == BF::X && !ows2.isN() && !ows2.isX())
				|| (ows2.getBioFunction() == BF::X && !ows1.isN() && !ows1.isX())
			)
			{
				BOOST_CHECK_THROW(Semantic(ows1, ows2), Semantic::InvalidSemanticArgument);
			}
			// If we create a valid Semantic, we check if the getters return the right OneWaySemantics
			else 
			{
				Semantic s (ows1, ows2);
				BOOST_REQUIRE_MESSAGE(s.getSemanticForward() == ows1, 
									  "Check the constructor (arguments : OneWaySemantic) and the getters - forward: "+
									  static_cast<string>(s.getSemanticForward())+" != "+
									  static_cast<string>(ows1));
				BOOST_REQUIRE_MESSAGE(s.getSemanticReverse() == ows2, 
									  "Check the constructor (arguments : OneWaySemantic) and the getters - reverse: "+
									  static_cast<string>(s.getSemanticReverse())+" != "+
									  static_cast<string>(ows2));
			}
		}
	}
	
	// Test of the constructor which receives two BioFunction in arguments
	for (auto &bf1 : BFList)
	{
		for (auto &bf2 : BFList)
		{
			// If we try to create an invalid Semantic, we must throw InvalidSemanticArgument exception
			if (
				(bf1 == BF::X && bf2 != BF::N && bf2 != BF::X)
				|| (bf2 == BF::X && bf1 != BF::N && bf1 != BF::X)
			)
			{
				BOOST_CHECK_THROW(Semantic(bf1, bf2), Semantic::InvalidSemanticArgument);
			}
			// If we create a valid Semantic, we check if the getters return the right OneWaySemantics
			else 
			{
				Semantic s (bf1, bf2);
				BOOST_REQUIRE_MESSAGE(s.getSemanticForward().getBioFunction() == bf1, 
									  "Check the constructor (arguments : BioFunction) and the getters - forward: "+
									  static_cast<string>(s.getSemanticForward())+" != "+
									  static_cast<string>(OneWaySemantic(bf1)));
				BOOST_REQUIRE_MESSAGE(s.getSemanticReverse().getBioFunction() == bf2, 
									  "Check the constructor (arguments : BioFunction) and the getters - reverse: "+
									  static_cast<string>(s.getSemanticReverse())+" != "+
									  static_cast<string>(OneWaySemantic(bf2)));
			}
		}
	}
	
	// Test of the default constructor
	Semantic s;
	BOOST_REQUIRE_MESSAGE(s.getSemanticForward().getBioFunction() == BF::N, 
							"Check the default constructor and the getters - forward: "+
							static_cast<string>(s.getSemanticForward())+" != N");
	BOOST_REQUIRE_MESSAGE(s.getSemanticReverse().getBioFunction() == BF::N, 
							"Check the default constructor and the getters -reverse: "+
							static_cast<string>(s.getSemanticReverse())+" != N");
	
}


BOOST_AUTO_TEST_CASE(checkCombine)
{
	array<array<BF,6>,6> results
	{{
		{BF::N, BF::P, BF::T, BF::G, BF::GP, BF::X},
		{BF::P, BF::P, BF::T, BF::X, BF::X, BF::X},
		{BF::T, BF::P, BF::T, BF::T, BF::P, BF::X},
		{BF::G, BF::GP, BF::G, BF::G, BF::GP, BF::X},
		{BF::GP, BF::GP, BF::G, BF::X, BF::X, BF::X},
		{BF::X, BF::X, BF::X, BF::X, BF::X, BF::X}
	}};
	
	for (size_t i(0); i < BFList.size(); ++i)
	{
		
		for (size_t j(0); j < BFList.size(); ++j)
		{
			
			for (size_t k(0); k < BFList.size(); ++k)
			{
				
				for (size_t l(0); l < BFList.size(); ++l)
				{
					BF bf1(BFList[i]), bf2(BFList[j]), bf3(BFList[k]), bf4(BFList[l]);
					if (
						!(
							(bf1 == BF::X && bf2 != BF::N && bf2 != BF::X)
							|| (bf2 == BF::X && bf1 != BF::N && bf1 != BF::X)
						)
						&& !(
							(bf3 == BF::X && bf4 != BF::N && bf4 != BF::X)
							|| (bf4 == BF::X && bf3 != BF::N && bf3 != BF::X)
						)
					)
					{
						Semantic s1(bf1, bf2);
						Semantic s2(bf3, bf4);
						
						// combine static method
						Semantic s3(Semantic::combine(s1, s2));
						if (!s3.isX())
						{
							BOOST_REQUIRE_MESSAGE(
								s3.getSemanticForward().getBioFunction() == results[i][k]
								&& s3.getSemanticReverse().getBioFunction() == results[l][j], 
								"combine("+static_cast<string>(s1)+","
									+static_cast<string>(s2)+") = "
									+static_cast<string>(s3)+" != " 
									+static_cast<string>(Semantic(results[i][k], results[l][j])));
						}
						else 
						{
							if (!(results[i][k] == BF::X || results[l][j] == BF::X))
							{
								 BOOST_ERROR ("combine("+static_cast<string>(s1)+","
									+static_cast<string>(s2)+") = "
									+static_cast<string>(s3)+" != " 
									+static_cast<string>(Semantic(results[i][k], results[l][j])));
							}
							
							if (results[i][k] == BF::X)
							{
								BOOST_REQUIRE_MESSAGE(
									s3.getSemanticForward().getBioFunction() == results[i][k]
									&& (s3.getSemanticReverse().getBioFunction() == BF::N 
										|| s3.getSemanticReverse().getBioFunction() == BF::X),
									"combine("+static_cast<string>(s1)+","
										+static_cast<string>(s2)+") = "
										+static_cast<string>(s3)+" != (" 
										+static_cast<string>(OneWaySemantic(results[i][k]))+
										", fX or fN)");
							}
							if (results[l][j] == BF::X)
							{
								BOOST_REQUIRE_MESSAGE(
									s3.getSemanticReverse().getBioFunction() == results[l][j]
									&& (s3.getSemanticForward().getBioFunction() == BF::N 
										|| s3.getSemanticForward().getBioFunction() == BF::X), 
										"combine("+static_cast<string>(s1)+","
												+static_cast<string>(s2)+") = "
												+static_cast<string>(s3)+" != (fX or fN, " 
												+static_cast<string>(OneWaySemantic(results[l][j]))+
												")");
							}
						}
						
						// operator+
						s3 = s1 + s2;
						if (!s3.isX())
						{
							BOOST_REQUIRE_MESSAGE(
								s3.getSemanticForward().getBioFunction() == results[i][k]
								&& s3.getSemanticReverse().getBioFunction() == results[l][j], 
									static_cast<string>(s1)+" + "
									+static_cast<string>(s2)+" = "
									+static_cast<string>(s3)+" != " 
									+static_cast<string>(Semantic(results[i][k], results[l][j])));
						}
						else 
						{
							if (!(results[i][k] == BF::X || results[l][j] == BF::X))
							{
								 BOOST_ERROR (static_cast<string>(s1)+" + "
									+static_cast<string>(s2)+" = "
									+static_cast<string>(s3)+" != " 
									+static_cast<string>(Semantic(results[i][k], results[l][j])));
							}
							
							if (results[i][k] == BF::X)
							{
								BOOST_REQUIRE_MESSAGE(
									s3.getSemanticForward().getBioFunction() == results[i][k]
									&& (s3.getSemanticReverse().getBioFunction() == BF::N 
										|| s3.getSemanticReverse().getBioFunction() == BF::X),
										static_cast<string>(s1)+" + "
										+static_cast<string>(s2)+" = "
										+static_cast<string>(s3)+" != (" 
										+static_cast<string>(OneWaySemantic(results[i][k]))+
										", fX or fN)");
							}
							if (results[l][j] == BF::X)
							{
								BOOST_REQUIRE_MESSAGE(
									s3.getSemanticReverse().getBioFunction() == results[l][j]
									&& (s3.getSemanticForward().getBioFunction() == BF::N 
										|| s3.getSemanticForward().getBioFunction() == BF::X), 
											static_cast<string>(s1)+" + "
												+static_cast<string>(s2)+" = "
												+static_cast<string>(s3)+" != (fX or fN, " 
												+static_cast<string>(OneWaySemantic(results[l][j]))+
												")");
							}
						}
						
						// combine method
						s3 = s1;
						s3.combine(s2);
						
						if (!s3.isX())
						{
							BOOST_REQUIRE_MESSAGE(
								s3.getSemanticForward().getBioFunction() == results[i][k]
								&& s3.getSemanticReverse().getBioFunction() == results[l][j], 
									static_cast<string>(s1)+".combine("
									+static_cast<string>(s2)+") = "
									+static_cast<string>(s3)+" != " 
									+static_cast<string>(Semantic(results[i][k], results[l][j])));
						}
						else 
						{
							if (!(results[i][k] == BF::X || results[l][j] == BF::X))
							{
								 BOOST_ERROR (static_cast<string>(s1)+".combine("
									+static_cast<string>(s2)+") = "
									+static_cast<string>(s3)+" != " 
									+static_cast<string>(Semantic(results[i][k], results[l][j])));
							}
							
							if (results[i][k] == BF::X)
							{
								BOOST_REQUIRE_MESSAGE(
									s3.getSemanticForward().getBioFunction() == results[i][k]
									&& (s3.getSemanticReverse().getBioFunction() == BF::N 
										|| s3.getSemanticReverse().getBioFunction() == BF::X),
										static_cast<string>(s1)+".combine("
										+static_cast<string>(s2)+") = "
										+static_cast<string>(s3)+" != (" 
										+static_cast<string>(OneWaySemantic(results[i][k]))+
										", fX or fN)");
							}
							if (results[l][j] == BF::X)
							{
								BOOST_REQUIRE_MESSAGE(
									s3.getSemanticReverse().getBioFunction() == results[l][j]
									&& (s3.getSemanticForward().getBioFunction() == BF::N 
										|| s3.getSemanticForward().getBioFunction() == BF::X), 
											static_cast<string>(s1)+".combine("
												+static_cast<string>(s2)+") = "
												+static_cast<string>(s3)+" != (fX or fN, " 
												+static_cast<string>(OneWaySemantic(results[l][j]))+
												")");
							}
						}
						
						// operator +=
						s3 = s1;
						s3 += s2;
						
						if (!s3.isX())
						{
							BOOST_REQUIRE_MESSAGE(
								s3.getSemanticForward().getBioFunction() == results[i][k]
								&& s3.getSemanticReverse().getBioFunction() == results[l][j], 
									static_cast<string>(s1)+" += "
									+static_cast<string>(s2)+" = "
									+static_cast<string>(s3)+" != " 
									+static_cast<string>(Semantic(results[i][k], results[l][j])));
						}
						else 
						{
							if (!(results[i][k] == BF::X || results[l][j] == BF::X))
							{
								 BOOST_ERROR (static_cast<string>(s1)+" += "
									+static_cast<string>(s2)+" = "
									+static_cast<string>(s3)+" != " 
									+static_cast<string>(Semantic(results[i][k], results[l][j])));
							}
							
							if (results[i][k] == BF::X)
							{
								BOOST_REQUIRE_MESSAGE(
									s3.getSemanticForward().getBioFunction() == results[i][k]
									&& (s3.getSemanticReverse().getBioFunction() == BF::N 
										|| s3.getSemanticReverse().getBioFunction() == BF::X),
										static_cast<string>(s1)+" += "
										+static_cast<string>(s2)+" = "
										+static_cast<string>(s3)+" != (" 
										+static_cast<string>(OneWaySemantic(results[i][k]))+
										", fX or fN)");
							}
							if (results[l][j] == BF::X)
							{
								BOOST_REQUIRE_MESSAGE(
									s3.getSemanticReverse().getBioFunction() == results[l][j]
									&& (s3.getSemanticForward().getBioFunction() == BF::N 
										|| s3.getSemanticForward().getBioFunction() == BF::X), 
											static_cast<string>(s1)+" += "
												+static_cast<string>(s2)+" = "
												+static_cast<string>(s3)+" != (fX or fN, " 
												+static_cast<string>(OneWaySemantic(results[l][j]))+
												")");
							}
						}
						
					}
				}
			}
		}
	}
}

BOOST_AUTO_TEST_CASE (checkElemantarySemantics)
{
	set<Semantic> semantics;
	
	for (auto &s : Semantic::elementarySemantics)
	{
		semantics.insert(s);
	}
	
	BOOST_REQUIRE_MESSAGE(semantics.size() == 26, "There aren't 26 different semantics in Semantic::elementarySemantics !");
}

BOOST_AUTO_TEST_CASE (checkAllSemantics)
{
	set<Semantic> semantics;
	
	for (auto &s : Semantic::allSemantics)
	{
		semantics.insert(s);
	}
	
	BOOST_REQUIRE_MESSAGE(semantics.size() == 28, "There aren't 28 different semantics in Semantic::allSemantics !");
}

BOOST_AUTO_TEST_CASE (checkEqual)
{
	for (size_t i(0); i < Semantic::elementarySemantics.size(); ++i)
	{
		for (size_t j(0); j < Semantic::elementarySemantics.size(); ++j)
		{
			Semantic s1(Semantic::elementarySemantics[i]), s2(Semantic::elementarySemantics[j]);
			if (i == j)
			{
				BOOST_REQUIRE_MESSAGE(s1 == s2, static_cast<string>(s1)+" must be equal to "+static_cast<string>(s2)+" (operator==)");
				BOOST_REQUIRE_MESSAGE(s1.isEqualTo(s2), static_cast<string>(s1)+" must be equal to "+static_cast<string>(s2)+" (isEqualTo method)");
				BOOST_REQUIRE_MESSAGE(!(s1 != s2), static_cast<string>(s1)+" must be equal to "+static_cast<string>(s2)+" (operator!=)");
			}
			else
			{
				BOOST_REQUIRE_MESSAGE(s1 != s2, static_cast<string>(s1)+" isn't equal to "+static_cast<string>(s2)+" (operator==)");
				BOOST_REQUIRE_MESSAGE(!s1.isEqualTo(s2), static_cast<string>(s1)+" isn't equal to "+static_cast<string>(s2)+" (isEqualTo method)");
				BOOST_REQUIRE_MESSAGE(!(s1 == s2), static_cast<string>(s1)+" isn't equal to "+static_cast<string>(s2)+" (operator==)");
			}
		}
	}
}

BOOST_AUTO_TEST_CASE (checkIsProperties)
{
	for (auto &s : Semantic::elementarySemantics)
	{
		OneWaySemantic forward(s.getSemanticForward()), reverse(s.getSemanticReverse());
		
		// properties of forward semantic
		if (forward.isP())
		{
			BOOST_REQUIRE_MESSAGE(s.isPF(), static_cast<string>(s)+" must have isPF() to true !");
		}
		else 
		{
			BOOST_REQUIRE_MESSAGE(!s.isPF(), static_cast<string>(s)+" must have isPF() to false !");
		}
		
		if (forward.isG())
		{
			BOOST_REQUIRE_MESSAGE(s.isGF(), static_cast<string>(s)+" must have isGF() to true !");
		}
		else 
		{
			BOOST_REQUIRE_MESSAGE(!s.isGF(), static_cast<string>(s)+" must have isPF() to false !");
		}
		
		if (forward.isT())
		{
			BOOST_REQUIRE_MESSAGE(s.isTF(), static_cast<string>(s)+" must have isTF() to true !");
		}
		else 
		{
			BOOST_REQUIRE_MESSAGE(!s.isTF(), static_cast<string>(s)+" must have isTF() to false !");
		}
		
		// properties of reverse semantic
		if (reverse.isP())
		{
			BOOST_REQUIRE_MESSAGE(s.isPR(), static_cast<string>(s)+" must have isPR() to true !");
		}
		else 
		{
			BOOST_REQUIRE_MESSAGE(!s.isPR(), static_cast<string>(s)+" must have isPR() to false !");
		}
		
		if (reverse.isG())
		{
			BOOST_REQUIRE_MESSAGE(s.isGR(), static_cast<string>(s)+" must have isGR() to true !");
		}
		else 
		{
			BOOST_REQUIRE_MESSAGE(!s.isGR(), static_cast<string>(s)+" must have isPR() to false !");
		}
		
		if (reverse.isT())
		{
			BOOST_REQUIRE_MESSAGE(s.isTR(), static_cast<string>(s)+" must have isTR() to true !");
		}
		else 
		{
			BOOST_REQUIRE_MESSAGE(!s.isTR(), static_cast<string>(s)+" must have isTR() to false !");
		}
		
		// Null BioFunction
		if (s.isN())
		{
			BOOST_REQUIRE_MESSAGE(forward.isN() && reverse.isN(), static_cast<string>(s)+" is not null BioFunction !");
		}
		else
		{
			BOOST_REQUIRE_MESSAGE(!(forward.isN() && reverse.isN()), static_cast<string>(s)+" is null BioFunction !");
		}
		
		// Expression BioFunction
		if (s.isX())
		{
			BOOST_REQUIRE_MESSAGE(forward.isX() || reverse.isX(), static_cast<string>(s)+" is not expressed BioFunction !");
		}
		else
		{
			BOOST_REQUIRE_MESSAGE(!(forward.isX() || reverse.isX()), static_cast<string>(s)+" is expressed BioFunction !");
		}
		
		// Mirror
		if (s.isMirror())
		{
			BOOST_REQUIRE_MESSAGE(forward == reverse, static_cast<string>(s)+" is not a mirror Semantic !");
		}
		else
		{
			BOOST_REQUIRE_MESSAGE(forward != reverse, static_cast<string>(s)+" is a mirror Semantic !");
		}
	}
}

BOOST_AUTO_TEST_CASE (checkReverse)
{
	for (auto s : Semantic::elementarySemantics)
	{
		OneWaySemantic forward(s.getSemanticForward()), reverse(s.getSemanticReverse());
		s.reverse();
		BOOST_REQUIRE_MESSAGE(forward == s.getSemanticReverse() && reverse == s.getSemanticForward(), static_cast<string>(s)+" is not properly reversed !");
	}
}

BOOST_AUTO_TEST_CASE (checkExcise)
{
	for (auto s : Semantic::elementarySemantics)
	{
		s.excise();
		BOOST_REQUIRE_MESSAGE(s.isN(), static_cast<string>(s)+" is not properly excised !");
	}
}

BOOST_AUTO_TEST_CASE (checkBioFunctionToPointer)
{
	for (size_t i(0); i < Semantic::bioFunctionToPointer.size(); ++i)
	{
		for (size_t j(0); j < Semantic::bioFunctionToPointer[i].size(); ++j)
		{
			try
			{
				Semantic sem(static_cast<Semantic::BF>(i),static_cast<Semantic::BF>(j));
				BOOST_REQUIRE_MESSAGE(Semantic::bioFunctionToPointer[i][j] != nullptr && *Semantic::bioFunctionToPointer[i][j] == sem, 
									  "*Semantic::bioFunctionToPointer["+to_string(i)+"]["+to_string(j)+"] must be equal to "+static_cast<string>(sem));
			}
			catch (Semantic::InvalidSemanticArgument &e)
			{
				BOOST_REQUIRE_MESSAGE(Semantic::bioFunctionToPointer[i][j] == nullptr, 
									  "*Semantic::bioFunctionToPointer["+to_string(i)+"]["+to_string(j)+"] must be equal to nullptr");
			}
		}
	}
}

BOOST_AUTO_TEST_CASE (isUsefulAfterPrefix)
{
	array<array<bool,6>,6> utilityAfterPrefixOWS {{
		{1,1,0,0,0,1},
		{1,0,1,1,0,0},
		{1,1,0,0,0,1},
		{1,1,0,0,0,1},
		{1,0,1,1,0,0},
		{1,0,0,0,0,0}}};
	array<array<bool,6>,6> utilityBeforeSuffixOWS {{
		{1,1,1,1,1,1},
		{0,0,0,1,1,0},
		{0,0,0,1,1,0},
		{1,1,1,0,0,0},
		{0,0,0,0,0,0},
		{1,1,1,0,0,0}}};
		
	for (size_t i(0); i < utilityAfterPrefixOWS.size(); ++i)
	{
		for (size_t j(0); j < utilityBeforeSuffixOWS.size(); ++j)
		{
			try 
			{
				OneWaySemantic OWSAfterForward(BFList[i]), OWSAfterReverse(BFList[j]);
				Semantic semAfter(OWSAfterForward, OWSAfterReverse);
				
				for (size_t k(0); k < utilityAfterPrefixOWS.size(); ++k)
				{
					for (size_t l(0); l < utilityBeforeSuffixOWS.size(); ++l)
					{
						try 
						{
							OneWaySemantic OWSPrefixForward(BFList[k]), OWSPrefixReverse(BFList[l]);
							Semantic prefix(OWSPrefixForward, OWSPrefixReverse);
							
							int utility = OWSAfterForward.isUsefulAfterPrefix(OWSPrefixForward)
								| 2*OWSAfterReverse.isUsefulBeforeSuffix(OWSPrefixReverse);
								
							BOOST_REQUIRE_MESSAGE((int)semAfter.isUsefulAfterPrefix(prefix) == utility,
								(string)semAfter+" is useful after "+(string)prefix+" : "+to_string(utility)+
								" - answer of isUsefulAfterPrefix() : "+to_string((int)semAfter.isUsefulAfterPrefix(prefix)));
						}
						catch (invalid_argument& ia) {}
					}
				}
			}
			catch (invalid_argument& ia) {}
		}
	}
}
