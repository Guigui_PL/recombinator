#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MAIN
#include <exception>
#include <set>
#include <vector>
#include <unordered_map>
#include <boost/test/unit_test.hpp>
#include <GenericFunctionnalStructure.hpp> 

using namespace std;
using namespace recombinator::api;

BOOST_AUTO_TEST_CASE(checkNbOfDistinctActivations)
{
	// We create a vector of (valid) structures 
	// number of activations -> structure
	unordered_multimap<int,string> structures ({
		{1,""},
		{2,"<AB <AP"}, {2,"<AB >AP"},
		{4,"<AB <BB >AP <BP"},
		{5,"<AB <BB <BP >AP"},
		{16,"<AB <BB <CB <CP >BP >AP"},
		{6,"<AB <BB <AP <BP >AP"}
	});
	
	for (auto &e : structures)
	{
		auto structure = GenericFunctionnalStructure::create<GenericFunctionnalStructure>(e.second);
		BOOST_REQUIRE_EQUAL(e.first,structure->getActivations().size());
		set<vector<uint8_t>> distinctActivations (structure->getActivations().begin(), structure->getActivations().end());
		BOOST_REQUIRE_EQUAL(e.first,distinctActivations.size());
	}
}

using AMI = GenericFunctionnalStructure::AMI;

BOOST_AUTO_TEST_CASE(checkDerivedBioDevs)
{
	auto firstStruct = GenericFunctionnalStructure::create<GenericFunctionnalStructure>("<AB <BB <AP <BP >AP");
	map<GenericFunctionnalStructure::Activation, GenericFunctionnalStructure::SemanticalBioDeviceStructure> firstStructAct ({
		{{},{0,1,2,3,4,5}},
		{{*firstStruct->getActivationNumber(AMI("<AB0"), AMI("<AP0"))},
			{0,3,4,5}},
		{{*firstStruct->getActivationNumber(AMI("<AB0"), AMI("<AP1"))},
			{0,-4,-3,-2,-1,5}},
		{{*firstStruct->getActivationNumber(AMI("<BB0"), AMI("<BP0"))},
			{0,1,4,5}},
		{{*firstStruct->getActivationNumber(AMI("<AB0"), AMI("<AP1")), *firstStruct->getActivationNumber(AMI("<BB0"), AMI("<BP0"))},
			{0,-4,-1,5}},
		{{*firstStruct->getActivationNumber(AMI("<BB0"), AMI("<BP0")), *firstStruct->getActivationNumber(AMI("<AB0"), AMI("<AP1"))},
			{0,-4,-1,5}}
	});
	
	vector<GenericFunctionnalStructure::Activation> invalid = {
		{*firstStruct->getActivationNumber(AMI("<AB0"), AMI("<AP0")),*firstStruct->getActivationNumber(AMI("<AB0"), AMI("<AP1"))},
		{*firstStruct->getActivationNumber(AMI("<AB0"), AMI("<AP0")),*firstStruct->getActivationNumber(AMI("<BB0"), AMI("<BP0"))}
		
	};
	for (auto &i : invalid)
	{
		BOOST_CHECK_THROW(firstStruct->getDerivedBioDevicesStructure(i), out_of_range);
	}
	
	for (auto &a: firstStructAct)
	{
		BOOST_CHECK(firstStruct->getDerivedBioDevicesStructure(a.first) == a.second);
	}
	
	auto secondStruct = GenericFunctionnalStructure::create<GenericFunctionnalStructure>("<AB <BB <BP >AP");
	map<GenericFunctionnalStructure::Activation, GenericFunctionnalStructure::SemanticalBioDeviceStructure> secondStructAct ({
		{{},{0,1,2,3,4}},
		{{*firstStruct->getActivationNumber(AMI("<AB0"), AMI("<AP0"))},
			{0,-3,-2,-1,4}},
		{{*firstStruct->getActivationNumber(AMI("<BB0"), AMI("<BP0"))},
			{0,1,3,4}},
		{{*firstStruct->getActivationNumber(AMI("<AB0"), AMI("<AP0")),*firstStruct->getActivationNumber(AMI("<BB0"), AMI("<BP0"))},
			{0,-3,-1,4}},
		{{*firstStruct->getActivationNumber(AMI("<BB0"), AMI("<BP0")),*firstStruct->getActivationNumber(AMI("<AB0"), AMI("<AP0"))},
			{0,-3,-1,4}}
	});
	
	for (auto &a: secondStructAct)
	{
		BOOST_CHECK(secondStruct->getDerivedBioDevicesStructure(a.first) == a.second);
	}
}