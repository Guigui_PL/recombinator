#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MAIN
#include <array>
#include <cmath>
#include <fstream>
#include <set>
#include <vector>
#include <boost/test/unit_test.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>
#include "../api/DyckSemanticalBioDevice.hpp"
#include "../api/IEDyckFunctionnalStructure.hpp"

using namespace recombinator::api;
using namespace std;

auto &sems = Semantic::allSemantics;

BOOST_AUTO_TEST_CASE(checkConstructorStructureAndSemantics)
{
	const vector<pair<shared_ptr<DyckFunctionnalStructure>,vector<const Semantic*>>> basesToBuild ({
		{shared_ptr<DyckFunctionnalStructure>(IEDyckFunctionnalStructure::create<IEDyckFunctionnalStructure>("[0]0").release()),{&sems[5], &sems[10], &sems[15]}},
		{shared_ptr<DyckFunctionnalStructure>(IEDyckFunctionnalStructure::create<IEDyckFunctionnalStructure>("(0)0").release()),{&sems[5], &sems[10], &sems[15]}},
		{shared_ptr<DyckFunctionnalStructure>(IEDyckFunctionnalStructure::create<IEDyckFunctionnalStructure>("[0(1)1]0").release()),{&sems[5], &sems[10], &sems[1], &sems[10], &sems[15]}},
		{shared_ptr<DyckFunctionnalStructure>(IEDyckFunctionnalStructure::create<IEDyckFunctionnalStructure>("[0[1]1]0").release()),{&sems[5], &sems[10], &sems[1], &sems[10], &sems[15]}}
	});
	
	for (const auto &b: basesToBuild)
	{
		DyckSemanticalBioDevice sbd(*(b.first),b.second);
		
		BOOST_REQUIRE_MESSAGE(
			sbd.getNbSemantics() == b.second.size(), 
			"nbdSemantics not valid in "+static_cast<string>(sbd)+"\n"
			+to_string(sbd.getNbDefinedSemantics())+" != "+to_string(b.second.size())
		);
		BOOST_REQUIRE_MESSAGE(
			sbd.getStructure().size() == b.first->size(), 
			"structure size not valid in "+static_cast<string>(sbd)+"\n"
			+to_string(sbd.getStructure().size())+" != "+to_string(b.first->size())
		);
		BOOST_REQUIRE_MESSAGE(
			sbd.getActivations().size() == static_cast<size_t>(pow(2,b.first->size()/2)), 
			"structure size not valid in "+static_cast<string>(sbd)+"\n"
			+to_string(sbd.getActivations().size())+" != "+to_string(static_cast<size_t>(pow(2,b.first->size()/2)))
		);
		BOOST_REQUIRE_MESSAGE(
			sbd.isAllSemanticsDefined(), 
			"isAllSemanticsDefined not valid in "+static_cast<string>(sbd)+"\n"
			+"It should return true"
		);
	}
	
	const vector<pair<shared_ptr<DyckFunctionnalStructure>,vector<const Semantic*>>> invalids ({
		{shared_ptr<DyckFunctionnalStructure>(IEDyckFunctionnalStructure::create<IEDyckFunctionnalStructure>("[0(1)1]0").release()),{&sems[5], &sems[1], &sems[10], &sems[15]}},
		{shared_ptr<DyckFunctionnalStructure>(IEDyckFunctionnalStructure::create<IEDyckFunctionnalStructure>("[0]0").release()),{&sems[5], &sems[10], &sems[1], &sems[15]}},
	});
	
	for (const auto &b: invalids)
	{
		BOOST_CHECK_THROW(DyckSemanticalBioDevice(*(b.first),b.second), invalid_argument);
	}
} 

BOOST_AUTO_TEST_CASE(checkConstructorString)
{
	const vector<string> basesToBuild ({
		"PF SFa TF SFA GF",
		"PR SRA TF SRA GF",
		"PF SFA TF SFA SFB GF SFB GF",
		"PF SFA TF SFA SFB TF SFB GF"
	});
	
	for (const auto &b: basesToBuild)
	{
		DyckSemanticalBioDevice sbd(b);
		
		BOOST_REQUIRE_MESSAGE(
			sbd.getNbSemantics() == sbd.getNbDefinedSemantics(), 
			"nbSemantics != nbDefinedSemantics in "+static_cast<string>(sbd)+"\n"
			+to_string(sbd.getNbDefinedSemantics())+" != "+to_string(sbd.getNbDefinedSemantics())
		);
		BOOST_REQUIRE_MESSAGE(
			sbd.isAllSemanticsDefined(), 
			"isAllSemanticsDefined not valid in "+static_cast<string>(sbd)+"\n"
			+"It should return true"
		);
	}
	
	const vector<string> invalids ({
		"PF SFa TF SFA sfa GF",
		"PR SRA TF SRA GF sdsq",
		"PF SFA TF SFA SFB GF SFa GF",
		"PF SFA TF SFA SFB TF SF GF",
		"PF SFB TF SFA TF SFA GF",
		"PF SFB TF SFA SFB TF SFA GF"
	});
	
	for (const string &i: invalids)
	{
		BOOST_CHECK_THROW(DyckSemanticalBioDevice sbd(i), invalid_argument);
	}
}

BOOST_AUTO_TEST_CASE(checkProperties)
{
	namespace fs = boost::filesystem;
	vector <string> folders;
	folders.push_back("tests/files/classicalBioDevices");
	
	while (!folders.empty())
	{
		string inputPath = folders.back();
		folders.pop_back();
		
		for(fs::directory_iterator it(inputPath), end; it != end; ++it) 
		{
			const fs::path path(it->path());
			if(fs::is_regular_file(it->status()))
			{
				string line;
				ifstream file (path.string());
				
				if (file.is_open())
				{
					while (getline(file, line))
					{
						boost::trim(line);
						DyckSemanticalBioDevice sbd(line);
						BOOST_REQUIRE_MESSAGE(!sbd.isReducible(),line + " : "+static_cast<string>(sbd)+" is not reducible !");
						BOOST_REQUIRE_MESSAGE(sbd.isIrredundant(),line + " : "+static_cast<string>(sbd)+" is not simplifiable !");
						BOOST_REQUIRE_MESSAGE(
							sbd.isRespectedStrongConstraint()<=sbd.isRespectedWeakConstraint(),
							line + " : "+static_cast<string>(sbd)+" respects weak constraint !"+to_string(sbd.isRespectedStrongConstraint())+"<="+to_string(sbd.isRespectedWeakConstraint()));
						BOOST_REQUIRE_MESSAGE(
							sbd.isAllSemanticsDefined(),
							line + " : "+static_cast<string>(sbd)+" has all of its semantics defined !");
					}
				}
			}
			else if (is_directory(path))
			{
				folders.push_back(path.string());
			}
		}
	}
} 

BOOST_AUTO_TEST_CASE(checkIsIrredundant)
{
	const vector<string> redundant ({
		"(0  )0",
		" (0  )0 PF GF",
		" (0 PR GR )0 GF PR",
		" (0 PR PF )0 GF",
		" (0 GF GR )0 PR",
		" (0 GF PF )0 GF PR",
		" (0 PF GF )0 ",
		"GR (0  )0 PR",
		"GR (0 PR )0 GF",
		"GR (0 PF )0 GF",
		"GR (0 PR PF )0 ",
		"PF (0  )0 GF",
		"PF (0 GF )0 PR",
		"PF (0 GR )0 PR",
		"PF (0 GF GR )0 ",
		"GR PF (0 PR GR )0 ",
		"GR PF (0 GF PF )0 ",
		"PF GF (0  )0 "
	});
	
	const vector<string> irredundant ({
		"GR (0 PR )0 ",
		" (0 PR )0 GF",
		" (0 GF )0 PR",
		" (0 PF )0 GF",
		" (0 GR )0 PR",
		"GR (0 TR )0 PR",
		"GR (0 PF )0 ",
		"GR (0 TF )0 PR",
		"PF (0 TR )0 GF",
		"PF (0 GF )0 ",
		"PF (0 TF )0 GF",
		"PF (0 GR )0 ",
	});
	
	for (auto &it : redundant)
	{
		DyckSemanticalBioDevice sbd(it);
		BOOST_REQUIRE_MESSAGE(!sbd.isIrredundant(),static_cast<string>(sbd)+" is redundant !");;
	}
	
	for (auto &it : irredundant)
	{
		DyckSemanticalBioDevice sbd(it);
		BOOST_REQUIRE_MESSAGE(sbd.isIrredundant(),static_cast<string>(sbd)+" is not redundant !");;
	}
}
