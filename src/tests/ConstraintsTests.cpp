#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MAIN
#include <sstream>
#include <boost/test/unit_test.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>
#include "../api/BioDeviceFile.hpp"
#include "../generator/IEDyckSemanticalBioDeviceBuilder.hpp"
#include "../generator/IEDyckSemanticalBioDeviceGenerator.hpp"

using namespace recombinator::api;
using namespace recombinator::generator;
using namespace std;
namespace fs = boost::filesystem;

//  GR (0 PF (1 TF )1 TR )0  (2  (3 GF PF )3 TF )2 GF
// GR [0 PF ]0  [1 TF ]1  [2 PR ]2 GF
/*ests/ConstraintsTests.cpp(140): fatal error: in "checkNoIPalindromeConstraintConsistancy": GR (0 PF (1 TF )1 TR )0  (2  (3 GF PF )3 TF )2 GF - GR (0 PF (1 TF )1 TR )0  (2  (3 GF PF )3 null )2 GF does not pass the test noIPalindrome consistancy - variable 7 - domain size : 21
Domain : (fN, fP)       (fN, fT)        (fN, fG)        (fN, fGP)       (fP, fP)        (fP, fT)        (fP, fG)        (fP, fGP)       (fT, fP)        (fT, fT)        (fT, fG)        (fT, fGP)       (fG, fP)        (fG, fT)        (fG, fG)        (fG, fGP)       (fGP, fP)     (fGP, fT)       (fGP, fG)       (fGP, fGP)      (fX, fX)
assignation order : 0 1 2 3 4 5 6 8 


tests/ConstraintsTests.cpp(142): fatal error: in "checkNoIPalindromeConstraintConsistancy": GR [0 PF ]0  [1 TF ]1  [2 PR ]2 GF - GR [0 PF ]0  [1 TF ]1 null [2 PR ]2 GF does not pass the test noIPalindrome consistancy - exluded=1 - variable 4 - domain size : 25
Domain : (fN, fP)       (fN, fT)        (fN, fG)        (fN, fGP)       (fP, fN)        (fP, fP)        (fP, fT)        (fP, fG)        (fP, fGP)       (fT, fN)        (fT, fP)        (fT, fT)        (fT, fG)        (fT, fGP)       (fG, fN)        (fG, fP)        (fG, fT)      (fG, fG)        (fG, fGP)       (fGP, fN)       (fGP, fP)       (fGP, fT)       (fGP, fG)       (fGP, fGP)      (fX, fX)
assignation order : 0 1 2 3 5 6 
GR [0 PF ]0  [1 TF ]1  [2 PR ]2 GF - GR [0 PF ]0  [1 TF ]1  [2 PR ]2 GF precedent  - exluded=0 - variable 6 - domain size : 22
Domain : (fN, fP)       (fN, fT)        (fN, fGP)       (fP, fP)        (fP, fT)        (fP, fG)        (fP, fGP)       (fT, fP)        (fT, fT)        (fT, fG)        (fT, fGP)       (fG, fN)        (fG, fP)        (fG, fT)        (fG, fG)        (fG, fGP)       (fGP, fN)     (fGP, fP)       (fGP, fT)       (fGP, fG)       (fGP, fGP)      (fX, fX)
assignation order : 0 1 2 3 5 4 6 

*/
/*
vector<string> hardCases ({"GR (0 PF (1 TF )1 TR )0  (2  (3 GF PF )3 TF )2 GF", "GR [0 PF ]0  [1 TF ]1  [2 PR ]2 GF"});

BOOST_AUTO_TEST_CASE(checkNoIPalindromeConstraintConsistancyHardCases)
{
	auto domToString = [](IEDyckSemanticalBioDeviceBuilder &builder, size_t i){ string ret; for (auto &it : builder.getDomain(i)) { ret += (string) (*it)+"\t"; } return ret; };
	auto lastAssignationsToString = [](IEDyckSemanticalBioDeviceBuilder &builder){ string ret; for (auto &it : builder.getLastAssignations()) { ret += to_string(it)+" "; } return ret; };
	
	for (auto &it : hardCases)
	{
		DyckSemanticalBioDevice sbd(it);
		if (IEDyckSemanticalBioDeviceGenerator::reverseDyckWord(sbd.getStructure().getStructure()) == sbd.getStructure().getStructure())
		{
			fs::path p("work/"+to_string(sbd.getNbIntegrase()));
			if (!fs::exists(p))
				fs::create_directory(p);
			
			fs::path good(p), bad(p);
			good += fs::path("/good");
			bad += fs::path("/bad");
			
			if (!fs::exists(good))
				fs::create_directory(good);
			if (!fs::exists(bad))
				fs::create_directory(bad);
			
			vector<size_t> assignationOrder(sbd.getNbSemantics());
			for (size_t i = 0; i < assignationOrder.size(); ++i)
				assignationOrder[i] = i;
			
			for (size_t j{0}; j < sbd.getNbSemantics(); ++j)
			{
				if (sbd.getSemantic(j).isX())
					sbd.setSemantic(j, &Semantic::elementarySemantics[25]);
			}
			
			bool firstTest = true, comparedValue;
			string precString;
			do 
			{
				DyckSemanticalBioDevice copy = sbd;
				IEDyckSemanticalBioDeviceBuilder builder(&copy, {IEDyckSemanticalBioDeviceBuilder::Setting::NO_IPALINDROME_CONSTRAINT});
				for (size_t j{0}; j < sbd.getNbSemantics(); ++j)
				{
					copy.setSemantic(j, nullptr);
				}
				
				bool excluded =false;
				ostringstream ss;
				size_t toAssign = 0;
				
				for (size_t i{0}; i <= sbd.getNbSemantics() && !excluded; ++i)
				{
					toAssign = assignationOrder[i-1];
					if (i > 0)
					{
						bool testOkSem = false;
						for (auto &it : builder.getDomain(toAssign))
						{
							if (*it == sbd.getSemantic(toAssign))
							{
								testOkSem = true;
								builder.assignVar(toAssign,it);
								break;
							}
						}
						
						if (!testOkSem)
						{
							excluded = true;
							break;
						}
						builder.checkConstraints();
					}
					else
					{
						builder.removeUnaryConstraints();
					}
					
					
					for (size_t j{0}; j < sbd.getNbSemantics(); ++j)
					{
						bool testOkSem = false;
						for (auto &it : builder.getDomain(j))
						{
							if (*it == sbd.getSemantic(j))
							{
								testOkSem = true;
								break;
							}
						}
							
						if (!testOkSem)
						{
							toAssign = j;
							excluded = true;
							break;
						}
					}
				}
				
				if (firstTest)
				{
					firstTest = false;
					comparedValue = excluded;
				}
				else
				{
					builder.printStatistics(ss);
					BOOST_REQUIRE_MESSAGE(excluded == comparedValue,  static_cast<string>(sbd)+" - "+((string) copy)+
					" does not pass the test noIPalindrome consistancy - exluded="+to_string(excluded)+" - variable "+to_string(toAssign)+" - domain size : "+to_string(builder.getDomain(toAssign).size())+
						"\nDomain : "+domToString(builder, toAssign)+"\n"+
						"assignation order : "+lastAssignationsToString(builder)+"\n"+
						precString
					);
				}
				builder.printStatistics(ss);
				precString =  static_cast<string>(sbd)+" - "+((string) copy)+
					" precedent  - exluded="+to_string(excluded)+" - variable "+to_string(toAssign)+" - domain size : "+to_string(builder.getDomain(toAssign).size())+
						"\nDomain : "+domToString(builder, toAssign)+"\n"+
						"assignation order : "+lastAssignationsToString(builder);
				
			} while (next_permutation(assignationOrder.begin(), assignationOrder.end()));
		}
	}
}

BOOST_AUTO_TEST_CASE(checkNoIPalindromeConstraintHardCases)
{
	auto domToString = [](IEDyckSemanticalBioDeviceBuilder &builder, size_t i){ string ret; for (auto &it : builder.getDomain(i)) { ret += (string) (*it)+"\t"; } return ret; };
	auto lastAssignationsToString = [](IEDyckSemanticalBioDeviceBuilder &builder){ string ret; for (auto &it : builder.getLastAssignations()) { ret += to_string(it)+" "; } return ret; };
	
	for (auto &it : hardCases)
	{
		DyckSemanticalBioDevice sbd(it);
		if (IEDyckSemanticalBioDeviceGenerator::reverseDyckWord(sbd.getStructure().getStructure()) == sbd.getStructure().getStructure())
		{
			fs::path p("work/"+to_string(sbd.getNbIntegrase()));
			if (!fs::exists(p))
				fs::create_directory(p);
			
			fs::path good(p), bad(p);
			good += fs::path("/good");
			bad += fs::path("/bad");
			
			if (!fs::exists(good))
				fs::create_directory(good);
			if (!fs::exists(bad))
				fs::create_directory(bad);
			
			for (size_t j{0}; j < sbd.getNbSemantics(); ++j)
			{
				if (sbd.getSemantic(j).isX())
					sbd.setSemantic(j, &Semantic::elementarySemantics[25]);
			}
			
			DyckSemanticalBioDevice copy = sbd;
			IEDyckSemanticalBioDeviceBuilder builder(&copy, {IEDyckSemanticalBioDeviceBuilder::Setting::NO_IPALINDROME_CONSTRAINT});
			for (size_t j{0}; j < sbd.getNbSemantics(); ++j)
			{
				copy.setSemantic(j, nullptr);
			}
			
			bool excluded =false;
			ostringstream ss;
			auto toAssign = builder.chooseNextAssignation();
			
			for (size_t i{0}; i <= sbd.getNbSemantics() && !excluded; ++i)
			{
				toAssign = builder.chooseNextAssignation();
				if (i > 0)
				{
					bool testOkSem = false;
					for (auto &it : builder.getDomain(toAssign))
					{
						if (*it == sbd.getSemantic(toAssign))
						{
							testOkSem = true;
							builder.assignVar(toAssign,it);
							break;
						}
					}
					
					if (!testOkSem)
					{
						excluded = true;
						break;
					}
					builder.checkConstraints();
				}
				else
				{
					builder.removeUnaryConstraints();
				}
				
				
				for (size_t j{0}; j < sbd.getNbSemantics(); ++j)
				{
					bool testOkSem = false;
					for (auto &it : builder.getDomain(j))
					{
						if (*it == sbd.getSemantic(j))
						{
							testOkSem = true;
							break;
						}
					}
						
					if (!testOkSem)
					{
						toAssign = j;
						excluded = true;
						break;
					}
				}
			}
			
			auto reverseSbd = sbd.getDyckSymmetric();
			
			if (reverseSbd == sbd)
			{
				builder.printStatistics(ss);
				BOOST_REQUIRE_MESSAGE(!excluded,  static_cast<string>(sbd)+" - "+((string) copy)+
				" does not pass the test noIPalindrome - variable "+to_string(toAssign)+" - domain size : "+to_string(builder.getDomain(toAssign).size())+
					"\nDomain : "+domToString(builder, toAssign)+"\n"+
					"assignation order : "+lastAssignationsToString(builder)
				);
			}
			else 
			{
				DyckSemanticalBioDevice copy2 = reverseSbd;
				IEDyckSemanticalBioDeviceBuilder builder2(&copy2, {IEDyckSemanticalBioDeviceBuilder::Setting::NO_IPALINDROME_CONSTRAINT});
				for (size_t j{0}; j < reverseSbd.getNbSemantics(); ++j)
				{
					copy2.setSemantic(j, nullptr);
				}
				
				bool excluded2 = false;
				ostringstream ss2;
				auto toAssign2 = builder2.chooseNextAssignation();
				
				for (size_t i{0}; i <= reverseSbd.getNbSemantics() && !excluded2; ++i)
				{
					toAssign2 = builder2.chooseNextAssignation();
					if (i > 0)
					{
						bool testOkSem = false;
						for (auto &it : builder2.getDomain(toAssign2))
						{
							if (*it == reverseSbd.getSemantic(toAssign2))
							{
								testOkSem = true;
								builder2.assignVar(toAssign2,it);
								break;
							}
						}
						
						if (!testOkSem)
						{
							excluded2 = true;
							break;
						}
						builder2.checkConstraints();
					}
					else
					{
						builder2.removeUnaryConstraints();
					}
					
					
					for (size_t j{0}; j < reverseSbd.getNbSemantics(); ++j)
					{
						bool testOkSem = false;
						for (auto &it : builder2.getDomain(j))
						{
							if (*it == reverseSbd.getSemantic(j))
							{
								testOkSem = true;
								break;
							}
						}
							
						if (!testOkSem)
						{
							toAssign2 = j;
							excluded2 = true;
							break;
						}
					}
				}
				builder.printStatistics(ss);
				builder2.printStatistics(ss2);
				BOOST_REQUIRE_MESSAGE((excluded && !excluded2) || (!excluded && excluded2),static_cast<string>(sbd)+" - "+((string) copy)+
				" - "+((string) copy2)+
				" does not pass the test noIPalindrome\n"+
				"First sbd - exluded="+to_string(excluded)+" : variable "+to_string(toAssign)+" - domain size : "+to_string(builder.getDomain(toAssign).size())+
					"\nDomain : "+domToString(builder, toAssign)+"\n"+
					"assignation order : "+lastAssignationsToString(builder)+"\n"
				+(ss.str())+"\n"+
				"Second sbd - exluded="+to_string(excluded2)+" : variable "+to_string(toAssign2)+" - domain size : "+to_string(builder2.getDomain(toAssign2).size())+
					"\nDomain : "+domToString(builder2, toAssign2)+"\n"+
					"assignation order : "+lastAssignationsToString(builder2)+"\n"
					+(ss2.str())+"\n"
				);
			}
		}
	}
}
*/
/*
BOOST_AUTO_TEST_CASE(checkNoIPalindromeConstraintConsistancy)
{
	vector <string> folders;
	folders.push_back("tests/files/4toTest");
	
	auto domToString = [](IEDyckSemanticalBioDeviceBuilder &builder, size_t i){ string ret; for (auto &it : builder.getDomain(i)) { ret += (string) (*it)+"\t"; } return ret; };
	auto lastAssignationsToString = [](IEDyckSemanticalBioDeviceBuilder &builder){ string ret; for (auto &it : builder.getLastAssignations()) { ret += to_string(it)+" "; } return ret; };
	
	while (!folders.empty())
	{
		string inputPath = folders.back();
		folders.pop_back();
		
		for(fs::directory_iterator it(inputPath), end; it != end; ++it) 
		{
			const fs::path path(it->path());
			if(fs::is_regular_file(it->status()))
			{
				DyckSemanticalBioDeviceFileReader reader(path.string());
				DyckSemanticalBioDevice sbd;
				
				while (reader >> sbd)
				{
					if (IEDyckSemanticalBioDeviceGenerator::reverseDyckWord(sbd.getStructure().getStructure()) == sbd.getStructure().getStructure())
					{
						fs::path p("work/"+to_string(sbd.getNbIntegrase()));
						if (!fs::exists(p))
							fs::create_directory(p);
						
						fs::path good(p), bad(p);
						good += fs::path("/good");
						bad += fs::path("/bad");
						
						if (!fs::exists(good))
							fs::create_directory(good);
						if (!fs::exists(bad))
							fs::create_directory(bad);
						
						vector<size_t> assignationOrder(sbd.getNbSemantics());
						for (size_t i = 0; i < assignationOrder.size(); ++i)
							assignationOrder[i] = i;
						
						for (size_t j{0}; j < sbd.getNbSemantics(); ++j)
						{
							if (sbd.getSemantic(j).isX())
								sbd.setSemantic(j, &Semantic::elementarySemantics[25]);
						}
						
						bool firstTest = true, comparedValue;
						string precString;
						do 
						{
							DyckSemanticalBioDevice copy = sbd;
							IEDyckSemanticalBioDeviceBuilder builder(&copy, {IEDyckSemanticalBioDeviceBuilder::Setting::NO_IPALINDROME_CONSTRAINT});
							for (size_t j{0}; j < sbd.getNbSemantics(); ++j)
							{
								copy.setSemantic(j, nullptr);
							}
							
							bool excluded =false;
							ostringstream ss;
							size_t toAssign = 0;
							
							for (size_t i{0}; i <= sbd.getNbSemantics() && !excluded; ++i)
							{
								toAssign = assignationOrder[i-1];
								if (i > 0)
								{
									bool testOkSem = false;
									for (auto &it : builder.getDomain(toAssign))
									{
										if (*it == sbd.getSemantic(toAssign))
										{
											testOkSem = true;
											builder.assignVar(toAssign,it);
											break;
										}
									}
									
									if (!testOkSem)
									{
										excluded = true;
										break;
									}
									builder.checkConstraints();
								}
								else
								{
									builder.removeUnaryConstraints();
								}
								
								
								for (size_t j{0}; j < sbd.getNbSemantics(); ++j)
								{
									bool testOkSem = false;
									for (auto &it : builder.getDomain(j))
									{
										if (*it == sbd.getSemantic(j))
										{
											testOkSem = true;
											break;
										}
									}
										
									if (!testOkSem)
									{
										toAssign = j;
										excluded = true;
										break;
									}
								}
							}
							
							if (firstTest)
							{
								firstTest = false;
								comparedValue = excluded;
							}
							else
							{
								builder.printStatistics(ss);
								BOOST_REQUIRE_MESSAGE(excluded == comparedValue,  static_cast<string>(sbd)+" - "+((string) copy)+
								" does not pass the test noIPalindrome consistancy - exluded="+to_string(excluded)+" - variable "+to_string(toAssign)+" - domain size : "+to_string(builder.getDomain(toAssign).size())+
									"\nDomain : "+domToString(builder, toAssign)+"\n"+
									"assignation order : "+lastAssignationsToString(builder)+"\n"+
									precString
								);
							}
							builder.printStatistics(ss);
							precString =  static_cast<string>(sbd)+" - "+((string) copy)+
								" precedent  - exluded="+to_string(excluded)+" - variable "+to_string(toAssign)+" - domain size : "+to_string(builder.getDomain(toAssign).size())+
									"\nDomain : "+domToString(builder, toAssign)+"\n"+
									"assignation order : "+lastAssignationsToString(builder);
							
						} while (next_permutation(assignationOrder.begin(), assignationOrder.end()));
					}
				}
			}
			else if (is_directory(path))
			{
				folders.push_back(path.string());
			}
		}
	}
}*/

/*
BOOST_AUTO_TEST_CASE(checkAllConstraintsConsistancy)
{
	vector <string> folders;
	folders.push_back("tests/files/4toTest");
	
	auto domToString = [](IEDyckSemanticalBioDeviceBuilder &builder, size_t i){ string ret; for (auto &it : builder.getDomain(i)) { ret += (string) (*it)+"\t"; } return ret; };
	auto lastAssignationsToString = [](IEDyckSemanticalBioDeviceBuilder &builder){ string ret; for (auto &it : builder.getLastAssignations()) { ret += to_string(it)+" "; } return ret; };
	
	while (!folders.empty())
	{
		string inputPath = folders.back();
		folders.pop_back();
		
		for(fs::directory_iterator it(inputPath), end; it != end; ++it) 
		{
			const fs::path path(it->path());
			if(fs::is_regular_file(it->status()))
			{
				DyckSemanticalBioDeviceFileReader reader(path.string());
				DyckSemanticalBioDevice sbd;
				
				while (reader >> sbd)
				{
						fs::path p("work/"+to_string(sbd.getNbIntegrase()));
						if (!fs::exists(p))
							fs::create_directory(p);
						
						fs::path good(p), bad(p);
						good += fs::path("/good");
						bad += fs::path("/bad");
						
						if (!fs::exists(good))
							fs::create_directory(good);
						if (!fs::exists(bad))
							fs::create_directory(bad);
						
						vector<size_t> assignationOrder(sbd.getNbSemantics());
						for (size_t i = 0; i < assignationOrder.size(); ++i)
							assignationOrder[i] = i;
						
						for (size_t j{0}; j < sbd.getNbSemantics(); ++j)
						{
							if (sbd.getSemantic(j).isX())
								sbd.setSemantic(j, &Semantic::elementarySemantics[25]);
						}
						
						bool firstTest = true, comparedValue;
						string precString;
						do 
						{
							DyckSemanticalBioDevice copy = sbd;
							IEDyckSemanticalBioDeviceBuilder builder(&copy);
							for (size_t j{0}; j < sbd.getNbSemantics(); ++j)
							{
								copy.setSemantic(j, nullptr);
							}
							
							bool excluded =false;
							ostringstream ss;
							size_t toAssign = 0;
							
							for (size_t i{0}; i <= sbd.getNbSemantics() && !excluded; ++i)
							{
								toAssign = assignationOrder[i-1];
								if (i > 0)
								{
									bool testOkSem = false;
									for (auto &it : builder.getDomain(toAssign))
									{
										if (*it == sbd.getSemantic(toAssign))
										{
											testOkSem = true;
											builder.assignVar(toAssign,it);
											break;
										}
									}
									
									if (!testOkSem)
									{
										excluded = true;
										break;
									}
									builder.checkConstraints();
								}
								else
								{
									builder.removeUnaryConstraints();
								}
								
								
								for (size_t j{0}; j < sbd.getNbSemantics(); ++j)
								{
									bool testOkSem = false;
									for (auto &it : builder.getDomain(j))
									{
										if (*it == sbd.getSemantic(j))
										{
											testOkSem = true;
											break;
										}
									}
										
									if (!testOkSem)
									{
										toAssign = j;
										excluded = true;
										break;
									}
								}
							}
							
							if (firstTest)
							{
								firstTest = false;
								comparedValue = excluded;
							}
							else
							{
								builder.printStatistics(ss);
								BOOST_REQUIRE_MESSAGE(excluded == comparedValue,  static_cast<string>(sbd)+" - "+((string) copy)+
								" does not pass the test noIPalindrome consistancy - exluded="+to_string(excluded)+" - variable "+to_string(toAssign)+" - domain size : "+to_string(builder.getDomain(toAssign).size())+
									"\nDomain : "+domToString(builder, toAssign)+"\n"+
									"assignation order : "+lastAssignationsToString(builder)+"\n"+
									precString
								);
							}
							builder.printStatistics(ss);
							precString =  static_cast<string>(sbd)+" - "+((string) copy)+
								" precedent  - exluded="+to_string(excluded)+" - variable "+to_string(toAssign)+" - domain size : "+to_string(builder.getDomain(toAssign).size())+
									"\nDomain : "+domToString(builder, toAssign)+"\n"+
									"assignation order : "+lastAssignationsToString(builder);
							
						} while (next_permutation(assignationOrder.begin(), assignationOrder.end()));
				}
			}
			else if (is_directory(path))
			{
				folders.push_back(path.string());
			}
		}
	}
}*/

BOOST_AUTO_TEST_CASE(checkNoIPalindromeConstraint)
{
	vector <string> folders;
	folders.push_back("tests/files/4toTest");
	
	auto domToString = [](IEDyckSemanticalBioDeviceBuilder &builder, size_t i){ string ret; for (auto &it : builder.getDomain(i)) { ret += (string) (*it)+"\t"; } return ret; };
	auto lastAssignationsToString = [](IEDyckSemanticalBioDeviceBuilder &builder){ string ret; for (auto &it : builder.getLastAssignations()) { ret += to_string(it)+" "; } return ret; };
	
	while (!folders.empty())
	{
		string inputPath = folders.back();
		folders.pop_back();
		const std::unordered_set<std::string> emptySetOfString;
		
		for(fs::directory_iterator it(inputPath), end; it != end; ++it) 
		{
			const fs::path path(it->path());
			if(fs::is_regular_file(it->status()))
			{
				DyckSemanticalBioDeviceFileReader reader(path.string());
				DyckSemanticalBioDevice sbd;
				
				while (reader >> sbd)
				{
					if (IEDyckSemanticalBioDeviceGenerator::reverseDyckWord(sbd.getStructure().getStructure()) == sbd.getStructure().getStructure())
					{
						fs::path p("work/"+to_string(sbd.getNbIntegrase()));
						if (!fs::exists(p))
							fs::create_directory(p);
						
						fs::path good(p), bad(p);
						good += fs::path("/good");
						bad += fs::path("/bad");
						
						if (!fs::exists(good))
							fs::create_directory(good);
						if (!fs::exists(bad))
							fs::create_directory(bad);
						
						for (size_t j{0}; j < sbd.getNbSemantics(); ++j)
						{
							if (sbd.getSemantic(j).isX())
								sbd.setSemantic(j, &Semantic::elementarySemantics[25]);
						}
						
						DyckSemanticalBioDevice copy = sbd;
						IEDyckSemanticalBioDeviceBuilder builder(&copy, &emptySetOfString, {IEDyckSemanticalBioDeviceBuilder::Setting::NO_IPALINDROME_CONSTRAINT});
						for (size_t j{0}; j < sbd.getNbSemantics(); ++j)
						{
							copy.setSemantic(j, nullptr);
						}
						
						bool excluded =false;
						ostringstream ss;
						auto toAssign = builder.chooseNextAssignation();
						
						for (size_t i{0}; i <= sbd.getNbSemantics() && !excluded; ++i)
						{
							toAssign = builder.chooseNextAssignation();
							if (i > 0)
							{
								bool testOkSem = false;
								for (auto &it : builder.getDomain(toAssign))
								{
									if (*it == sbd.getSemantic(toAssign))
									{
										testOkSem = true;
										builder.assignVar(toAssign,it);
										break;
									}
								}
								
								if (!testOkSem)
								{
									excluded = true;
									break;
								}
								builder.checkConstraints();
							}
							else
							{
								builder.removeUnaryConstraints();
							}
							
							
							for (size_t j{0}; j < sbd.getNbSemantics(); ++j)
							{
								bool testOkSem = false;
								for (auto &it : builder.getDomain(j))
								{
									if (*it == sbd.getSemantic(j))
									{
										testOkSem = true;
										break;
									}
								}
									
								if (!testOkSem)
								{
									toAssign = j;
									excluded = true;
									break;
								}
							}
						}
						
						auto reverseSbd = sbd.getDyckSymmetric();
						
						if (reverseSbd == sbd)
						{
							builder.printStatistics(ss);
							BOOST_REQUIRE_MESSAGE(!excluded,  static_cast<string>(sbd)+" - "+((string) copy)+
							" does not pass the test noIPalindrome - variable "+to_string(toAssign)+" - domain size : "+to_string(builder.getDomain(toAssign).size())+
								"\nDomain : "+domToString(builder, toAssign)+"\n"+
								"assignation order : "+lastAssignationsToString(builder)
							);
						}
						else 
						{
							DyckSemanticalBioDevice copy2 = reverseSbd;
							IEDyckSemanticalBioDeviceBuilder builder2(&copy2, &emptySetOfString, {IEDyckSemanticalBioDeviceBuilder::Setting::NO_IPALINDROME_CONSTRAINT});
							for (size_t j{0}; j < reverseSbd.getNbSemantics(); ++j)
							{
								copy2.setSemantic(j, nullptr);
							}
							
							bool excluded2 = false;
							ostringstream ss2;
							auto toAssign2 = builder2.chooseNextAssignation();
							
							for (size_t i{0}; i <= reverseSbd.getNbSemantics() && !excluded2; ++i)
							{
								toAssign2 = builder2.chooseNextAssignation();
								if (i > 0)
								{
									bool testOkSem = false;
									for (auto &it : builder2.getDomain(toAssign2))
									{
										if (*it == reverseSbd.getSemantic(toAssign2))
										{
											testOkSem = true;
											builder2.assignVar(toAssign2,it);
											break;
										}
									}
									
									if (!testOkSem)
									{
										excluded2 = true;
										break;
									}
									builder2.checkConstraints();
								}
								else
								{
									builder2.removeUnaryConstraints();
								}
								
								
								for (size_t j{0}; j < reverseSbd.getNbSemantics(); ++j)
								{
									bool testOkSem = false;
									for (auto &it : builder2.getDomain(j))
									{
										if (*it == reverseSbd.getSemantic(j))
										{
											testOkSem = true;
											break;
										}
									}
										
									if (!testOkSem)
									{
										toAssign2 = j;
										excluded2 = true;
										break;
									}
								}
							}
							builder.printStatistics(ss);
							builder2.printStatistics(ss2);
							BOOST_REQUIRE_MESSAGE((excluded && !excluded2) || (!excluded && excluded2),static_cast<string>(sbd)+" - "+((string) copy)+
							" - "+((string) copy2)+
							" does not pass the test noIPalindrome\n"+
							"First sbd - exluded="+to_string(excluded)+" : variable "+to_string(toAssign)+" - domain size : "+to_string(builder.getDomain(toAssign).size())+
								"\nDomain : "+domToString(builder, toAssign)+"\n"+
								"assignation order : "+lastAssignationsToString(builder)+"\n"
							+(ss.str())+"\n"+
							"Second sbd - exluded="+to_string(excluded2)+" : variable "+to_string(toAssign2)+" - domain size : "+to_string(builder2.getDomain(toAssign2).size())+
								"\nDomain : "+domToString(builder2, toAssign2)+"\n"+
								"assignation order : "+lastAssignationsToString(builder2)+"\n"
								+(ss2.str())+"\n"
							);
						}
						//builder.printStatistics(cout);
					}
				}
			}
			else if (is_directory(path))
			{
				folders.push_back(path.string());
			}
		}
	}
}

/*
BOOST_AUTO_TEST_CASE(checkNoIPalindromeConstraintWithAllOthers)
{
	vector <string> folders;
	folders.push_back("tests/files/4toTest");
	
	auto domToString = [](IEDyckSemanticalBioDeviceBuilder &builder, size_t i){ string ret; for (auto &it : builder.getDomain(i)) { ret += (string) (*it)+"\t"; } return ret; };
	auto lastAssignationsToString = [](IEDyckSemanticalBioDeviceBuilder &builder){ string ret; for (auto &it : builder.getLastAssignations()) { ret += to_string(it)+" "; } return ret; };
	
	while (!folders.empty())
	{
		string inputPath = folders.back();
		folders.pop_back();
		
		for(fs::directory_iterator it(inputPath), end; it != end; ++it) 
		{
			const fs::path path(it->path());
			if(fs::is_regular_file(it->status()))
			{
				DyckSemanticalBioDeviceFileReader reader(path.string());
				DyckSemanticalBioDevice sbd;
				
				while (reader >> sbd)
				{
					if (IEDyckSemanticalBioDeviceGenerator::reverseDyckWord(sbd.getStructure().getStructure()) == sbd.getStructure().getStructure())
					{
						fs::path p("work/"+to_string(sbd.getNbIntegrase()));
						if (!fs::exists(p))
							fs::create_directory(p);
						
						fs::path good(p), bad(p);
						good += fs::path("/good");
						bad += fs::path("/bad");
						
						if (!fs::exists(good))
							fs::create_directory(good);
						if (!fs::exists(bad))
							fs::create_directory(bad);
						
						for (size_t j{0}; j < sbd.getNbSemantics(); ++j)
						{
							if (sbd.getSemantic(j).isX())
								sbd.setSemantic(j, &Semantic::elementarySemantics[25]);
						}
						
						DyckSemanticalBioDevice copy = sbd;
						IEDyckSemanticalBioDeviceBuilder builder(&copy, {IEDyckSemanticalBioDeviceBuilder::Setting::NO_IPALINDROME_CONSTRAINT});
						for (size_t j{0}; j < sbd.getNbSemantics(); ++j)
						{
							copy.setSemantic(j, nullptr);
						}
						SemanticalBioDeviceConstraintBuilder constraintsBuilder (&copy, &builder);
						builder.addConstraints(constraintsBuilder.buildCheckUtilityAfterPrefixConstraints());
						builder.addConstraints(constraintsBuilder.buildCheckUtilityAfterSemanticConstraints());
						builder.addConstraints(constraintsBuilder.buildCheckUtilityBeforeSuffixConstraints());
						builder.addConstraints(constraintsBuilder.buildCheckUtilityBeforeSemanticConstraints());
						builder.addConstraints(constraintsBuilder.buildAtomicSitesConstraints());
						builder.addConstraints(constraintsBuilder.buildExpOnlyInExcisionConstraints());
						
						bool excluded =false;
						ostringstream ss;
						auto toAssign = builder.chooseNextAssignation();
						
						for (size_t i{0}; i <= sbd.getNbSemantics() && !excluded; ++i)
						{
							toAssign = builder.chooseNextAssignation();
							if (i > 0)
							{
								bool testOkSem = false;
								for (auto &it : builder.getDomain(toAssign))
								{
									if (*it == sbd.getSemantic(toAssign))
									{
										testOkSem = true;
										builder.assignVar(toAssign,it);
										break;
									}
								}
								
								if (!testOkSem)
								{
									excluded = true;
									break;
								}
								builder.checkConstraints();
							}
							else
							{
								builder.removeUnaryConstraints();
							}
							
							
							for (size_t j{0}; j < sbd.getNbSemantics(); ++j)
							{
								bool testOkSem = false;
								for (auto &it : builder.getDomain(j))
								{
									if (*it == sbd.getSemantic(j))
									{
										testOkSem = true;
										break;
									}
								}
									
								if (!testOkSem)
								{
									toAssign = j;
									excluded = true;
									break;
								}
							}
						}
						
						auto reverseSbd = sbd.getDyckSymmetric();
						
						if (reverseSbd == sbd)
						{
							builder.printStatistics(ss);
							BOOST_REQUIRE_MESSAGE(!excluded,  static_cast<string>(sbd)+" - "+((string) copy)+
							" does not pass the test noIPalindrome - variable "+to_string(toAssign)+" - domain size : "+to_string(builder.getDomain(toAssign).size())+
								"\nDomain : "+domToString(builder, toAssign)+"\n"+
								"assignation order : "+lastAssignationsToString(builder)
							);
						}
						else 
						{
							DyckSemanticalBioDevice copy2 = reverseSbd;
							IEDyckSemanticalBioDeviceBuilder builder2(&copy2, {IEDyckSemanticalBioDeviceBuilder::Setting::NO_IPALINDROME_CONSTRAINT});
							for (size_t j{0}; j < reverseSbd.getNbSemantics(); ++j)
							{
								copy2.setSemantic(j, nullptr);
							}
							SemanticalBioDeviceConstraintBuilder constraintsBuilder2 (&copy2, &builder2);
							builder2.addConstraints(constraintsBuilder2.buildCheckUtilityAfterPrefixConstraints());
							builder2.addConstraints(constraintsBuilder2.buildCheckUtilityAfterSemanticConstraints());
							builder2.addConstraints(constraintsBuilder2.buildCheckUtilityBeforeSuffixConstraints());
							builder2.addConstraints(constraintsBuilder2.buildCheckUtilityBeforeSemanticConstraints());
							builder2.addConstraints(constraintsBuilder2.buildAtomicSitesConstraints());
							builder2.addConstraints(constraintsBuilder2.buildExpOnlyInExcisionConstraints());
							
							bool excluded2 = false;
							ostringstream ss2;
							auto toAssign2 = builder2.chooseNextAssignation();
							
							for (size_t i{0}; i <= reverseSbd.getNbSemantics() && !excluded2; ++i)
							{
								toAssign2 = builder2.chooseNextAssignation();
								if (i > 0)
								{
									bool testOkSem = false;
									for (auto &it : builder2.getDomain(toAssign2))
									{
										if (*it == reverseSbd.getSemantic(toAssign2))
										{
											testOkSem = true;
											builder2.assignVar(toAssign2,it);
											break;
										}
									}
									
									if (!testOkSem)
									{
										excluded2 = true;
										break;
									}
									builder2.checkConstraints();
								}
								else
								{
									builder2.removeUnaryConstraints();
								}
								
								
								for (size_t j{0}; j < reverseSbd.getNbSemantics(); ++j)
								{
									bool testOkSem = false;
									for (auto &it : builder2.getDomain(j))
									{
										if (*it == reverseSbd.getSemantic(j))
										{
											testOkSem = true;
											break;
										}
									}
										
									if (!testOkSem)
									{
										toAssign2 = j;
										excluded2 = true;
										break;
									}
								}
							}
							builder.printStatistics(ss);
							builder2.printStatistics(ss2);
							BOOST_REQUIRE_MESSAGE((excluded && !excluded2) || (!excluded && excluded2),static_cast<string>(sbd)+" - "+((string) copy)+
							" - "+((string) copy2)+
							" does not pass the test noIPalindrome\n"+
							"First sbd - exluded="+to_string(excluded)+" : variable "+to_string(toAssign)+" - domain size : "+to_string(builder.getDomain(toAssign).size())+
								"\nDomain : "+domToString(builder, toAssign)+"\n"+
								"assignation order : "+lastAssignationsToString(builder)+"\n"
							+(ss.str())+"\n"+
							"Second sbd - exluded="+to_string(excluded2)+" : variable "+to_string(toAssign2)+" - domain size : "+to_string(builder2.getDomain(toAssign2).size())+
								"\nDomain : "+domToString(builder2, toAssign2)+"\n"+
								"assignation order : "+lastAssignationsToString(builder2)+"\n"
								+(ss2.str())+"\n"
							);
						}
						//builder.printStatistics(cout);
					}
				}
			}
			else if (is_directory(path))
			{
				folders.push_back(path.string());
			}
		}
	}
}*/
/*
BOOST_AUTO_TEST_CASE(checkAllConstraintsDontRemoveIrreducible)
{
	vector <string> folders;
	//folders.push_back("tests/files/unique/1");
	//folders.push_back("tests/files/unique/2");
	folders.push_back("tests/files/4toTest");
	
	auto domToString = [](IEDyckSemanticalBioDeviceBuilder &builder, size_t i){ string ret; for (auto &it : builder.getDomain(i)) { ret += (string) (*it)+"\t"; } return ret; };
	auto lastAssignationsToString = [](IEDyckSemanticalBioDeviceBuilder &builder){ string ret; for (auto &it : builder.getLastAssignations()) { ret += to_string(it)+" "; } return ret; };
	
	while (!folders.empty())
	{
		string inputPath = folders.back();
		folders.pop_back();
		
		for(fs::directory_iterator it(inputPath), end; it != end; ++it) 
		{
			const fs::path path(it->path());
			if(fs::is_regular_file(it->status()))
			{
				DyckSemanticalBioDeviceFileReader reader(path.string());
				DyckSemanticalBioDevice sbd;
				
				while (reader >> sbd)
				{
					fs::path p("work/"+to_string(sbd.getNbIntegrase()));
					if (!fs::exists(p))
						fs::create_directory(p);
					
					fs::path good(p), bad(p);
					good += fs::path("/good");
					bad += fs::path("/bad");
					
					if (!fs::exists(good))
						fs::create_directory(good);
					if (!fs::exists(bad))
						fs::create_directory(bad);
					
					DyckSemanticalBioDevice copy = sbd;
					IEDyckSemanticalBioDeviceBuilder builder(&copy, {IEDyckSemanticalBioDeviceBuilder::Setting::NO_IPALINDROME_CONSTRAINT});
					for (size_t j{0}; j < sbd.getNbSemantics(); ++j)
					{
						copy.setSemantic(j, nullptr);
					}
					SemanticalBioDeviceConstraintBuilder constraintsBuilder (&copy, &builder);
					builder.addConstraints(constraintsBuilder.buildCheckUtilityAfterPrefixConstraints());
					builder.addConstraints(constraintsBuilder.buildCheckUtilityAfterSemanticConstraints());
					builder.addConstraints(constraintsBuilder.buildCheckUtilityBeforeSuffixConstraints());
					builder.addConstraints(constraintsBuilder.buildCheckUtilityBeforeSemanticConstraints());
					builder.addConstraints(constraintsBuilder.buildAtomicSitesConstraints());
					builder.addConstraints(constraintsBuilder.buildExpOnlyInExcisionConstraints());
				
					for (size_t i{0}; i <= sbd.getNbSemantics(); ++i)
					{
						auto toAssign = builder.chooseNextAssignation();
						if (i > 0)
						{
							bool testOkSem = false;
							for (auto &it : builder.getDomain(toAssign))
							{
								if (*it == sbd.getSemantic(toAssign))
								{
									testOkSem = true;
									builder.assignVar(toAssign,it);
									break;
								}
							}
							ostringstream ss;
							builder.printStatistics(ss);
							BOOST_REQUIRE_MESSAGE(testOkSem, ss.str()+  static_cast<string>(sbd)+" - "+((string) copy)+
							" does not pass the test with utility constraints - variable "+to_string(toAssign)+" - domain size : "+to_string(builder.getDomain(toAssign).size())+
								"\nDomain : "+domToString(builder, toAssign)+
								"assignation order : "+lastAssignationsToString(builder)+"\n"
							);
							builder.checkConstraints();
						}
						else
						{
							builder.removeUnaryConstraints();
						}
						 
						
						for (size_t j{0}; j < sbd.getNbSemantics(); ++j)
						{
							bool testOkSem = false;
							for (auto &it : builder.getDomain(j))
							{
								if (*it == sbd.getSemantic(j))
								{
									testOkSem = true;
									break;
								}
							}
							ostringstream ss;
							builder.printStatistics(ss);
						
							BOOST_REQUIRE_MESSAGE(testOkSem, ss.str()+ static_cast<string>(sbd)+" - "+((string) copy)+
							" does not pass the test with utility constraints - variable "+to_string(j)+" - domain size : "+to_string(builder.getDomain(j).size())+
								"\nDomain : "+domToString(builder, j)+
								"\nAssigned var : "+to_string(toAssign)+
								"assignation order : "+lastAssignationsToString(builder)+"\n"
							);
						}
					}
					//builder.printStatistics(cout);
				}
			}
			else if (is_directory(path))
			{
				folders.push_back(path.string());
			}
		}
	}
}*/
/*

BOOST_AUTO_TEST_CASE(checkConstraintsUtilityDontRemoveIrreducible0)
{
	vector <string> folders;
	//folders.push_back("tests/files/classicalBioDevices");
	//folders.push_back("tests/files/unique");
	
	folders.push_back("tests/files/4toTest");
	
	auto domToString = [](IEDyckSemanticalBioDeviceBuilder &builder, size_t i){ string ret; for (auto &it : builder.getDomain(i)) { ret += (string) (*it)+"\t"; } return ret; };
	
	while (!folders.empty())
	{
		string inputPath = folders.back();
		folders.pop_back();
		
		for(fs::directory_iterator it(inputPath), end; it != end; ++it) 
		{
			const fs::path path(it->path());
			if(fs::is_regular_file(it->status()))
			{
				DyckSemanticalBioDeviceFileReader reader(path.string());
				DyckSemanticalBioDevice sbd;
				
				while (reader >> sbd)
				{
					fs::path p("work/"+to_string(sbd.getNbIntegrase()));
					if (!fs::exists(p))
						fs::create_directory(p);
					
					fs::path good(p), bad(p);
					good += fs::path("/good");
					bad += fs::path("/bad");
					
					if (!fs::exists(good))
						fs::create_directory(good);
					if (!fs::exists(bad))
						fs::create_directory(bad);
					
					DyckSemanticalBioDevice copy = sbd;
					IEDyckSemanticalBioDeviceBuilder builder(&copy, {});
					for (size_t j{0}; j < sbd.getNbSemantics(); ++j)
					{
						copy.setSemantic(j, nullptr);
					}
					SemanticalBioDeviceConstraintBuilder constraintsBuilder (&copy, &builder);
					builder.addConstraints(constraintsBuilder.buildCheckUtilityAfterPrefixConstraints());
					builder.addConstraints(constraintsBuilder.buildCheckUtilityAfterSemanticConstraints());
					builder.addConstraints(constraintsBuilder.buildCheckUtilityBeforeSuffixConstraints());
					builder.addConstraints(constraintsBuilder.buildCheckUtilityBeforeSemanticConstraints());
					builder.addConstraints(constraintsBuilder.buildAtomicSitesConstraints());
					builder.addConstraints(constraintsBuilder.buildExpOnlyInExcisionConstraints());
				
					for (size_t i{0}; i <= sbd.getNbSemantics(); ++i)
					{
						auto toAssign = builder.chooseNextAssignation();
						if (i > 0)
						{
							bool testOkSem = false;
							for (auto &it : builder.getDomain(toAssign))
							{
								if (*it == sbd.getSemantic(toAssign))
								{
									testOkSem = true;
									builder.assignVar(toAssign,it);
									break;
								}
							}
							builder.checkConstraints();
							BOOST_REQUIRE_MESSAGE(testOkSem,  static_cast<string>(sbd)+" - "+((string) copy)+
							" does not pass the test with utility constraints - variable "+to_string(toAssign)+" - domain size : "+to_string(builder.getDomain(toAssign).size())+
								"\nDomain : "+domToString(builder, toAssign)
							);
						}
						else
						{
							builder.removeUnaryConstraints();
						}
						 
						
						for (size_t j{0}; j < sbd.getNbSemantics(); ++j)
						{
							bool testOkSem = false;
							for (auto &it : builder.getDomain(j))
							{
								if (*it == sbd.getSemantic(j))
								{
									testOkSem = true;
									break;
								}
							}
						
							BOOST_REQUIRE_MESSAGE(testOkSem,static_cast<string>(sbd)+" - "+((string) copy)+
							" does not pass the test with utility constraints - variable "+to_string(j)+" - domain size : "+to_string(builder.getDomain(j).size())+
								"\nDomain : "+domToString(builder, j)+
								"\nAssigned var : "+to_string(toAssign)
							);
						}
					}
					//builder.printStatistics(cout);
				}
			}
			else if (is_directory(path))
			{
				folders.push_back(path.string());
			}
		}
	}
}
*/
/*

BOOST_AUTO_TEST_CASE(checkConstraintsUtilityDontRemoveIrreducible)
{
	vector <string> folders;
	folders.push_back("tests/files/classicalBioDevices");
	folders.push_back("tests/files/unique");
	
	auto domToString = [](IEDyckSemanticalBioDeviceBuilder &builder, size_t i){ string ret; for (auto &it : builder.getDomain(i)) { ret += (string) (*it)+"\t"; } return ret; };
	
	while (!folders.empty())
	{
		string inputPath = folders.back();
		folders.pop_back();
		
		for(fs::directory_iterator it(inputPath), end; it != end; ++it) 
		{
			const fs::path path(it->path());
			if(fs::is_regular_file(it->status()))
			{
				DyckSemanticalBioDeviceFileReader reader(path.string());
				
				DyckSemanticalBioDevice sbd;
				
				while (reader >> sbd)
				{
					fs::path p("work/"+to_string(sbd.getNbIntegrase()));
					if (!fs::exists(p))
						fs::create_directory(p);
					
					fs::path good(p), bad(p);
					good += fs::path("/good");
					bad += fs::path("/bad");
					
					if (!fs::exists(good))
						fs::create_directory(good);
					if (!fs::exists(bad))
						fs::create_directory(bad);
				
					DyckSemanticalBioDevice copy = sbd;
					for (size_t i{0}; i < sbd.getNbSemantics(); ++i)
					{
						IEDyckSemanticalBioDeviceBuilder builder(&copy, {});
						//builder.generateUtilityConstraints();
						
						for (size_t j{0}; j < sbd.getNbSemantics(); ++j)
						{
							copy.setSemantic(j, nullptr);
							if (i != j)
								builder.assignVar(j,&(sbd.getSemantic(j)));
						}
						SemanticalBioDeviceConstraintBuilder constraintsBuilder (&copy, &builder);
						builder.addConstraints(constraintsBuilder.buildCheckUtilityAfterPrefixConstraints());
						builder.addConstraints(constraintsBuilder.buildCheckUtilityAfterSemanticConstraints());
						builder.addConstraints(constraintsBuilder.buildCheckUtilityBeforeSuffixConstraints());
						builder.addConstraints(constraintsBuilder.buildCheckUtilityBeforeSemanticConstraints());
						 
						builder.checkConstraints();
						
						bool testOk = false;
						for (auto &it : builder.getDomain(i))
						{
							if (*it == sbd.getSemantic(i))
							{
								testOk = true;
								break;
							}
						}
						
						BOOST_REQUIRE_MESSAGE(testOk,static_cast<string>(sbd)+" - "+((string) copy)+
						" does not pass the test with utility constraints - variable "+to_string(i)+" - domain size : "+to_string(builder.getDomain(i).size())+
							"\nDomain : "+domToString(builder, i)
						);
					}
				}
			}
			else if (is_directory(path))
			{
				folders.push_back(path.string());
			}
		}
	}
}


BOOST_AUTO_TEST_CASE(checkAllConstraintsExceptIPalDontRemoveIrreducible)
{
	vector <string> folders;
	folders.push_back("tests/files/classicalBioDevices");
	folders.push_back("tests/files/unique");
	
	auto domToString = [](IEDyckSemanticalBioDeviceBuilder &builder, size_t i){ string ret; for (auto &it : builder.getDomain(i)) { ret += (string) (*it)+"\t"; } return ret; };
	
	while (!folders.empty())
	{
		string inputPath = folders.back();
		folders.pop_back();
		
		for(fs::directory_iterator it(inputPath), end; it != end; ++it) 
		{
			const fs::path path(it->path());
			if(fs::is_regular_file(it->status()))
			{
				DyckSemanticalBioDeviceFileReader reader(path.string());
				
				DyckSemanticalBioDevice sbd;
				
				while (reader >> sbd)
				{
					fs::path p("work/"+to_string(sbd.getNbIntegrase()));
					if (!fs::exists(p))
						fs::create_directory(p);
					
					fs::path good(p), bad(p);
					good += fs::path("/good");
					bad += fs::path("/bad");
					
					if (!fs::exists(good))
						fs::create_directory(good);
					if (!fs::exists(bad))
						fs::create_directory(bad);
				
					DyckSemanticalBioDevice copy = sbd;
					
					for (size_t i{0}; i < sbd.getNbSemantics(); ++i)
					{
						IEDyckSemanticalBioDeviceBuilder builder(&copy, {});
						for (size_t j{0}; j < sbd.getNbSemantics(); ++j)
						{
							copy.setSemantic(j, nullptr);
							if (i != j)
								builder.assignVar(j,&(sbd.getSemantic(j)));
						}
						SemanticalBioDeviceConstraintBuilder constraintsBuilder (&copy, &builder);
						builder.addConstraints(constraintsBuilder.buildCheckUtilityAfterPrefixConstraints());
						builder.addConstraints(constraintsBuilder.buildCheckUtilityAfterSemanticConstraints());
						builder.addConstraints(constraintsBuilder.buildCheckUtilityBeforeSuffixConstraints());
						builder.addConstraints(constraintsBuilder.buildCheckUtilityBeforeSemanticConstraints());
						builder.addConstraints(constraintsBuilder.buildAtomicSitesConstraints());
						builder.addConstraints(constraintsBuilder.buildExpOnlyInExcisionConstraints());
						 
						builder.checkConstraints();
						
						bool testOk = false;
						for (auto &it : builder.getDomain(i))
						{
							if (*it == sbd.getSemantic(i))
							{
								testOk = true;
								break;
							}
						}
						
						BOOST_REQUIRE_MESSAGE(testOk,static_cast<string>(sbd)+" - "+((string) copy)+
						" does not pass the test with all constraints - variable "+to_string(i)+" - domain size : "+to_string(builder.getDomain(i).size())+
							"\nDomain : "+domToString(builder, i)
						);
					}
				}
			}
			else if (is_directory(path))
			{
				folders.push_back(path.string());
			}
		}
	}
}*/
