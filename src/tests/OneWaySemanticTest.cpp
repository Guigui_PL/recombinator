#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MAIN
#include <array>
#include <set>
#include <vector>
#include <boost/test/unit_test.hpp>
#include "../api/OneWaySemantic.hpp"

using namespace recombinator::api;
using namespace std;

typedef OneWaySemantic::BioFunction BF;

vector<OneWaySemantic> OWSList {{OneWaySemantic(BF::N), OneWaySemantic(BF::X), OneWaySemantic(BF::T), OneWaySemantic(BF::P), OneWaySemantic(BF::G), OneWaySemantic(BF::GP)}};
vector<BF> BFList {{BF::N, BF::P, BF::T, BF::G, BF::GP, BF::X}};

BOOST_AUTO_TEST_CASE(isThereSixDifferentsOWS)
{
	// We put the OWS in a set : each element appears once in a set 
	// So, it must have the same number of elements that the OWSList
	set<OneWaySemantic> OWSSet;
	for (auto &it : OWSList)
	{
		OWSSet.insert(it);
	}
	
	BOOST_REQUIRE_EQUAL(OWSSet.size(), OWSList.size());
}

BOOST_AUTO_TEST_CASE(isThereSetAndGetBioFunctionOK)
{
	// For each BF, we set its value with setBioFunction() and we chech if, when we use getBioFunction(), the value is the same
	OneWaySemantic ows;
	for (auto &it : BFList)
	{
		ows.setBioFunction(it);
		BOOST_REQUIRE_EQUAL(ows.getBioFunction(), it);
	}
}

BOOST_AUTO_TEST_CASE(isThere_isFunction_OK)
{
	// each line représents a BF and each colon a is[BioFunction]()
	// The cells are booleans
	// Lines : BF::N, BF::P, BF::T, BF::G, BF::GP, BF::X (the same as BFList)
	// Colons : isP(), isG(), isT(), isX(), isN()
	array<array<bool,5>,6> matrix
	{{
		{0,0,0,0,1},
		{1,0,0,0,0},
		{0,0,1,0,0},
		{0,1,0,0,0},
		{1,1,0,0,0},
		{0,0,0,1,0}
	}};
	
	for (uint8_t i(0); i < matrix.size(); ++i)
	{
		OneWaySemantic ows(BFList[i]);
		for (uint8_t j(0); j < matrix[i].size(); ++j)
		{
			switch (j)
			{
				case 0: // isP
					BOOST_REQUIRE_EQUAL(ows.isP(),matrix[i][j]);
					break;
				case 1: // isG
					BOOST_REQUIRE_EQUAL(ows.isG(),matrix[i][j]);
					break;
				case 2: // isT
					BOOST_REQUIRE_EQUAL(ows.isT(),matrix[i][j]);
					break;
				case 3: // isX
					BOOST_REQUIRE_EQUAL(ows.isX(),matrix[i][j]);
					break;
				case 4: // isN
					BOOST_REQUIRE_EQUAL(ows.isN(),matrix[i][j]);
					break;
			}
		}
	}
}

BOOST_AUTO_TEST_CASE(checkIsEqualTo)
{
	// We compare two elements of the OWSList
	// If i == j, our two elements must be equals
	// If i != j, our two elements must be differents
	for (uint8_t i(0); i < OWSList.size(); ++i)
	{
		for (uint8_t j(0); j < OWSList.size(); ++j)
		{
			if (i == j)
			{
				BOOST_REQUIRE(OWSList[i] == OWSList[j]);
				BOOST_REQUIRE(OWSList[i].isEqualTo(OWSList[j]));
				BOOST_REQUIRE(!(OWSList[i] != OWSList[j]));
			}
			else
			{
				BOOST_REQUIRE(OWSList[i] != OWSList[j]);
				BOOST_REQUIRE(!OWSList[i].isEqualTo(OWSList[j]));
				BOOST_REQUIRE(!(OWSList[i] == OWSList[j]));
			}
		}
	}
}

BOOST_AUTO_TEST_CASE(checkCombine)
{
	array<array<BF,6>,6> results
	{{
		{BF::N, BF::P, BF::T, BF::G, BF::GP, BF::X},
		{BF::P, BF::P, BF::T, BF::X, BF::X, BF::X},
		{BF::T, BF::P, BF::T, BF::T, BF::P, BF::X},
		{BF::G, BF::GP, BF::G, BF::G, BF::GP, BF::X},
		{BF::GP, BF::GP, BF::G, BF::X, BF::X, BF::X},
		{BF::X, BF::X, BF::X, BF::X, BF::X, BF::X}
	}};
	
	for (uint8_t i(0); i < BFList.size(); ++i)
	{
		for (uint8_t j(0); j < BFList.size(); ++j)
		{
			OneWaySemantic ows1(BFList[i]);
			OneWaySemantic ows2(BFList[j]);
			
			// combine static method
			BOOST_REQUIRE_MESSAGE(OneWaySemantic::combine(ows1, ows2).getBioFunction() == results[i][j], 
								  "combine("+static_cast<string>(ows1)+","
									+static_cast<string>(ows2)+") = "
									+static_cast<string>(OneWaySemantic::combine(ows1, ows2))+" != "
									+static_cast<string>(OneWaySemantic(results[i][j])));
			// operator+
			BOOST_REQUIRE_MESSAGE((ows1 + ows2).getBioFunction() == results[i][j], 
								    static_cast<string>(ows1)+" + "
									+static_cast<string>(ows2)+" = "
									+static_cast<string>(ows1 + ows2)+" != "
									+static_cast<string>(OneWaySemantic(results[i][j])));
			// combine method
			ows1.combine(ows2);
			BOOST_REQUIRE_MESSAGE(ows1.getBioFunction() == results[i][j], 
								    static_cast<string>(OneWaySemantic(BFList[i]))+".combine("
									+static_cast<string>(ows2)+") = "
									+static_cast<string>(ows1)+" != "
									+static_cast<string>(OneWaySemantic(results[i][j])));
			// operator +=
			ows1 = OneWaySemantic(BFList[i]);
			ows1 += ows2;
			BOOST_REQUIRE_MESSAGE(ows1.getBioFunction() == results[i][j], 
								    static_cast<string>(OneWaySemantic(BFList[i]))+" += "
									+static_cast<string>(ows2)+" = "
									+static_cast<string>(ows1)+" != "
									+static_cast<string>(OneWaySemantic(results[i][j])));
		}
	}
}

BOOST_AUTO_TEST_CASE (utilityPrefix)
{
	array<OneWaySemantic,6> OWSList {{OneWaySemantic(BF::N), OneWaySemantic(BF::P), OneWaySemantic(BF::T), OneWaySemantic(BF::G), OneWaySemantic(BF::GP), OneWaySemantic(BF::X)}};

	array<array<bool,6>,6> utilityAfterPrefix {{
		{1,1,0,0,0,1},
		{1,0,1,1,0,0},
		{1,1,0,0,0,1},
		{1,1,0,0,0,1},
		{1,0,1,1,0,0},
		{1,0,0,0,0,0}}};
	
	
	for (uint8_t i(0); i < OWSList.size(); ++i)
	{
		for (uint8_t j(0); j < OWSList.size(); ++j)
		{
			auto sem1{OWSList[i]}, sem2{OWSList[j]};
			BOOST_REQUIRE_MESSAGE(sem2.isUsefulAfterPrefix(sem1) == utilityAfterPrefix[i][j],
								  (string)sem2+" is useful after prefix "+(string)sem1+" : "
								  +to_string(utilityAfterPrefix[i][j])
								  +" - answer of isUsefulAfterPrefix() : "
								  +to_string(sem2.isUsefulAfterPrefix(sem1)));
		}
	}
}

BOOST_AUTO_TEST_CASE (utilitySuffix)
{
	array<OneWaySemantic,6> OWSList {{OneWaySemantic(BF::N), OneWaySemantic(BF::P), OneWaySemantic(BF::T), OneWaySemantic(BF::G), OneWaySemantic(BF::GP), OneWaySemantic(BF::X)}};

	array<array<bool,6>,6> utilityBeforeSuffix {{
		{1,1,1,1,1,1},
		{0,0,0,1,1,0},
		{0,0,0,1,1,0},
		{1,1,1,0,0,0},
		{0,0,0,0,0,0},
		{1,1,1,0,0,0}}};
	
	
	for (uint8_t i(0); i < OWSList.size(); ++i)
	{
		for (uint8_t j(0); j < OWSList.size(); ++j)
		{
			auto sem1{OWSList[i]}, sem2{OWSList[j]};
			BOOST_REQUIRE_MESSAGE(sem2.isUsefulBeforeSuffix(sem1) == utilityBeforeSuffix[j][i],
								  (string)sem2+" is useful before suffix "+(string)sem1+" : "
								  +to_string(utilityBeforeSuffix[j][i])
								  +" - answer of isUsefulBeforeSuffix() : "
								  +to_string(sem2.isUsefulBeforeSuffix(sem1)));
		}
	}
}

