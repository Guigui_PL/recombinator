#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MAIN
#include <exception>
#include <map>
#include <set>
#include <vector>
#include <unordered_map>
#include <boost/test/unit_test.hpp>
#include "../api/IEFunctionnalStructure.hpp"

using namespace recombinator::api;
using namespace std; 


BOOST_AUTO_TEST_CASE(checkStructureValidity)
{
	// We start by create a vector with invalid structures
	vector<string> invalid ({
		"(1[2&]2)1", // Invalid character in structure 
		"(1(2)2)1)0", "(1(2)2)1]0", // Too closing AMIs
		"(1(2)2", "[1(2)2", // Some AMIs unclosed
		"(1(2)3)1", "(1(1)1)0", // Invalid indexes
		"(1[2]1)2", "[1(2)1]2" // Wrong associations between indexes and AMIs of different types
	});
	
	for (auto &i : invalid)
	{
		BOOST_CHECK_THROW(IEFunctionnalStructure::create<IEFunctionnalStructure>(i), invalid_argument);
	}
}

BOOST_AUTO_TEST_CASE(checkNbOfDistinctActivations)
{
	// We create a vector of (valid) structures 
	// number of activations -> structure
	unordered_multimap<int,string> structures ({
		{1,""},
		{2,"[0]0"}, {2,"(0)0"},
		{5, "(1[2)1]2"}, {5, "(1[2]2)1"},
		{16, "(1(2[3]3)2)1"},
		{65, "(1(2[3]3(4)4)2)1"}
	});
	
	for (auto &e : structures)
	{
		auto structure = IEFunctionnalStructure::create<IEFunctionnalStructure>(e.second);
		BOOST_REQUIRE_EQUAL(e.first,structure->getActivations().size());
		set<vector<uint8_t>> distinctActivations (structure->getActivations().begin(), structure->getActivations().end());
		BOOST_REQUIRE_EQUAL(e.first,distinctActivations.size());
	}
}

BOOST_AUTO_TEST_CASE(checkDerivedBioDevs)
{
	auto firstStruct = IEFunctionnalStructure::create<IEFunctionnalStructure>("(1[2)1]2");
	map<IEFunctionnalStructure::Activation, IEFunctionnalStructure::SemanticalBioDeviceStructure> firstStructAct ({
		{{},{0,1,2,3,4}},
		{{0},{0,-2,-1,3,4}},
		{{1},{0,1,4}},
		{{0,1},{0,-2,-3,1,4}},
		{{1,0},{0,1,4}}
	});
	
	for (auto &a: firstStructAct)
	{
		BOOST_CHECK(firstStruct->getDerivedBioDevicesStructure(a.first) == a.second);
	}
	
	auto secondStruct = IEFunctionnalStructure::create<IEFunctionnalStructure>("(1[2]2)1");
	map<IEFunctionnalStructure::Activation, IEFunctionnalStructure::SemanticalBioDeviceStructure> secondStructAct ({
		{{},{0,1,2,3,4}},
		{{0},{0,-3,-2,-1,4}},
		{{1},{0,1,3,4}},
		{{0,1},{0,-3,-1,4}},
		{{1,0},{0,-3,-1,4}}
	});
	
	for (auto &a: secondStructAct)
	{
		BOOST_CHECK(secondStruct->getDerivedBioDevicesStructure(a.first) == a.second);
	}
}