#include <string>
#include <iostream>
#include <future>
#include "boost/program_options.hpp"
#include "boost/algorithm/string/trim.hpp"
#include <boost/filesystem.hpp>
#include "../api/DyckSemanticalBioDevice.hpp"
#include "../api/BioDeviceFile.hpp"
#include "../api/DyckSemanticalBioDeviceTruthTable.hpp"

using namespace std;
using namespace recombinator::api;
namespace fs = boost::filesystem;

// Return value for the main function
namespace 
{
	const size_t SUCCESS = 0;
	const size_t ERROR_IN_COMMAND_LINE = 1;
	const size_t ERROR_IN_MAIN = 2;
	const size_t UNKNOWN_EXCEPTION = 3;
}

int main (int argc, char** argv)
{
	// For manage the program options, we use the library boost::program_options
	namespace po = boost::program_options;
	
	// We create container for paramaters of options
	vector<string> optContainer;
	po::variables_map vm;
	po::options_description desc("Allowed options");
	
	try
	{
		// We create the list of all options of our program
		desc.add_options()
		("check-and-rewrite,r", po::value<vector<string>>(&optContainer)->multitoken(),
		 "arg : \t<source_folder>\n ")
		("count,c", po::value<vector<string>>(&optContainer)->multitoken(),
		 "arg : \t<source_folder>\n ")
		("show,s", po::value<vector<string>>(&optContainer)->multitoken(),
		 "arg : \t<source_folder>\n ")
		("count-logical-functions,l", po::value<vector<string>>(&optContainer)->multitoken(),
		 "arg : \t<source_folder>\n ")
		;
		
		// We parse the options used by the user
		po::store(po::parse_command_line(argc, argv, desc), vm);
		po::notify(vm);
		
		/* ----------
		 * launch-l
		 * Launch a generation
		 * ---------- */
		
		if (vm.count("check-and-rewrite"))
		{
			fs::path checkedFolder{"checked/"};
			if(!(fs::exists(checkedFolder) || create_directory(checkedFolder)))
				cerr << "The programm can't create a folder to save checked biodevices" << endl;
			
			vector <string> folders(optContainer.begin(), optContainer.end());
			//ThreadsManager &tm = ThreadsManager::getThreadsManager();
			//using ThreadId = ThreadsManager::ThreadId;
			//ThreadsManager::_limitNbLaunchedThreads = 32;
			size_t threadsLimit = 32;
			vector<future<void>> threads;
			
			while (!folders.empty())
			{
				string inputPath = folders.back();
				folders.pop_back();
				
				for(fs::directory_iterator it(inputPath), end; it != end; ++it) 
				{
					const fs::path path(it->path());
					if(fs::is_regular_file(it->status()))
					{
						threads.push_back(std::async(std::launch::async, [path, checkedFolder]()
						{
							try
							{
								unordered_map<string,DyckSemanticalBioDeviceFileWriter> writers;
								unordered_map<string,unordered_set<string>> alreadySeen;
								DyckSemanticalBioDeviceFileReader reader(path.string());
								DyckSemanticalBioDevice sbd;
								
								while (reader >> sbd)
								{
									const string &structure = sbd.getStructure().getStructure();
									string semanticBytes = sbd.getSemanticBytes();
									
									if (alreadySeen[structure].find(semanticBytes) == alreadySeen[structure].end())
									{
										alreadySeen[structure].insert(semanticBytes);
									}
									else 
									{
										continue;
									}
									
									if (writers.find(structure) == writers.end())
									{
										writers.try_emplace(structure,
											checkedFolder.string()+structure+".bin", 
																				SemanticalBioDeviceFile::FileType::UNIQUE_STRUCTURE);
									}
									
									writers.at(structure) << sbd;
								}
								
								for (auto &it : alreadySeen)
								{
									cout << it.first << ": " << it.second.size() << endl;
								}
							}
							catch (const exception& e)
							{
								cerr << e.what() << endl;
								//throw "Error";
							}
						}));
						
						size_t t(0);
						while (threads.size() >= threadsLimit)
						{
							auto status = threads[t].wait_for(std::chrono::milliseconds(10));
							
							if (status == std::future_status::ready)
								threads.erase(threads.begin()+t);
							
							if (t < threads.size()-1)
								++t;
							else
								t = 0;
						}
					}
					else if (is_directory(path))
					{
						folders.push_back(path.string());
					}
				}
				
				for (auto &it : threads)
				{
					it.wait();
				}
			}
			
			return SUCCESS;
		}
		
		if (vm.count("count"))
		{
			vector <string> folders(optContainer.begin(), optContainer.end());
			
			while (!folders.empty())
			{
				string inputPath = folders.back();
				folders.pop_back();
				
				for(fs::directory_iterator it(inputPath), end; it != end; ++it) 
				{
					const fs::path path(it->path());
					if(fs::is_regular_file(it->status()))
					{
						unordered_map<string,size_t> number;
						
						DyckSemanticalBioDeviceFileReader reader(path.string());
						DyckSemanticalBioDevice sbd;
						
						while (reader >> sbd)
						{
							const string &structure = sbd.getStructure().getStructure();
							
							if (number.find(structure) == number.end())
							{
								number[structure] = 0;
							}
							
							++number[structure];
						}
						
						for (auto &it : number)
						{
							cout << it.first << ": " << it.second << endl;
						}
					}
					else if (is_directory(path))
					{
						folders.push_back(path.string());
					}
				}
			}
			
			return SUCCESS;
		}
		
		if (vm.count("show"))
		{
			string fileName = optContainer.front();
			
			DyckSemanticalBioDeviceFileReader reader(fileName);
			DyckSemanticalBioDevice sbd;
			
			while (reader >> sbd)
			{
				cout << (string)sbd << endl;
			}
			
			return SUCCESS;
		}
		
		if (vm.count("count-logical-functions"))
		{
			vector <string> folders(optContainer.begin(), optContainer.end());
			unordered_map<uint64_t,uint64_t> functions;
			uint64_t PClass = 0;
			
			while (!folders.empty())
			{
				string inputPath = folders.back();
				folders.pop_back();
				
				for(fs::directory_iterator it(inputPath), end; it != end; ++it) 
				{
					const fs::path path(it->path());
					if(fs::is_regular_file(it->status()))
					{
						DyckSemanticalBioDeviceFileReader reader(path.string());
						DyckSemanticalBioDevice sbd;
						
						while (reader >> sbd)
						{
							DyckSemanticalBioDeviceTruthTable tt(&sbd);
							uint64_t function = tt.getDisjunctiveNormalFormInt();
							
							if (functions.find(function) == functions.end())
							{
								uint64_t nb = PClass++;
								for (uint64_t &it : tt.getPEquivalentFunctionsInt())
								{
									functions[it] = nb;
								}
							}
						}
						
						cout << "Work in progress... \t" << functions.size() << " functions and " << (PClass+1) << " P-Classes already counted." << endl;
					}
					else if (is_directory(path))
					{
						folders.push_back(path.string());
					}
				}
			}
			
			cout << "Number of logical functions: " << functions.size() << endl;
			cout << "Number of P-Classes: " << (PClass+1) << endl;
			
			return SUCCESS;
		}
	}
	catch(boost::program_options::error& e)
	{
		cerr << "Command line error: " << e.what() << endl << "For further help, read the --help." << endl;
		return ERROR_IN_COMMAND_LINE;
	}
	catch(exception& e)
	{
		cerr << e.what() << endl << "End of process." << endl;
		return UNKNOWN_EXCEPTION;
	}
	
	return 0;
}
