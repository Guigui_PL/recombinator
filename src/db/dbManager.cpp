#include "boost/program_options.hpp"
#include "DyckSemanticalBioDeviceInserter.hpp"
#include "IEDyckGenerationChecker.hpp"

using namespace std;
using namespace recombinator::DB;
using namespace recombinator::api;

// Return value for the main function
namespace 
{
	const size_t SUCCESS = 0;
	const size_t ERROR_IN_COMMAND_LINE = 1;
	const size_t ERROR_IN_MAIN = 2;
	const size_t UNKNOWN_EXCEPTION = 3;
}

int main (int argc, char** argv)
{
	// For manage the program options, we use the library boost::program_options
	namespace po = boost::program_options;
	
	// We create container for paramaters of options
	vector<string> optContainer;
	po::variables_map vm;
	po::options_description desc("Allowed options");
	
	try
	{
		// We create the list of all options of our program
		desc.add_options()
		("insert,i", po::value<vector<string>>(&optContainer)->multitoken(),
		 "arg : \t<folder>\n")
		("check,c", po::value<vector<string>>(&optContainer)->multitoken(),
		 "arg : \t<nb_inputs>\n")
		("connectionsLimit,l", po::value<size_t>(), "Modify the limit of connections' number.\n")
		;
		
		// We parse the options used by the user
		po::store(po::parse_command_line(argc, argv, desc), vm);
		po::notify(vm);
		
		/* ----------
		 * insert-i
		 * Insert biodevices in database
		 * ---------- */
		
		if (vm.count("insert"))
		{
			auto& inserter = DyckSemanticalBioDeviceInserter::getInstance();
			
			if (vm.count("connectionsLimit") && vm["connectionsLimit"].as<size_t>())
				inserter.setNbLimitConnections(vm["connectionsLimit"].as<size_t>());
			
			for (auto &f : optContainer)
			{
				inserter.insertDyckSemanticalBioDevices(f);
			}
			
			return SUCCESS;
		}
		
		if (vm.count("check"))
		{
			auto& checker = IEDyckGenerationChecker::getInstance();
		
			if (vm.count("connectionsLimit") && vm["connectionsLimit"].as<size_t>())
				checker.setNbLimitConnections(vm["connectionsLimit"].as<size_t>());
			
			checker.checkDatabase(stoi(optContainer[0]));
			
			return SUCCESS;
		}
		
		/* ----------
		 *        Help-h
		 * ---------- */
		
		if(vm.count("help"))
		{
			cout << desc << endl;
			return SUCCESS;
		}
	}
	catch(boost::program_options::error& e)
	{
		cerr << "Command line error: " << e.what() << endl << "For further help, read the --help." << endl;
		return ERROR_IN_COMMAND_LINE;
	}
	catch(exception& e)
	{
		cerr << e.what() << endl << "End of process." << endl;
		return UNKNOWN_EXCEPTION;
	}
	
	return 0;
}
