#ifndef _DYCKSEMANTICALBIODEVICEINSERTER_HPP
#define _DYCKSEMANTICALBIODEVICEINSERTER_HPP

#include <atomic>
#include <condition_variable>
#include <future>
#include <memory>
#include <mutex>
#include <string>
#include <unordered_map>
#include <vector>
#include <pqxx/pqxx>
#include "../api/FunctionnalStructure.hpp"
#include "../api/DyckSemanticalBioDevice.hpp"


namespace recombinator 
{
	namespace DB 
	{
		namespace api = recombinator::api;
		class DyckSemanticalBioDeviceInserter 
		{
		public:
			friend class std::shared_ptr<DyckSemanticalBioDeviceInserter>;
			
			static DyckSemanticalBioDeviceInserter& getInstance(
				std::string dbname = "recombinatordb5", 
				std::string user = "recombinatoruser",
				std::string password = "JN7m7a2d",
				std::string hostaddr = "127.0.0.1",
				std::string port = "5432");
			
			void setNbLimitConnections(uint8_t nb);
			uint8_t getNbLimitConnections() const;
			
			void insertDyckSemanticalBioDevices (std::string folder);
			
		private:
			static std::shared_ptr<DyckSemanticalBioDeviceInserter> _instance;
			DyckSemanticalBioDeviceInserter(
				std::string dbname, 
				std::string user,
				std::string password,
				std::string hostaddr,
				std::string port);
			
			std::string _dbname;
			std::string _user;
			std::string _password;
			std::string _hostaddr;
			std::string _port;
			std::shared_ptr<pqxx::connection> _mainConnection;
			std::mutex _lockMainConnection;
			std::atomic<size_t> _nbInsertedBioDevices {0};
			
			std::atomic<uint8_t> _nbLimitConnections {32};
			std::vector<std::shared_ptr<pqxx::connection>> _usedConnections;
			std::vector<std::shared_ptr<pqxx::connection>> _unUsedConnections;
			std::mutex _lockConnection;
			std::condition_variable _lockConnectionCondition;
			std::shared_ptr<pqxx::connection> getAConnnection();
			bool canAddAConnection();
			void setUnUsed(const std::shared_ptr<pqxx::connection>& connection);
			
			void prepareTables();
			void prepareQueries(pqxx::connection& connection);
			
			std::unordered_map<std::string, uint32_t> _existingFunctionsToPClass;
			std::unordered_map<std::string, uint32_t> _existingDyckFunctionnalStructure;
			std::mutex _lockExistingFunctionsToPClass;
			std::mutex _lockExistingDyckFunctionnalStructure;
			void loadExistingFunctionsToPClass();
			void loadExistingDyckFunctionnalStructure();
			
			uint32_t getIdPermutationClass(const std::string& function);
			uint32_t getIdDyckFunctionnalStructure(const api::DyckSemanticalBioDevice& bd);
			
			void insertFunction(const std::string& function);
			void insertDyckFunctionnalStructure(const api::DyckSemanticalBioDevice& bd);
			void insertDyckSemanticalBioDevice(const api::DyckSemanticalBioDevice& bd, pqxx::nontransaction& W);
			
		};
	}
}

#endif
