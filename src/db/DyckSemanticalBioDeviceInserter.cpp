#include "DyckSemanticalBioDeviceInserter.hpp"
#include <cmath>
#include <boost/dynamic_bitset.hpp>
#include <boost/filesystem.hpp>
#include "../api/BioDeviceFile.hpp" 
#include "../api/DyckSemanticalBioDeviceTruthTable.hpp" 

using namespace std;
using namespace recombinator::DB;
using namespace recombinator::api;

std::shared_ptr<DyckSemanticalBioDeviceInserter> DyckSemanticalBioDeviceInserter::_instance = nullptr;

DyckSemanticalBioDeviceInserter& DyckSemanticalBioDeviceInserter::getInstance(
	std::string dbname, 
	std::string user,
	std::string password,
	std::string hostaddr,
	std::string port)
{
	if (_instance == nullptr)
	{
		_instance = std::shared_ptr<DyckSemanticalBioDeviceInserter> (new DyckSemanticalBioDeviceInserter(dbname, user, password, hostaddr, port));
	}
	
	return *_instance;
}

void DyckSemanticalBioDeviceInserter::setNbLimitConnections(uint8_t nb)
{
	_nbLimitConnections = nb;
}

uint8_t DyckSemanticalBioDeviceInserter::getNbLimitConnections() const
{
	return _nbLimitConnections;
}

void DyckSemanticalBioDeviceInserter::insertDyckSemanticalBioDevices (std::string folder)
{
	namespace fs = boost::filesystem;
	fs::recursive_directory_iterator it(folder), end;
	mutex lockNextFile;
	bool noMoreFiles = false;
	
	auto nextFile = [&it, &lockNextFile, &noMoreFiles, end](fs::path &path)
	{ 
		std::lock_guard<mutex> lock(lockNextFile);
		while (it != end)
		{
			if(it != end && fs::is_regular_file(it->status()))
			{
				path = it->path();
				++it;
				return true;
			}
			++it;
		}
		noMoreFiles = true;
		return false; 
	};
	
	loadExistingDyckFunctionnalStructure();
	//loadExistingSemantics();
	loadExistingFunctionsToPClass();
	
	bool stop(false);
	std::future<void> affiche(std::async(std::launch::async,[this, &stop]()
	{
		std::chrono::system_clock::time_point chronoStart = chrono::system_clock::now();
		
		while (!stop)
		{
			std::this_thread::sleep_for(std::chrono::seconds(5));
			chrono::duration<double> elapsedTime = chrono::system_clock::now() - chronoStart;
			cout << _nbInsertedBioDevices << " Semantical_bio_device inserted \t" <<
			"Elapsed time : " << static_cast<size_t>(elapsedTime.count()) << "s" << endl;
		}
	}));
	
	std::vector<std::future<void>> threads;
	
	while (canAddAConnection() && !noMoreFiles)
	{
		auto numThread = threads.size();
		threads.push_back(std::async(std::launch::async,[this, &nextFile, numThread]()
		{
			namespace fs = boost::filesystem;
			auto connection (getAConnnection());
			unique_ptr<pqxx::nontransaction> W(make_unique<pqxx::nontransaction>(*connection));
			W->exec("begin;");
			
			fs::path path;
			try
			{
				while (nextFile(path))
				{
					DyckSemanticalBioDeviceFileReader reader(path.string());
					DyckSemanticalBioDevice sbd;
					
					try 
					{
						while (reader >> sbd)
						{
							try 
							{
								if (!sbd.isReducible() && sbd.isIrredundant())
								{
									insertDyckSemanticalBioDevice(sbd, *W);
									++_nbInsertedBioDevices;
								}
							}
							catch (const exception& e)
							{
								W->exec("commit;");
								W = make_unique<pqxx::nontransaction> (*connection);
								cerr << e.what() << endl;
							}
						}
					}
					catch (const exception& e)
					{
						W->exec("commit;");
						W = make_unique<pqxx::nontransaction> (*connection);
						cerr << e.what() << endl;
					}
				}
			}
			catch (const exception& e)
			{
				W->exec("commit;");
				W = make_unique<pqxx::nontransaction> (*connection);
				cerr << e.what() << endl;
			}
			W->exec("commit;");
			setUnUsed(connection);
			
		}));
		std::this_thread::sleep_for(std::chrono::milliseconds(10));
	}
	
	for (auto &t : threads)
	{
		try 
		{   
			t.wait();
		}
		catch (const exception &e)
		{
			cerr << e.what() << endl;
		}
	}
	
	stop = true;
	cout << _nbInsertedBioDevices << " Semantical_bio_device inserted - end of insertion." << endl;
}

DyckSemanticalBioDeviceInserter::DyckSemanticalBioDeviceInserter(
	std::string dbname, 
	std::string user,
	std::string password,
	std::string hostaddr,
	std::string port) :
_dbname(dbname),
_user(user),
_password(password),
_hostaddr(hostaddr),
_port(port),
_mainConnection(nullptr)
{
	_mainConnection = getAConnnection();
	prepareTables();
}

std::shared_ptr<pqxx::connection> DyckSemanticalBioDeviceInserter::getAConnnection()
{
	{
		std::lock_guard<mutex> lock(_lockConnection);
		
		if (!_unUsedConnections.empty())
		{
			_usedConnections.push_back(_unUsedConnections.back());
			_unUsedConnections.pop_back();
			
			return _usedConnections.back();
		}
		
		if (_usedConnections.size() < _nbLimitConnections)
		{
			try 
			{
				_usedConnections.emplace_back(
					make_shared<pqxx::connection>(
						"dbname="+_dbname+" user="+_user+" password="+_password+" hostaddr="+_hostaddr+ " port="+_port
					));
				prepareQueries(*(_usedConnections.back()));
				return _usedConnections.back();
			}
			catch (const exception &e)
			{
				cerr << e.what() << endl;
			}
		}
	}
	
    std::unique_lock<std::mutex> lock(_lockConnection);
    _lockConnectionCondition.wait(lock, [this]{ return !_unUsedConnections.empty(); });
	
	_usedConnections.push_back(_unUsedConnections.back());
	_unUsedConnections.pop_back();
	
	return _usedConnections.back();
}

void DyckSemanticalBioDeviceInserter::setUnUsed(const std::shared_ptr<pqxx::connection>& connection)
{
	{
		std::lock_guard<mutex> lock(_lockConnection);
		
		_unUsedConnections.push_back(connection);
		_usedConnections.erase(find(begin(_usedConnections),end(_usedConnections),connection));
	}
    _lockConnectionCondition.notify_one();
}

bool DyckSemanticalBioDeviceInserter::canAddAConnection()
{
	std::lock_guard<mutex> lock(_lockConnection);
	return (_nbLimitConnections - _usedConnections.size()) > 0;
}

void DyckSemanticalBioDeviceInserter::prepareTables()
{
	vector<string> sql;
	
	sql.push_back("CREATE TABLE IF NOT EXISTS Dyck_functionnal_structure("  \
	"id_dyck_functionnal_structure		SERIAL PRIMARY KEY	NOT NULL," \
	"dyck_functionnal_structure     	VARCHAR(18)			NOT NULL," \
	"nb_excisions		   			    INTEGER				NOT NULL,"\
	"nb_inversions		   			    INTEGER				NOT NULL,"\
	"UNIQUE(dyck_functionnal_structure));"
	);
	
	sql.push_back("CREATE TABLE IF NOT EXISTS Permutation_class(" \
	"permutation_class	SERIAL PRIMARY KEY	NOT NULL," \
	"nb_inputs			INTEGER				NOT NULL);"
	);
	
	sql.push_back("CREATE TABLE IF NOT EXISTS Semantical_bio_device(" \
	"id_semantical_bio_device			SERIAL 		PRIMARY KEY	NOT NULL," \
	"id_dyck_functionnal_structure		INTEGER		NOT NULL," \
	"permutation_class					INTEGER		NOT NULL," \
	"weak_constraint					BOOLEAN		NOT NULL," \
	"strong_constraint					BOOLEAN		NOT NULL,"\
	"nb_asymetric_terminators			INTEGER		NOT NULL,"\
	"distance_PG						INTEGER		NOT NULL,"\
	"semantics		BYTEA	    		NOT NULL," \
	"length 		INTEGER          	NOT NULL," \
	"nb_genes		INTEGER				NOT NULL," \
	"nb_promoters	INTEGER				NOT NULL," \
	"nb_terminators	INTEGER				NOT NULL," \
	"gene_at_ends	BOOLEAN				NOT NULL," \
	"nb_parts		INTEGER				NOT NULL"\
	/*", UNIQUE(id_dyck_functionnal_structure,semantics)"\*/
		");"
	);
	
	//"id_dyck_functionnal_structure		INTEGER		REFERENCES Dyck_functionnal_structure(id_dyck_functionnal_structure)," \
	"permutation_class					INTEGER		REFERENCES Permutation_class(permutation_class)," 
	sql.push_back("CREATE TABLE IF NOT EXISTS Boolean_function("  \
	"dnf				BIT VARYING(64)		PRIMARY KEY NOT NULL," \
	"permutation_class	INTEGER				NOT NULL);"
	//"permutation_class	INTEGER				REFERENCES Permutation_class(permutation_class));"
	);
	
	for (auto &it : sql)
	{
		try
		{
			pqxx::work W (*_mainConnection);
			W.exec(it);
			W.commit();
		}
		catch (const exception& e) 
		{
			throw;
		}
	}
}

void DyckSemanticalBioDeviceInserter::prepareQueries(pqxx::connection& connection)
{
	connection.prepare("insert_Dyck_functionnal_structure", 
		"INSERT INTO Dyck_functionnal_structure "\
			"(id_dyck_functionnal_structure, dyck_functionnal_structure, nb_excisions, nb_inversions) "\
			"VALUES(DEFAULT, $1, $2, $3) RETURNING id_dyck_functionnal_structure;");
	
	connection.prepare("insert_Permutation_class", 
		"INSERT INTO Permutation_class "\
			"(permutation_class, nb_inputs) "\
			"VALUES(DEFAULT, $1) RETURNING permutation_class;");
	
	connection.prepare("insert_Semantical_bio_device", 
		"INSERT INTO Semantical_bio_device "\
			"(id_semantical_bio_device, id_dyck_functionnal_structure, permutation_class, weak_constraint, strong_constraint, nb_asymetric_terminators, distance_PG, "\
				"semantics, length, nb_genes, nb_promoters, nb_terminators, gene_at_ends, nb_parts) "\
			"VALUES(DEFAULT, $1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13);");
	
	connection.prepare("insert_Boolean_function", 
		"INSERT INTO Boolean_function "\
			"(dnf, permutation_class) "\
			"VALUES($1, $2);");
	
	connection.prepare("get_id_semantics", 
		"SELECT id_semantics "\
		"FROM Semantics "\
		"WHERE semantics = $1;");
}

void DyckSemanticalBioDeviceInserter::loadExistingFunctionsToPClass()
{
	std::lock_guard<mutex> lock(_lockExistingFunctionsToPClass);
	std::lock_guard<mutex> lock2(_lockMainConnection);
	
	string sql = "SELECT dnf, permutation_class FROM Boolean_function;";
	
	pqxx::nontransaction N(*_mainConnection);
	pqxx::result R(N.exec( sql ));
	
	for (auto c = R.begin(); c != R.end(); ++c) 
	{
		_existingFunctionsToPClass[c[0].as<string>()] = c[1].as<uint32_t>();
	}
	
	N.commit();
}

void DyckSemanticalBioDeviceInserter::loadExistingDyckFunctionnalStructure()
{
	std::lock_guard<mutex> lock(_lockExistingDyckFunctionnalStructure);
	std::lock_guard<mutex> lock2(_lockMainConnection);
	
	string sql = "SELECT dyck_functionnal_structure, id_dyck_functionnal_structure FROM Dyck_functionnal_structure;";
	
	pqxx::nontransaction N(*_mainConnection);
	pqxx::result R(N.exec( sql ));
	
	for (auto c = R.begin(); c != R.end(); ++c) 
		_existingDyckFunctionnalStructure[c[0].as<string>()] = c[1].as<uint32_t>();
	
	N.commit();
}

uint32_t DyckSemanticalBioDeviceInserter::getIdPermutationClass(const std::string& function)
{
	{
		std::lock_guard<mutex> lock(_lockExistingFunctionsToPClass);
		
		auto search = _existingFunctionsToPClass.find(function);
		
		if (search != _existingFunctionsToPClass.end())
		{
			return search->second;
		}
	}
	{
		try
		{
			insertFunction(function);
		}
		catch (const exception& e)
		{
			cerr << e.what() << endl;
		}
		std::lock_guard<mutex> lock(_lockExistingFunctionsToPClass);
		
		auto search = _existingFunctionsToPClass.find(function);
		
		if (search != _existingFunctionsToPClass.end())
		{
			return search->second;
		}
	}
	
	throw runtime_error("Impossible to get the permutation class !\n");
	
	return 0;    
}

uint32_t DyckSemanticalBioDeviceInserter::getIdDyckFunctionnalStructure(const DyckSemanticalBioDevice& bd)
{
	{
		std::lock_guard<mutex> lock(_lockExistingDyckFunctionnalStructure);
		
		auto search = _existingDyckFunctionnalStructure.find(bd.getStructure().getStructure());
		
		if (search != _existingDyckFunctionnalStructure.end())
		{
			return search->second;
		}
	}
	{
		try
		{
			insertDyckFunctionnalStructure(bd);
		}
		catch (const exception& e)
		{
			cerr << e.what() << endl;
		}
		std::lock_guard<mutex> lock(_lockExistingDyckFunctionnalStructure);
		
		auto search = _existingDyckFunctionnalStructure.find(bd.getStructure().getStructure());
		
		if (search != _existingDyckFunctionnalStructure.end())
		{
			return search->second;
		}
	}
	
	throw runtime_error("Impossible to get the permutation class !\n");
	
	return 0; 
}

void DyckSemanticalBioDeviceInserter::insertFunction(const std::string& function)
{
	std::lock_guard<mutex> lock(_lockExistingFunctionsToPClass);
	std::lock_guard<mutex> lock2(_lockMainConnection);
	
	/*
	* If the function is not in the database, we insert all functions of the same P-class 
	*/
	uint16_t nbInputs(log(function.size())/log(2));
	unordered_set<string> allFunctions{function};
	vector<uint8_t> basePermutation(nbInputs), nextPermutation(nbInputs);
	vector<boost::dynamic_bitset<>> baseTerms;
	
	for (uint8_t i(0); i < nbInputs; ++i)
	{
		basePermutation[i] = i;
		nextPermutation[i] = i;
	}
	
	for (uint64_t i(0); i < function.size(); ++i)
	{
		if (function[i] == '1')
		{
			baseTerms.emplace_back(nbInputs,i);
		}
	}
	
	while (next_permutation(nextPermutation.begin(), nextPermutation.end()))
	{
		vector<boost::dynamic_bitset<>> newTerms (baseTerms.size(), boost::dynamic_bitset<>(nbInputs,0));
		
		for (uint64_t i(0); i < baseTerms.size(); ++i)
		{
			for (uint8_t j(0); j < nbInputs; ++j)
			{
				newTerms[i][nextPermutation[j]] = baseTerms[i][j];
			}
		}
		
		string newFunction(function.size(),'0');
		
		for (auto &t: newTerms)
		{
			newFunction[t.to_ulong()] = '1';
		}
		
		allFunctions.insert(newFunction);
	}
	
	pqxx::work W (*_mainConnection);
	pqxx::result R = W.prepared("insert_Permutation_class")(nbInputs).exec();
	W.commit();
	
	uint32_t numPClass(0);
	
	if (R.begin() != R.end())
	{
		auto c = R.begin();
		numPClass = c[0].as<uint32_t>();
	}
	else 
	{
		throw std::runtime_error("Insertion of a Permutation_class failed !\n");
	}
	
	for (auto &f : allFunctions)
	{
		try 
		{
			pqxx::work W (*_mainConnection);
			pqxx::result R = W.prepared("insert_Boolean_function")(f)(numPClass).exec();
			W.commit();
			
			_existingFunctionsToPClass[f] = numPClass;
		}
		catch (const exception &e)
		{
			cerr << e.what() << endl;
		}
	}
}

void DyckSemanticalBioDeviceInserter::insertDyckFunctionnalStructure(const DyckSemanticalBioDevice& bd)
{
	std::lock_guard<mutex> lock(_lockExistingDyckFunctionnalStructure);
	std::lock_guard<mutex> lock2(_lockMainConnection);
	
	pqxx::work W (*_mainConnection);
	pqxx::result R = W.prepared("insert_Dyck_functionnal_structure")
		(bd.getStructure().getStructure())
		(bd.getStructure().getNbExcisions())
		(bd.getStructure().getNbInversions())
		.exec();
	W.commit();
	
	if (R.begin() != R.end())
	{
		auto c = R.begin();
		_existingDyckFunctionnalStructure[bd.getStructure().getStructure()] = c[0].as<uint32_t>();
	}
	else 
	{
		throw std::runtime_error("Insertion of a Dyck_functionnal_structure failed !\n");
	}
}

void DyckSemanticalBioDeviceInserter::insertDyckSemanticalBioDevice(const DyckSemanticalBioDevice& bd, pqxx::nontransaction& W)
{
	uint32_t id_dyck_functionnal_structure = getIdDyckFunctionnalStructure(bd);
	
	DyckSemanticalBioDeviceTruthTable tt(&bd);
	uint32_t permutation_class = getIdPermutationClass(tt.getDisjunctiveNormalForm());
	
	//pqxx::work W (connection);
	pqxx::result R = W.prepared("insert_Semantical_bio_device")
		(id_dyck_functionnal_structure)
		(permutation_class)
		(bd.isRespectedWeakConstraint())
		(bd.isRespectedStrongConstraint())
		(bd.getNbTerminatorsReplaceableByDoubles())
		(bd.getMaxDistancePromoterToGene())
		(pqxx::binarystring(bd.getSemanticBytes()))
		(bd.getMinLength())
		(bd.getNbGenes())
		(bd.getNbPromoters())
		(bd.getNbTerminators())
		(bd.hasGenesAtEnds())
		(bd.getMinNbParts())
		.exec();
}
