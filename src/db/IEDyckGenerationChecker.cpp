#include "IEDyckGenerationChecker.hpp"
#include <cmath>
#include <boost/dynamic_bitset.hpp>
#include <boost/filesystem.hpp>
#include "../api/BioDeviceFile.hpp" 
#include "../api/DyckSemanticalBioDeviceTruthTable.hpp" 
#include "../api/IEDyckFunctionnalStructure.hpp"

using namespace std;
using namespace recombinator::DB;
using namespace recombinator::api;

std::shared_ptr<IEDyckGenerationChecker> IEDyckGenerationChecker::_instance = nullptr;

IEDyckGenerationChecker& IEDyckGenerationChecker::getInstance(
	std::string dbname, 
	std::string user,
	std::string password,
	std::string hostaddr,
	std::string port)
{
	if (_instance == nullptr)
	{
		_instance = std::shared_ptr<IEDyckGenerationChecker> (new IEDyckGenerationChecker(dbname, user, password, hostaddr, port));
	}
	
	return *_instance;
}

void IEDyckGenerationChecker::setNbLimitConnections(uint8_t nb)
{
	_nbLimitConnections = nb;
}

uint8_t IEDyckGenerationChecker::getNbLimitConnections() const
{
	return _nbLimitConnections;
}

void IEDyckGenerationChecker::checkDatabase(uint8_t nbInputs)
{
	loadListSBD(nbInputs);
	namespace fs = boost::filesystem;
	fs::recursive_directory_iterator it("work/"+to_string(nbInputs)+"/good"), end;
	mutex lockNextFile;
	bool noMoreFiles = false;
	
	auto nextFile = [&it, &lockNextFile, &noMoreFiles, end](fs::path &path)
	{ 
		std::lock_guard<mutex> lock(lockNextFile);
		while (it != end)
		{
			if(it != end && fs::is_regular_file(it->status()))
			{
				path = it->path();
				++it;
				return true;
			}
			++it;
		}
		noMoreFiles = true;
		return false; 
	};
	
	bool stop(false);
	std::future<void> affiche(std::async(std::launch::async,[this, &stop]()
	{
		std::chrono::system_clock::time_point chronoStart = chrono::system_clock::now();
		
		while (!stop)
		{
			std::this_thread::sleep_for(std::chrono::seconds(5));
			chrono::duration<double> elapsedTime = chrono::system_clock::now() - chronoStart;
			cout << _nbCheckedBioDevices << " Semantical_bio_device checked \t" <<
			"Elapsed time : " << static_cast<size_t>(elapsedTime.count()) << "s" << endl;
		}
	}));
	
	std::vector<std::future<void>> threads;
	
	while (canAddAConnection() && !noMoreFiles)
	{
		auto numThread = threads.size();
		threads.push_back(std::async(std::launch::async,[this, &nextFile, numThread]()
		{
			namespace fs = boost::filesystem;
			auto connection (getAConnnection());
			
			fs::path path;
			try
			{
				while (nextFile(path))
				{
					DyckSemanticalBioDeviceFileReader reader(path.string());
					DyckSemanticalBioDevice sbd;
					
					try 
					{
						while (reader >> sbd)
						{
							try 
							{
								checkSBD(*connection, sbd);
								++_nbCheckedBioDevices;
							}
							catch (const exception& e)
							{
								cerr << e.what() << endl;
							}
						}
					}
					catch (const exception& e)
					{
						cerr << e.what() << endl;
					}
				}
			}
			catch (const exception& e)
			{
				cerr << e.what() << endl;
			}
			setUnUsed(connection);
			
		}));
		std::this_thread::sleep_for(std::chrono::milliseconds(10));
	}
	
	for (auto &t : threads)
	{
		try 
		{   
			t.wait();
		}
		catch (const exception &e)
		{
			cerr << e.what() << endl;
		}
	}
	
	stop = true;
	cout << "Elements not in the generation : " << endl;
	printListSBD(cerr);
	cout << "Nb elements unfound in the database : " << _nbUnfound << endl;
	cout << "Nb elements duplicates in the database : " << _nbDuplicates << endl;
	cout << "Nb elements not in the generation : " << to_string(_listSBD.size()) << endl;
	
	cout << _nbCheckedBioDevices << " Semantical_bio_device checked - end of checking." << endl;
}

IEDyckGenerationChecker::IEDyckGenerationChecker(
	std::string dbname, 
	std::string user,
	std::string password,
	std::string hostaddr,
	std::string port) :
_dbname(dbname),
_user(user),
_password(password),
_hostaddr(hostaddr),
_port(port),
_mainConnection(nullptr)
{
	_mainConnection = getAConnnection();
}

std::shared_ptr<pqxx::connection> IEDyckGenerationChecker::getAConnnection()
{
	{
		std::lock_guard<mutex> lock(_lockConnection);
		
		if (!_unUsedConnections.empty())
		{
			_usedConnections.push_back(_unUsedConnections.back());
			_unUsedConnections.pop_back();
			
			return _usedConnections.back();
		}
		
		if (_usedConnections.size() < _nbLimitConnections)
		{
			try 
			{
				_usedConnections.emplace_back(
					make_shared<pqxx::connection>(
						"dbname="+_dbname+" user="+_user+" password="+_password+" hostaddr="+_hostaddr+ " port="+_port
					));
				prepareQueries(*(_usedConnections.back()));
				return _usedConnections.back();
			}
			catch (const exception &e)
			{
				cerr << e.what() << endl;
			}
		}
	}
	
    std::unique_lock<std::mutex> lock(_lockConnection);
    _lockConnectionCondition.wait(lock, [this]{ return !_unUsedConnections.empty(); });
	
	_usedConnections.push_back(_unUsedConnections.back());
	_unUsedConnections.pop_back();
	
	return _usedConnections.back();
}

void IEDyckGenerationChecker::setUnUsed(const std::shared_ptr<pqxx::connection>& connection)
{
	{
		std::lock_guard<mutex> lock(_lockConnection);
		
		_unUsedConnections.push_back(connection);
		_usedConnections.erase(find(begin(_usedConnections),end(_usedConnections),connection));
	}
    _lockConnectionCondition.notify_one();
}

bool IEDyckGenerationChecker::canAddAConnection()
{
	std::lock_guard<mutex> lock(_lockConnection);
	return (_nbLimitConnections - _usedConnections.size()) > 0;
}

void IEDyckGenerationChecker::prepareQueries(pqxx::connection& connection)
{
	connection.prepare("load_SBD", 
		"SELECT sbd.id_semantics, sbd.id_dyck_functionnal_structure, dyck_functionnal_structure, semantics "\
			"FROM Semantical_bio_device sbd "\
			"JOIN Semantics s ON s.id_semantics=sbd.id_semantics "\
			"JOIN Dyck_functionnal_structure dfs ON dfs.id_dyck_functionnal_structure = sbd.id_dyck_functionnal_structure "\
			"JOIN Permutation_class pc ON pc.permutation_class = sbd.permutation_class "\
			"WHERE pc.nb_inputs = $1;");
	
	connection.prepare("get_id_SBD",
		"SELECT sbd.id_semantics, sbd.id_dyck_functionnal_structure "\
			"FROM Semantical_bio_device sbd "\
			"JOIN Semantics s ON s.id_semantics=sbd.id_semantics "\
			"JOIN Dyck_functionnal_structure dfs ON dfs.id_dyck_functionnal_structure = sbd.id_dyck_functionnal_structure "\
			"WHERE s.semantics = $1 AND dfs.dyck_functionnal_structure = $2;");
}

void IEDyckGenerationChecker::loadListSBD(size_t nbInputs)
{
	std::lock_guard<mutex> lock(_lockListSBD);
	std::lock_guard<mutex> lock2(_lockMainConnection);
	
	pqxx::nontransaction N(*_mainConnection);
	pqxx::result R(N.prepared("load_SBD")(nbInputs).exec());
	
	for (auto c = R.begin(); c != R.end(); ++c) 
	{
		auto fs = IEDyckFunctionnalStructure::create<IEDyckFunctionnalStructure>(c[2].as<string>());
		auto sbd = DyckSemanticalBioDevice(*fs);
		string semantics = pqxx::binarystring(c[3]).str();
		
		for (size_t i = 0; i < sbd.getNbSemantics(); ++i)
		{
			sbd.setSemantic(i, Semantic::getPointerWithKey(semantics[i]));
		}
		
		_listSBD[make_pair(c[0].as<uint64_t>(), c[1].as<uint64_t>())] = (string)sbd;
	}
	
	N.commit();
}

pqxx::result IEDyckGenerationChecker::getIdSBD (pqxx::connection& connection, const DyckSemanticalBioDevice& sbd) const
{
	pqxx::nontransaction N(connection);
	pqxx::result result(N.prepared("get_id_SBD")(pqxx::binarystring(sbd.getSemanticBytes())) (sbd.getStructure().getStructure()).exec());
	
	N.commit();
	return result;
}

bool IEDyckGenerationChecker::removeFromListSBD(std::pair<uint64_t,uint64_t> id)
{
	std::lock_guard<mutex> lock(_lockListSBD);
	
	if (_listSBD.find(id) == _listSBD.end())
	{
		return false;
	}
	
	_listSBD.erase(id);
	return true;
}

void IEDyckGenerationChecker::checkSBD(pqxx::connection& connection, const DyckSemanticalBioDevice &sbd)
{
	auto reverseSbd = sbd.getDyckSymmetric();
	
	pqxx::result result = getIdSBD(connection, sbd);
	
	size_t nbResults = 0, nbNotFoundOnList = 0;
	for (auto c = result.begin(); c != result.end(); ++c)
	{
		++nbResults;
		if (!removeFromListSBD(make_pair(c[0].as<uint64_t>(), c[1].as<uint64_t>())))
		{
			++nbNotFoundOnList;
		}
	}
	
	if (sbd == reverseSbd && nbResults == 0)
	{
		cerr << (string)sbd << " doesn't exist in the database." << endl;
		++_nbUnfound;
	}
	if (nbResults >= 2)
	{
		cerr << (string)sbd << " appears " << nbResults << " times in the databse." << endl;
	}
	
	if (nbNotFoundOnList > 0)
	{
		cerr << (string)sbd << " has already been treated." << endl;
	}
	
	nbNotFoundOnList = 0;
	
	if  (sbd != reverseSbd)
	{
		for (size_t j{0}; j < reverseSbd.getNbSemantics(); ++j)
		{
			if (reverseSbd.getSemantic(j).isX())
				reverseSbd.setSemantic(j, &Semantic::allSemantics[27]);
		}
		
		pqxx::result result = getIdSBD(connection, reverseSbd);
	
		size_t nbResultsReverse = 0;
		for (auto c = result.begin(); c != result.end(); ++c)
		{
			++nbResultsReverse;
			if (!removeFromListSBD(make_pair(c[0].as<uint64_t>(), c[1].as<uint64_t>())))
			{
				++nbNotFoundOnList;
			}
		}
	
		if (nbNotFoundOnList > 0)
		{
			cerr << (string)reverseSbd << " has already been treated." << endl;
		}
		
		if (nbResults == 0 && nbResultsReverse == 0)
		{
			cerr << (string)sbd << " and its reverse " << (string)reverseSbd << " don't exist in the database." << endl;
			++_nbUnfound;
		}
		
		if (nbResultsReverse >= 2)
		{
			cerr << (string)sbd << " appears " << nbResults << " times in the databse." << endl;
			_nbDuplicates += (nbResults-1);
		}
		
		if (nbResults > 0 && nbResultsReverse > 0)
		{
			cerr << (string)sbd << " appears " << nbResults << " times in the databse" 
			<< " and its " << (string)reverseSbd << " appears " << nbResultsReverse << " times." << endl;
			_nbDuplicates += nbResultsReverse;
		}
	}
}

void IEDyckGenerationChecker::printListSBD(std::ostream &os)
{
	std::lock_guard<mutex> lock(_lockListSBD);
	
	for (auto &it : _listSBD)
	{
		os << "id_semantics = " << to_string(it.first.first) << " - id_dyck_functionnal_structure = " << to_string(it.first.second) << " - sbd : " << it.second << endl;
	}
}
