#ifndef _IEDYCKGENERATORCHECKER_H
#define _IEDYCKGENERATORCHECKER_H

#include <atomic>
#include <condition_variable>
#include <future>
#include <memory>
#include <mutex>
#include <string>
#include <unordered_map>
#include <vector>
#include <pqxx/pqxx>
#include "../api/FunctionnalStructure.hpp"
#include "../api/DyckSemanticalBioDevice.hpp"

namespace recombinator 
{
	namespace DB 
	{
		namespace api = recombinator::api;
		class IEDyckGenerationChecker 
		{
		public:
			friend class std::shared_ptr<IEDyckGenerationChecker>;
			
			static IEDyckGenerationChecker& getInstance(
				std::string dbname = "recombinatordb", 
				std::string user = "recombinatoruser",
				std::string password = "JN7m7a2d",
				std::string hostaddr = "127.0.0.1",
				std::string port = "5432");
			
			void setNbLimitConnections(uint8_t nb);
			uint8_t getNbLimitConnections() const;
			
			void checkDatabase(uint8_t nbInputs);
			
		private:
			static std::shared_ptr<IEDyckGenerationChecker> _instance;
			IEDyckGenerationChecker(
				std::string dbname, 
				std::string user,
				std::string password,
				std::string hostaddr,
				std::string port);
			
			std::string _dbname;
			std::string _user;
			std::string _password;
			std::string _hostaddr;
			std::string _port;
			std::shared_ptr<pqxx::connection> _mainConnection;
			std::mutex _lockMainConnection;
			std::atomic<size_t> _nbCheckedBioDevices {0};
			std::atomic<size_t> _nbUnfound {0};
			std::atomic<size_t> _nbDuplicates {0};
			
			std::atomic<uint8_t> _nbLimitConnections {32};
			std::vector<std::shared_ptr<pqxx::connection>> _usedConnections;
			std::vector<std::shared_ptr<pqxx::connection>> _unUsedConnections;
			std::mutex _lockConnection;
			std::condition_variable _lockConnectionCondition;
			std::shared_ptr<pqxx::connection> getAConnnection();
			bool canAddAConnection();
			void setUnUsed(const std::shared_ptr<pqxx::connection>& connection);
			
			void prepareQueries(pqxx::connection& connection);
			
			std::map<std::pair<uint64_t,uint64_t>,std::string> _listSBD;
			std::mutex _lockListSBD;
			void loadListSBD(size_t nbInputs);
			
			pqxx::result getIdSBD (pqxx::connection& connection, const recombinator::api::DyckSemanticalBioDevice& sbd) const;
			bool removeFromListSBD(std::pair<uint64_t,uint64_t> id);
			void checkSBD(pqxx::connection& connection, const recombinator::api::DyckSemanticalBioDevice &sbd);
			void printListSBD(std::ostream &os);
		};
	}
}


#endif 
